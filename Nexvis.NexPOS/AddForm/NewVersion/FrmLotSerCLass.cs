﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.Helper;
using ZooMangement;
using DevExpress.XtraGrid.Views.Grid;
using DgoStore;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace ZooManagement.AddForm.NewVersion
{
    public partial class FrmLotSerCLass : DevExpress.XtraEditors.XtraForm
    {
        public FrmLotSerCLass()
        {
            InitializeComponent();

        }
        ZooEntity DB = new ZooEntity();

        List<INLotSerClass> _LotSerClass = new List<INLotSerClass>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
       // List<getGenderCus_Result> getGenderCus_Results = new List<getGenderCus_Result>();
        public bool isSuccessful = true;

        private void FrmLotSerCLass_Load(object sender, EventArgs e)
        {
            //    _Cus = DB.ARCustomers.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            _LotSerClass = DB.INLotSerClasses.ToList();
            GC.DataSource = _LotSerClass.ToList();

            _RoleItems = DB.CSRoleItems.ToList();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

           
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FrmLotSerCLass_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    INLotSerClass result = new INLotSerClass();
                    result = (INLotSerClass)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {
                            INLotSerClass customer = DB.INLotSerClasses.FirstOrDefault(st => st.LotSerClassID.Equals(result.LotSerClassID));

                            DB.INLotSerClasses.Remove(customer);
                            DB.SaveChanges();
                            FrmLotSerCLass_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }   
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    AddLotSerClass objForm = new AddLotSerClass("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    // GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    INLotSerClass result = new INLotSerClass();

                    result = (INLotSerClass)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        objForm.txtLotSerClassID.Text = result.LotSerClassID;
                        // objForm.txtDescr.Enabled = false;
                        objForm.txtDescr.Focus();
                        objForm.txtDescr.Text = result.Descr;
                        objForm.txtLotSerTrack.Text = result.LotSerTrack;
                        objForm.ckTrackExpiration.Checked = result.LotSerTrackExpiration;


                        //  objForm.cmbGender.Text = Gender.setFullGender(objForm.cmbGender.Text);

                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "POS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role  && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    AddLotSerClass objForm = new AddLotSerClass("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Actions
        public void GsSaveData(AddLotSerClass objChild)
        {
            try
            {
                objChild.isExiting = false;
                if (_LotSerClass.Any(x => x.LotSerClassID.ToString() == objChild.txtLotSerClassID.Text.Trim().ToLower()))
                {
                    objChild.isExiting = true;
                    return;
                }

          
                INLotSerClass cus = new INLotSerClass()
                {
                    CompanyID = DACClasses.CompanyID,
                    LotSerClassID = objChild.txtLotSerClassID.Text,
                    Descr = objChild.txtDescr.Text,
                    LotSerTrack = objChild.txtLotSerTrack.Text,
                    LotSerTrackExpiration = (bool)objChild.ckTrackExpiration.Checked
                    

                };


                DB.INLotSerClasses.Add(cus);
                var Row = DB.SaveChanges();

             

                if (Row == 1)
                {
                    isSuccessful = true;
                }
                GV.FocusedRowHandle = 0;
                navRefresh_ElementClick(null, null);
                XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        public void GsUpdateData(AddLotSerClass objChild)
        {
            try
            {
                if (_LotSerClass.Any(x => x.LotSerClassID.Trim().ToLower() != objChild.txtLotSerClassID.Text.Trim().ToLower()))
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtDescr.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                var _obj = DB.INLotSerClasses.FirstOrDefault(w => w.LotSerClassID.ToString() == (objChild.txtLotSerClassID.Text) && w.CompanyID.Equals(DACClasses.CompanyID));


                _obj.Descr = objChild.txtDescr.Text;
                _obj.LotSerTrack = objChild.txtLotSerTrack.Text;
                _obj.LotSerTrackExpiration = objChild.ckTrackExpiration.Checked;
            



                DB.INLotSerClasses.Attach(_obj);
                DB.Entry(_obj).Property(w => w.Descr).IsModified = true;
                DB.Entry(_obj).Property(w => w.LotSerTrack).IsModified = true;
                DB.Entry(_obj).Property(w => w.LotSerTrackExpiration).IsModified = true;

                var Row = DB.SaveChanges();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "POS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}