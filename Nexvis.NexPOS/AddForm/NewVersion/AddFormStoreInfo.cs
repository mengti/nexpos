﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DgoStore.LoadForm;
using Nexvis.NexPOS.Helper;

namespace DgoStore.AddForm.NewVersion
{
    public partial class AddFormStoreInfo : DevExpress.XtraEditors.XtraForm
    {
        #region Me


        #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        private scrStoreInfor parentForm;

        #endregion

        #region --- Form Action ---
        public AddFormStoreInfo(string transactionType, scrStoreInfor objParent)
        {
            InitializeComponent();
            parentForm = objParent;

            if (transactionType == "NEW")
            {
                navEdit.Visible = false;
                isNew = true;
            }
            else if (transactionType == "EDIT")
            {
               // navSave.Visible = false;
            }

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;

        }

        #endregion

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Clear value for new
            // GlobalInitializer.GsClearValue(txtItemCode, txtItemID, txtItemName, txtDisPrice, cmbMealTime, luCategory, txtDisPercent, txtDesr, txtPrice);
            navEdit.Visible = false;
           // navSave.Visible = true;
            isNew = true;

            //  pbItem.Image = null;
            txtStoreName.Focus();
        }

        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //if (GlobalInitializer.GfCheckNullValue(luCategory, txtItemName) == false)
            //{
            //    //    // TODO: Save value to DB.


            //    //// TODO: Validate which record exiting.
            //    //if (isExiting && parentForm.isSuccessful)
            //    //{
            //    //    XtraMessageBox.Show("This record ready existing !", "POS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    //    txtItemCode.Focus();
            //    //    return;
            //    //}

            //    if (parentForm.isSuccessful)
            //    {
            //        // navNew_ElementClick(sender, e);
            //        DialogResult = System.Windows.Forms.DialogResult.OK;
            //        parentForm.GsSaveData(this);

            //    }

            //}
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtStoreID) == false)
                parentForm.GsUpdateData(this);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close(); // this.Parent.Controls.Remove(this);
        }



        #endregion

        private void txtStoreID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtInformation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                navSave_ElementClick(null,null);
            }
        }
    }
}