﻿namespace DgoStore.AddForm.NewVersion
{
    partial class AddFormStoreInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtRate = new DevExpress.XtraEditors.TextEdit();
            this.txtStoreID = new DevExpress.XtraEditors.TextEdit();
            this.txtStoreName = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtInformation = new DevExpress.XtraEditors.TextEdit();
            this.txtAtmosphere = new DevExpress.XtraEditors.TextEdit();
            this.txtShortAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtSpokenLanguage = new DevExpress.XtraEditors.TextEdit();
            this.txtContact = new DevExpress.XtraEditors.TextEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.txtWebsite = new DevExpress.XtraEditors.TextEdit();
            this.pbLogo = new DevExpress.XtraEditors.PictureEdit();
            this.dtRegisterDate = new DevExpress.XtraEditors.DateEdit();
            this.tStartTime = new DevExpress.XtraEditors.TimeEdit();
            this.tCloseTime = new DevExpress.XtraEditors.TimeEdit();
            this.txtOpenDay = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.textEdit73 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit53 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit93 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit43 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit23 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit33 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit83 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit63 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEdit72 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit52 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit42 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit82 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInformation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAtmosphere.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShortAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpokenLanguage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtRegisterDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtRegisterDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStartTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCloseTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpenDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit73.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit93.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit83.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit63.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit72.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit82.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navEdit);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Margin = new System.Windows.Forms.Padding(4);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(906, 42);
            this.navHeader.TabIndex = 7;
            this.navHeader.Text = "q";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Store";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Update";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Appearance.Control.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControl1.Appearance.Control.Options.UseFont = true;
            this.layoutControl1.Appearance.Control.Options.UseTextOptions = true;
            this.layoutControl1.Appearance.Control.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControl1.Controls.Add(this.txtRate);
            this.layoutControl1.Controls.Add(this.txtStoreID);
            this.layoutControl1.Controls.Add(this.txtStoreName);
            this.layoutControl1.Controls.Add(this.txtAddress);
            this.layoutControl1.Controls.Add(this.txtInformation);
            this.layoutControl1.Controls.Add(this.txtAtmosphere);
            this.layoutControl1.Controls.Add(this.txtShortAddress);
            this.layoutControl1.Controls.Add(this.txtSpokenLanguage);
            this.layoutControl1.Controls.Add(this.txtContact);
            this.layoutControl1.Controls.Add(this.txtEmail);
            this.layoutControl1.Controls.Add(this.txtWebsite);
            this.layoutControl1.Controls.Add(this.pbLogo);
            this.layoutControl1.Controls.Add(this.dtRegisterDate);
            this.layoutControl1.Controls.Add(this.tStartTime);
            this.layoutControl1.Controls.Add(this.tCloseTime);
            this.layoutControl1.Controls.Add(this.txtOpenDay);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControl1.Location = new System.Drawing.Point(0, 42);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(716, 235, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(906, 427);
            this.layoutControl1.TabIndex = 8;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtRate
            // 
            this.txtRate.EditValue = "0.00";
            this.txtRate.Location = new System.Drawing.Point(257, 76);
            this.txtRate.Name = "txtRate";
            this.txtRate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtRate.Properties.Appearance.Options.UseFont = true;
            this.txtRate.Properties.Mask.EditMask = "f";
            this.txtRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtRate.Size = new System.Drawing.Size(235, 22);
            this.txtRate.StyleController = this.layoutControl1;
            this.txtRate.TabIndex = 9;
            this.txtRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtStoreID
            // 
            this.txtStoreID.Location = new System.Drawing.Point(32, 31);
            this.txtStoreID.Name = "txtStoreID";
            this.txtStoreID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtStoreID.Properties.Appearance.Options.UseFont = true;
            this.txtStoreID.Size = new System.Drawing.Size(197, 22);
            this.txtStoreID.StyleController = this.layoutControl1;
            this.txtStoreID.TabIndex = 0;
            this.txtStoreID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtStoreName
            // 
            this.txtStoreName.Location = new System.Drawing.Point(32, 76);
            this.txtStoreName.Name = "txtStoreName";
            this.txtStoreName.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtStoreName.Properties.Appearance.Options.UseFont = true;
            this.txtStoreName.Size = new System.Drawing.Size(197, 22);
            this.txtStoreName.StyleController = this.layoutControl1;
            this.txtStoreName.TabIndex = 1;
            this.txtStoreName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(32, 386);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtAddress.Properties.Appearance.Options.UseFont = true;
            this.txtAddress.Size = new System.Drawing.Size(460, 22);
            this.txtAddress.StyleController = this.layoutControl1;
            this.txtAddress.TabIndex = 7;
            this.txtAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtInformation
            // 
            this.txtInformation.Location = new System.Drawing.Point(257, 331);
            this.txtInformation.Name = "txtInformation";
            this.txtInformation.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtInformation.Properties.Appearance.Options.UseFont = true;
            this.txtInformation.Size = new System.Drawing.Size(235, 22);
            this.txtInformation.StyleController = this.layoutControl1;
            this.txtInformation.TabIndex = 14;
            this.txtInformation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInformation_KeyDown);
            // 
            // txtAtmosphere
            // 
            this.txtAtmosphere.Location = new System.Drawing.Point(257, 31);
            this.txtAtmosphere.Name = "txtAtmosphere";
            this.txtAtmosphere.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtAtmosphere.Properties.Appearance.Options.UseFont = true;
            this.txtAtmosphere.Size = new System.Drawing.Size(235, 22);
            this.txtAtmosphere.StyleController = this.layoutControl1;
            this.txtAtmosphere.TabIndex = 8;
            this.txtAtmosphere.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtShortAddress
            // 
            this.txtShortAddress.Location = new System.Drawing.Point(257, 131);
            this.txtShortAddress.Name = "txtShortAddress";
            this.txtShortAddress.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtShortAddress.Properties.Appearance.Options.UseFont = true;
            this.txtShortAddress.Size = new System.Drawing.Size(235, 22);
            this.txtShortAddress.StyleController = this.layoutControl1;
            this.txtShortAddress.TabIndex = 10;
            this.txtShortAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtSpokenLanguage
            // 
            this.txtSpokenLanguage.Location = new System.Drawing.Point(32, 286);
            this.txtSpokenLanguage.Name = "txtSpokenLanguage";
            this.txtSpokenLanguage.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtSpokenLanguage.Properties.Appearance.Options.UseFont = true;
            this.txtSpokenLanguage.Size = new System.Drawing.Size(197, 22);
            this.txtSpokenLanguage.StyleController = this.layoutControl1;
            this.txtSpokenLanguage.TabIndex = 5;
            this.txtSpokenLanguage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtContact
            // 
            this.txtContact.Location = new System.Drawing.Point(257, 176);
            this.txtContact.Name = "txtContact";
            this.txtContact.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtContact.Properties.Appearance.Options.UseFont = true;
            this.txtContact.Size = new System.Drawing.Size(235, 22);
            this.txtContact.StyleController = this.layoutControl1;
            this.txtContact.TabIndex = 11;
            this.txtContact.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(257, 221);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtEmail.Properties.Appearance.Options.UseFont = true;
            this.txtEmail.Size = new System.Drawing.Size(235, 22);
            this.txtEmail.StyleController = this.layoutControl1;
            this.txtEmail.TabIndex = 12;
            this.txtEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtWebsite
            // 
            this.txtWebsite.Location = new System.Drawing.Point(257, 286);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtWebsite.Properties.Appearance.Options.UseFont = true;
            this.txtWebsite.Size = new System.Drawing.Size(235, 22);
            this.txtWebsite.StyleController = this.layoutControl1;
            this.txtWebsite.TabIndex = 13;
            this.txtWebsite.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // pbLogo
            // 
            this.pbLogo.Location = new System.Drawing.Point(506, 29);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.pbLogo.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pbLogo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pbLogo.Size = new System.Drawing.Size(371, 389);
            this.pbLogo.StyleController = this.layoutControl1;
            this.pbLogo.TabIndex = 16;
            this.pbLogo.TabStop = true;
            // 
            // dtRegisterDate
            // 
            this.dtRegisterDate.EditValue = null;
            this.dtRegisterDate.Location = new System.Drawing.Point(32, 131);
            this.dtRegisterDate.Name = "dtRegisterDate";
            this.dtRegisterDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.dtRegisterDate.Properties.Appearance.Options.UseFont = true;
            this.dtRegisterDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtRegisterDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtRegisterDate.Properties.DisplayFormat.FormatString = "";
            this.dtRegisterDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtRegisterDate.Properties.EditFormat.FormatString = "";
            this.dtRegisterDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtRegisterDate.Properties.Mask.EditMask = "D";
            this.dtRegisterDate.Size = new System.Drawing.Size(198, 22);
            this.dtRegisterDate.StyleController = this.layoutControl1;
            this.dtRegisterDate.TabIndex = 2;
            this.dtRegisterDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // tStartTime
            // 
            this.tStartTime.EditValue = null;
            this.tStartTime.Location = new System.Drawing.Point(32, 176);
            this.tStartTime.Name = "tStartTime";
            this.tStartTime.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.tStartTime.Properties.Appearance.Options.UseFont = true;
            this.tStartTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tStartTime.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.tStartTime.Properties.Mask.EditMask = "t";
            this.tStartTime.Size = new System.Drawing.Size(198, 22);
            this.tStartTime.StyleController = this.layoutControl1;
            this.tStartTime.TabIndex = 3;
            this.tStartTime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // tCloseTime
            // 
            this.tCloseTime.EditValue = null;
            this.tCloseTime.Location = new System.Drawing.Point(32, 221);
            this.tCloseTime.Name = "tCloseTime";
            this.tCloseTime.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.tCloseTime.Properties.Appearance.Options.UseFont = true;
            this.tCloseTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tCloseTime.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.tCloseTime.Properties.Mask.EditMask = "t";
            this.tCloseTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.tCloseTime.Size = new System.Drawing.Size(198, 22);
            this.tCloseTime.StyleController = this.layoutControl1;
            this.tCloseTime.TabIndex = 4;
            this.tCloseTime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // txtOpenDay
            // 
            this.txtOpenDay.EditValue = "";
            this.txtOpenDay.Location = new System.Drawing.Point(32, 331);
            this.txtOpenDay.Name = "txtOpenDay";
            this.txtOpenDay.Properties.AllowMultiSelect = true;
            this.txtOpenDay.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtOpenDay.Properties.Appearance.Options.UseFont = true;
            this.txtOpenDay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtOpenDay.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("Mon"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("Tue"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("Wed"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("Thu"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("Fri"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("Sat"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("Sun")});
            this.txtOpenDay.Size = new System.Drawing.Size(197, 22);
            this.txtOpenDay.StyleController = this.layoutControl1;
            this.txtOpenDay.TabIndex = 6;
            this.txtOpenDay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStoreID_KeyDown);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem21,
            this.layoutControlItem40,
            this.layoutControlItem41,
            this.emptySpaceItem1,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.layoutControlItem13,
            this.layoutControlItem16,
            this.layoutControlItem15,
            this.layoutControlItem12,
            this.emptySpaceItem13,
            this.emptySpaceItem15,
            this.layoutControlItem14,
            this.layoutControlItem17,
            this.layoutControlItem24,
            this.layoutControlItem38,
            this.layoutControlItem2,
            this.emptySpaceItem2,
            this.emptySpaceItem9,
            this.layoutControlItem9,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(889, 430);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.txtStoreID;
            this.layoutControlItem4.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(20, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(201, 45);
            this.layoutControlItem4.Text = "Store ID";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.txtStoreName;
            this.layoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(20, 45);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(201, 45);
            this.layoutControlItem7.Text = "Store Name";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.dtRegisterDate;
            this.layoutControlItem10.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem10.Location = new System.Drawing.Point(20, 100);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(202, 45);
            this.layoutControlItem10.Text = "Register Date";
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.Control = this.txtSpokenLanguage;
            this.layoutControlItem21.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem21.Location = new System.Drawing.Point(20, 255);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(201, 45);
            this.layoutControlItem21.Text = "SpokenLanguage";
            this.layoutControlItem21.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem40.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem40.Control = this.txtOpenDay;
            this.layoutControlItem40.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem40.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem40.Location = new System.Drawing.Point(20, 300);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(201, 45);
            this.layoutControlItem40.Text = "OpenDay";
            this.layoutControlItem40.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.Control = this.txtWebsite;
            this.layoutControlItem41.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem41.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem41.Location = new System.Drawing.Point(245, 255);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(239, 45);
            this.layoutControlItem41.Text = "Website";
            this.layoutControlItem41.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(108, 16);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(20, 90);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(225, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(20, 235);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(464, 10);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(221, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(24, 90);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(222, 100);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(23, 135);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(20, 410);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.tStartTime;
            this.layoutControlItem13.Location = new System.Drawing.Point(20, 145);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(202, 45);
            this.layoutControlItem13.Text = "Start Time";
            this.layoutControlItem13.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.Control = this.tCloseTime;
            this.layoutControlItem16.Location = new System.Drawing.Point(20, 190);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(202, 45);
            this.layoutControlItem16.Text = "Close Time";
            this.layoutControlItem16.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.Control = this.pbLogo;
            this.layoutControlItem15.Location = new System.Drawing.Point(494, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(375, 410);
            this.layoutControlItem15.Text = "Logo";
            this.layoutControlItem15.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.Control = this.txtInformation;
            this.layoutControlItem12.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem12.Location = new System.Drawing.Point(245, 300);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(239, 45);
            this.layoutControlItem12.Text = "Information";
            this.layoutControlItem12.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(108, 16);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(221, 255);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(24, 90);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(245, 90);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(239, 10);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.Control = this.txtAtmosphere;
            this.layoutControlItem14.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem14.Location = new System.Drawing.Point(245, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(239, 45);
            this.layoutControlItem14.Text = "Atmosphere";
            this.layoutControlItem14.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.Control = this.txtShortAddress;
            this.layoutControlItem17.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem17.Location = new System.Drawing.Point(245, 100);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(239, 45);
            this.layoutControlItem17.Text = "Short Address";
            this.layoutControlItem17.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.Control = this.txtContact;
            this.layoutControlItem24.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem24.Location = new System.Drawing.Point(245, 145);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(239, 45);
            this.layoutControlItem24.Text = "Contact";
            this.layoutControlItem24.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem38.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem38.Control = this.txtEmail;
            this.layoutControlItem38.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem38.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem38.Location = new System.Drawing.Point(245, 190);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(239, 45);
            this.layoutControlItem38.Text = "Email";
            this.layoutControlItem38.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(108, 16);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.txtRate;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(245, 45);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(239, 45);
            this.layoutControlItem2.Text = "Rate";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(108, 16);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(484, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 410);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(20, 245);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(464, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.txtAddress;
            this.layoutControlItem9.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(20, 355);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(464, 45);
            this.layoutControlItem9.Text = "Address";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(108, 16);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(20, 400);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(464, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(20, 345);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(464, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(589, 251);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(589, 363);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // textEdit73
            // 
            this.textEdit73.Location = new System.Drawing.Point(117, 311);
            this.textEdit73.Name = "textEdit73";
            this.textEdit73.Size = new System.Drawing.Size(92, 20);
            this.textEdit73.TabIndex = 10;
            // 
            // textEdit53
            // 
            this.textEdit53.Location = new System.Drawing.Point(318, 287);
            this.textEdit53.Name = "textEdit53";
            this.textEdit53.Size = new System.Drawing.Size(89, 20);
            this.textEdit53.TabIndex = 8;
            // 
            // textEdit93
            // 
            this.textEdit93.Location = new System.Drawing.Point(516, 311);
            this.textEdit93.Name = "textEdit93";
            this.textEdit93.Size = new System.Drawing.Size(81, 20);
            this.textEdit93.TabIndex = 12;
            // 
            // textEdit43
            // 
            this.textEdit43.Location = new System.Drawing.Point(117, 287);
            this.textEdit43.Name = "textEdit43";
            this.textEdit43.Size = new System.Drawing.Size(92, 20);
            this.textEdit43.TabIndex = 7;
            // 
            // textEdit23
            // 
            this.textEdit23.Location = new System.Drawing.Point(318, 263);
            this.textEdit23.Name = "textEdit23";
            this.textEdit23.Size = new System.Drawing.Size(89, 20);
            this.textEdit23.TabIndex = 5;
            // 
            // textEdit33
            // 
            this.textEdit33.Location = new System.Drawing.Point(516, 263);
            this.textEdit33.Name = "textEdit33";
            this.textEdit33.Size = new System.Drawing.Size(81, 20);
            this.textEdit33.TabIndex = 6;
            // 
            // textEdit83
            // 
            this.textEdit83.Location = new System.Drawing.Point(318, 311);
            this.textEdit83.Name = "textEdit83";
            this.textEdit83.Size = new System.Drawing.Size(89, 20);
            this.textEdit83.TabIndex = 11;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(117, 263);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(92, 20);
            this.textEdit14.TabIndex = 4;
            // 
            // textEdit63
            // 
            this.textEdit63.Location = new System.Drawing.Point(516, 287);
            this.textEdit63.Name = "textEdit63";
            this.textEdit63.Size = new System.Drawing.Size(81, 20);
            this.textEdit63.TabIndex = 9;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.textEdit14;
            this.layoutControlItem28.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 251);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(201, 24);
            this.layoutControlItem28.Text = "layoutControlItem1";
            this.layoutControlItem28.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.textEdit23;
            this.layoutControlItem29.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem29.Location = new System.Drawing.Point(201, 251);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem29.Text = "layoutControlItem2";
            this.layoutControlItem29.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.textEdit33;
            this.layoutControlItem30.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem30.Location = new System.Drawing.Point(399, 251);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem30.Text = "layoutControlItem3";
            this.layoutControlItem30.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.textEdit43;
            this.layoutControlItem31.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem31.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 275);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(201, 24);
            this.layoutControlItem31.Text = "layoutControlItem4";
            this.layoutControlItem31.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.textEdit53;
            this.layoutControlItem32.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem32.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem32.Location = new System.Drawing.Point(201, 275);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem32.Text = "layoutControlItem5";
            this.layoutControlItem32.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.textEdit63;
            this.layoutControlItem33.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem33.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem33.Location = new System.Drawing.Point(399, 275);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem33.Text = "layoutControlItem6";
            this.layoutControlItem33.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.textEdit73;
            this.layoutControlItem34.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 299);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(201, 24);
            this.layoutControlItem34.Text = "layoutControlItem7";
            this.layoutControlItem34.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.textEdit83;
            this.layoutControlItem35.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem35.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem35.Location = new System.Drawing.Point(201, 299);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem35.Text = "layoutControlItem8";
            this.layoutControlItem35.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.textEdit93;
            this.layoutControlItem36.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem36.Location = new System.Drawing.Point(399, 299);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem36.Text = "layoutControlItem9";
            this.layoutControlItem36.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(84, 13);
            // 
            // textEdit72
            // 
            this.textEdit72.Location = new System.Drawing.Point(127, 204);
            this.textEdit72.Name = "textEdit72";
            this.textEdit72.Size = new System.Drawing.Size(111, 20);
            this.textEdit72.TabIndex = 10;
            // 
            // textEdit52
            // 
            this.textEdit52.Location = new System.Drawing.Point(423, 180);
            this.textEdit52.Name = "textEdit52";
            this.textEdit52.Size = new System.Drawing.Size(763, 20);
            this.textEdit52.TabIndex = 8;
            // 
            // textEdit42
            // 
            this.textEdit42.Location = new System.Drawing.Point(127, 180);
            this.textEdit42.Name = "textEdit42";
            this.textEdit42.Size = new System.Drawing.Size(111, 20);
            this.textEdit42.TabIndex = 7;
            // 
            // textEdit22
            // 
            this.textEdit22.Location = new System.Drawing.Point(423, 156);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Size = new System.Drawing.Size(763, 20);
            this.textEdit22.TabIndex = 5;
            // 
            // textEdit82
            // 
            this.textEdit82.Location = new System.Drawing.Point(423, 204);
            this.textEdit82.Name = "textEdit82";
            this.textEdit82.Size = new System.Drawing.Size(763, 20);
            this.textEdit82.TabIndex = 11;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(127, 156);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(111, 20);
            this.textEdit13.TabIndex = 4;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit13;
            this.layoutControlItem19.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem19.Location = new System.Drawing.Point(10, 144);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem19.Text = "layoutControlItem1";
            this.layoutControlItem19.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit22;
            this.layoutControlItem20.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem20.Location = new System.Drawing.Point(306, 144);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(872, 24);
            this.layoutControlItem20.Text = "layoutControlItem2";
            this.layoutControlItem20.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.textEdit42;
            this.layoutControlItem22.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem22.Location = new System.Drawing.Point(10, 168);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem22.Text = "layoutControlItem4";
            this.layoutControlItem22.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textEdit52;
            this.layoutControlItem23.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem23.Location = new System.Drawing.Point(306, 168);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(872, 24);
            this.layoutControlItem23.Text = "layoutControlItem5";
            this.layoutControlItem23.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.textEdit72;
            this.layoutControlItem25.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem25.Location = new System.Drawing.Point(10, 192);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(220, 422);
            this.layoutControlItem25.Text = "layoutControlItem7";
            this.layoutControlItem25.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.textEdit82;
            this.layoutControlItem26.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem26.Location = new System.Drawing.Point(306, 192);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(872, 422);
            this.layoutControlItem26.Text = "layoutControlItem8";
            this.layoutControlItem26.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(84, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(460, 270);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(29, 120);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // AddFormStoreInfo
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 469);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AddFormStoreInfo";
            this.Text = "Store Information";
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStoreName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInformation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAtmosphere.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShortAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSpokenLanguage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtRegisterDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtRegisterDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStartTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tCloseTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpenDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit73.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit93.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit83.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit63.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit72.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit82.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.TextEdit textEdit73;
        private DevExpress.XtraEditors.TextEdit textEdit53;
        private DevExpress.XtraEditors.TextEdit textEdit93;
        private DevExpress.XtraEditors.TextEdit textEdit43;
        private DevExpress.XtraEditors.TextEdit textEdit23;
        private DevExpress.XtraEditors.TextEdit textEdit33;
        private DevExpress.XtraEditors.TextEdit textEdit83;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.TextEdit textEdit63;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.TextEdit textEdit72;
        private DevExpress.XtraEditors.TextEdit textEdit52;
        private DevExpress.XtraEditors.TextEdit textEdit42;
        private DevExpress.XtraEditors.TextEdit textEdit22;
        private DevExpress.XtraEditors.TextEdit textEdit82;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        public DevExpress.XtraEditors.TimeEdit tStartTime;
        public DevExpress.XtraEditors.TimeEdit tCloseTime;
        public DevExpress.XtraEditors.TextEdit txtSpokenLanguage;
        public DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        public DevExpress.XtraEditors.TextEdit txtRate;
        public DevExpress.XtraEditors.TextEdit txtStoreID;
        public DevExpress.XtraEditors.TextEdit txtStoreName;
        public DevExpress.XtraEditors.TextEdit txtAddress;
        public DevExpress.XtraEditors.TextEdit txtInformation;
        public DevExpress.XtraEditors.TextEdit txtAtmosphere;
        public DevExpress.XtraEditors.TextEdit txtShortAddress;
        public DevExpress.XtraEditors.TextEdit txtContact;
        public DevExpress.XtraEditors.TextEdit txtWebsite;
        public DevExpress.XtraEditors.PictureEdit pbLogo;
        public DevExpress.XtraEditors.DateEdit dtRegisterDate;
        public DevExpress.XtraEditors.CheckedComboBoxEdit txtOpenDay;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
    }
}