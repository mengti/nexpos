﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using ZooManagement.Helper;

namespace Nexvis.NexPOS.Report
{
    public partial class rptCashierBalance : DevExpress.XtraReports.UI.XtraReport
    {
        public rptCashierBalance()
        {
            InitializeComponent();
        }

        private void rptCashierBalance_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDate.Text = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss tt");
            lbShop.Text = CSShopModel.ZooName;
        }
    }
}
