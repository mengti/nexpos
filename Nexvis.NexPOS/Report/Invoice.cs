﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ZooManagement.Helper;
using ZooMangement;

namespace GymCoffee.Report
{
    public partial class Invoice : DevExpress.XtraReports.UI.XtraReport
    {
        public Invoice()
        {
            InitializeComponent();
        }

        private void Invoice_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
          
                lblDate.Text = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss tt");

               // lblocation.Text = CSZooModel.ZooAddress;
               // lbContact.Text = "Tell : " + CSZooModel.ZooPhone;
                lbShop.Text = CSShopModel.ZooName;
                ptBox.Image = DACClasses.ReadImage(CSShopModel.ShopLogo);
        }
    }
}
