﻿using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nexvis.NexPOS.Report
{
    public partial class FrmAdvance : DevExpress.XtraEditors.XtraForm
    {

        ZooEntity DB = new ZooEntity();
        public FrmAdvance()
        {
            InitializeComponent();

           
        }

        List<INItemCategory> _ItemCategories = new List<INItemCategory>();
        List<CSUOM> _UOM = new List<CSUOM>();
        public bool isAdvance = false;


        // public  FrmAdvance advanceObj = new FrmAdvance();
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmAdvance_Load(object sender, EventArgs e)
        {
            

            _ItemCategories = DB.INItemCategories.ToList();
            _UOM = DB.CSUOMs.ToList();

            GlobalInitializer.GsFillLookUpEdit(_ItemCategories.ToList(), "CategoryID", "CategoryName", luCategory, (int)_ItemCategories.ToList().Count);
            GlobalInitializer.GsFillLookUpEdit(_UOM.ToList(), "UOMID", "Descr", luUOM, (int)_UOM.ToList().Count);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            isAdvance = true;
            this.Close();
        }
    }
}