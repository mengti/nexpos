﻿using DevExpress.XtraEditors;
using DevExpress.XtraPrinting.BarCode;
using DevExpress.XtraReports.UI;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZooMangement;
//using Microsoft.Office.Interop.Word;
using DevExpress.Map.Kml.Model;
using DevExpress.XtraRichEdit;
using System.Diagnostics;

namespace Nexvis.NexPOS.Report
{
    public partial class scrBarcode : DevExpress.XtraEditors.XtraForm
    {
        public scrBarcode()
        {
            InitializeComponent();
            downloadsPath = KnownFolders.GetPath(KnownFolder.Downloads);

        }

        ZooEntity DB = new ZooEntity();


        List<INItem> _Product = new List<INItem>();
        List<MDSalesPrice> _SalePrice = new List<MDSalesPrice>();
      //  List<SaleProductDomain> _ProductSalePrice = new List<SaleProductDomain>();
        public bool isSuccessful = true;
        bool IsShow = false;
        string fileName = "";
        string downloadsPath = "";

        BarCodeGeneratorBase eaN128Generator1;
        //DevExpress.XtraPrinting.BarCode.EAN128Generator eaN128Generator1 = new EAN128Generator();

        private void FrmBarcode_Load(object sender, EventArgs e)
        {
            _SalePrice = DB.MDSalesPrices.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
             _Product = DB.INItems.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();

            GlobalInitializer.GsFillComboboxEdit(_Product.ToList(), "InventoryID", "InventoryCD", cboItemName, (int)_Product.ToList().Count);
        }
        rptCuscen rpt = new rptCuscen();
        public FrmAdvance _advance = new FrmAdvance();
        private void btnFilter_Click(object sender, EventArgs e)
        {
          
       


            int i = 0;
            float y = 0F;
            float yLabel = 34.31406F;
            float space = 0F;

            documentViewer1.DocumentSource = null;
            rpt = new rptCuscen();

            if (cboItemName.EditValue == null || cboItemName.EditValue == "")
            {
                return;
            }

            string[] array;
            array = cboItemName.EditValue.ToString().Split(',');

            if (array.Length == 0 || cboItemName.EditValue == null)
            {
                return;
            }

            for (int k = 1; k <= Convert.ToInt32(txtQty.Text); k ++)
            {
                for (int p = 0; p < array.Length; p++)
                {


                    var list = _Product.Where(j => j.InventoryID == Convert.ToInt32(array[p])).ToList();
                    var listSalePrice = _SalePrice.Where(j => j.InventoryID == Convert.ToInt32(array[p])).ToList();

                    if (_advance.luCategory.EditValue != null)
                    {
                        list = list.Where(x => x.CategoryID.ToString() == _advance.luCategory.EditValue.ToString()).ToList();
                    }
                    
                    if (_advance.luUOM.EditValue != null)
                    {
                        list = list.Where(x => x.BaseUnit == _advance.luUOM.EditValue.ToString()).ToList();
                        listSalePrice = listSalePrice.Where(x => x.UOM == _advance.luUOM.EditValue.ToString()).ToList();
                    }

                    if (_advance.ckcboColumnView.EditValue != null)
                    {
                       // list = list.Where(x => x.BaseUnit == _advance.luUOM.EditValue.ToString()).ToList();
                    }


                    if (list.Count != 0)
                    {
                        XRBarCode barCodeItem1 = new XRBarCode();
                        XRLabel lbName = new XRLabel();
                        XRLabel lbPrice = new XRLabel();
                        XRPictureBox pictureBox = new XRPictureBox();

                        if (_advance.ckcboColumnView.EditValue.ToString() != "" || _advance.ckcboColumnView.EditValue != null)
                        {

                            string[] arrayColView;
                            arrayColView = _advance.ckcboColumnView.EditValue.ToString().Split(',');

                            if (arrayColView.Length == 0 || _advance.ckcboColumnView.EditValue == null)
                            {
                                return;
                            }

                            for (int m = 0; m < arrayColView.Length; m++) 
                            {
                                string NameView = arrayColView[m].Trim().ToString();

                                lbName.LocationFloat = new DevExpress.Utils.PointFloat(0F, y);
                                lbName.Multiline = true;
                                lbName.Name = "lbName" + i++;
                                lbName.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
                                lbName.SizeF = new System.Drawing.SizeF(208.0251F, 34.31406F);
                                lbName.StylePriority.UseTextAlignment = false;
                                lbName.StylePriority.UsePadding = false;
                                lbName.StylePriority.UseTextAlignment = false;
                                lbName.CanGrow = false;
                                lbName.Font = new System.Drawing.Font("Arial", 9F);
                                lbName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;

                                barCodeItem1.Font = new System.Drawing.Font("Arial", 5F);
                                barCodeItem1.LocationFloat = new DevExpress.Utils.PointFloat(0F, yLabel);
                                barCodeItem1.Name = "barCodeItem" + i++;
                                barCodeItem1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
                                barCodeItem1.SizeF = new System.Drawing.SizeF(208.0251F, 63.66663F);
                                barCodeItem1.StylePriority.UseTextAlignment = false;
                                barCodeItem1.Symbology = eaN128Generator1;
                                barCodeItem1.Text = list.FirstOrDefault().Barcode.Trim();
                                barCodeItem1.AutoModule = true;
                                barCodeItem1.ShowText = false;
                                if (IsShow) barCodeItem1.ShowText = true;

                                if (NameView == "Items")
                                {         
                                    lbName.Tag = list.FirstOrDefault().InventoryCD;
                                }

                                if (NameView == "Price")
                                {
                                    lbPrice.Tag = list.FirstOrDefault().SalePrice;
                                    lbPrice.Text = String.Format("${0:N2} ", lbPrice.Tag);

                                    if (listSalePrice.Count != 0)
                                    {
                                        foreach (var salesPrice in listSalePrice)
                                        {
                                            if (salesPrice.UOM == list.FirstOrDefault().BaseUnit)
                                            {
                                                lbPrice.Tag = salesPrice.SalesPrice;
                                                lbPrice.Text = String.Format("${0:N2} ", lbPrice.Tag);
                                            }
                                        }
                                    }
                                }

                                if (NameView == "Photo")
                                {
                                    //Add picture
                                    pictureBox.LocationFloat = new DevExpress.Utils.PointFloat(208.0251F, yLabel);
                                    pictureBox.Name = "xrPictureBox1";
                                    pictureBox.SizeF = new System.Drawing.SizeF(44.30823F, 36.45833F);
                                    pictureBox.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
                                    //pictureBox.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.TopLeft;
                                    pictureBox.Image = DACClasses.ReadImage(list.FirstOrDefault().ImageUrl);

                                    rpt.Detail.Controls.Add(pictureBox);
                                }

                                if (NameView == "Barcode")
                                {
                                    // barCodeItem1.ShowText = true;
                                    IsShow = true;
                                }
                            }

                            rpt.Detail.Controls.AddRange(
                                new DevExpress.XtraReports.UI.XRControl[] { lbName, barCodeItem1 });

                            y = 63.66663F + 34.31406F + y;
                            yLabel = y + 34.31406F;
                            lbName.Text = lbName.Tag + " " + lbPrice.Text;

                            //Have Sale Price
                            if (listSalePrice.Count != 0)
                            {
                                foreach (var salesPrice in listSalePrice)
                                {
                                    if (salesPrice.UOM != list.FirstOrDefault().BaseUnit)
                                    {
                                        XRBarCode barCodeItemPrice = new XRBarCode();
                                        XRLabel lbNamePrice = new XRLabel();
                                        XRPictureBox xrPictureBox1 = new XRPictureBox();

                                        for (int m = 0; m < arrayColView.Length; m++)
                                        {
                                            string NameView = arrayColView[m].Trim().ToString();

                                            lbNamePrice.LocationFloat = new DevExpress.Utils.PointFloat(0F, y);
                                            lbNamePrice.Multiline = true;
                                            lbNamePrice.Name = "lbName" + i++;
                                            lbNamePrice.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
                                            lbNamePrice.SizeF = new System.Drawing.SizeF(208.0251F, 34.31406F);
                                            lbNamePrice.StylePriority.UseTextAlignment = false;
                                            lbNamePrice.StylePriority.UsePadding = false;
                                            lbNamePrice.StylePriority.UseTextAlignment = false;
                                            lbNamePrice.CanGrow = false;
                                            lbNamePrice.Font = new System.Drawing.Font("Arial", 9F);
                                            lbNamePrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;

                                            barCodeItemPrice.Font = new System.Drawing.Font("Arial", 5F);
                                            barCodeItemPrice.LocationFloat = new DevExpress.Utils.PointFloat(0F, yLabel);
                                            barCodeItemPrice.Name = "barCodeItem" + i++;
                                            barCodeItemPrice.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
                                            barCodeItemPrice.SizeF = new System.Drawing.SizeF(208.0251F, 63.66663F);
                                            barCodeItemPrice.StylePriority.UseTextAlignment = false;
                                            barCodeItemPrice.Symbology = eaN128Generator1;
                                            barCodeItemPrice.Text = list.FirstOrDefault().Barcode.Trim();
                                            barCodeItemPrice.AutoModule = true;
                                            barCodeItemPrice.ShowText = false;
                                            if (IsShow) barCodeItemPrice.ShowText = true;

                                            if (NameView == "Items")
                                            {
                                                lbNamePrice.Tag = list.FirstOrDefault().InventoryCD;
                                            }

                                            if (NameView == "Price")
                                            {
                                                lbPrice.Tag = salesPrice.SalesPrice;
                                                lbPrice.Text = String.Format("${0:N2} ", lbPrice.Tag);
                                            }

                                            if (NameView == "Photo")
                                            {
                                                //Add picture
                                                xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(208.0251F, yLabel);
                                                xrPictureBox1.Name = "xrPictureBox1";
                                                xrPictureBox1.SizeF = new System.Drawing.SizeF(44.30823F, 36.45833F);
                                                xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
                                                //pictureBox.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.TopLeft;
                                                xrPictureBox1.Image = DACClasses.ReadImage(list.FirstOrDefault().ImageUrl);

                                                rpt.Detail.Controls.Add(xrPictureBox1);
                                            }

                                            if (NameView == "Barcode")
                                            {
                                                // barCodeItem1.ShowText = true;
                                                IsShow = true;
                                            }
                                        }


                                        rpt.Detail.Controls.AddRange(
                                            new DevExpress.XtraReports.UI.XRControl[] { lbNamePrice, barCodeItemPrice });

                                        y = 63.66663F + 34.31406F + y;
                                        yLabel = y + 34.31406F;
                                        lbNamePrice.Text = lbNamePrice.Tag + " " + lbPrice.Text;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            documentViewer1.DocumentSource = rpt;
            documentViewer1.InitiateDocumentCreation();
            rpt.CreateDocument();
            documentViewer1.Refresh();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
           // string currentDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
         //   fileName = downloadsPath + "\\ItemBarcode_" + currentDateTime + ".docx";
            ReportPrintTool printTool = new ReportPrintTool(rpt);
            // printTool.Load(downloadsPath+"\\");
            
            printTool.PrintDialog();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            _SalePrice = DB.MDSalesPrices.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            _Product = DB.INItems.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();

            GlobalInitializer.GsFillComboboxEdit(_Product.ToList(), "InventoryID", "InventoryCD", cboItemName, (int)_Product.ToList().Count);

            documentViewer1.DocumentSource = null;
            txtQty.Text = "1";
            cboItemName.EditValue = null;
            cboItemName.Text = "";
            cboItemName.RefreshEditValue();
            _advance = new FrmAdvance();
        }

        private void cboPrintType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string currentDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                 

            if (cboPrintType.EditValue.ToString() == "Word")
            {
                fileName = downloadsPath + "\\ItemBarcode_" + currentDateTime + ".docx";
                rpt.ExportToDocx(fileName);
                
                //  XtraMessageBox.Show("Print complete!", "NEXPOS", MessageBoxButtons.OK, MessageBoxIcon.Information);

                var result = XtraMessageBox.Show("Print complete!. Are you want open "+ fileName + "?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    Process.Start(fileName);
                }
                    
            }
            else if(cboPrintType.EditValue.ToString() == "Excel")
            {
                fileName = downloadsPath + "\\ItemBarcode_" + currentDateTime + ".xlsx";
                rpt.ExportToXlsx(fileName);
                
                // XtraMessageBox.Show("Print complete!", "NEXPOS", MessageBoxButtons.OK, MessageBoxIcon.Information);

                var result = XtraMessageBox.Show("Print complete!. Are you want open " + fileName + "?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    Process.Start(fileName);
                }
            }
        }

        private void btnAdvance_Click(object sender, EventArgs e)
        {
            if (_advance.isAdvance)
            {
                FrmAdvance advance = new FrmAdvance();
                advance = _advance;
                advance.ShowDialog();        
            }
            else
            {
                FrmAdvance advance = new FrmAdvance();
                advance.ShowDialog();

                _advance = advance;
            }
                
           
        }

        private void cboItemName_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}