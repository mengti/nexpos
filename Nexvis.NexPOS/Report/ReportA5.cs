﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ZooManagement.Helper;

namespace ZooManagement.Report
{
    public partial class ReportA5 : DevExpress.XtraReports.UI.XtraReport
    {
        #region --- Variable Declaration --
        Double TotalAmount = 0;
        #endregion
        public ReportA5()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblPrice.Text = string.Format("{0} {1:N0}", GetCurrentColumnValue("Price"), "៛");
            lblAmount.Text = string.Format("{0} {1:N0}", GetCurrentColumnValue("Amount"), "៛");
            lblName.Text = GetCurrentColumnValue("ItemCode").ToString();
            // TotalAmount += (double)GetCurrentColumnValue("TotalAmount");
            //        TotalAmount = (double)lbltotaldola.Tag;
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
          // lblTotalAmount.Text = string.Format("{0} {1:N0}", TotalAmount, "៛");
            lblCashInUSD.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("CashInUSD"), "$");
            lblCashInRiel.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("CashInRiel"), "៛");
            lblChangeUSD.Text = string.Format("{0:N2} {1}", GetCurrentColumnValue("ChangeUSD"), "$");
            lblChangeRiel.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("ChangeRiel"), "៛");
            //      lbltotaldola.Text = string.Format("{0:N2} {1}", (TotalAmount / (double)GetCurrentColumnValue("ExchangeRate")), "$");
            // lblDiscountAmount.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("DiscountAmount"), "$");
            // lblVATAmount.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("VATAmount"), "$");
        }
       

        private void rptReceipt_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDate.Text = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss tt");

            lbLocationStore.Text = CSShopModel.ZooAddress;
            lbContact.Text = "Tell : " + CSShopModel.ZooPhone;
            lbShop.Text = CSShopModel.ZooName;
        //    picbox.ImageUrl = 
        }

    }
}
