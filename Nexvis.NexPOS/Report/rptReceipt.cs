﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ZooManagement.Helper;
using ZooMangement;

namespace Nexvis.NexPOS
{
    public partial class rptReceipt : DevExpress.XtraReports.UI.XtraReport
    {
        #region --- Variable Declaration --
        Double TotalAmount=0;
        #endregion

        #region --- Form Action ---
        public rptReceipt()
        {
            InitializeComponent();
        }

  
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblPrice.Text = string.Format("{0} {1:N0}", GetCurrentColumnValue("Price"), "៛");
            lblAmount.Text = string.Format("{0} {1:N0}", GetCurrentColumnValue("Amount"), "៛");
            lblName.Text = GetCurrentColumnValue("ItemCode").ToString();
           // TotalAmount += (double)GetCurrentColumnValue("TotalAmount");
             //        TotalAmount = (double)lbltotaldola.Tag;
        }

        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
          //  lblTotalAmount.Text = string.Format("{0} {1:N0}", TotalAmount, "៛");
            lblCashInUSD.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("CashInUSD"), "$");
            lblCashInRiel.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("CashInRiel"), "៛");
            lblChangeUSD.Text = string.Format("{0:N2} {1}", GetCurrentColumnValue("ChangeUSD"), "$");
            lblChangeRiel.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("ChangeRiel"), "៛");
      //      lbltotaldola.Text = string.Format("{0:N2} {1}", (TotalAmount / (double)GetCurrentColumnValue("ExchangeRate")), "$");
            // lblDiscountAmount.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("DiscountAmount"), "$");
            // lblVATAmount.Text = string.Format("{0:N0} {1}", GetCurrentColumnValue("VATAmount"), "$");
        }
        #endregion

        private void rptReceipt_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDate.Text = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss tt");

            lbLocation.Text = CSShopModel.ZooAddress;
            lbContact.Text = "Tell : " + CSShopModel.ZooPhone;
            lbShop.Text = CSShopModel.ZooName;
            ptBox.Image = DACClasses.ReadImage(CSShopModel.ShopLogo);
        }

        private void xrLabel18_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel19_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel9_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel25_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel26_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel20_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel21_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel11_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel39_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel28_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel22_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel29_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblTotalAmount_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel23_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lbltotaldola_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel30_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblCashInUSD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel31_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblCashInRiel_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblChangeUSD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblChangeRiel_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel33_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrLabel32_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
        }
    }
}
