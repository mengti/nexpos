﻿using DevExpress.XtraReports.UI;
using Nexvis.NexPOS.Helper;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using ZooManagement.Helper;
using ZooMangement;

namespace Nexvis.NexPOS.Report
{
    public partial class rptCuscen : DevExpress.XtraReports.UI.XtraReport
    {
        public rptCuscen()
        {
            InitializeComponent();
        }

        private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lbNameUser.Text =  CSUserModel.LoginName;
            lbCompany.Text = "Store : "+ CSShopModel.ZooName;

        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pictureBox1.Image = DACClasses.ReadImage(CSShopModel.ShopLogo);
        }
    }
}
