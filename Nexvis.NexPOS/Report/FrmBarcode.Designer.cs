﻿
namespace Nexvis.NexPOS.Report
{
    partial class scrBarcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(scrBarcode));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            this.sidePanel1 = new DevExpress.XtraEditors.SidePanel();
            this.btnAdvance = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.cboPrintType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnFilter = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtQty = new DevExpress.XtraEditors.SpinEdit();
            this.txtItemType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cboItemName = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.printPreviewBarItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem31 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem32 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem33 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem34 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem35 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem36 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem37 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem38 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem39 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem40 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem41 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem42 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem43 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem44 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem45 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem46 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem47 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem48 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem49 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.documentViewer1 = new DevExpress.XtraPrinting.Preview.DocumentViewer();
            this.sidePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboPrintType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboItemName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // sidePanel1
            // 
            this.sidePanel1.AllowResize = false;
            this.sidePanel1.Controls.Add(this.btnAdvance);
            this.sidePanel1.Controls.Add(this.btnPrint);
            this.sidePanel1.Controls.Add(this.cboPrintType);
            this.sidePanel1.Controls.Add(this.btnClear);
            this.sidePanel1.Controls.Add(this.btnFilter);
            this.sidePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.sidePanel1.Location = new System.Drawing.Point(0, 0);
            this.sidePanel1.Name = "sidePanel1";
            this.sidePanel1.Size = new System.Drawing.Size(940, 37);
            this.sidePanel1.TabIndex = 0;
            this.sidePanel1.Text = "sidePanel1";
            // 
            // btnAdvance
            // 
            this.btnAdvance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdvance.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnAdvance.Appearance.Options.UseFont = true;
            this.btnAdvance.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdvance.ImageOptions.Image")));
            this.btnAdvance.Location = new System.Drawing.Point(838, 3);
            this.btnAdvance.Name = "btnAdvance";
            this.btnAdvance.Size = new System.Drawing.Size(90, 28);
            this.btnAdvance.TabIndex = 3;
            this.btnAdvance.Text = "Advance";
            this.btnAdvance.Click += new System.EventHandler(this.btnAdvance_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnPrint.Appearance.Options.UseFont = true;
            this.btnPrint.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.ImageOptions.Image")));
            this.btnPrint.Location = new System.Drawing.Point(604, 2);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(77, 28);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // cboPrintType
            // 
            this.cboPrintType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPrintType.EditValue = "";
            this.cboPrintType.Location = new System.Drawing.Point(681, 2);
            this.cboPrintType.Name = "cboPrintType";
            this.cboPrintType.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cboPrintType.Properties.Appearance.Options.UseFont = true;
            this.cboPrintType.Properties.Appearance.Options.UseTextOptions = true;
            this.cboPrintType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cboPrintType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPrintType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboPrintType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboPrintType.Properties.Items.AddRange(new object[] {
            "Word",
            "Excel"});
            this.cboPrintType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboPrintType.Size = new System.Drawing.Size(20, 28);
            this.cboPrintType.TabIndex = 3;
            this.cboPrintType.SelectedIndexChanged += new System.EventHandler(this.cboPrintType_SelectedIndexChanged);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnClear.Appearance.Options.UseFont = true;
            this.btnClear.Location = new System.Drawing.Point(709, 2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(123, 28);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Refresh / Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnFilter
            // 
            this.btnFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilter.Appearance.BackColor = System.Drawing.Color.White;
            this.btnFilter.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnFilter.Appearance.Options.UseBackColor = true;
            this.btnFilter.Appearance.Options.UseFont = true;
            this.btnFilter.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFilter.ImageOptions.Image")));
            this.btnFilter.Location = new System.Drawing.Point(521, 2);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(74, 28);
            this.btnFilter.TabIndex = 0;
            this.btnFilter.Text = "Filter";
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtQty);
            this.layoutControl1.Controls.Add(this.txtItemType);
            this.layoutControl1.Controls.Add(this.cboItemName);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 37);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(940, 40);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtQty
            // 
            this.txtQty.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtQty.Location = new System.Drawing.Point(709, 12);
            this.txtQty.Name = "txtQty";
            this.txtQty.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtQty.Properties.Appearance.Options.UseFont = true;
            this.txtQty.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtQty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtQty.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.txtQty.Properties.Mask.EditMask = "n0";
            this.txtQty.Properties.MaxLength = 5;
            this.txtQty.Size = new System.Drawing.Size(202, 28);
            this.txtQty.StyleController = this.layoutControl1;
            this.txtQty.TabIndex = 6;
            // 
            // txtItemType
            // 
            this.txtItemType.EditValue = "Inventory";
            this.txtItemType.Location = new System.Drawing.Point(92, 12);
            this.txtItemType.Name = "txtItemType";
            this.txtItemType.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtItemType.Properties.Appearance.Options.UseFont = true;
            this.txtItemType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtItemType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtItemType.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtItemType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtItemType.Properties.Items.AddRange(new object[] {
            "Inventory"});
            this.txtItemType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtItemType.Size = new System.Drawing.Size(185, 28);
            this.txtItemType.StyleController = this.layoutControl1;
            this.txtItemType.TabIndex = 4;
            // 
            // cboItemName
            // 
            this.cboItemName.EditValue = "";
            this.cboItemName.Location = new System.Drawing.Point(394, 12);
            this.cboItemName.Name = "cboItemName";
            this.cboItemName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cboItemName.Properties.Appearance.Options.UseFont = true;
            this.cboItemName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cboItemName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboItemName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.cboItemName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboItemName.Properties.PopupFormMinSize = new System.Drawing.Size(50, 50);
            this.cboItemName.Properties.PopupSizeable = false;
            this.cboItemName.Size = new System.Drawing.Size(190, 28);
            this.cboItemName.StyleController = this.layoutControl1;
            this.cboItemName.TabIndex = 5;
            this.cboItemName.EditValueChanged += new System.EventHandler(this.cboItemName_EditValueChanged);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(923, 52);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtItemType;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(269, 32);
            this.layoutControlItem1.Text = "Item Type";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(77, 21);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(269, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(33, 32);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(576, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(41, 32);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.cboItemName;
            this.layoutControlItem2.Location = new System.Drawing.Point(302, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(274, 32);
            this.layoutControlItem2.Text = "Item Name";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(77, 21);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.txtQty;
            this.layoutControlItem3.Location = new System.Drawing.Point(617, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(286, 32);
            this.layoutControlItem3.Text = "Quantities";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(77, 21);
            // 
            // printPreviewBarItem9
            // 
            this.printPreviewBarItem9.Caption = "Custom Margins...";
            this.printPreviewBarItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup;
            this.printPreviewBarItem9.Enabled = false;
            this.printPreviewBarItem9.Id = 9;
            this.printPreviewBarItem9.Name = "printPreviewBarItem9";
            superToolTip1.FixedTooltipWidth = true;
            toolTipTitleItem1.Text = "Page Setup";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Show the Page Setup dialog.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.MaxWidth = 210;
            this.printPreviewBarItem9.SuperTip = superToolTip1;
            // 
            // printPreviewBarItem31
            // 
            this.printPreviewBarItem31.Caption = "PDF File";
            this.printPreviewBarItem31.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf;
            this.printPreviewBarItem31.Enabled = false;
            this.printPreviewBarItem31.Id = 31;
            this.printPreviewBarItem31.Name = "printPreviewBarItem31";
            superToolTip2.FixedTooltipWidth = true;
            toolTipTitleItem2.Text = "E-Mail As PDF";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Export the document to PDF and attach it to the e-mail.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            superToolTip2.MaxWidth = 210;
            this.printPreviewBarItem31.SuperTip = superToolTip2;
            // 
            // printPreviewBarItem32
            // 
            this.printPreviewBarItem32.Caption = "Text File";
            this.printPreviewBarItem32.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt;
            this.printPreviewBarItem32.Enabled = false;
            this.printPreviewBarItem32.Id = 32;
            this.printPreviewBarItem32.Name = "printPreviewBarItem32";
            superToolTip3.FixedTooltipWidth = true;
            toolTipTitleItem3.Text = "E-Mail As Text";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Export the document to Text and attach it to the e-mail.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            superToolTip3.MaxWidth = 210;
            this.printPreviewBarItem32.SuperTip = superToolTip3;
            // 
            // printPreviewBarItem33
            // 
            this.printPreviewBarItem33.Caption = "CSV File";
            this.printPreviewBarItem33.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv;
            this.printPreviewBarItem33.Enabled = false;
            this.printPreviewBarItem33.Id = 33;
            this.printPreviewBarItem33.Name = "printPreviewBarItem33";
            superToolTip4.FixedTooltipWidth = true;
            toolTipTitleItem4.Text = "E-Mail As CSV";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Export the document to CSV and attach it to the e-mail.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip4.MaxWidth = 210;
            this.printPreviewBarItem33.SuperTip = superToolTip4;
            // 
            // printPreviewBarItem34
            // 
            this.printPreviewBarItem34.Caption = "MHT File";
            this.printPreviewBarItem34.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht;
            this.printPreviewBarItem34.Enabled = false;
            this.printPreviewBarItem34.Id = 34;
            this.printPreviewBarItem34.Name = "printPreviewBarItem34";
            superToolTip5.FixedTooltipWidth = true;
            toolTipTitleItem5.Text = "E-Mail As MHT";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Export the document to MHT and attach it to the e-mail.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            superToolTip5.MaxWidth = 210;
            this.printPreviewBarItem34.SuperTip = superToolTip5;
            // 
            // printPreviewBarItem35
            // 
            this.printPreviewBarItem35.Caption = "XLS File";
            this.printPreviewBarItem35.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls;
            this.printPreviewBarItem35.Enabled = false;
            this.printPreviewBarItem35.Id = 35;
            this.printPreviewBarItem35.Name = "printPreviewBarItem35";
            superToolTip6.FixedTooltipWidth = true;
            toolTipTitleItem6.Text = "E-Mail As XLS";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Export the document to XLS and attach it to the e-mail.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            superToolTip6.MaxWidth = 210;
            this.printPreviewBarItem35.SuperTip = superToolTip6;
            // 
            // printPreviewBarItem36
            // 
            this.printPreviewBarItem36.Caption = "XLSX File";
            this.printPreviewBarItem36.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXlsx;
            this.printPreviewBarItem36.Enabled = false;
            this.printPreviewBarItem36.Id = 36;
            this.printPreviewBarItem36.Name = "printPreviewBarItem36";
            superToolTip7.FixedTooltipWidth = true;
            toolTipTitleItem7.Text = "E-Mail As XLSX";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Export the document to XLSX and attach it to the e-mail.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            superToolTip7.MaxWidth = 210;
            this.printPreviewBarItem36.SuperTip = superToolTip7;
            // 
            // printPreviewBarItem37
            // 
            this.printPreviewBarItem37.Caption = "RTF File";
            this.printPreviewBarItem37.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf;
            this.printPreviewBarItem37.Enabled = false;
            this.printPreviewBarItem37.Id = 37;
            this.printPreviewBarItem37.Name = "printPreviewBarItem37";
            superToolTip8.FixedTooltipWidth = true;
            toolTipTitleItem8.Text = "E-Mail As RTF";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Export the document to RTF and attach it to the e-mail.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            superToolTip8.MaxWidth = 210;
            this.printPreviewBarItem37.SuperTip = superToolTip8;
            // 
            // printPreviewBarItem38
            // 
            this.printPreviewBarItem38.Caption = "DOCX File";
            this.printPreviewBarItem38.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendDocx;
            this.printPreviewBarItem38.Enabled = false;
            this.printPreviewBarItem38.Id = 38;
            this.printPreviewBarItem38.Name = "printPreviewBarItem38";
            superToolTip9.FixedTooltipWidth = true;
            toolTipTitleItem9.Text = "E-Mail As DOCX";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Export the document to DOCX and attach it to the e-mail.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            superToolTip9.MaxWidth = 210;
            this.printPreviewBarItem38.SuperTip = superToolTip9;
            // 
            // printPreviewBarItem39
            // 
            this.printPreviewBarItem39.Caption = "Image File";
            this.printPreviewBarItem39.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic;
            this.printPreviewBarItem39.Enabled = false;
            this.printPreviewBarItem39.Id = 39;
            this.printPreviewBarItem39.Name = "printPreviewBarItem39";
            superToolTip10.FixedTooltipWidth = true;
            toolTipTitleItem10.Text = "E-Mail As Image";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Export the document to Image and attach it to the e-mail.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            superToolTip10.MaxWidth = 210;
            this.printPreviewBarItem39.SuperTip = superToolTip10;
            // 
            // printPreviewBarItem40
            // 
            this.printPreviewBarItem40.Caption = "PDF File";
            this.printPreviewBarItem40.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            this.printPreviewBarItem40.Enabled = false;
            this.printPreviewBarItem40.Id = 40;
            this.printPreviewBarItem40.Name = "printPreviewBarItem40";
            superToolTip11.FixedTooltipWidth = true;
            toolTipTitleItem11.Text = "Export to PDF";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Export the document to PDF and save it to the file on a disk.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            superToolTip11.MaxWidth = 210;
            this.printPreviewBarItem40.SuperTip = superToolTip11;
            // 
            // printPreviewBarItem41
            // 
            this.printPreviewBarItem41.Caption = "HTML File";
            this.printPreviewBarItem41.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm;
            this.printPreviewBarItem41.Enabled = false;
            this.printPreviewBarItem41.Id = 41;
            this.printPreviewBarItem41.Name = "printPreviewBarItem41";
            superToolTip12.FixedTooltipWidth = true;
            toolTipTitleItem12.Text = "Export to HTML";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Export the document to HTML and save it to the file on a disk.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            superToolTip12.MaxWidth = 210;
            this.printPreviewBarItem41.SuperTip = superToolTip12;
            // 
            // printPreviewBarItem42
            // 
            this.printPreviewBarItem42.Caption = "Text File";
            this.printPreviewBarItem42.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt;
            this.printPreviewBarItem42.Enabled = false;
            this.printPreviewBarItem42.Id = 42;
            this.printPreviewBarItem42.Name = "printPreviewBarItem42";
            superToolTip13.FixedTooltipWidth = true;
            toolTipTitleItem13.Text = "Export to Text";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Export the document to Text and save it to the file on a disk.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            superToolTip13.MaxWidth = 210;
            this.printPreviewBarItem42.SuperTip = superToolTip13;
            // 
            // printPreviewBarItem43
            // 
            this.printPreviewBarItem43.Caption = "CSV File";
            this.printPreviewBarItem43.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv;
            this.printPreviewBarItem43.Enabled = false;
            this.printPreviewBarItem43.Id = 43;
            this.printPreviewBarItem43.Name = "printPreviewBarItem43";
            superToolTip14.FixedTooltipWidth = true;
            toolTipTitleItem14.Text = "Export to CSV";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Export the document to CSV and save it to the file on a disk.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            superToolTip14.MaxWidth = 210;
            this.printPreviewBarItem43.SuperTip = superToolTip14;
            // 
            // printPreviewBarItem44
            // 
            this.printPreviewBarItem44.Caption = "MHT File";
            this.printPreviewBarItem44.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht;
            this.printPreviewBarItem44.Enabled = false;
            this.printPreviewBarItem44.Id = 44;
            this.printPreviewBarItem44.Name = "printPreviewBarItem44";
            superToolTip15.FixedTooltipWidth = true;
            toolTipTitleItem15.Text = "Export to MHT";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Export the document to MHT and save it to the file on a disk.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            superToolTip15.MaxWidth = 210;
            this.printPreviewBarItem44.SuperTip = superToolTip15;
            // 
            // printPreviewBarItem45
            // 
            this.printPreviewBarItem45.Caption = "XLS File";
            this.printPreviewBarItem45.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls;
            this.printPreviewBarItem45.Enabled = false;
            this.printPreviewBarItem45.Id = 45;
            this.printPreviewBarItem45.Name = "printPreviewBarItem45";
            superToolTip16.FixedTooltipWidth = true;
            toolTipTitleItem16.Text = "Export to XLS";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Export the document to XLS and save it to the file on a disk.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            superToolTip16.MaxWidth = 210;
            this.printPreviewBarItem45.SuperTip = superToolTip16;
            // 
            // printPreviewBarItem46
            // 
            this.printPreviewBarItem46.Caption = "XLSX File";
            this.printPreviewBarItem46.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXlsx;
            this.printPreviewBarItem46.Enabled = false;
            this.printPreviewBarItem46.Id = 46;
            this.printPreviewBarItem46.Name = "printPreviewBarItem46";
            superToolTip17.FixedTooltipWidth = true;
            toolTipTitleItem17.Text = "Export to XLSX";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Export the document to XLSX and save it to the file on a disk.";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            superToolTip17.MaxWidth = 210;
            this.printPreviewBarItem46.SuperTip = superToolTip17;
            // 
            // printPreviewBarItem47
            // 
            this.printPreviewBarItem47.Caption = "RTF File";
            this.printPreviewBarItem47.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf;
            this.printPreviewBarItem47.Enabled = false;
            this.printPreviewBarItem47.Id = 47;
            this.printPreviewBarItem47.Name = "printPreviewBarItem47";
            superToolTip18.FixedTooltipWidth = true;
            toolTipTitleItem18.Text = "Export to RTF";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Export the document to RTF and save it to the file on a disk.";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            superToolTip18.MaxWidth = 210;
            this.printPreviewBarItem47.SuperTip = superToolTip18;
            // 
            // printPreviewBarItem48
            // 
            this.printPreviewBarItem48.Caption = "DOCX File";
            this.printPreviewBarItem48.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportDocx;
            this.printPreviewBarItem48.Enabled = false;
            this.printPreviewBarItem48.Id = 48;
            this.printPreviewBarItem48.Name = "printPreviewBarItem48";
            superToolTip19.FixedTooltipWidth = true;
            toolTipTitleItem19.Text = "Export to DOCX";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Export the document to DOCX and save it to the file on a disk.";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            superToolTip19.MaxWidth = 210;
            this.printPreviewBarItem48.SuperTip = superToolTip19;
            // 
            // printPreviewBarItem49
            // 
            this.printPreviewBarItem49.Caption = "Image File";
            this.printPreviewBarItem49.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic;
            this.printPreviewBarItem49.Enabled = false;
            this.printPreviewBarItem49.Id = 49;
            this.printPreviewBarItem49.Name = "printPreviewBarItem49";
            superToolTip20.FixedTooltipWidth = true;
            toolTipTitleItem20.Text = "Export to Image";
            toolTipItem20.LeftIndent = 6;
            toolTipItem20.Text = "Export the document to Image and save it to the file on a disk.";
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem20);
            superToolTip20.MaxWidth = 210;
            this.printPreviewBarItem49.SuperTip = superToolTip20;
            // 
            // documentViewer1
            // 
            this.documentViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.documentViewer1.IsMetric = false;
            this.documentViewer1.Location = new System.Drawing.Point(0, 77);
            this.documentViewer1.Name = "documentViewer1";
            this.documentViewer1.Size = new System.Drawing.Size(940, 389);
            this.documentViewer1.TabIndex = 2;
            // 
            // scrBarcode
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(940, 466);
            this.Controls.Add(this.documentViewer1);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.sidePanel1);
            this.Font = new System.Drawing.Font("Tahoma", 8F);
            this.Name = "scrBarcode";
            this.Text = "FrmBarcode";
            this.Load += new System.EventHandler(this.FrmBarcode_Load);
            this.sidePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboPrintType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItemType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboItemName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SidePanel sidePanel1;
        private DevExpress.XtraEditors.ComboBoxEdit cboPrintType;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnFilter;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SpinEdit txtQty;
        private DevExpress.XtraEditors.ComboBoxEdit txtItemType;
        private DevExpress.XtraPrinting.Preview.DocumentViewer documentViewer1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cboItemName;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem9;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem31;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem32;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem33;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem34;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem35;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem36;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem37;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem38;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem39;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem40;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem41;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem42;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem43;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem44;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem45;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem46;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem47;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem48;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem49;
        private DevExpress.XtraEditors.SimpleButton btnPrint;
        internal DevExpress.XtraEditors.SimpleButton btnAdvance;
    }
}