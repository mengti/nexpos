﻿
namespace Nexvis.NexPOS.Report
{
    partial class rptCashierBalance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo objectConstructorInfo1 = new DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo();
            DevExpress.XtraReports.Parameters.DynamicListLookUpSettings dynamicListLookUpSettings1 = new DevExpress.XtraReports.Parameters.DynamicListLookUpSettings();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.label9 = new DevExpress.XtraReports.UI.XRLabel();
            this.label10 = new DevExpress.XtraReports.UI.XRLabel();
            this.label1 = new DevExpress.XtraReports.UI.XRLabel();
            this.label6 = new DevExpress.XtraReports.UI.XRLabel();
            this.label19 = new DevExpress.XtraReports.UI.XRLabel();
            this.label20 = new DevExpress.XtraReports.UI.XRLabel();
            this.label23 = new DevExpress.XtraReports.UI.XRLabel();
            this.label24 = new DevExpress.XtraReports.UI.XRLabel();
            this.label13 = new DevExpress.XtraReports.UI.XRLabel();
            this.label14 = new DevExpress.XtraReports.UI.XRLabel();
            this.label17 = new DevExpress.XtraReports.UI.XRLabel();
            this.label18 = new DevExpress.XtraReports.UI.XRLabel();
            this.label7 = new DevExpress.XtraReports.UI.XRLabel();
            this.label8 = new DevExpress.XtraReports.UI.XRLabel();
            this.label11 = new DevExpress.XtraReports.UI.XRLabel();
            this.label12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.label4 = new DevExpress.XtraReports.UI.XRLabel();
            this.label5 = new DevExpress.XtraReports.UI.XRLabel();
            this.label2 = new DevExpress.XtraReports.UI.XRLabel();
            this.label3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDate = new DevExpress.XtraReports.UI.XRLabel();
            this.lbShop = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.label27 = new DevExpress.XtraReports.UI.XRLabel();
            this.label26 = new DevExpress.XtraReports.UI.XRLabel();
            this.label25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.objectDataSource2 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.Cashier = new DevExpress.XtraReports.Parameters.Parameter();
            this.ShiffNumber = new DevExpress.XtraReports.Parameters.Parameter();
            this.objectDataSource3 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label9,
            this.label10,
            this.label1,
            this.label6,
            this.label19,
            this.label20,
            this.label23,
            this.label24,
            this.label13,
            this.label14,
            this.label17,
            this.label18,
            this.label7,
            this.label8,
            this.label11,
            this.label12,
            this.xrLabel48,
            this.xrLabel45,
            this.xrLabel46,
            this.xrLabel49,
            this.label4,
            this.label5,
            this.label2,
            this.label3});
            this.Detail.HeightF = 171.8416F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorderDashStyle = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 3F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 23.26062F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.lblDate,
            this.lbShop,
            this.xrLabel15,
            this.xrLabel2,
            this.xrLabel3,
            this.xrLabel10});
            this.PageHeader.HeightF = 95.83922F;
            this.PageHeader.Name = "PageHeader";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label27,
            this.label26,
            this.label25,
            this.xrLabel52});
            this.GroupFooter1.HeightF = 65.13251F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            // 
            // label9
            // 
            this.label9.BorderColor = System.Drawing.Color.DimGray;
            this.label9.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label9.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.label9.CanGrow = false;
            this.label9.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label9.LocationFloat = new DevExpress.Utils.PointFloat(0.6785282F, 166.8474F);
            this.label9.Name = "label9";
            this.label9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label9.SizeF = new System.Drawing.SizeF(128.5971F, 4.770866F);
            this.label9.StylePriority.UseBorderColor = false;
            this.label9.StylePriority.UseBorderDashStyle = false;
            this.label9.StylePriority.UseBorders = false;
            this.label9.StylePriority.UseFont = false;
            this.label9.StylePriority.UseTextAlignment = false;
            this.label9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.BorderColor = System.Drawing.Color.DimGray;
            this.label10.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label10.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.label10.CanGrow = false;
            this.label10.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label10.LocationFloat = new DevExpress.Utils.PointFloat(129.2755F, 166.8474F);
            this.label10.Name = "label10";
            this.label10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label10.SizeF = new System.Drawing.SizeF(152.7245F, 4.770866F);
            this.label10.StylePriority.UseBorderColor = false;
            this.label10.StylePriority.UseBorderDashStyle = false;
            this.label10.StylePriority.UseBorders = false;
            this.label10.StylePriority.UseFont = false;
            this.label10.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:yyyy-MM-dd h:mm:ss tt}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom;
            this.label10.Summary = xrSummary1;
            this.label10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.BorderColor = System.Drawing.Color.DimGray;
            this.label1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label1.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.label1.CanGrow = false;
            this.label1.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label1.LocationFloat = new DevExpress.Utils.PointFloat(0.6785272F, 0F);
            this.label1.Name = "label1";
            this.label1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label1.SizeF = new System.Drawing.SizeF(128.5971F, 4.770866F);
            this.label1.StylePriority.UseBorderColor = false;
            this.label1.StylePriority.UseBorderDashStyle = false;
            this.label1.StylePriority.UseBorders = false;
            this.label1.StylePriority.UseFont = false;
            this.label1.StylePriority.UseTextAlignment = false;
            this.label1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BorderColor = System.Drawing.Color.DimGray;
            this.label6.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label6.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.label6.CanGrow = false;
            this.label6.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label6.LocationFloat = new DevExpress.Utils.PointFloat(129.2755F, 0F);
            this.label6.Name = "label6";
            this.label6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label6.SizeF = new System.Drawing.SizeF(152.7245F, 4.770866F);
            this.label6.StylePriority.UseBorderColor = false;
            this.label6.StylePriority.UseBorderDashStyle = false;
            this.label6.StylePriority.UseBorders = false;
            this.label6.StylePriority.UseFont = false;
            this.label6.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:yyyy-MM-dd h:mm:ss tt}";
            xrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom;
            this.label6.Summary = xrSummary2;
            this.label6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label19.CanGrow = false;
            this.label19.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 145.4395F);
            this.label19.Name = "label19";
            this.label19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label19.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.label19.StylePriority.UseBorderDashStyle = false;
            this.label19.StylePriority.UseBorders = false;
            this.label19.StylePriority.UseFont = false;
            this.label19.StylePriority.UseTextAlignment = false;
            this.label19.Text = "Sale Amount Receive By Bank:";
            this.label19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.CanGrow = false;
            this.label20.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label20.LocationFloat = new DevExpress.Utils.PointFloat(0F, 129.7311F);
            this.label20.Name = "label20";
            this.label20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label20.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.label20.StylePriority.UseFont = false;
            this.label20.StylePriority.UseTextAlignment = false;
            this.label20.Text = "Sale Amount Receive By Cash";
            this.label20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.CanGrow = false;
            this.label23.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SaleAmountReceiveCash]")});
            this.label23.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 145.4395F);
            this.label23.Name = "label23";
            this.label23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label23.SizeF = new System.Drawing.SizeF(152.7245F, 15.70836F);
            this.label23.StylePriority.UseTextAlignment = false;
            this.label23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label23.TextFormatString = "{0:N2}$";
            // 
            // label24
            // 
            this.label24.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label24.CanGrow = false;
            this.label24.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SaleAmountReceiveBank]")});
            this.label24.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 129.7308F);
            this.label24.Name = "label24";
            this.label24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label24.SizeF = new System.Drawing.SizeF(152.7245F, 15.70836F);
            this.label24.StylePriority.UseBorderDashStyle = false;
            this.label24.StylePriority.UseBorders = false;
            this.label24.StylePriority.UseTextAlignment = false;
            this.label24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label24.TextFormatString = "{0:n2}$";
            // 
            // label13
            // 
            this.label13.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label13.CanGrow = false;
            this.label13.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Vat]")});
            this.label13.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 98.31356F);
            this.label13.Name = "label13";
            this.label13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label13.SizeF = new System.Drawing.SizeF(152.7245F, 15.70836F);
            this.label13.StylePriority.UseBorderDashStyle = false;
            this.label13.StylePriority.UseBorders = false;
            this.label13.StylePriority.UseTextAlignment = false;
            this.label13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label13.TextFormatString = "{0:n2}$";
            // 
            // label14
            // 
            this.label14.CanGrow = false;
            this.label14.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[GrandTotal]")});
            this.label14.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 114.0222F);
            this.label14.Name = "label14";
            this.label14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label14.SizeF = new System.Drawing.SizeF(152.7245F, 15.70836F);
            this.label14.StylePriority.UseTextAlignment = false;
            this.label14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label14.TextFormatString = "{0:n2}$";
            // 
            // label17
            // 
            this.label17.CanGrow = false;
            this.label17.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 114.0224F);
            this.label17.Name = "label17";
            this.label17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label17.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.label17.StylePriority.UseFont = false;
            this.label17.StylePriority.UseTextAlignment = false;
            this.label17.Text = "Grand Total:";
            this.label17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label18.CanGrow = false;
            this.label18.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label18.LocationFloat = new DevExpress.Utils.PointFloat(0F, 98.31388F);
            this.label18.Name = "label18";
            this.label18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label18.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.label18.StylePriority.UseBorderDashStyle = false;
            this.label18.StylePriority.UseBorders = false;
            this.label18.StylePriority.UseFont = false;
            this.label18.StylePriority.UseTextAlignment = false;
            this.label18.Text = "VAT ($):";
            this.label18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label7.CanGrow = false;
            this.label7.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 66.89658F);
            this.label7.Name = "label7";
            this.label7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label7.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.label7.StylePriority.UseBorderDashStyle = false;
            this.label7.StylePriority.UseBorders = false;
            this.label7.StylePriority.UseFont = false;
            this.label7.StylePriority.UseTextAlignment = false;
            this.label7.Text = "Total Amount:";
            this.label7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.CanGrow = false;
            this.label8.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 82.60521F);
            this.label8.Name = "label8";
            this.label8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label8.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.label8.StylePriority.UseFont = false;
            this.label8.StylePriority.UseTextAlignment = false;
            this.label8.Text = "Discounts:";
            this.label8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.CanGrow = false;
            this.label11.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Discount]")});
            this.label11.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 82.60496F);
            this.label11.Name = "label11";
            this.label11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label11.SizeF = new System.Drawing.SizeF(152.7245F, 15.70836F);
            this.label11.StylePriority.UseTextAlignment = false;
            this.label11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label11.TextFormatString = "{0:N2}$";
            // 
            // label12
            // 
            this.label12.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label12.CanGrow = false;
            this.label12.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TotalAmount]")});
            this.label12.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 66.89629F);
            this.label12.Name = "label12";
            this.label12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label12.SizeF = new System.Drawing.SizeF(152.7245F, 15.70836F);
            this.label12.StylePriority.UseBorderDashStyle = false;
            this.label12.StylePriority.UseBorders = false;
            this.label12.StylePriority.UseTextAlignment = false;
            this.label12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label12.TextFormatString = "{0:n2}$";
            // 
            // xrLabel48
            // 
            this.xrLabel48.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel48.CanGrow = false;
            this.xrLabel48.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OpeningAmount]")});
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 36.1876F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(152.7245F, 15.00003F);
            this.xrLabel48.StylePriority.UseBorderDashStyle = false;
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrLabel48.TextFormatString = "{0:n2}$";
            // 
            // xrLabel45
            // 
            this.xrLabel45.CanGrow = false;
            this.xrLabel45.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SaleAmount]")});
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 51.1879F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(152.7245F, 15.70836F);
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrLabel45.TextFormatString = "{0:N2}$";
            // 
            // xrLabel46
            // 
            this.xrLabel46.CanGrow = false;
            this.xrLabel46.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(0F, 51.1879F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "Sale Amount:";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel49
            // 
            this.xrLabel49.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.CanGrow = false;
            this.xrLabel49.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(0F, 36.18788F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(128.5971F, 15.00003F);
            this.xrLabel49.StylePriority.UseBorderDashStyle = false;
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = "Opening Amount:";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label4.CanGrow = false;
            this.label4.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 20.47925F);
            this.label4.Name = "label4";
            this.label4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label4.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.label4.StylePriority.UseBorderDashStyle = false;
            this.label4.StylePriority.UseBorders = false;
            this.label4.StylePriority.UseFont = false;
            this.label4.StylePriority.UseTextAlignment = false;
            this.label4.Text = "Close Date:";
            this.label4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label5.CanGrow = false;
            this.label5.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[EndDateTime]")});
            this.label5.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label5.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 20.47925F);
            this.label5.Name = "label5";
            this.label5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label5.SizeF = new System.Drawing.SizeF(152.7245F, 15.70836F);
            this.label5.StylePriority.UseBorderDashStyle = false;
            this.label5.StylePriority.UseBorders = false;
            this.label5.StylePriority.UseFont = false;
            this.label5.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:yyyy-MM-dd h:mm:ss tt}";
            xrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom;
            this.label5.Summary = xrSummary3;
            this.label5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.BorderColor = System.Drawing.Color.DimGray;
            this.label2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label2.CanGrow = false;
            this.label2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StartDateTime]")});
            this.label2.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label2.LocationFloat = new DevExpress.Utils.PointFloat(129.2755F, 4.770871F);
            this.label2.Name = "label2";
            this.label2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label2.SizeF = new System.Drawing.SizeF(152.7245F, 15.70836F);
            this.label2.StylePriority.UseBorderColor = false;
            this.label2.StylePriority.UseBorderDashStyle = false;
            this.label2.StylePriority.UseBorders = false;
            this.label2.StylePriority.UseFont = false;
            this.label2.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:yyyy-MM-dd h:mm:ss tt}";
            xrSummary4.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom;
            this.label2.Summary = xrSummary4;
            this.label2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.BorderColor = System.Drawing.Color.DimGray;
            this.label3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.label3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label3.CanGrow = false;
            this.label3.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.label3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.770866F);
            this.label3.Name = "label3";
            this.label3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label3.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.label3.StylePriority.UseBorderColor = false;
            this.label3.StylePriority.UseBorderDashStyle = false;
            this.label3.StylePriority.UseBorders = false;
            this.label3.StylePriority.UseFont = false;
            this.label3.StylePriority.UseTextAlignment = false;
            this.label3.Text = "Opening Date:";
            this.label3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "?Cashier")});
            this.xrLabel7.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(128.5971F, 82.2816F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(153.4029F, 13.55758F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "xrLabel7";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblDate
            // 
            this.lblDate.BorderColor = System.Drawing.Color.DimGray;
            this.lblDate.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.lblDate.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDate.CanGrow = false;
            this.lblDate.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.lblDate.LocationFloat = new DevExpress.Utils.PointFloat(128.5971F, 46.58452F);
            this.lblDate.Name = "lblDate";
            this.lblDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDate.SizeF = new System.Drawing.SizeF(153.4029F, 15.70836F);
            this.lblDate.StylePriority.UseBorderColor = false;
            this.lblDate.StylePriority.UseBorderDashStyle = false;
            this.lblDate.StylePriority.UseBorders = false;
            this.lblDate.StylePriority.UseFont = false;
            xrSummary5.FormatString = "{0:yyyy-MM-dd h:mm:ss tt}";
            xrSummary5.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom;
            this.lblDate.Summary = xrSummary5;
            // 
            // lbShop
            // 
            this.lbShop.BorderColor = System.Drawing.Color.White;
            this.lbShop.Font = new System.Drawing.Font("Khmer OS Muol", 8F, System.Drawing.FontStyle.Bold);
            this.lbShop.ForeColor = System.Drawing.Color.Black;
            this.lbShop.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lbShop.Name = "lbShop";
            this.lbShop.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbShop.SizeF = new System.Drawing.SizeF(282F, 46.58453F);
            this.lbShop.StylePriority.UseBorderColor = false;
            this.lbShop.StylePriority.UseFont = false;
            this.lbShop.StylePriority.UseForeColor = false;
            this.lbShop.StylePriority.UseTextAlignment = false;
            this.lbShop.Text = "The MyStore";
            this.lbShop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.CanGrow = false;
            this.xrLabel15.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ShiffNo]")});
            this.xrLabel15.Font = new System.Drawing.Font("Khmer OS Muol Light", 6F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(128.5971F, 66.57351F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(153.4029F, 15.70821F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "xrLabel15";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.CanGrow = false;
            this.xrLabel2.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(4.674874E-06F, 66.5733F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Shiff No:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BorderColor = System.Drawing.Color.DimGray;
            this.xrLabel3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 46.58431F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(128.5971F, 15.70836F);
            this.xrLabel3.StylePriority.UseBorderColor = false;
            this.xrLabel3.StylePriority.UseBorderDashStyle = false;
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Print Date:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.CanGrow = false;
            this.xrLabel10.Font = new System.Drawing.Font("Khmer OS Battambang", 6F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(4.674874E-06F, 82.2816F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(128.5971F, 13.55762F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Cashier:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label27.CanGrow = false;
            this.label27.Font = new System.Drawing.Font("Khmer OS Muol", 6F);
            this.label27.LocationFloat = new DevExpress.Utils.PointFloat(128.5971F, 43.81748F);
            this.label27.Multiline = true;
            this.label27.Name = "label27";
            this.label27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label27.SizeF = new System.Drawing.SizeF(153.4029F, 21.31503F);
            this.label27.StylePriority.UseBorders = false;
            this.label27.StylePriority.UseFont = false;
            this.label27.StylePriority.UseTextAlignment = false;
            this.label27.Text = "....................";
            this.label27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label26.CanGrow = false;
            this.label26.Font = new System.Drawing.Font("Khmer OS Muol", 6F);
            this.label26.LocationFloat = new DevExpress.Utils.PointFloat(0F, 43.81749F);
            this.label26.Multiline = true;
            this.label26.Name = "label26";
            this.label26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label26.SizeF = new System.Drawing.SizeF(128.597F, 21.31503F);
            this.label26.StylePriority.UseBorders = false;
            this.label26.StylePriority.UseFont = false;
            this.label26.StylePriority.UseTextAlignment = false;
            this.label26.Text = "...............";
            this.label26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label25.CanGrow = false;
            this.label25.Font = new System.Drawing.Font("Khmer OS Muol", 6F);
            this.label25.LocationFloat = new DevExpress.Utils.PointFloat(128.597F, 0F);
            this.label25.Multiline = true;
            this.label25.Name = "label25";
            this.label25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label25.SizeF = new System.Drawing.SizeF(153.4029F, 21.31503F);
            this.label25.StylePriority.UseBorders = false;
            this.label25.StylePriority.UseFont = false;
            this.label25.StylePriority.UseTextAlignment = false;
            this.label25.Text = "Cashier";
            this.label25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.CanGrow = false;
            this.xrLabel52.Font = new System.Drawing.Font("Khmer OS Muol", 6F);
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel52.Multiline = true;
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(128.5971F, 21.31503F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "Suppervisor";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // objectDataSource2
            // 
            this.objectDataSource2.Constructor = objectConstructorInfo1;
            this.objectDataSource2.DataSource = typeof(Nexvis.NexPOS.Data.CashierBalance);
            this.objectDataSource2.Name = "objectDataSource2";
            // 
            // Cashier
            // 
            this.Cashier.Description = "Cashier";
            this.Cashier.Name = "Cashier";
            this.Cashier.Visible = false;
            // 
            // ShiffNumber
            // 
            this.ShiffNumber.Description = "ShiffNumber";
            this.ShiffNumber.Name = "ShiffNumber";
            this.ShiffNumber.Type = typeof(int);
            this.ShiffNumber.ValueInfo = "0";
            dynamicListLookUpSettings1.DataSource = this.objectDataSource3;
            dynamicListLookUpSettings1.DisplayMember = "InvoiceID";
            dynamicListLookUpSettings1.ValueMember = "InvoiceID";
            this.ShiffNumber.ValueSourceSettings = dynamicListLookUpSettings1;
            this.ShiffNumber.Visible = false;
            // 
            // objectDataSource3
            // 
            this.objectDataSource3.DataSource = typeof(Nexvis.NexPOS.Helper.SaleProductDomain);
            this.objectDataSource3.Name = "objectDataSource3";
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataSource = typeof(Nexvis.NexPOS.Helper.SaleProductDomain);
            this.objectDataSource1.Name = "objectDataSource1";
            // 
            // rptCashierBalance
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.GroupFooter1});
            this.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dot;
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource3,
            this.objectDataSource1,
            this.objectDataSource2});
            this.DataSource = this.objectDataSource2;
            this.FilterString = "[ShiffNo] = ?ShiffNumber";
            this.Font = new System.Drawing.Font("Tahoma", 7F);
            this.Margins = new System.Drawing.Printing.Margins(4, 3, 3, 23);
            this.PageHeight = 1654;
            this.PageWidth = 289;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.Cashier,
            this.ShiffNumber});
            this.RollPaper = true;
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.StyleSheetPath = "";
            this.Version = "19.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.rptCashierBalance_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel label9;
        private DevExpress.XtraReports.UI.XRLabel label10;
        private DevExpress.XtraReports.UI.XRLabel label1;
        private DevExpress.XtraReports.UI.XRLabel label6;
        private DevExpress.XtraReports.UI.XRLabel label19;
        private DevExpress.XtraReports.UI.XRLabel label20;
        private DevExpress.XtraReports.UI.XRLabel label23;
        private DevExpress.XtraReports.UI.XRLabel label24;
        private DevExpress.XtraReports.UI.XRLabel label13;
        private DevExpress.XtraReports.UI.XRLabel label14;
        private DevExpress.XtraReports.UI.XRLabel label17;
        private DevExpress.XtraReports.UI.XRLabel label18;
        private DevExpress.XtraReports.UI.XRLabel label7;
        private DevExpress.XtraReports.UI.XRLabel label8;
        private DevExpress.XtraReports.UI.XRLabel label11;
        private DevExpress.XtraReports.UI.XRLabel label12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel label4;
        private DevExpress.XtraReports.UI.XRLabel label5;
        private DevExpress.XtraReports.UI.XRLabel label2;
        private DevExpress.XtraReports.UI.XRLabel label3;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lblDate;
        private DevExpress.XtraReports.UI.XRLabel lbShop;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel label27;
        private DevExpress.XtraReports.UI.XRLabel label26;
        private DevExpress.XtraReports.UI.XRLabel label25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource2;
        private DevExpress.XtraReports.Parameters.Parameter Cashier;
        private DevExpress.XtraReports.Parameters.Parameter ShiffNumber;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource3;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
    }
}
