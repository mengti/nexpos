﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DgoStore.LoadForm.Sale;
using DgoStore.LoadForm.Organizations;
using DgoStore.LoadForm.Employee;
using ZooManagement;
using ZooManagement.Report;
using DgoStore.LoadForm.Configuration.Currency;
using Nexvis.NexPOS.Sale;
using Nexvis.NexPOS.LoadForm;

namespace Nexvis.NexPOS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmLogin());
           // Application.Run(new FrmSalePrice());
           //Application.Run(new ReportDesign());
        }
    }
}
