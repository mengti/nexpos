﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ZooManagement.Connection
{
    public class AppSetting
    {
        Configuration config;
        public AppSetting()
        {
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        }
        public string GetConnectionString(string Key)
        {
            return config.ConnectionStrings.ConnectionStrings[Key].ConnectionString;
        }
        //Change connection Report
        public void SaveConnectSting(string Key, string Value)
        {
            config.ConnectionStrings.ConnectionStrings[Key].ConnectionString = Value;
            config.ConnectionStrings.ConnectionStrings[Key].ProviderName = "System.Data.SqlClient";
            config.Save(ConfigurationSaveMode.Modified);
        }
        //Update on Entity Client
        public void UpdateConnectSting(string Key, string ConnectionString)
        {
            System.Configuration.Configuration config =
            System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);

            // Because it's an EF connection string it's not a normal connection string
            // so we pull it into the EntityConnectionStringBuilder instead
            EntityConnectionStringBuilder efb = new EntityConnectionStringBuilder(
                    config.ConnectionStrings.ConnectionStrings[Key].ConnectionString);

            // Then we extract the actual underlying provider connection string
            SqlConnectionStringBuilder sqb = new SqlConnectionStringBuilder(efb.ProviderConnectionString);
            // Now we can set the datasource
            //sqb.InitialCatalog = InitialCatalog;
            //sqb.DataSource = DataSource;
            //sqb.UserID = UserID;
            //sqb.Password = Password;
            sqb.ConnectionString = ConnectionString;
            // Pop it back into the EntityConnectionStringBuilder 
            efb.ProviderConnectionString = sqb.ConnectionString;

            // And update...
            config.ConnectionStrings.ConnectionStrings[Key]
                .ConnectionString = efb.ConnectionString;

            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");
        }
    }
}
