﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.IO;
using ZooManagement;
//using Connection;
namespace DgoStore
{
    public class Connections
    {

        public static SqlConnection cnn = new SqlConnection();
        public static void Connection(String SqlCon)
        {
            try
            {
                cnn = new SqlConnection(SqlCon);
                cnn.Open();
            }
            catch (Exception)
            {
            }
        }



        public static SqlConnection conn = new SqlConnection();
        public static SqlCommand cmd = new SqlCommand();
        public static SqlDataAdapter da = new SqlDataAdapter();
        public static string Role;
        public static DateTime exp = DateTime.Now.AddMonths(2);
        public static string SearchInfor;
        public static int totalMb = 0;

        public static void ClearData(Control parent)
        {
            foreach (Control c in parent.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    c.Text = null;
                }
                else
                {
                    ClearData(c);
                }
                if (c.GetType() == typeof(ComboBox))
                {
                    c.Text = null;
                }
                else
                {
                    ClearData(c);
                }

                if (c.GetType() == typeof(DateTimePicker))
                {
                    c.Text = null;
                }
                else
                {
                    ClearData(c);
                }
            }
        }
        public static bool ConnectionDB(string str)
        {
            try
            {
                conn.ConnectionString = str;
                conn.Open();

                return true;
            }

            catch
            {
                return false;
            }

        }
       

        public static void SQL_Grid(DataGridView DG, string select)
        {
            //SqlConnection conn = new SqlConnection();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, conn); //c.con is the connection string

            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);

            DG.ReadOnly = true;
            DG.DataSource = dt;
            DG.AutoGenerateColumns = true;
            DG.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            DG.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            DG.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        public static void SQL_String_CBO_SQL(ComboBox CBO, String SQL_STR, String Vm, string Dm)
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter(SQL_STR, conn);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);
            CBO.DataSource = dt;
            CBO.DisplayMember = Dm;
            CBO.ValueMember = Vm;
        }

        public static bool TStatement_SQL(string SQL)
        {
            try
            {
                cmd.Connection = conn;
                cmd.CommandText = SQL;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string Get1Record(string SQL_STR)//
        {
            SqlDataAdapter objDa = new SqlDataAdapter(SQL_STR, conn);
            DataTable objTable = new DataTable();
            objDa.Fill(objTable);
            if (objTable.Rows.Count > 0)
            {
                if (Convert.ToString(objTable.Rows[0][0]) != string.Empty)
                {
                    return objTable.Rows[0][0].ToString();
                }
            }
            return "0";
        }
    }
}
