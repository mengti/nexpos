﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexvis.NexPOS.Helper
{
    class DateTimeConvert
    {
        public static DateTime? ConvertStringToDate(string stringDate)
        {
            DateTime? date;
            try
            {
                date = DateTime.Parse(stringDate);
            }
            catch
            {
                date = null;
            }
            return date;
        }
    }
}
