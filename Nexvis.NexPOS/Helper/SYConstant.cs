﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexvis.NexPOS.Helper
{
    public class SYConstant
    {
        public const string OK = "OK";

         public enum SYDocumentStatus
    {
            HOLD, OPEN, PENDING, CLOSED, RELEASED,REVERSED
        }
    }
}
