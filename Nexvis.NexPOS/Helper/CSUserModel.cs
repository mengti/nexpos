﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexvis.NexPOS.Helper
{
    public class CSUserModel
    {

        static List<string> _list;
        public static string User { get; set; }
        public static string LoginName { get; set; }
        public static int Role { get; set; }
        static CSUserModel()
        { 
            //
            // Allocate the list.
            //
            _list = new List<string>();
        } 
        public  int CompanyID { get; set; }
        public  string UserID { get; set; }
        public  string UserName { get; set; }
        public string RoleID { get; set; }
        public  string RoleName { get; set; }
        public  string Status { get; set; }
        public  string Password { get; set; }
        public  string UserEmployee { get; set; }

        public string Ur { get; set; }
    }
}
