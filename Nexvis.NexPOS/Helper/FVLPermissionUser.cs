﻿using DevComponents.DotNetBar.Controls;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nexvis.NexPOS.Helper
{
    public class FVLPermissionUser
    {
        public static string GR_DB_CODE { get; set; }
        public static string SCREENID { get; set; }
        public static bool Action_Check { get; set; }
        public static string SELECTED_USERID { get; set; }
        public static int SELECTED_ROLEID { get; set; }

        ZooEntity DB = new ZooEntity();
        
       
        public void SaveUserPermission(ListViewEx listView)
        {
            try
            {
                var checkName = "";
                for (int i = 0; i < listView.Items.Count; i++)
                {
                    checkName = listView.Items[i].Text;
                    if (listView.Items[i].Checked == true)
                    {
                        if (!DB.CSRoleItems.Any(x => x.RoleId == SELECTED_ROLEID && x.ScreenId == SCREENID && x.ActionName == checkName))
                        {
                            CSRoleItem roleItem = new CSRoleItem()
                            {
                                RoleId = SELECTED_ROLEID,
                                ScreenId = SCREENID,
                                ActionName = checkName,
                                ActionTemplateID = "NONE"
                            };
                            DB.CSRoleItems.Add(roleItem);
                        }          
                    }
                    else
                    {
                        if (DB.CSRoleItems.Any(x => x.RoleId == SELECTED_ROLEID && x.ScreenId == SCREENID && x.ActionName == checkName))
                        {
                            CSRoleItem role = DB.CSRoleItems.FirstOrDefault(x => x.RoleId == SELECTED_ROLEID && x.ScreenId == SCREENID && x.ActionName == checkName);
                            DB.CSRoleItems.Remove(role);
                        }
                    }             
                }
                
                int row = DB.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }
        

    }
}
