﻿using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace DgoStore
{
    public  class Fuction
    {
        //Design Gridview
        public static void Gridviews(GridView gv)
        {
            gv.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            gv.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            gv.OptionsBehavior.Editable = false;
            gv.OptionsSelection.EnableAppearanceFocusedCell = false;
            gv.OptionsView.ShowAutoFilterRow = true;
            gv.OptionsView.EnableAppearanceEvenRow = true;
        }
       //Load to Excel
        public void Eport(GridView GV)
        {
            if (GV.RowCount > 0)
            {
                if (System.IO.File.Exists(Application.StartupPath + "\\UserManagement.xlsx"))
                    System.IO.File.Delete(Application.StartupPath + "\\UserManagement.xlsx");
                GV.ExportToXlsx(Application.StartupPath + "\\UserManagement.xlsx");
                System.Diagnostics.Process.Start(Application.StartupPath + "\\UserManagement.xlsx");
            }
        }
        public static string EncodePassword(string originalPassword)
        {
            //Declarations
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;

            //Instantiate MD5 Crypto ServiceProvider, get bytes for original password and compute hash (encoded password)
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
            encodedBytes = md5.ComputeHash(originalBytes);

            //Convert encoded bytes back to a 'readable' string
            return BitConverter.ToString(encodedBytes);
        }

    }
}
