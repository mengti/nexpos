﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexvis.NexPOS.Helper
{
    public class SaleProductDomain
    {
        public int ProductTypeID { get; set; }
        public string ProductType { get; set; }
        public int NumberOfInvoice { get; set; }
        public string InvoiceID { get; set; }
        public int InvoiceDetailID { get; set; }
        public int LocationID { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string LocationAddress { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerNameEn { get; set; }
        public string CustomerNameKh { get; set; }
        public int EmployeeNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeNameEn { get; set; }
        public string EmployeeNameKh { get; set; }
        public DateTime InvoiceDate { get; set; }
        public Nullable<DateTime> VoidDate { get; set; }
        public byte[] CustomerSignature { get; set; }
        public byte[] EmployeeSignature { get; set; }
        public int StatusID { get; set; }
        public string Status { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryDescription { get; set; }
        public string ItemID { get; set; }
        public string ItemCode { get; set; }
        public string Barcode { get; set; }
        public string Altermate { get; set; }
        public string UOM { get; set; }
        public string ItemDescriptionEn { get; set; }
        public string ItemDescriptionKh { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountPrice { get; set; }  //mengti add
        public decimal Amount { get; set; }
        public decimal Currency { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal VATAmount { get; set; }
        public decimal CashInUSD { get; set; }
        public decimal CashInRiel { get; set; }
        public decimal ChangeUSD { get; set; }
        public bool Reverse { get; set; } //mengti
        //public decimal Change { get; set; } // mengti add
        public decimal ChangeRiel { get; set; }
        public Decimal ExchangeRate { get; set; }
        public string UserCrea { get; set; }
        public DateTime DateCrea { get; set; }
        public string UserUpdt { get; set; }
        public DateTime DateUpdt { get; set; }
        public Decimal DisAmount { get; set; }
        public Decimal DisPercent { get; set; }
        public Decimal TotalPayment { get; set; }
        public bool IsCheckKHR { get; set; }

    }
}
