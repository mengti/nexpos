﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexvis.NexPOS.Helper
{
    public class CSRolesModel
    {
        static List<string> _list;

        static CSRolesModel()
        {
            //
            // Allocate the list.
            //
            _list = new List<string>();
        }
        public static int CompanyID { get; set; }
        public static string RoleID { get; set; }
        public static string RoleName { get; set; }
    }
}
