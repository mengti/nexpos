﻿
using Nexvis.NexPOS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ZooManagement
{

    public partial class CFNumberRank
    {
        private ZooEntity DB = new ZooEntity();
        public string POType { get; set; }
        public BSNumberRank Number { get; set; }
        public BSNumberRankItemN NumberItem { get; set; }
     
        public string Indentification { get; set; }
        public string NextNumberRank { get; set; }
        public int NextNumber { get; set; }

        public string CompanyCode { get; set; }       

        public string Separated { get; set; }

       

        public CFNumberRank(string DocType, DocConfType Doc, bool IsRunNext = false)
        {
            if (Doc == DocConfType.Normal)
            {
                       Number = DB.BSNumberRanks.Find(DocType);
                    NumberItem = DB.BSNumberRankItemNs.FirstOrDefault(w=>w.NbrObject== DocType);
            }
           

            if (IsRunNext == true)
            {
                if (NumberItem != null)
                {
                    if (NumberItem.Status == 0)
                    {
                        NumberItem.Status = NumberItem.FromNumber;
                    }
                    else
                    {
                        NumberItem.Status = NumberItem.Status + 1;
                    }
                    DB.Entry(NumberItem).State = System.Data.Entity.EntityState.Modified;
                    int row = DB.SaveChanges();
                    if (row == 1)
                    {
                        this.Separated = NumberItem.Separated;
                        this.NextNumber = NumberItem.Status;
                        NextNumberRank = this.getLenOfPrefix(Number.Length, NumberItem.Status);
                        NextNumberRank = NumberItem.Prefix + NumberItem.Separated + NextNumberRank;
                    }
                }
            }
        }

     
        public string getLenOfPrefix(int len, int currentNumber)
        {
            string r = "";
            for (int i = currentNumber.ToString().Length; i < len; i++)
            {
                r = r + "0";
            }
            r = r + currentNumber.ToString();
            return r;
        }        
    }
    public enum DocConfType
    {
        Normal
    }

}