﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooManagement.Helper
{
    public class CSShopModel
    {

        static List<string> _list;
        public static int ZooID { get; set; }
        public static string ZooName { get; set; }
        public static string ZooAddress { get; set; }
        public static string ZooPhone { get; set; }
        public static byte[] ShopLogo { get; set; }
        //    public static byte[] logo { get; set; }
        static CSShopModel()
        {
            //
            // Allocate the list.
            //
            _list = new List<string>();
        }
        public int CompanyID { get; set; }
        public int ZooInfoID { get; set; }
        public string ZooInfoName { get; set; }
        public Nullable<System.DateTime> RegisterDate { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public Nullable<System.DateTime> CloseTime { get; set; }
        public string SpokenLanguage { get; set; }
        public string OpenDay { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public string Atmosphere { get; set; }
        public string ShortAddress { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string Information { get; set; }
        public byte[] UrlImageLogo { get; set; }
        public byte[] UrlImageTimeLine { get; set; }
    }
}
