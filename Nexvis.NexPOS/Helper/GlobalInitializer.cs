﻿using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nexvis.NexPOS.Helper
{
    public static class GlobalInitializer
    {
        #region --- Variable Declaration ---

        private static string defaultFontName = "Tahoma";
        private static float defaultFontSize = 9.75f;
        private static FontStyle defaultFontStyle = FontStyle.Regular;

        private static DXErrorProvider errorProvider = new DXErrorProvider();
        private static ToolTipController toolTipController = new ToolTipController();

        public static string FORMAT_DATE_STR = "dd-MMM-yyyy hh:mm:ss tt";
        public static string FORMAT_TIME_STR = "hh:mm:ss tt";

        #endregion

        #region --- Message Confirmation ---

        public enum MessageType
        {
            Save,
            Update,
            Delete
        };

        public static DialogResult GfCheckConfirmation(MessageType aMessageType)
        {
            return XtraMessageBox.Show(string.Format("Are you sure you want to {0} this data?", aMessageType.ToString().ToLower()),
                   string.Format("{0} Data", aMessageType),
                   MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public static void GsDisplayMessageResult(DialogResult aResult, MessageType aMessageType)
        {
            if (aResult == DialogResult.Yes || aResult == DialogResult.OK)
            {
                XtraMessageBox.Show(string.Format("Data was {0} successful!", aMessageType.ToString().ToLower()),
                   string.Format("{0} Data", aMessageType),
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                XtraMessageBox.Show(string.Format("There is a problem when {0} data!", aMessageType.ToString().ToLower()),
                       string.Format("{0} Data", aMessageType),
                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        #region --- Global Sub ---

        public static void GsFillLookUpEdit<T>(List<T> objDataView, string objValueMember, string objDisplayMember, LookUpEdit objLookUpEdit)
        {
            objLookUpEdit.Properties.DataSource = objDataView;
            objLookUpEdit.Properties.ValueMember = objValueMember;
            objLookUpEdit.Properties.DisplayMember = objDisplayMember;
        }    
        public static void GsFillComboboxEdit<T>(List<T> objDataView, string objValueMember, string objDisplayMember, CheckedComboBoxEdit objLookUpEdit)
        {
            objLookUpEdit.Properties.DataSource = objDataView;
            objLookUpEdit.Properties.ValueMember = objValueMember;
            objLookUpEdit.Properties.DisplayMember = objDisplayMember;
        }

        // TODO: CREATED BY MR. RON VANDA [2017-04-26 11:15 AM]-To dynamic assign properties to fields
        public static void GsFillLookUpEditTwoColumnsWithNoCaption<T>(List<T> objDataView, string objValueMember, string objDisplayMember, LookUpEdit objLookUpEdit)
        {
            objLookUpEdit.Properties.DataSource = objDataView;
            objLookUpEdit.Properties.Columns[0].FieldName = objValueMember;
            objLookUpEdit.Properties.Columns[1].FieldName = objDisplayMember;
            objLookUpEdit.Properties.ValueMember = objValueMember;
            objLookUpEdit.Properties.DisplayMember = objDisplayMember;
        }
        // TODO: CREATED BY MR. RON VANDA [2017-04-20 10:51 AM]-To allow filter with contain characters
        public static void GsFillSearchLookUpEdit<T>(List<T> objDataView, string objValueMember, string objDisplayMember, SearchLookUpEdit objSearchLookUpEdit)
        {
            objSearchLookUpEdit.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            objSearchLookUpEdit.Properties.DataSource = objDataView;
            objSearchLookUpEdit.Properties.View.Columns[0].FieldName = objValueMember;
            objSearchLookUpEdit.Properties.View.Columns[1].FieldName = objDisplayMember;
            objSearchLookUpEdit.Properties.ValueMember = objValueMember;
            objSearchLookUpEdit.Properties.DisplayMember = objDisplayMember;
        }

        public static void GsExportData(GridControl objGridControl)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Excel 2010|*.xlsx|Excel 2003|*.xls";
            saveDialog.Title = "Save Excel File";
            if (saveDialog.ShowDialog() != DialogResult.Cancel)
            {
                string filePath = saveDialog.FileName;
                string fileExtenstion = new FileInfo(filePath).Extension;

                switch (fileExtenstion)
                {
                    case ".xls":
                        objGridControl.ExportToXls(filePath);
                        break;
                    case ".xlsx":
                        objGridControl.ExportToXlsx(filePath);
                        break;
                    default:
                        break;
                }
                if (File.Exists(filePath))
                {
                    try
                    {
                        // TODO: Try to open the file and let windows decide how to open it.
                        System.Diagnostics.Process.Start(filePath);
                    }
                    catch
                    {
                        String msg = "The file could not be opened." + Environment.NewLine + Environment.NewLine + "Path: " + filePath;
                        MessageBox.Show(msg, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    String msg = "The file could not be saved." + Environment.NewLine + Environment.NewLine + "Path: " + filePath;
                    MessageBox.Show(msg, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public static void GsSetColumnFilterDisplay(params GridView[] objGridView)
        {
            for (int i = 0; i <= objGridView.Length - 1; i++)
            {
                foreach (GridColumn Col in objGridView[i].Columns)
                {
                    Col.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                    Col.OptionsFilter.AutoFilterCondition = AutoFilterCondition.Contains;
                }
            }
        }

        public static void GsGridViewReadOnly(params GridView[] objGridView)
        {
            for (int i = 0; i <= objGridView.Length - 1; i++)
            {
                objGridView[i].OptionsBehavior.Editable = false;
            }
        }
         
        public static void GsGridViewAppearance(bool isReadOnly, params GridView[] objGridView)
        {
            bool editable = (isReadOnly == true) ? false : true;
            for (int i = 0; i <= objGridView.Length - 1; i++)
            {
                objGridView[i].RowHeight = 30;
                objGridView[i].ColumnPanelRowHeight = 30;
                objGridView[i].IndicatorWidth = 35;

                objGridView[i].OptionsBehavior.Editable = editable;
                objGridView[i].OptionsBehavior.AllowAddRows = DefaultBoolean.False;
                objGridView[i].OptionsBehavior.AllowDeleteRows = DefaultBoolean.False;

                objGridView[i].OptionsSelection.EnableAppearanceFocusedCell = false;
                objGridView[i].OptionsView.EnableAppearanceEvenRow = true;

                objGridView[i].Appearance.Row.Font = new Font(defaultFontName, defaultFontSize, defaultFontStyle);
                objGridView[i].Appearance.GroupPanel.Font = new Font(defaultFontName, defaultFontSize, defaultFontStyle);
                objGridView[i].Appearance.GroupRow.Font = new Font(defaultFontName, defaultFontSize, defaultFontStyle);
                objGridView[i].Appearance.HeaderPanel.Font = new Font(defaultFontName, defaultFontSize, defaultFontStyle);
            }
        }
        
        public static void GsDisplayAutoRowNumber(GridView objGridView)
        {
           // objGridView.CustomDrawRowIndicator += GlobalEvent.objGridView_CustomDrawRowIndicator;
        }

        public static void GsFormatDate([Optional] string formatDisplay, params DateEdit[] objDateEdit)
        {
            formatDisplay = formatDisplay ?? "dd-MM-yyyy";
            for (int i = 0; i <= objDateEdit.Length - 1; i++)
            {
                objDateEdit[i].EditValue = DateTime.Now;
                objDateEdit[i].Properties.DisplayFormat.FormatString = formatDisplay;
                objDateEdit[i].Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                objDateEdit[i].Properties.EditFormat.FormatString = formatDisplay;
                objDateEdit[i].Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                objDateEdit[i].Properties.Mask.EditMask = formatDisplay;
                objDateEdit[i].Properties.ShowOk = DefaultBoolean.True;
                objDateEdit[i].Properties.AllowDropDownWhenReadOnly = DefaultBoolean.False;
            }
        }

        public static void GsGridFormatDate([Optional] string formatDisplay, params DevExpress.XtraGrid.Columns.GridColumn[] objGridColumn)
        {
            formatDisplay = formatDisplay ?? "dd-MM-yyyy";
            for (int i = 0; i <= objGridColumn.Length - 1; i++)
            {
                objGridColumn[i].DisplayFormat.FormatType = FormatType.DateTime;
                objGridColumn[i].DisplayFormat.FormatString = formatDisplay;

                RepositoryItemDateEdit repoDate = new RepositoryItemDateEdit() { NullDate = DateTime.MinValue, NullText = String.Empty };
                objGridColumn[i].ColumnEdit = repoDate;
                objGridColumn[i].ColumnEdit.NullText = String.Empty;
            }
        }

        public static void GsClearValue(params TextEdit[] objComponent)
        {
            for (int i = 0; i <= objComponent.Length - 1; i++)
            {
                objComponent[i].Tag = null;
                objComponent[i].EditValue = null;
            }
        }
          
        public static void GsSetNumberOnly(object objTextEdit, KeyPressEventArgs e)
        {
            // TODO: Only allow one decimal point.
            //char ch = e.KeyChar;
            //decimal x;
            //if (ch == (char)Keys.Back) e.Handled = false;
            //else if (!char.IsDigit(ch) && ch != '.' || !Decimal.TryParse(txtAmount.Text + ch, out x)) e.Handled = true;

            // TODO: Do nothing when out of condition.
            if (!char.IsControl(e.KeyChar) && (!char.IsDigit(e.KeyChar)) && (e.KeyChar != '.') && (e.KeyChar != '-'))
                e.Handled = true;

            // TODO: Only allow one decimal point.
            if (e.KeyChar == '.' && (objTextEdit as TextEdit).Text.IndexOf('.') > -1)
                e.Handled = true;

            // TODO: Only allow minus sign at the beginning.
            if (e.KeyChar == '-' && (objTextEdit as TextEdit).Text.Length > 0)
                e.Handled = true;
        }

        #endregion

        #region --- Global Function ---

        public static bool GfCheckNullValue(params TextEdit[] objComponent)
        {
            bool hasNullValue = false;
            for (int i = 0; i <= objComponent.Length - 1; i++)
            {
                toolTipController.ToolTipType = ToolTipType.Default;
                toolTipController.Appearance.Font = new Font(defaultFontName, defaultFontSize, defaultFontStyle);
                toolTipController.AddClientControl(objComponent[i]);

                if (objComponent[i].EditValue == null || objComponent[i].Text.Length <= 0)
                {
                    hasNullValue = true;
                    errorProvider.SetError(objComponent[i], "This field is required!");
                }
                else
                {
                    errorProvider.SetError(objComponent[i], String.Empty);
                }
            }
            if (hasNullValue == false) errorProvider.ClearErrors();
            return hasNullValue;
            
        }

        public static byte[] GfConvertImageToByteArray(System.Drawing.Image objImage)
        {
            MemoryStream ms = new MemoryStream();
            objImage.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray(); 
        }
          
        public static Image GfConvertByteArrayToImage(byte[] objByteArray)
        {
            MemoryStream ms = new MemoryStream(objByteArray);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public static void GsGridFormatTimeSpan(bool showSecound, params DevExpress.XtraGrid.Columns.GridColumn[] objGridColumn)
        {
            string formatDisplay = (showSecound == true) ? "hh:mm:ss tt" : "hh:mm tt";
            for (int i = 0; i <= objGridColumn.Length - 1; i++)
            {
                RepositoryItemTimeSpanEdit repoTime = new RepositoryItemTimeSpanEdit() { NullText = String.Empty };
                //   repoTime.AllowEditMilliseconds = true;
                repoTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                repoTime.Mask.EditMask = formatDisplay;
                repoTime.Mask.UseMaskAsDisplayFormat = true;
                objGridColumn[i].ColumnEdit = repoTime;
                objGridColumn[i].ColumnEdit.NullText = String.Empty;
            }
        }
       
        public static Image GfConvertImageTo32x32(Image objImage)
        {
            // CREATED BY : Mr. You Lim
            // CREATED DATE : 2017-04-24 04:55 PM
            // DESCRIPTION : Convert image's size to 32x32

            Image newImage = objImage.GetThumbnailImage(32, 32, null, new IntPtr());
            return newImage;
        }

        public static void GsFillLookUpEdit<T>(List<T> objDataView, string objValueMember, string objDisplayMember, LookUpEdit objLookUpEdit, int dropdownrow)
        {
            // CREATED BY   : Mr. THOU LONGDY.
            // CREATED DATE : 2017-04-25 10:38 AM.
            // DESCRIPTION  : Add more property with dropdownrows.
            objLookUpEdit.Properties.DropDownRows = dropdownrow;
            objLookUpEdit.Properties.DataSource = objDataView;
            objLookUpEdit.Properties.ValueMember = objValueMember;
            objLookUpEdit.Properties.DisplayMember = objDisplayMember;
        }   
        public static void GsFillComboboxEdit<T>(List<T> objDataView, string objValueMember, string objDisplayMember, CheckedComboBoxEdit objLookUpEdit, int dropdownrow)
        {
            // CREATED BY   : Mr. THOU LONGDY.
            // CREATED DATE : 2017-04-25 10:38 AM.
            // DESCRIPTION  : Add more property with dropdownrows.
            objLookUpEdit.Properties.DropDownRows = dropdownrow;
            objLookUpEdit.Properties.DataSource = objDataView;
            objLookUpEdit.Properties.ValueMember = objValueMember;
            objLookUpEdit.Properties.DisplayMember = objDisplayMember;
        }




        public static void clearPic(PictureEdit pictureEdit)
        {
            if (pictureEdit == null)
            {
                pictureEdit.Image = null;
                pictureEdit.Invalidate();
            }
        }

        #endregion
    }
}
