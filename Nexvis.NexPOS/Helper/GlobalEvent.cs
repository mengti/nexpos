﻿using DevExpress.XtraBars.Navigation;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nexvis.NexPOS.Helper
{
    public class GlobalEvent
    {
        public static void objNavPanel_MouseMove(object sender, MouseEventArgs e)
        {
            TileNavPane parent = sender as TileNavPane;
            //TileNavPaneHitInfo childInfo = parent.CalcHitInfo(PointToClient(MousePosition));
            TileNavPaneHitInfo childInfo = parent.CalcHitInfo(new Point(e.X, e.Y));

            if (childInfo.ButtonInfo == null)
            {
                parent.Cursor = Cursors.Default;
            }
            else
            {
                parent.Cursor = Cursors.Hand;
                var buttonCaption = childInfo.ButtonInfo.Element.Caption;
                var buttonName = childInfo.ButtonInfo.Element.Name;
            }
        }

        public static void objGridView_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            // TODO: Add row number of each row in GridView.
            e.Info.DisplayText = "1";
            e.Info.ImageIndex = -1;
            int rowIndex = e.RowHandle;
            if (e.Info.IsRowIndicator && rowIndex >= 0)
            {
                rowIndex++;
                e.Info.DisplayText = rowIndex.ToString();
            }
            else
            {
                // TODO: Add text to filter row of GridView.
                e.Info.DisplayText = "";
            }

            // TODO: Add column header text to column indicator.
            if (e.Info.Kind == DevExpress.Utils.Drawing.IndicatorKind.Header)
            {
                e.Info.DisplayText = "Nº";
            }
        }

    }
}
