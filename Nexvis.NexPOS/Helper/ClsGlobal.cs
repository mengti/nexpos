﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexvis.NexPOS.Helper
{
    class ClsGlobal
    {

        public static string GetCurrentFileLocation()
        {

            return System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
        }
        public decimal RoundRielAmountDown(decimal RielAmount)
        {
            decimal RielValue = 0;
            if (RielAmount >= 100)
                RielValue = Convert.ToDecimal(String.Concat(RielAmount.ToString().Substring(0, RielAmount.ToString().Length - 2), "00"));
            else
                RielValue = 0;
            return RielValue;
        }
        public decimal RoundRielAmountUp(decimal RielAmount)
        {
            decimal RielValue1 = 0;
            decimal RielValue2 = 0;
            if (RielAmount >= 100)
            {
                RielValue1 = Convert.ToDecimal(RielAmount.ToString().Substring(0, RielAmount.ToString().Length - 2));
                RielValue2 = Convert.ToDecimal(RielAmount.ToString().Substring(RielAmount.ToString().Length - 2, 2));
                if (RielValue2 > 0)
                    RielValue1 = Convert.ToDecimal(String.Concat((RielValue1 + 1), "00"));
                else
                    RielValue1 = Convert.ToDecimal(String.Concat(RielValue1, "00"));
            }
            else
                RielValue1 = 100;

            return RielValue1;
        }
    }
}
