﻿namespace Nexvis.NexPOS.LoadForm.Counter
{
    partial class CloseCashierBalance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navCloseBalance = new DevExpress.XtraBars.Navigation.NavButton();
            this.navCancel = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtCategoryID = new DevExpress.XtraEditors.TextEdit();
            this.txtSaleByBank = new DevExpress.XtraEditors.TextEdit();
            this.txtVat = new DevExpress.XtraEditors.TextEdit();
            this.txtGrandTotal = new DevExpress.XtraEditors.TextEdit();
            this.txtDiscount = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalAmount = new DevExpress.XtraEditors.TextEdit();
            this.txtOpeningAmount = new DevExpress.XtraEditors.TextEdit();
            this.txtSaleAmount = new DevExpress.XtraEditors.TextEdit();
            this.txtUserName = new DevExpress.XtraEditors.TextEdit();
            this.txtShiffNo = new DevExpress.XtraEditors.TextEdit();
            this.txtSaleByCash = new DevExpress.XtraEditors.TextEdit();
            this.dtStartDate = new DevExpress.XtraEditors.DateEdit();
            this.dtEndDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleByBank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpeningAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShiffNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleByCash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navCloseBalance);
            this.navHeader.Buttons.Add(this.navCancel);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(703, 40);
            this.navHeader.TabIndex = 5;
            this.navHeader.Text = " ";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Close Cashier Balance";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navCloseBalance
            // 
            this.navCloseBalance.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navCloseBalance.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navCloseBalance.AppearanceHovered.Options.UseForeColor = true;
            this.navCloseBalance.Caption = "Close Balance";
            this.navCloseBalance.Name = "navCloseBalance";
            this.navCloseBalance.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navCloseBalance_ElementClick);
            // 
            // navCancel
            // 
            this.navCancel.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navCancel.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navCancel.AppearanceHovered.Options.UseForeColor = true;
            this.navCancel.Caption = "Cancel";
            this.navCancel.Name = "navCancel";
            this.navCancel.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navCancel_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtCategoryID);
            this.layoutControl1.Controls.Add(this.txtSaleByBank);
            this.layoutControl1.Controls.Add(this.txtVat);
            this.layoutControl1.Controls.Add(this.txtGrandTotal);
            this.layoutControl1.Controls.Add(this.txtDiscount);
            this.layoutControl1.Controls.Add(this.txtTotalAmount);
            this.layoutControl1.Controls.Add(this.txtOpeningAmount);
            this.layoutControl1.Controls.Add(this.txtSaleAmount);
            this.layoutControl1.Controls.Add(this.txtUserName);
            this.layoutControl1.Controls.Add(this.txtShiffNo);
            this.layoutControl1.Controls.Add(this.txtSaleByCash);
            this.layoutControl1.Controls.Add(this.dtStartDate);
            this.layoutControl1.Controls.Add(this.dtEndDate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(703, 492);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtCategoryID
            // 
            this.txtCategoryID.Enabled = false;
            this.txtCategoryID.Location = new System.Drawing.Point(45, 62);
            this.txtCategoryID.Name = "txtCategoryID";
            this.txtCategoryID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.txtCategoryID.Properties.Appearance.Options.UseFont = true;
            this.txtCategoryID.Size = new System.Drawing.Size(340, 24);
            this.txtCategoryID.StyleController = this.layoutControl1;
            this.txtCategoryID.TabIndex = 4;
            // 
            // txtSaleByBank
            // 
            this.txtSaleByBank.Enabled = false;
            this.txtSaleByBank.Location = new System.Drawing.Point(272, 410);
            this.txtSaleByBank.Name = "txtSaleByBank";
            this.txtSaleByBank.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtSaleByBank.Properties.Appearance.Options.UseFont = true;
            this.txtSaleByBank.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSaleByBank.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSaleByBank.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtSaleByBank.Properties.AppearanceDisabled.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.ControlText;
            this.txtSaleByBank.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtSaleByBank.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtSaleByBank.Properties.DisplayFormat.FormatString = "N2";
            this.txtSaleByBank.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSaleByBank.Properties.Mask.EditMask = "N2";
            this.txtSaleByBank.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSaleByBank.Size = new System.Drawing.Size(371, 26);
            this.txtSaleByBank.StyleController = this.layoutControl1;
            this.txtSaleByBank.TabIndex = 5;
            // 
            // txtVat
            // 
            this.txtVat.EditValue = "0";
            this.txtVat.Enabled = false;
            this.txtVat.Location = new System.Drawing.Point(272, 330);
            this.txtVat.Name = "txtVat";
            this.txtVat.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtVat.Properties.Appearance.Options.UseFont = true;
            this.txtVat.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVat.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVat.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtVat.Properties.AppearanceDisabled.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.ControlText;
            this.txtVat.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtVat.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtVat.Properties.Mask.EditMask = "P";
            this.txtVat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtVat.Properties.NullText = "0";
            this.txtVat.Size = new System.Drawing.Size(371, 26);
            this.txtVat.StyleController = this.layoutControl1;
            this.txtVat.TabIndex = 6;
            this.txtVat.EditValueChanged += new System.EventHandler(this.txtVat_EditValueChanged);
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.Enabled = false;
            this.txtGrandTotal.Location = new System.Drawing.Point(272, 370);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtGrandTotal.Properties.Appearance.ForeColor = System.Drawing.Color.LimeGreen;
            this.txtGrandTotal.Properties.Appearance.Options.UseFont = true;
            this.txtGrandTotal.Properties.Appearance.Options.UseForeColor = true;
            this.txtGrandTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.txtGrandTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtGrandTotal.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtGrandTotal.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.LimeGreen;
            this.txtGrandTotal.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtGrandTotal.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtGrandTotal.Properties.DisplayFormat.FormatString = "N2";
            this.txtGrandTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGrandTotal.Properties.Mask.EditMask = "N2";
            this.txtGrandTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGrandTotal.Size = new System.Drawing.Size(371, 26);
            this.txtGrandTotal.StyleController = this.layoutControl1;
            this.txtGrandTotal.TabIndex = 7;
            this.txtGrandTotal.EditValueChanged += new System.EventHandler(this.txtGrandTotal_EditValueChanged);
            // 
            // txtDiscount
            // 
            this.txtDiscount.EditValue = "0";
            this.txtDiscount.Enabled = false;
            this.txtDiscount.Location = new System.Drawing.Point(272, 290);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtDiscount.Properties.Appearance.Options.UseFont = true;
            this.txtDiscount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDiscount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDiscount.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtDiscount.Properties.AppearanceDisabled.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.ControlText;
            this.txtDiscount.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtDiscount.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtDiscount.Properties.DisplayFormat.FormatString = "n2";
            this.txtDiscount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDiscount.Properties.Mask.EditMask = "N2";
            this.txtDiscount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDiscount.Properties.NullText = "0";
            this.txtDiscount.Size = new System.Drawing.Size(371, 26);
            this.txtDiscount.StyleController = this.layoutControl1;
            this.txtDiscount.TabIndex = 8;
            this.txtDiscount.EditValueChanged += new System.EventHandler(this.txtDiscount_EditValueChanged);
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Enabled = false;
            this.txtTotalAmount.Location = new System.Drawing.Point(272, 250);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtTotalAmount.Properties.Appearance.Options.UseFont = true;
            this.txtTotalAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTotalAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTotalAmount.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtTotalAmount.Properties.AppearanceDisabled.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.ControlText;
            this.txtTotalAmount.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtTotalAmount.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtTotalAmount.Properties.DisplayFormat.FormatString = "N2";
            this.txtTotalAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtTotalAmount.Properties.Mask.EditMask = "N2";
            this.txtTotalAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTotalAmount.Size = new System.Drawing.Size(371, 26);
            this.txtTotalAmount.StyleController = this.layoutControl1;
            this.txtTotalAmount.TabIndex = 9;
            // 
            // txtOpeningAmount
            // 
            this.txtOpeningAmount.Enabled = false;
            this.txtOpeningAmount.Location = new System.Drawing.Point(272, 170);
            this.txtOpeningAmount.Name = "txtOpeningAmount";
            this.txtOpeningAmount.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtOpeningAmount.Properties.Appearance.Options.UseFont = true;
            this.txtOpeningAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtOpeningAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtOpeningAmount.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtOpeningAmount.Properties.AppearanceDisabled.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.ControlText;
            this.txtOpeningAmount.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtOpeningAmount.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtOpeningAmount.Properties.DisplayFormat.FormatString = "N2";
            this.txtOpeningAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtOpeningAmount.Properties.Mask.EditMask = "n2";
            this.txtOpeningAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtOpeningAmount.Size = new System.Drawing.Size(371, 26);
            this.txtOpeningAmount.StyleController = this.layoutControl1;
            this.txtOpeningAmount.TabIndex = 10;
            // 
            // txtSaleAmount
            // 
            this.txtSaleAmount.EditValue = "0";
            this.txtSaleAmount.Enabled = false;
            this.txtSaleAmount.Location = new System.Drawing.Point(272, 210);
            this.txtSaleAmount.Name = "txtSaleAmount";
            this.txtSaleAmount.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtSaleAmount.Properties.Appearance.Options.UseFont = true;
            this.txtSaleAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSaleAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSaleAmount.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtSaleAmount.Properties.AppearanceDisabled.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.ControlText;
            this.txtSaleAmount.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtSaleAmount.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtSaleAmount.Properties.DisplayFormat.FormatString = "N2";
            this.txtSaleAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSaleAmount.Properties.Mask.EditMask = "n2";
            this.txtSaleAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSaleAmount.Size = new System.Drawing.Size(371, 26);
            this.txtSaleAmount.StyleController = this.layoutControl1;
            this.txtSaleAmount.TabIndex = 11;
            this.txtSaleAmount.EditValueChanged += new System.EventHandler(this.txtSaleAmount_EditValueChanged);
            // 
            // txtUserName
            // 
            this.txtUserName.Enabled = false;
            this.txtUserName.Location = new System.Drawing.Point(272, 132);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtUserName.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtUserName.Properties.Appearance.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.ControlText;
            this.txtUserName.Properties.Appearance.Options.UseBackColor = true;
            this.txtUserName.Properties.Appearance.Options.UseFont = true;
            this.txtUserName.Properties.Appearance.Options.UseForeColor = true;
            this.txtUserName.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtUserName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtUserName.Properties.AppearanceDisabled.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.ControlText;
            this.txtUserName.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtUserName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtUserName.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtUserName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtUserName.Size = new System.Drawing.Size(371, 24);
            this.txtUserName.StyleController = this.layoutControl1;
            this.txtUserName.TabIndex = 12;
            // 
            // txtShiffNo
            // 
            this.txtShiffNo.Enabled = false;
            this.txtShiffNo.Location = new System.Drawing.Point(272, 12);
            this.txtShiffNo.Name = "txtShiffNo";
            this.txtShiffNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtShiffNo.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtShiffNo.Properties.Appearance.Options.UseBackColor = true;
            this.txtShiffNo.Properties.Appearance.Options.UseFont = true;
            this.txtShiffNo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtShiffNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtShiffNo.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtShiffNo.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtShiffNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtShiffNo.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtShiffNo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtShiffNo.Size = new System.Drawing.Size(371, 24);
            this.txtShiffNo.StyleController = this.layoutControl1;
            this.txtShiffNo.TabIndex = 14;
            // 
            // txtSaleByCash
            // 
            this.txtSaleByCash.Enabled = false;
            this.txtSaleByCash.Location = new System.Drawing.Point(272, 454);
            this.txtSaleByCash.Name = "txtSaleByCash";
            this.txtSaleByCash.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtSaleByCash.Properties.Appearance.Options.UseFont = true;
            this.txtSaleByCash.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSaleByCash.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSaleByCash.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.txtSaleByCash.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.txtSaleByCash.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.txtSaleByCash.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.txtSaleByCash.Properties.DisplayFormat.FormatString = "n2";
            this.txtSaleByCash.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSaleByCash.Properties.Mask.EditMask = "n2";
            this.txtSaleByCash.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSaleByCash.Size = new System.Drawing.Size(371, 26);
            this.txtSaleByCash.StyleController = this.layoutControl1;
            this.txtSaleByCash.TabIndex = 16;
            // 
            // dtStartDate
            // 
            this.dtStartDate.EditValue = null;
            this.dtStartDate.Enabled = false;
            this.dtStartDate.Location = new System.Drawing.Point(272, 50);
            this.dtStartDate.Name = "dtStartDate";
            this.dtStartDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.dtStartDate.Properties.Appearance.Options.UseFont = true;
            this.dtStartDate.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.dtStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtStartDate.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dtStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtStartDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dtStartDate.Properties.DisplayFormat.FormatString = "";
            this.dtStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtStartDate.Properties.EditFormat.FormatString = "";
            this.dtStartDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtStartDate.Properties.Mask.EditMask = "g";
            this.dtStartDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtStartDate.Size = new System.Drawing.Size(371, 28);
            this.dtStartDate.StyleController = this.layoutControl1;
            this.dtStartDate.TabIndex = 15;
            // 
            // dtEndDate
            // 
            this.dtEndDate.EditValue = null;
            this.dtEndDate.Enabled = false;
            this.dtEndDate.Location = new System.Drawing.Point(272, 92);
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.dtEndDate.Properties.Appearance.Options.UseFont = true;
            this.dtEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtEndDate.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dtEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtEndDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dtEndDate.Properties.DisplayFormat.FormatString = "";
            this.dtEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtEndDate.Properties.EditFormat.FormatString = "";
            this.dtEndDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtEndDate.Properties.Mask.EditMask = "g";
            this.dtEndDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtEndDate.Size = new System.Drawing.Size(371, 26);
            this.dtEndDate.StyleController = this.layoutControl1;
            this.dtEndDate.TabIndex = 13;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtCategoryID;
            this.layoutControlItem1.Location = new System.Drawing.Point(33, 29);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(344, 49);
            this.layoutControlItem1.Text = "CategoryID";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(98, 18);
            // 
            // Root
            // 
            this.Root.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.Root.AppearanceGroup.Options.UseBackColor = true;
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem7,
            this.emptySpaceItem8,
            this.emptySpaceItem9,
            this.emptySpaceItem10,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.emptySpaceItem15});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(703, 492);
            this.Root.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(635, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(48, 472);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(41, 472);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.txtSaleByBank;
            this.layoutControlItem2.Location = new System.Drawing.Point(41, 398);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(594, 30);
            this.layoutControlItem2.Text = "Sale Amount Receive By Bank";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.txtVat;
            this.layoutControlItem3.Location = new System.Drawing.Point(41, 318);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(594, 30);
            this.layoutControlItem3.Text = "VAT ($)";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.Color.LimeGreen;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.Control = this.txtGrandTotal;
            this.layoutControlItem4.Location = new System.Drawing.Point(41, 358);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(594, 30);
            this.layoutControlItem4.Text = "Grand Total ($)";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtDiscount;
            this.layoutControlItem5.Location = new System.Drawing.Point(41, 278);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(594, 30);
            this.layoutControlItem5.Text = "Discount ($)";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.txtTotalAmount;
            this.layoutControlItem6.Location = new System.Drawing.Point(41, 238);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(594, 30);
            this.layoutControlItem6.Text = "Total Amount ($)";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.txtOpeningAmount;
            this.layoutControlItem7.Location = new System.Drawing.Point(41, 158);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(594, 30);
            this.layoutControlItem7.Text = "Opening Amount ($)";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.Control = this.txtSaleAmount;
            this.layoutControlItem8.Location = new System.Drawing.Point(41, 198);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(594, 30);
            this.layoutControlItem8.Text = "Sale Amount ($)";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.txtUserName;
            this.layoutControlItem9.Location = new System.Drawing.Point(41, 120);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(594, 28);
            this.layoutControlItem9.Text = "User Name";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.dtEndDate;
            this.layoutControlItem10.Location = new System.Drawing.Point(41, 80);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(594, 30);
            this.layoutControlItem10.Text = "End Date && Time";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.Control = this.txtShiffNo;
            this.layoutControlItem11.Location = new System.Drawing.Point(41, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(594, 28);
            this.layoutControlItem11.Text = "ShiffNo";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.Control = this.dtStartDate;
            this.layoutControlItem12.Location = new System.Drawing.Point(41, 38);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(594, 32);
            this.layoutControlItem12.Text = "Start Date && Time";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(216, 20);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.txtSaleByCash;
            this.layoutControlItem13.Location = new System.Drawing.Point(41, 442);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(594, 30);
            this.layoutControlItem13.Text = "Sale Receive By Cash";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(216, 20);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(41, 28);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(41, 70);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(41, 110);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(41, 148);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(41, 188);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(41, 228);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(41, 268);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(41, 308);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(41, 348);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(41, 388);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(41, 428);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(594, 14);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // CloseCashierBalance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 532);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Name = "CloseCashierBalance";
            this.Text = "Close Cashier";
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleByBank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpeningAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShiffNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaleByCash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navCloseBalance;
        private DevExpress.XtraBars.Navigation.NavButton navCancel;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        public DevExpress.XtraEditors.TextEdit txtCategoryID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        internal DevExpress.XtraEditors.TextEdit txtSaleByBank;
        internal DevExpress.XtraEditors.TextEdit txtVat;
        internal DevExpress.XtraEditors.TextEdit txtGrandTotal;
        internal DevExpress.XtraEditors.TextEdit txtDiscount;
        internal DevExpress.XtraEditors.TextEdit txtTotalAmount;
        internal DevExpress.XtraEditors.TextEdit txtOpeningAmount;
        internal DevExpress.XtraEditors.TextEdit txtSaleAmount;
        internal DevExpress.XtraEditors.TextEdit txtUserName;
        internal DevExpress.XtraEditors.TextEdit txtShiffNo;
        internal DevExpress.XtraEditors.TextEdit txtSaleByCash;
        internal DevExpress.XtraEditors.DateEdit dtStartDate;
        internal DevExpress.XtraEditors.DateEdit dtEndDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
    }
}