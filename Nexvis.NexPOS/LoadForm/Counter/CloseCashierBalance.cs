﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;
using Nexvis.NexPOS.Report;
using DevExpress.XtraReports.UI;

namespace Nexvis.NexPOS.LoadForm.Counter
{
    public partial class CloseCashierBalance : DevExpress.XtraEditors.XtraForm
    {
        #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        private scrCashierBalance parentForm;

        #endregion

        #region --- Form Action ---
        public CloseCashierBalance(scrCashierBalance objParent)
        {
            InitializeComponent();
            parentForm = objParent;

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;
        }

        #endregion

        #region --- Header Action ---

        private void navCloseBalance_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtShiffNo,txtSaleAmount,
                dtEndDate) == false)
            {
                
                parentForm.GsCloseBalanceData(this);

                //if (parentForm.isSuccessful)
                //{
                    

                //}
                   
            }
        }

        private void navCancel_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close();
        }

        #endregion

      
        private void txtCategoryName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }
        void ChangeGrandTotal()
        {
            decimal Discount = Convert.ToDecimal(txtDiscount.Text == "" ? 0 : txtDiscount.EditValue);
            decimal GrandTotal = Convert.ToDecimal(txtGrandTotal.Text == "" ? 0 : txtGrandTotal.EditValue);
            decimal Total = Convert.ToDecimal(txtTotalAmount.Text == "" ? 0 : txtTotalAmount.EditValue);
            decimal Vat = Convert.ToDecimal(txtVat.Text == "" ? 0 : txtVat.EditValue);


           // if (Vat == 0) Vat = null;


           if (Discount != 0 && Vat != 0)
           {
                var vatAmount = Total * (Vat / 100);
                GrandTotal = Total - (vatAmount + Discount);
                txtGrandTotal.EditValue = GrandTotal;
                    //string.Format("{0:N2}", GrandTotal);
            }
            else
            {
                if (Discount != 0)
                {
                    GrandTotal = Total - Discount;
                    txtGrandTotal.EditValue = GrandTotal;
                        //string.Format("{0:N2}", GrandTotal);
                }
                else if (Vat != 0)
                {
                    var vatAmount = Total * (Vat / 100);
                    GrandTotal = Total - vatAmount;
                    txtGrandTotal.EditValue = GrandTotal;
                    // txtGrandTotal.EditValue = string.Format("{0:N2}", GrandTotal);
                }
            }
        }

        private void txtSaleAmount_EditValueChanged(object sender, EventArgs e)
        {
            txtTotalAmount.EditValue = Convert.ToDecimal(txtOpeningAmount.EditValue) + Convert.ToDecimal(txtSaleAmount.EditValue);
            txtGrandTotal.EditValue = txtTotalAmount.EditValue;
                //string.Format("{0:N2}", txtTotalAmount.EditValue);

        }

        private void txtDiscount_EditValueChanged(object sender, EventArgs e)
        {
           ChangeGrandTotal();
        }

        private void txtVat_EditValueChanged(object sender, EventArgs e)
        {
           ChangeGrandTotal();
        }

        private void txtGrandTotal_EditValueChanged(object sender, EventArgs e)
        {
            ChangeGrandTotal();
        }
    }
}