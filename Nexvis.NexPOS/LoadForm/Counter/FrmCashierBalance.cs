﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using ZooMangement;
using Nexvis.NexPOS.Helper;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;
using Nexvis.NexPOS.Report;
using DevExpress.XtraReports.UI;

namespace Nexvis.NexPOS.LoadForm.Counter
{
    public partial class scrCashierBalance : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrCashierBalance()
        {
            InitializeComponent();
      
        }

        List<CashierBalance> _CashierBalance = new List<CashierBalance>();
        List<CSUser> _User = new List<CSUser>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        public bool isSuccessful = true;
        private void FormItemCategory_Load(object sender, EventArgs e)
        {
            _CashierBalance = DB.CashierBalances.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            GC.DataSource = _CashierBalance.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
            _User = DB.CSUsers.ToList();

        }
        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

           
        }
        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }
        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormItemCategory_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }
        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                CashierBalance result = new CashierBalance();
                result = (CashierBalance)GV.GetRow(GV.FocusedRowHandle);

                if (result != null)
                {
                    if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                    {
                        CashierBalance customer = DB.CashierBalances.FirstOrDefault(st => st.CompanyID.Equals(result.CompanyID) && st.ShiffNo.Equals(result.ShiffNo));

                        DB.CashierBalances.Remove(customer);
                        DB.SaveChanges();
                        FormItemCategory_Load(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                //NavButton navigator = (NavButton)sender;
                var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    CreateCashierBalance objForm = new CreateCashierBalance("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    GlobalInitializer.GsFillLookUpEdit(_User.ToList(), "UserID", "UserName", objForm.luCashier, (int)_User.ToList().Count);

                    CashierBalance result = new CashierBalance();

                    result = (CashierBalance)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (result.Status == "Close")
                        {
                            XtraMessageBox.Show("This balance has been closed.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        objForm.txtShiffNo.Text = result.ShiffNo.ToString();
                        objForm.txtShiffNo.Enabled = false;
                      //  objForm.luCashier.Enabled = false;
                        objForm.luCashier.Focus();
                        objForm.txtBeginBalance.EditValue = result.OpeningAmount;
                       
                        objForm.luCashier.Text = result.UserName;
                        objForm.luCashier.EditValue = result.UserID;
                        objForm.txtMemo.Text = result.Memo;
                        objForm.dtDate.DateTime = result.StartDateTime.Value;
                      
                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

             

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Args_Showing(object sender, XtraMessageShowingArgs e)
        {
            //bold message caption
            e.Form.Appearance.FontStyleDelta = FontStyle.Bold;
            //increased button height and font size
            MessageButtonCollection buttons = e.Buttons as MessageButtonCollection;
            SimpleButton btn = buttons[System.Windows.Forms.DialogResult.OK] as SimpleButton;
            if (btn != null)
            {
                btn.Appearance.FontSizeDelta = 5;
                btn.Height += 10;
            }
        }
        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    CreateCashierBalance objForm = new CreateCashierBalance("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    objForm.txtShiffNo.Enabled = false;
                    objForm.dtDate.DateTime = DateTime.Now;

                    //TODO: Binding value to comboBox or LookupEdit.
                    GlobalInitializer.GsFillLookUpEdit(_User.ToList(), "UserID", "UserName", objForm.luCashier, (int)_User.ToList().Count);

                    // objForm._cities = _cities;  
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

              
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void navCloseBalance_ElementClick(object sender, NavElementEventArgs e)
        {
            try
            {
                //NavButton navigator = (NavButton)sender;
                var nameAction = "Close Balance";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    CloseCashierBalance objForm = new CloseCashierBalance(this);
                    FormShadow Shadow = new FormShadow(objForm);

                    CashierBalance result = new CashierBalance();

                    result = (CashierBalance)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (result.Status == "Close")
                        {
                            XtraMessageBox.Show("This balance has been closed.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        var obj = DB.ARInvoices.Where(x => x.CreatedBy == CSUserModel.User && x.CreatedBy == result.UserID && x.InvoiceDate <= DateTime.Today && x.IsCloseBalan == false).ToList();
                        var objPay = DB.ARPaymentDetails.Where(x => x.CreatedBy == CSUserModel.User && x.CreatedBy == result.UserID && x.PaymentDate <= DateTime.Today && x.IsCloseBalan == false).ToList();

                        objForm.txtShiffNo.Text = result.ShiffNo.ToString();
                        objForm.txtShiffNo.Enabled = false;
                        objForm.txtOpeningAmount.EditValue = string.Format("{0:N2}", result.OpeningAmount);
                     //   objForm.txtOpeningAmount.EditValue = string.Format("{0:N0}", result.OpeningAmountKHR);
                        objForm.txtGrandTotal.EditValue = result.GrandTotal;
                        objForm.txtDiscount.EditValue = objPay.Sum(x => x.DiscountAmount);
                        objForm.txtSaleAmount.EditValue = obj.Sum(x => x.TotalAmount);
           
          
                        objForm.txtSaleByBank.EditValue = objPay.Where(x => x.PaymentTypes == "BANK").Sum(x => x.PaymentAmount);
                        objForm.txtSaleByCash.EditValue = objPay.Where(x => x.PaymentTypes == "CASH").Sum(x => x.PaymentAmount);
                        objForm.txtUserName.Text = result.UserName;
                        objForm.dtEndDate.DateTime = DateTime.Now;
                        objForm.dtStartDate.DateTime = (DateTime)result.StartDateTime;

                        //objForm.txt.EditValue = result.Discount;

                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }



            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #region Actions
        public void GsSaveData(CreateCashierBalance objChild)
        {
            objChild.isExiting = false;
            if (_CashierBalance.Any(x => x.UserName.ToString() == objChild.luCashier.Text.Trim().ToLower() && x.Status == "Open"))
            {
                objChild.isExiting = true;
                return;
            }

            CashierBalance itemCategory = new CashierBalance()
            {
                CompanyID = DACClasses.CompanyID,
                //CategoryID = DB.INItemCategories.Max(x => x.CategoryID) + 1,
                UserID = objChild.luCashier.EditValue.ToString(),
                UserName = objChild.luCashier.Text,
                Memo = objChild.txtMemo.Text,
                Status = "Open",
                OpeningAmount = Convert.ToDecimal(objChild.txtBeginBalance.EditValue),
                StartDateTime = objChild.dtDate.DateTime,
                StartDate = objChild.dtDate.DateTime.Date

            };

            DB.CashierBalances.Add(itemCategory);
            var Row = DB.SaveChanges();
            if (Row == 1)
            {
                isSuccessful = true;
            }
            GV.FocusedRowHandle = 0;
            navRefresh_ElementClick(null,null);
            XtraMessageBox.Show("This field has been Saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public void GsUpdateData(CreateCashierBalance objChild)
        {
            try
            {
                if (_CashierBalance.Any(x => x.UserID.ToString() == objChild.luCashier.EditValue.ToString() 
                && x.OpeningAmount == (decimal)objChild.txtBeginBalance.EditValue && x.StartDateTime == objChild.dtDate.DateTime 
                && x.Memo == objChild.txtMemo.Text))
                {
                    XtraMessageBox.Show("Cannot update data because it is doublicate.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    objChild.luCashier.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                var _obj = DB.CashierBalances.SingleOrDefault(w => w.ShiffNo.ToString() == objChild.txtShiffNo.Text && w.CompanyID.Equals(DACClasses.CompanyID));

                _obj.UserID = objChild.luCashier.EditValue.ToString();
                _obj.UserName = objChild.luCashier.Text;
                _obj.Memo = objChild.txtMemo.Text;
                _obj.StartDateTime = objChild.dtDate.DateTime;
                _obj.StartDate = objChild.dtDate.DateTime.Date;
                _obj.OpeningAmount = Convert.ToDecimal(objChild.txtBeginBalance.EditValue);

                DB.CashierBalances.Attach(_obj);
                DB.Entry(_obj).Property(w => w.Memo).IsModified = true;
                DB.Entry(_obj).Property(w => w.UserID).IsModified = true;
                DB.Entry(_obj).Property(w => w.UserName).IsModified = true;
                DB.Entry(_obj).Property(w => w.StartDateTime).IsModified = true;
                DB.Entry(_obj).Property(w => w.StartDate).IsModified = true;
                DB.Entry(_obj).Property(w => w.OpeningAmount).IsModified = true;
             

                var Row = DB.SaveChanges();

                XtraMessageBoxArgs args = new XtraMessageBoxArgs();
                args.Caption = "Message";
                args.Text = "This field has been upadated.";
                args.Buttons = new DialogResult[] { DialogResult.OK };
                args.Showing += Args_Showing;
                XtraMessageBox.Show(args).ToString();
                objChild.Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void GsCloseBalanceData(CloseCashierBalance objChild)
        {
            try
            {
                var _obj = DB.CashierBalances.SingleOrDefault(w => w.ShiffNo.ToString() == objChild.txtShiffNo.Text && w.CompanyID.Equals(DACClasses.CompanyID));

                var obj = DB.ARInvoices.Where(x => x.CreatedBy == CSUserModel.User && x.InvoiceDate <= DateTime.Today && x.IsCloseBalan == false).ToList();
                var objPay = DB.ARPaymentDetails.Where(x => x.CreatedBy == CSUserModel.User && x.PaymentDate <= DateTime.Today && x.IsCloseBalan == false).ToList();
                
                if (obj.Count == 0)
                {
                    XtraMessageBox.Show("This balance cannot close.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                _obj.Status = "Close";
                _obj.StartDateTime = objChild.dtStartDate.DateTime;
                _obj.EndDateTime = objChild.dtEndDate.DateTime;         
                _obj.StartDate = objChild.dtStartDate.DateTime.Date;
                _obj.EndDate = objChild.dtEndDate.DateTime.Date;
                _obj.OpeningAmount = Convert.ToDecimal(objChild.txtOpeningAmount.EditValue);
                _obj.Discount = Convert.ToDecimal(objChild.txtDiscount.EditValue);
                _obj.Vat = Convert.ToDecimal(objChild.txtVat.EditValue);
                _obj.SaleAmount = Convert.ToDecimal(objChild.txtSaleAmount.EditValue);
                _obj.TotalAmount = Convert.ToDecimal(objChild.txtTotalAmount.EditValue);
                _obj.GrandTotal = Convert.ToDecimal(objChild.txtGrandTotal.EditValue);
                _obj.SaleAmountReceiveBank = Convert.ToDecimal(objChild.txtSaleByBank.EditValue);
                _obj.SaleAmountReceiveCash = Convert.ToDecimal(objChild.txtSaleByCash.EditValue);

                DB.CashierBalances.Attach(_obj);
                DB.Entry(_obj).Property(w => w.StartDateTime).IsModified = true;
                DB.Entry(_obj).Property(w => w.EndDateTime).IsModified = true;
                DB.Entry(_obj).Property(w => w.StartDate).IsModified = true;
                DB.Entry(_obj).Property(w => w.EndDate).IsModified = true;
                DB.Entry(_obj).Property(w => w.Status).IsModified = true;
                DB.Entry(_obj).Property(w => w.Discount).IsModified = true;
                DB.Entry(_obj).Property(w => w.OpeningAmount).IsModified = true;
                DB.Entry(_obj).Property(w => w.SaleAmount).IsModified = true;
                DB.Entry(_obj).Property(w => w.TotalAmount).IsModified = true;
                DB.Entry(_obj).Property(w => w.GrandTotal).IsModified = true;
                DB.Entry(_obj).Property(w => w.Vat).IsModified = true;
                DB.Entry(_obj).Property(w => w.SaleAmountReceiveBank).IsModified = true;
                DB.Entry(_obj).Property(w => w.SaleAmountReceiveCash).IsModified = true;

                /*------Update Close Balance to All Table---------*/

               
                obj.ForEach(x => x.IsCloseBalan = true);
                objPay.ForEach(x => x.IsCloseBalan = true);

                foreach (var item in obj)
                {
                    DB.Entry(item).State = (System.Data.Entity.EntityState)EntityState.Modified;
                } 
                foreach (var item in objPay)
                {
                    DB.Entry(item).State = (System.Data.Entity.EntityState)EntityState.Modified;
                }

                var Row = DB.SaveChanges();
                if (Row != 0)
                {
                    isSuccessful = true;

                    rptCashierBalance rpt = new rptCashierBalance();

                    rpt.DataSource = DB.CashierBalances.Where(x => x.ShiffNo.ToString() == objChild.txtShiffNo.Text).ToList();
                    rpt.Parameters["Cashier"].Value = CSUserModel.LoginName;
                    rpt.Parameters["ShiffNumber"].Value = objChild.txtShiffNo.Text;
                    rpt.RequestParameters = false;
                    ReportPrintTool printTool = new ReportPrintTool(rpt);
                    printTool.ShowPreview();
                }

                //XtraMessageBoxArgs args = new XtraMessageBoxArgs();
                //args.Caption = "Message";
                //args.Text = "This field has been closed.";
                //args.Buttons = new DialogResult[] { DialogResult.OK };
                //args.Showing += Args_Showing;
                //XtraMessageBox.Show(args).ToString();
                navRefresh_ElementClick(null,null);
                objChild.Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}