﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using ZooMangement;
using Nexvis.NexPOS.Helper;
using DevExpress.XtraGrid.Views.Grid;

namespace DgoStore.LoadForm.Organizations
{
    public partial class AddCategory : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
         #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        //private FormItemCategory parentForm;

        #endregion

        #region --- Form Action ---
        //public ItemCategorySetup(string transactionType, FormItemCategory objParent)
        //{
        //    InitializeComponent();
        //    parentForm = objParent;


        //    if (transactionType == "NEW")
        //    {
        //        navEdit.Visible = false;
        //        isNew = true;
        //    }
        //    else if (transactionType == "EDIT")
        //    {
        //        navSave.Visible = false;
        //    }

        //    // TODO: Set hand symbol when mouse over in header button.
        //    navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;

        //}

        #endregion

        #region --- Header Action ---

        

        //private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        //{
        //    if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
        //        this.Close(); // this.Parent.Controls.Remove(this);
        //    try
        //    {
        //        //parentForm.navRefresh_ElementClick(sender, e);
        //    }
        //    catch (Exception ex)
        //    {
        //        XtraMessageBox.Show(ex.Message, "POS Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        #endregion
        /// </summary>
        ZooEntity DB = new ZooEntity();
        public AddCategory()
        {
            InitializeComponent();
            navUpdate.Enabled = false;

            // TODO: Set hand symbol when mouse over in header button.
            txtCategoryIDNew.Enabled = false;
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;
        }

        

        List<INItemCategory> _ItemCategory = new List<INItemCategory>();
        public bool isSuccessful = true;

        private void AddCategory_Load(object sender, EventArgs e)
        {
            _ItemCategory = DB.INItemCategories.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            GC.DataSource = _ItemCategory.ToList();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GV.RowCount > 0)
            {
                // TODO: Export data in GridControl as Excel file.
                GlobalInitializer.GsExportData(GC);
            }
            else
                XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //this.Parent.Controls.Remove(this);
            this.Close();
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            AddCategory_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }
        void clear()
        {
            GlobalInitializer.GsClearValue(txtCategoryIDNew, txtCategoryCodeNew, txtCategoryNameNew, txtDescrNew);
        }
        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                INItemCategory result = new INItemCategory();
                result = (INItemCategory)GV.GetRow(GV.FocusedRowHandle);

                if (result != null)
                {
                    if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                    {
                        INItemCategory customer = DB.INItemCategories.FirstOrDefault(st => st.CompanyID.Equals(result.CompanyID) && st.CategoryID.Equals(result.CategoryID));

                        DB.INItemCategories.Remove(customer);
                        DB.SaveChanges();
                        AddCategory_Load(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            navNew_ElementClick(sender, e);
            //navEdit
        }


        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtCategoryCodeNew,
                txtCategoryNameNew) == false)
            {
                //    // TODO: Save value to DB.
                GsSaveData();

                // TODO: Validate which record exiting.
                if (isExiting && isSuccessful)
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCategoryCodeNew.Focus();

                    return;
                }
             
                if (isSuccessful)
                    //navNew_ElementClick(sender, e);
                    navRefresh_ElementClick(sender,e); 
                clear();
                XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
           
        //private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        //{
        //    try
        //    {
        //        // TODO: Declare from FrmCompanyProfileModification for asign values.
        //       // ItemCategorySetup objForm = new ItemCategorySetup("NEW", this);
        //      //  FormShadow Shadow = new FormShadow(objForm);

        //      //  txtCategoryCode.Enabled = false;
        //      //  txtImagesUrl.Image = null;
        //      //  objForm.txtImagesUrl.Invalidate();

        //        //TODO: Binding value to comboBox or LookupEdit.
        //        //Global Initializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

        //        // objForm._cities = _cities;  
        //        Shadow.ShowDialog();
        //    }
        //    catch (Exception ex)
        //    {
        //        XtraMessageBox.Show(ex.Message, "POS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}


        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Clear value for new
            GlobalInitializer.GsClearValue(txtCategoryIDNew,txtCategoryCodeNew, txtCategoryNameNew, txtDescrNew);

           navUpdate.Enabled = false;
           navSave.Enabled = true;
            isNew = true;

            pbImage.Image = null;
            pbImage.Invalidate();

            txtCategoryNameNew.Focus();

        }
        #region Actions
        public void GsSaveData()
        {
            isExiting = false;
            if (_ItemCategory.Any(x => x.CategoryID.ToString() == txtCategoryIDNew.Text.Trim().ToLower()))
            {
                isExiting = true;
                return;
            }

            //if (XtraMessageBox.Show("Are you sure you want to save this record?", "POS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            INItemCategory itemCategory = new INItemCategory()
            {
                CompanyID = DACClasses.CompanyID,
                CategoryID = DB.INItemCategories.Max(x => x.CategoryID) + 1,
                CategoryCD = txtCategoryCodeNew.Text,
                CategoryName = txtCategoryNameNew.Text,
                Descr = txtDescrNew.Text,
                ImagesUrl = DACClasses.ReturnImage(pbImage)

            };

            DB.INItemCategories.Add(itemCategory);
            var Row = DB.SaveChanges();
            if (Row == 1)
            {
                isSuccessful = true;
            }
            GV.FocusedRowHandle = 0;

            AddCategory_Load(null, null);
        }
        public void GsUpdateData()
        {
            //navUpdate.Enabled = true;
            try
            {
                if (_ItemCategory.Any(x => x.CategoryCD.ToString() != txtCategoryCodeNew.Text.Trim().ToLower()
                && x.CategoryCD.Trim().ToLower() == txtCategoryCodeNew.Text.Trim().ToLower()))
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCategoryCodeNew.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                var _obj = DB.INItemCategories.SingleOrDefault(w => w.CategoryID.ToString() == txtCategoryIDNew.Text && w.CompanyID.Equals(DACClasses.CompanyID));

                _obj.CategoryName = txtCategoryNameNew.Text;
                _obj.CategoryCD = txtCategoryCodeNew.Text;
                _obj.Descr = txtDescrNew.Text;
                _obj.ImagesUrl = DACClasses.ReturnImage(pbImage);

                DB.INItemCategories.Attach(_obj);
                DB.Entry(_obj).Property(w => w.CategoryName).IsModified = true;
                DB.Entry(_obj).Property(w => w.CategoryCD).IsModified = true;
                DB.Entry(_obj).Property(w => w.Descr).IsModified = true;
                DB.Entry(_obj).Property(w => w.ImagesUrl).IsModified = true;

                var Row = DB.SaveChanges();
                clear();
                navRefresh_ElementClick(null,null);
                navSave.Enabled = true;
                navUpdate.Enabled = false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            Point point = view.GridControl.PointToClient(Control.MousePosition);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
            if (info.InRow || info.InRowCell)
                navED_ElementClick(null, null);
        }

        private void navED_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            navUpdate.Enabled = true;
            navSave.Enabled = false;
           if (txtCategoryIDNew.Text != null)
           {
                try
                {
                    INItemCategory result = new INItemCategory();

                    result = (INItemCategory)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        txtCategoryIDNew.Text = result.CategoryID.ToString();
                        txtCategoryIDNew.Enabled = false;
                        txtCategoryCodeNew.Focus();
                        txtDescrNew.Text = result.Descr;
                        txtCategoryCodeNew.Text = result.CategoryCD;
                        txtCategoryNameNew.Text = result.CategoryName;
                        pbImage.Image = result.ImagesUrl == null ? pbImage.Image : DACClasses.ReadImage(result.ImagesUrl);


                        //Shadow.ShowDialog();
                    }

                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
           }        
        }

        private void navUpdate_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtCategoryCodeNew) == false)
                GsUpdateData();
        }

        private void txtCategoryCodeNew_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtDescrNew_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && navUpdate.Enabled == true)
            {
                navUpdate_ElementClick(null, null);
                
            }
            else if (e.KeyCode == Keys.Enter && navUpdate.Enabled == false)
            {
                navSave_ElementClick(null, null);
            }
        }

        private void pbImage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && navSave.Visible == true)
            {
                navSave_ElementClick(null, null);
            }
            else if (e.KeyCode == Keys.Enter && navSave.Visible == false)
            {
                navUpdate_ElementClick(null, null);
            }
        }
    }
}