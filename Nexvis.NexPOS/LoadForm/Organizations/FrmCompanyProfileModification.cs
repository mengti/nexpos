﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;

namespace DgoStore.LoadForm.Organizations
{
    public partial class FrmCompanyProfileModification : DevExpress.XtraEditors.XtraForm
    {
        #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        private scrCompany parentForm;

        #endregion

        #region --- Form Action ---
        public FrmCompanyProfileModification(string transactionType, scrCompany objParent)
        {
            InitializeComponent();
            parentForm = objParent;

            if (transactionType == "NEW")
            {
                navEdit.Visible = false;
                isNew = true;
            }
            else if (transactionType == "EDIT")
            {
                navSave.Visible = false;
            }

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;

        }

        #endregion

        #region --- Header Action ---

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Clear value for new
            GlobalInitializer.GsClearValue(txtCompanyCD, txtCompanyName, txtPhoneNumber,  cboCurrency, txtAddress, txtTaxRegister);
            navEdit.Visible = false;
            navSave.Visible = true;
            isNew = true;
            txtCompanyCD.Enabled = true;
            txtCompanyCD.Focus();
        }

        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtCompanyCD, cboCurrency) == false)
            {
                //    // TODO: Save value to DB.
                parentForm.GsSaveData(this);

                // TODO: Validate which record exiting.
                if (isExiting && parentForm.isSuccessful)
                {
                    XtraMessageBox.Show("This record ready existing !", "POS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCompanyName.Focus();
                    return;
                }

                if (parentForm.isSuccessful)
                    navNew_ElementClick(sender, e);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtCompanyCD, cboCurrency) == false)
                parentForm.GsUpdateData(this);
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes) 
                this.Close(); // this.Parent.Controls.Remove(this);
        }

        #endregion

        private void txtPhoneNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (e.KeyChar == '+' && !txtPhoneNumber.Text.Contains('+') && txtPhoneNumber.SelectionStart == 0) || e.KeyChar == (char)8)
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void FrmCompanyProfileModification_Load(object sender, EventArgs e)
        {

        }
    }
}