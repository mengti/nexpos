﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.Helper;
using ZooMangement;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.Organizations
{
    public partial class scrCompany : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrCompany()
        {
            InitializeComponent();
        }
        List<CSCurrency> currencies = new List<CSCurrency>();
        List<CSCompany> _Company = new List<CSCompany>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        public bool isSuccessful = true;
        private void FormCompany_Load(object sender, EventArgs e)
        {
            _Company = DB.CSCompanies.ToList();
            GC.DataSource = _Company.ToList();
            currencies = DB.CSCurrencies.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
           
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    FrmCompanyProfileModification objForm = new FrmCompanyProfileModification("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //TODO: Binding value to comboBox or LookupEdit.
                    GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    // objForm._cities = _cities;
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
              
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        private void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormCompany_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    CSCompany result = new CSCompany();
                    result = (CSCompany)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {
                            CSCompany cSCompany = DB.CSCompanies.FirstOrDefault(st => st.CompanyID.Equals(result.CompanyID));

                            DB.CSCompanies.Remove(cSCompany);
                            DB.SaveChanges();
                            FormCompany_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


                
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void GsSaveData(FrmCompanyProfileModification objChild)
        {
            objChild.isExiting = false;
            if (_Company.Any(x => x.CompanyCD.Trim().ToLower() == objChild.txtCompanyCD.Text.Trim().ToLower()))
            {
                objChild.isExiting = true;
                return;
            }

            if (XtraMessageBox.Show("Are you sure you want to save this record?", "POS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            CSCompany company = new CSCompany()
            {
                CompanyID = DB.CSCompanies.Max(p => p.CompanyID) + 1,
                CompanyCD = objChild.txtCompanyCD.Text,
                CompanyName = objChild.txtCompanyName.Text,
                CompanyAddress = objChild.txtAddress.Text,
                CompanyPhone = objChild.txtPhoneNumber.Text,
                BaseCuryID = objChild.cboCurrency.EditValue.ToString(),
                TaxRegisterID = objChild.txtTaxRegister.Text,
                ImageUrl = DACClasses.ReturnImage(objChild.imgCompany)
            };

            DB.CSCompanies.Add(company);
            var Row = DB.SaveChanges();
            if (Row == 1)
            {
                isSuccessful = true;
            }
            GV.FocusedRowHandle = 0;
        }
        public void GsUpdateData(FrmCompanyProfileModification objChild)
        {
            try
            {
                if (_Company.Any(x => x.CompanyCD.Trim().ToLower() != objChild.txtCompanyCD.Text.Trim().ToLower()
                && x.CompanyName.Trim().ToLower() == objChild.txtCompanyName.Text.Trim().ToLower()))
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtCompanyName.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;


                var _comp = DB.CSCompanies.SingleOrDefault(w => w.CompanyCD == objChild.txtCompanyCD.Text);

                _comp.CompanyCD = objChild.txtCompanyCD.Text;
                _comp.CompanyName = objChild.txtCompanyName.Text;
                _comp.CompanyAddress = objChild.txtAddress.Text;
                _comp.CompanyPhone = objChild.txtPhoneNumber.Text;
                _comp.BaseCuryID = objChild.cboCurrency.EditValue.ToString();
                _comp.TaxRegisterID = objChild.txtTaxRegister.Text;
                _comp.ImageUrl = DACClasses.ReturnImage(objChild.imgCompany);

                DB.CSCompanies.Attach(_comp);
                DB.Entry(_comp).Property(w => w.CompanyName).IsModified = true;
                DB.Entry(_comp).Property(w => w.CompanyAddress).IsModified = true;
                DB.Entry(_comp).Property(w => w.CompanyPhone).IsModified = true;
                DB.Entry(_comp).Property(w => w.BaseCuryID).IsModified = true;
                DB.Entry(_comp).Property(w => w.TaxRegisterID).IsModified = true;
                DB.Entry(_comp).Property(w => w.ImageUrl).IsModified = true;
                var Row = DB.SaveChanges();
                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                objChild.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                // NavButton navigator = (NavButton)sender;
                var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    FrmCompanyProfileModification objForm = new FrmCompanyProfileModification("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    CSCompany result = new CSCompany();

                    result = (CSCompany)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        objForm.txtCompanyCD.Text = result.CompanyCD;
                        objForm.txtCompanyCD.Enabled = false;
                        objForm.txtCompanyName.Focus();
                        objForm.txtCompanyName.Text = result.CompanyName;
                        objForm.txtAddress.Text = result.CompanyAddress;
                        objForm.txtPhoneNumber.Text = result.CompanyPhone;
                        objForm.cboCurrency.EditValue = result.BaseCuryID;
                        objForm.txtTaxRegister.Text = result.TaxRegisterID;
                        objForm.imgCompany.Image = result.ImageUrl == null ? objForm.imgCompany.Image : DACClasses.ReadImage(result.ImageUrl);

                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


               

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}