﻿namespace DgoStore.LoadForm.Organizations
{
    partial class FrmCompanyProfileModification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navNew = new DevExpress.XtraBars.Navigation.NavButton();
            this.navSave = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.imgCompany = new DevExpress.XtraEditors.PictureEdit();
            this.txtCompanyCD = new DevExpress.XtraEditors.TextEdit();
            this.txtCompanyName = new DevExpress.XtraEditors.TextEdit();
            this.txtPhoneNumber = new DevExpress.XtraEditors.TextEdit();
            this.cboCurrency = new DevExpress.XtraEditors.LookUpEdit();
            this.txtTaxRegister = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.D1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.D2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.D3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.D6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCompany.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyCD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCurrency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxRegister.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navNew);
            this.navHeader.Buttons.Add(this.navSave);
            this.navHeader.Buttons.Add(this.navEdit);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(844, 40);
            this.navHeader.TabIndex = 1;
            this.navHeader.Text = "tileNavPane1";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Company Profile Information";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navNew
            // 
            this.navNew.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navNew.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navNew.AppearanceHovered.Options.UseForeColor = true;
            this.navNew.Caption = "New";
            this.navNew.Name = "navNew";
            this.navNew.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navSave
            // 
            this.navSave.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navSave.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navSave.AppearanceHovered.Options.UseForeColor = true;
            this.navSave.Caption = "Save";
            this.navSave.Name = "navSave";
            this.navSave.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navSave_ElementClick);
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Update";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.imgCompany);
            this.layoutControl1.Controls.Add(this.txtCompanyCD);
            this.layoutControl1.Controls.Add(this.txtCompanyName);
            this.layoutControl1.Controls.Add(this.txtPhoneNumber);
            this.layoutControl1.Controls.Add(this.cboCurrency);
            this.layoutControl1.Controls.Add(this.txtTaxRegister);
            this.layoutControl1.Controls.Add(this.txtAddress);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(716, 239, 650, 400);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(844, 436);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // imgCompany
            // 
            this.imgCompany.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgCompany.Location = new System.Drawing.Point(566, 73);
            this.imgCompany.Name = "imgCompany";
            this.imgCompany.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.imgCompany.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.imgCompany.Size = new System.Drawing.Size(266, 351);
            this.imgCompany.StyleController = this.layoutControl1;
            this.imgCompany.TabIndex = 5;
            // 
            // txtCompanyCD
            // 
            this.txtCompanyCD.EnterMoveNextControl = true;
            this.txtCompanyCD.Location = new System.Drawing.Point(12, 31);
            this.txtCompanyCD.Name = "txtCompanyCD";
            this.txtCompanyCD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyCD.Properties.Appearance.Options.UseFont = true;
            this.txtCompanyCD.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.txtCompanyCD.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtCompanyCD.Properties.MaxLength = 100;
            this.txtCompanyCD.Size = new System.Drawing.Size(268, 22);
            this.txtCompanyCD.StyleController = this.layoutControl1;
            this.txtCompanyCD.TabIndex = 0;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.EnterMoveNextControl = true;
            this.txtCompanyName.Location = new System.Drawing.Point(284, 31);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyName.Properties.Appearance.Options.UseFont = true;
            this.txtCompanyName.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.txtCompanyName.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtCompanyName.Properties.MaxLength = 100;
            this.txtCompanyName.Size = new System.Drawing.Size(279, 22);
            this.txtCompanyName.StyleController = this.layoutControl1;
            this.txtCompanyName.TabIndex = 1;
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.EnterMoveNextControl = true;
            this.txtPhoneNumber.Location = new System.Drawing.Point(567, 31);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNumber.Properties.Appearance.Options.UseFont = true;
            this.txtPhoneNumber.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.txtPhoneNumber.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtPhoneNumber.Properties.MaxLength = 50;
            this.txtPhoneNumber.Size = new System.Drawing.Size(265, 22);
            this.txtPhoneNumber.StyleController = this.layoutControl1;
            this.txtPhoneNumber.TabIndex = 2;
            this.txtPhoneNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhoneNumber_KeyPress);
            // 
            // cboCurrency
            // 
            this.cboCurrency.EnterMoveNextControl = true;
            this.cboCurrency.Location = new System.Drawing.Point(12, 76);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCurrency.Properties.Appearance.Options.UseFont = true;
            this.cboCurrency.Properties.AppearanceDropDown.BorderColor = System.Drawing.Color.Transparent;
            this.cboCurrency.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCurrency.Properties.AppearanceDropDown.Options.UseBorderColor = true;
            this.cboCurrency.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboCurrency.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCurrency.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.cboCurrency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCurrency.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CuryID", "CuryID")});
            this.cboCurrency.Properties.NullText = "--- Select ---";
            this.cboCurrency.Properties.PopupSizeable = false;
            this.cboCurrency.Properties.ShowFooter = false;
            this.cboCurrency.Properties.ShowHeader = false;
            this.cboCurrency.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboCurrency.Size = new System.Drawing.Size(268, 22);
            this.cboCurrency.StyleController = this.layoutControl1;
            this.cboCurrency.TabIndex = 3;
            // 
            // txtTaxRegister
            // 
            this.txtTaxRegister.EnterMoveNextControl = true;
            this.txtTaxRegister.Location = new System.Drawing.Point(284, 76);
            this.txtTaxRegister.Name = "txtTaxRegister";
            this.txtTaxRegister.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxRegister.Properties.Appearance.Options.UseFont = true;
            this.txtTaxRegister.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.txtTaxRegister.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtTaxRegister.Properties.MaxLength = 200;
            this.txtTaxRegister.Size = new System.Drawing.Size(278, 22);
            this.txtTaxRegister.StyleController = this.layoutControl1;
            this.txtTaxRegister.TabIndex = 4;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(12, 121);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress.Properties.Appearance.Options.UseFont = true;
            this.txtAddress.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.txtAddress.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtAddress.Properties.MaxLength = 100;
            this.txtAddress.Size = new System.Drawing.Size(550, 303);
            this.txtAddress.StyleController = this.layoutControl1;
            this.txtAddress.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.D1,
            this.D2,
            this.D3,
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.D6,
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(844, 436);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // D1
            // 
            this.D1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.D1.AppearanceItemCaption.Options.UseFont = true;
            this.D1.Control = this.txtCompanyCD;
            this.D1.CustomizationFormText = "User ID";
            this.D1.Location = new System.Drawing.Point(0, 0);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(272, 45);
            this.D1.Text = "ID";
            this.D1.TextLocation = DevExpress.Utils.Locations.Top;
            this.D1.TextSize = new System.Drawing.Size(90, 16);
            // 
            // D2
            // 
            this.D2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.D2.AppearanceItemCaption.Options.UseFont = true;
            this.D2.Control = this.txtCompanyName;
            this.D2.CustomizationFormText = "User ID";
            this.D2.Location = new System.Drawing.Point(272, 0);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(283, 45);
            this.D2.Text = "Company Name";
            this.D2.TextLocation = DevExpress.Utils.Locations.Top;
            this.D2.TextSize = new System.Drawing.Size(90, 16);
            // 
            // D3
            // 
            this.D3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.D3.AppearanceItemCaption.Options.UseFont = true;
            this.D3.Control = this.txtPhoneNumber;
            this.D3.CustomizationFormText = "User ID";
            this.D3.Location = new System.Drawing.Point(555, 0);
            this.D3.Name = "D3";
            this.D3.Size = new System.Drawing.Size(269, 45);
            this.D3.Text = "Phone Number";
            this.D3.TextLocation = DevExpress.Utils.Locations.Top;
            this.D3.TextSize = new System.Drawing.Size(90, 16);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.cboCurrency;
            this.layoutControlItem1.CustomizationFormText = "Student Name";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 45);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(272, 45);
            this.layoutControlItem1.Text = "Currency";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.txtAddress;
            this.layoutControlItem7.CustomizationFormText = "Student Name";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(554, 326);
            this.layoutControlItem7.Text = "Address";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 16);
            // 
            // D6
            // 
            this.D6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.D6.AppearanceItemCaption.Options.UseFont = true;
            this.D6.Control = this.txtTaxRegister;
            this.D6.CustomizationFormText = "User ID";
            this.D6.Location = new System.Drawing.Point(272, 45);
            this.D6.Name = "D6";
            this.D6.Size = new System.Drawing.Size(282, 45);
            this.D6.Text = "Tax Register ID";
            this.D6.TextLocation = DevExpress.Utils.Locations.Top;
            this.D6.TextSize = new System.Drawing.Size(90, 16);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.imgCompany;
            this.layoutControlItem2.Location = new System.Drawing.Point(554, 45);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(270, 371);
            this.layoutControlItem2.Text = "Image";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 13);
            // 
            // FrmCompanyProfileModification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 476);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Name = "FrmCompanyProfileModification";
            this.Text = "FrmCompanyProfileModification";
            this.Load += new System.EventHandler(this.FrmCompanyProfileModification_Load);
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgCompany.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyCD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhoneNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCurrency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxRegister.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navNew;
        private DevExpress.XtraBars.Navigation.NavButton navSave;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem D1;
        private DevExpress.XtraLayout.LayoutControlItem D2;
        private DevExpress.XtraLayout.LayoutControlItem D3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem D6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        public DevExpress.XtraEditors.TextEdit txtCompanyCD;
        public DevExpress.XtraEditors.TextEdit txtCompanyName;
        public DevExpress.XtraEditors.TextEdit txtPhoneNumber;
        public DevExpress.XtraEditors.LookUpEdit cboCurrency;
        public DevExpress.XtraEditors.TextEdit txtTaxRegister;
        public DevExpress.XtraEditors.MemoEdit txtAddress;
        internal DevExpress.XtraEditors.PictureEdit imgCompany;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}