﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.Helper;
using ZooMangement;
using DevExpress.XtraGrid.Views.Grid;
using ZooManagement;
using DevExpress.XtraEditors.Filtering.Templates;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.Employee
{
    public partial class scrEmployee : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();

        public scrEmployee()
        {
            InitializeComponent();


        }

       // StoredProcedureEntities context = new StoredProcedureEntities();
        //List<CSCurrency> currencies = new List<CSCurrency>();
        List<EPEmployee> employee = new List<EPEmployee>();

        List<getGender_Result> getGender_Results = new List<getGender_Result>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        public bool isSuccessful = true;
        private void FormEmployee_Load(object sender, EventArgs e)
        {

             getGender_Results = DB.getGender().ToList();
            _RoleItems = DB.CSRoleItems.ToList();
            //employee = DB.EPEmployees.ToList();
             //employee = DB.EPEmployees.Where(x => getGender_Results.Where(y => y.EmployeeName == x.EmployeeName).Any()).ToList();
             // x3getGender_Results.Where(x => getGender_Results.Where(y => y.EmployeeID == x.EmployeeID).Any()).ToList();
            GC.DataSource = getGender_Results.ToList();

        }
        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormEmployee_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    EmployeeSetup objForm = new EmployeeSetup("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //TODO: Binding value to comboBox or LookupEdit.
                    // GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    // objForm._cities = _cities;
                    objForm.dtStartdate.Text = DateTime.Today.ToShortDateString();
                    objForm.dtResignDate.Enabled = false;
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    getGender_Result result = new getGender_Result();
                    result = (getGender_Result)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "POS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return;
                        {
                            EPEmployee site = DB.EPEmployees.FirstOrDefault(st => st.EmployeeID.Equals(result.EmployeeID));

                            DB.EPEmployees.Remove(site);
                            DB.SaveChanges();
                            FormEmployee_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void GsSaveData(EmployeeSetup objChild)
        {
            objChild.isExiting = false;
            if (employee.Any(x => x.EmployeeID.Trim().ToLower() == objChild.txtEmployeeID.Text.Trim().ToLower()))
            {
                objChild.isExiting = true;
                return;
            }
            
            try
            {
                
                var resul = new CFNumberRank("EMP", DocConfType.Normal, true);
                EPEmployee emp = new EPEmployee()
                {
                    CompanyID = DACClasses.CompanyID,
                    EmployeeID = resul.NextNumberRank.Trim(),
                    EmployeeName = objChild.txtEmployeeName.Text,
                    Status = Convert.ToBoolean(objChild.ckStatus.CheckState),
                    Gender = objChild.cboGender.Text,
                    DateOfBirth = (DateTime)objChild.dtDOB.DateTime.Date,
                    EmployeeAddress = objChild.txtEmployeeAdress.Text,
                    EmployeePhone = objChild.txtEmployeePhone.Text,
                    StartDate = (DateTime?)objChild.dtStartdate.DateTime.Date,
                    IsLog = false,
                  //  ResignDate = (DateTime?)(objChild.dtResignDate.DateTime.Date),
                    Salary = Convert.ToDecimal(objChild.txtSalary.EditValue)
                };

                if (objChild.cboGender.Text == "M")
                {
                    objChild.cboGender.EditValue = "Male";
                }
                else
                {
                    objChild.cboGender.EditValue = "Female";
                }

                DB.EPEmployees.Add(emp);
                var Row = DB.SaveChanges();

                if (Row == 1)
                {
                    isSuccessful = true;
                }
                GV.FocusedRowHandle = 0;
                FormEmployee_Load(null, null);
                XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "POS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
        private void Args_Showing(object sender, XtraMessageShowingArgs e)
        {
            //bold message caption
            e.Form.Appearance.FontStyleDelta = FontStyle.Bold;
            //increased button height and font size
            MessageButtonCollection buttons = e.Buttons as MessageButtonCollection;
            SimpleButton btn = buttons[System.Windows.Forms.DialogResult.OK] as SimpleButton;
            if (btn != null)
            {
                btn.Appearance.FontSizeDelta = 3;
                btn.Height += 10;
            }
        }
        public void GsUpdateData(EmployeeSetup objChild)
        {
            try
            {
                if (employee.Any(x => x.EmployeeID.Trim().ToLower() != objChild.txtEmployeeID.Text.Trim().ToLower()
                && x.EmployeeName.Trim().ToLower() == objChild.txtEmployeeName.Text.Trim().ToLower()))
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtEmployeeName.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;


                var _emp = DB.EPEmployees.FirstOrDefault(w => w.EmployeeID == objChild.txtEmployeeID.Text);

                _emp.CompanyID = DACClasses.CompanyID;
                _emp.EmployeeID = objChild.txtEmployeeID.Text;
                _emp.EmployeeName = objChild.txtEmployeeName.Text;
                _emp.Status = (bool)objChild.ckStatus.EditValue;
              
                if (objChild.cboGender.Text == "Male")
                {
                    objChild.cboGender.EditValue = "M";
                }
                else
                {
                    objChild.cboGender.EditValue = "F";
                }

                _emp.Gender = objChild.cboGender.Text;
                _emp.DateOfBirth = (objChild.dtDOB.DateTime.Date);
                _emp.EmployeeAddress = objChild.txtEmployeeAdress.Text;
                _emp.EmployeePhone = objChild.txtEmployeePhone.Text;
                _emp.StartDate = (DateTime?)objChild.dtStartdate.DateTime.Date;
                _emp.ResignDate = (DateTime?)objChild.dtResignDate.DateTime.Date;
                _emp.Salary = Convert.ToDecimal(objChild.txtSalary.EditValue);


                DB.EPEmployees.Attach(_emp);
                DB.Entry(_emp).Property(w => w.EmployeeName).IsModified = true;
                DB.Entry(_emp).Property(w => w.Status).IsModified = true;
                DB.Entry(_emp).Property(w => w.Gender).IsModified = true;
                DB.Entry(_emp).Property(w => w.DateOfBirth).IsModified = true;
                DB.Entry(_emp).Property(w => w.EmployeeAddress).IsModified = true;
                DB.Entry(_emp).Property(w => w.EmployeePhone).IsModified = true;
         //       DB.Entry(_emp).Property(w => w.StartDate).IsModified = true;
         //       DB.Entry(_emp).Property(w => w.ResignDate).IsModified = true;
                DB.Entry(_emp).Property(w => w.Salary).IsModified = true;

                var Row = DB.SaveChanges();

                XtraMessageBoxArgs args = new XtraMessageBoxArgs();
                args.Caption = "NEXPOS";
                args.Text = "This field has been upadated.";
                args.Buttons = new DialogResult[] { DialogResult.OK };
                args.Showing += Args_Showing;
                XtraMessageBox.Show(args).ToString();
                FormEmployee_Load(null, null);

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                //NavButton navigator = (NavButton)sender;
               var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    EmployeeSetup objForm = new EmployeeSetup("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    // GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    //  EPEmployee result = new EPEmployee();

                    getGender_Result result = new getGender_Result();

                    result = (getGender_Result)GV.GetRow(GV.FocusedRowHandle);

                    objForm.dtResignDate.Enabled = true;

                    if (result != null)
                    {
                        //objForm.txtCompanyID.Text = result.CompanyID;
                        objForm.txtEmployeeID.Enabled = false;
                        objForm.txtEmployeeName.Focus();
                        objForm.txtEmployeeID.Text = result.EmployeeID;
                        objForm.txtEmployeeName.Text = result.EmployeeName;
                        objForm.ckStatus.EditValue = result.Status;
                        objForm.cboGender.Text = result.Gender;
                        objForm.dtDOB.EditValue = result.DateOfBirth;
                        objForm.cboGender.Text = result.Gender;
                        objForm.cboGender.Text = result.Gender;
                        objForm.txtEmployeeAdress.Text = result.EmployeeAddress;
                        objForm.txtEmployeePhone.Text = result.EmployeePhone;
                        objForm.dtStartdate.EditValue = result.StartDate;
                        objForm.dtResignDate.EditValue = result.ResignDate;
                        objForm.txtSalary.Text = result.Salary.ToString();
                        if (objForm.cboGender.Text == "M")
                        {
                            objForm.cboGender.EditValue = "Male";
                        }
                        else
                        {
                            objForm.cboGender.EditValue = "Female";
                        }

                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            //NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
           
        }
    }
}