﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS.Helper;
using ZooMangement;
using ZooManagement;
using Nexvis.NexPOS;
using DevExpress.Utils;
//using DevComponents.DotNetBar.Controls;
using DevExpress.XtraEditors.Controls;
using Nexvis.NexPOS.LoadForm.User;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.User
{
    public partial class scrUser : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrUser()
        {
            InitializeComponent();

        }


        List<CSRole> userrole = new List<CSRole>();
      public List<CSUser> user = new List<CSUser>();

        List<EPEmployee> _EMP = new List<EPEmployee>();

       // List<CSRole> _CSRole = new List<CSRole>();
       // List<CSUserRole> _UserRole = new List<CSUserRole>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        public bool isSuccessful = true;
        private void FormUser_Load(object sender, EventArgs e)
        {
            user = DB.CSUsers.ToList();
            GC.DataSource = user.ToList();
            userrole = DB.CSRoles.ToList();
            _EMP = DB.EPEmployees.ToList();
          //  _CSRole = DB.CSRoles.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
           // InitialOptionDetail();

        }

        #region navbar Action
       
        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        
        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                // TODO: Declare from FrmCompanyProfileModification for asign values.
                UserSetup objForm = new UserSetup("NEW", this);
                FormShadow Shadow = new FormShadow(objForm);

                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    //TODO: Binding value to comboBox or LookupEdit.
                    navRefresh_ElementClick(sender,e);
                    GlobalInitializer.GsFillLookUpEdit(userrole.ToList(), "RoleID", "Description", objForm.luRoleID, (int)userrole.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(_EMP.ToList(), "EmployeeID", "EmployeeName", objForm.luUserEmployee, (int)_EMP.ToList().Count);
                    objForm.ckShow1.Visible = true;
                  //  objForm.ckShow2.Visible = true;
                    // objForm._cities = _cities;
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                UserSetup objForm = new UserSetup("EDIT", this);
                FormShadow Shadow = new FormShadow(objForm);

                var nameAction = "Edit";
               // NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    GlobalInitializer.GsFillLookUpEdit(userrole.ToList(), "RoleID", "Description", objForm.luRoleID, (int)userrole.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(_EMP.ToList(), "EmployeeID", "EmployeeName", objForm.luUserEmployee, (int)_EMP.ToList().Count);

                    CSUser result = new CSUser();
                    result = (CSUser)GV.GetRow(GV.FocusedRowHandle);
                    if (result != null)
                    {
                        var roleID = DB.CSUserRoles.FirstOrDefault(x => x.UserID == result.UserID).RoleId;
                        objForm.layoutControlPassword.HideToCustomization();
                        objForm.layoutControlRePassword.HideToCustomization();
                        objForm.layoutControlItem5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                       // objForm.layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

                        objForm.txtUserID.Enabled = false;
                        objForm.luRoleID.EditValue = roleID;
                        objForm.txtUserName.Focus();
                        objForm.txtUserID.Text = result.UserID.ToString();
                        objForm.txtUserName.Text = result.UserName;
                        objForm.ckStatus.EditValue = result.Status;
                        objForm.txtPassword.Text = result.Password;
                        objForm.txtConfirmPass.Text = result.Password;
                        objForm.luUserEmployee.EditValue = result.UserEmployee;

                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }
        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormUser_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }
        private void navChange_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            ChangePassword objForm = new ChangePassword("CHANGE", this);
            FormShadow Shadow = new FormShadow(objForm);

            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption.Trim()))
            {
                CSUser result = new CSUser();
                result = (CSUser)GV.GetRow(GV.FocusedRowHandle);
                if (result != null)
                {
                    objForm.userText = result.UserID;
                    // objForm.roleText = 
                    Shadow.ShowDialog();
                }
            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role  && x.ScreenId == this.Name && x.ActionName == navigator.Caption.Trim()))
                {
                    CSUser result = new CSUser();
                    result = (CSUser)GV.GetRow(GV.FocusedRowHandle);

                    if (result.IsLog == true)
                    {
                        XtraMessageBox.Show("Can't delete admin user.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;
                    if (result != null)
                    {

                        {
                            CSUser csuser = DB.CSUsers.FirstOrDefault(st => st.UserID.Equals(result.UserID));
                            DB.CSUsers.Remove(csuser);
                            DB.SaveChanges();
                            FormUser_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion




        public void GsSaveData(UserSetup objChild)
        {
            objChild.isExiting = false;
            if (user.Any(x => x.UserName.Trim().ToLower() == objChild.txtUserName.Text.Trim().ToLower()))
            {
                objChild.isExiting = true;
                return;
            }

            try
            {
                var resul1 = new CFNumberRank("US", DocConfType.Normal, true);
               
                var user = resul1.NextNumberRank.Trim();
                CSUser csuser = new CSUser()
                {
                    CompanyID = DACClasses.CompanyID,
                    UserID = user,
                    UserName = objChild.txtUserName.Text,
                 ///   RoleID = Convert.ToInt32(objChild.luRoleID.EditValue),
                  //  RoleName = objChild.luRoleID.Text,
                    Status = (bool)objChild.ckStatus.EditValue,
                    Password = PasswordHash.HashPassword(objChild.txtPassword.Text),
                    IsLog = false,
                    UserEmployee = objChild.luUserEmployee.EditValue.ToString()

                };
                DB.CSUsers.Add(csuser);

                CSUserRole userRole = new CSUserRole()
                {
                     RoleId = Convert.ToInt32(objChild.luRoleID.EditValue),
                     UserID = user
                };
                DB.CSUserRoles.Add(userRole);

                
                var Row = DB.SaveChanges();
                if (Row == 1)
                {
                    isSuccessful = true;
                }
                GV.FocusedRowHandle = 0;
                navRefresh_ElementClick(null,null);
                XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            catch (Exception ex)
            {

                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        public void GsUpdateData(UserSetup objChild)
        {
            try
            {
                //List<CSUser> objExist = DB.CSUsers.Where(w => w.TokenCode == tokend).ToList();
                List<CSUser> objExist = DB.CSUsers.Where(w => w.UserID == objChild.txtUserID.Text).ToList();

                if (user.Any(x => x.UserID.Trim().ToLower() != objChild.txtUserID.Text.Trim().ToLower()
                   && x.UserName.Trim().ToLower() == objChild.txtUserName.Text.Trim().ToLower()))
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtUserName.Focus();
                    return;
                }
                var userObj = objExist.First();

                if (userObj.IsLog == true)
                {
                    XtraMessageBox.Show("Can't update admin user.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;


                var _comp = DB.CSUsers.SingleOrDefault(w => w.UserID.ToString() == objChild.txtUserID.Text);

               

                _comp.CompanyID = DACClasses.CompanyID;
                _comp.UserID = objChild.txtUserID.Text;
                _comp.UserName = objChild.txtUserName.Text;
                _comp.Status = (bool)objChild.ckStatus.EditValue;
               // _comp.Password = PasswordHash.HashPassword(objChild.txtPassword.Text);
                _comp.UserEmployee = objChild.luUserEmployee.EditValue.ToString();

                DB.CSUsers.Attach(_comp);
                DB.Entry(_comp).Property(w => w.UserName).IsModified = true;
                DB.Entry(_comp).Property(w => w.Status).IsModified = true;
                DB.Entry(_comp).Property(w => w.UserEmployee).IsModified = true;


                //Check Detail Update 
                var listUserRole = DB.CSUserRoles.Where(w => w.UserID == userObj.UserID).ToList();
                if (listUserRole.Count > 0)
                {
                    foreach (var read in listUserRole)
                    {
                        DB.CSUserRoles.Remove(read);
                    }
                    DB.SaveChanges();
                }
                CSUserRole objRole = new CSUserRole();
                objRole.UserID = userObj.UserID;
                objRole.RoleId = Convert.ToInt32(objChild.luRoleID.EditValue);
                DB.CSUserRoles.Add(objRole);
         
                var Row = DB.SaveChanges();
                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void GsChangePas(ChangePassword objChild)
        {
            objChild.isExiting = false;
           
            var _comp = user.SingleOrDefault(w => w.UserID.ToString() == objChild.userText);
            bool bb = PasswordHash.ValidatePassword(objChild.txtCurrenPass.Text, _comp.Password);
            if (bb == true)
            {
                if (user.Any(x => x.UserID.Trim().ToLower() == objChild.userText.ToLower()))
                {
                    try
                    {
                        if (XtraMessageBox.Show("Are you want to change password?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return;


                        bool b = PasswordHash.ValidatePassword(objChild.txtNewPass.Text, _comp.Password);
                        if (b == true)
                        {
                            objChild.isExiting = true;
                           // XtraMessageBox.Show("This password is ready existing!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        _comp.Password = PasswordHash.HashPassword(objChild.txtNewPass.Text);



                        DB.CSUsers.Attach(_comp);
                        DB.Entry(_comp).Property(w => w.Password).IsModified = true;

                        var Row = DB.SaveChanges();
                        XtraMessageBox.Show("Your password has been successfully updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {

                    }
                }
                
            }
            else
                XtraMessageBox.Show("Your Password is incorrect!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);


        }
        
        #region --- Internal Function ---
        private Font getTitleAppearanceFont(float size)
        {
            AppearanceObject a = new AppearanceObject();
            a.Font = new System.Drawing.Font("Segoe UI Light", size,
                System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            return a.Font;
        }
        private Color getGlobalColor(int R, int G, int B)
        {
            return System.Drawing.Color.FromArgb(R, G, B);
        }
        #endregion

    }
}