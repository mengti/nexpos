﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.Helper;
using ZooMangement;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.User
{
    public partial class scrRole : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrRole()
        {
            InitializeComponent();
        }
        List<CSRole> userrole = new List<CSRole>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        public bool isSuccessful = true;
        private void FormRole_Load(object sender, EventArgs e)
        {
            userrole = DB.CSRoles.ToList();
            GC.DataSource = userrole.ToList();
            //currencies = DB.CSCurrencies.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }  
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {

                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    UserRoleSetup objForm = new UserRoleSetup("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //TODO: Binding value to comboBox or LookupEdit.
                    //GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    // objForm._cities = _cities;
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }
        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormRole_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    CSRole result = new CSRole();
                    result = (CSRole)GV.GetRow(GV.FocusedRowHandle);

                    if (result.IsLog == true)
                    {
                        XtraMessageBox.Show("Can't delete "+ result.Description + " role.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {
                            CSRole role = DB.CSRoles.FirstOrDefault(st => st.RoleID.Equals(result.RoleID));

                            DB.CSRoles.Remove(role);
                            DB.SaveChanges();
                            FormRole_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void GsSaveData(UserRoleSetup objChild)
        {
            objChild.isExiting = false;
            if (userrole.Any(x => x.RoleID.ToString() != objChild.txtRoleID.Text.Trim().ToLower()
                    && x.Description.Trim().ToLower() == objChild.txtDescr.Text.Trim().ToLower()))

            {
                objChild.isExiting = true;
                return;
            }

           

            CSRole csrole = new CSRole()
            {
                CompanyID = DACClasses.CompanyID,
                //RoleID = Convert.ToInt32(objChild.txtRoleID.Text),
                Description = objChild.txtDescr.Text,
                IsLog = false
              //  IsActive = objChild.ckActive.Checked
            };

            DB.CSRoles.Add(csrole);
            var Row = DB.SaveChanges();
            XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (Row == 1)
            {
                isSuccessful = true;
            }
            GV.FocusedRowHandle = 0;
        }
        public void GsUpdateData(UserRoleSetup objChild)
        {
            try
            {
                // && x.CompanyName.Trim().ToLower() == objChild.txtCompanyName.Text.Trim().ToLower())
                if (userrole.Any(x => x.Description.Trim().ToLower() == objChild.txtDescr.Text.Trim().ToLower()))
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtDescr.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                var _comp = DB.CSRoles.SingleOrDefault(w => w.RoleID.ToString() == objChild.txtRoleID.Text);

                if (_comp.IsLog == true)
                {
                    XtraMessageBox.Show("Can't update " + _comp.Description + " role.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

               


                _comp.CompanyID = DACClasses.CompanyID;
                //_comp.RoleID = Convert.ToInt32(objChild.txtRoleID.Text);
                _comp.Description = objChild.txtDescr.Text;
              //  _comp.IsActive = objChild.ckActive.Checked;


                DB.CSRoles.Attach(_comp);
                DB.Entry(_comp).Property(w => w.Description).IsModified = true;
             //   DB.Entry(_comp).Property(w => w.IsActive).IsModified = true;
                var Row = DB.SaveChanges();
                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                //NavButton navigator = (NavButton)sender;
                var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    UserRoleSetup objForm = new UserRoleSetup("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    CSRole result = new CSRole();

                    result = (CSRole)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        // objForm.txtCompanyID.Text = result.CompanyCD;
                        objForm.txtRoleID.Enabled = false;
                       // objForm.txtDescr.Enabled = false;
                        objForm.txtDescr.Focus();
                        objForm.txtRoleID.Text = result.RoleID.ToString();
                        objForm.txtDescr.Text = result.Description;
                      //  objForm.ckActive.Checked = result.IsActive;


                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}