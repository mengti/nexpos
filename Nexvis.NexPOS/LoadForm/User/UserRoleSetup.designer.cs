﻿namespace DgoStore.LoadForm.User
{
    partial class UserRoleSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navSave = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtRoleID = new DevExpress.XtraEditors.TextEdit();
            this.txtDescr = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRoleID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navSave);
            this.navHeader.Buttons.Add(this.navEdit);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(472, 40);
            this.navHeader.TabIndex = 3;
            this.navHeader.Text = "tileNavPane1";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Add Role";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navSave
            // 
            this.navSave.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navSave.Caption = "Save";
            this.navSave.Name = "navSave";
            this.navSave.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navSave_ElementClick);
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Update";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtRoleID);
            this.layoutControl1.Controls.Add(this.txtDescr);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(472, 234);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtRoleID
            // 
            this.txtRoleID.Location = new System.Drawing.Point(53, 58);
            this.txtRoleID.Name = "txtRoleID";
            this.txtRoleID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtRoleID.Properties.Appearance.Options.UseFont = true;
            this.txtRoleID.Size = new System.Drawing.Size(369, 22);
            this.txtRoleID.StyleController = this.layoutControl1;
            this.txtRoleID.TabIndex = 4;
            this.txtRoleID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRoleID_KeyDown);
            // 
            // txtDescr
            // 
            this.txtDescr.Location = new System.Drawing.Point(39, 68);
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Properties.Appearance.Options.UseFont = true;
            this.txtDescr.Properties.MaxLength = 200;
            this.txtDescr.Size = new System.Drawing.Size(352, 22);
            this.txtDescr.StyleController = this.layoutControl1;
            this.txtDescr.TabIndex = 0;
            this.txtDescr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRoleName_KeyDown);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtRoleID;
            this.layoutControlItem1.Location = new System.Drawing.Point(41, 27);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(373, 45);
            this.layoutControlItem1.Text = "Role ID :";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(62, 16);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem3});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(472, 234);
            this.Root.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(27, 82);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(356, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(27, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(356, 37);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.txtDescr;
            this.layoutControlItem2.Location = new System.Drawing.Point(27, 37);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(356, 45);
            this.layoutControlItem2.Text = "Description : ";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(77, 16);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(27, 214);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(383, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(69, 214);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(27, 105);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(356, 109);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // UserRoleSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 274);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Name = "UserRoleSetup";
            this.Text = "Add Role";
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRoleID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        public DevExpress.XtraEditors.TextEdit txtRoleID;
        public DevExpress.XtraEditors.TextEdit txtDescr;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraBars.Navigation.NavButton navSave;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}