﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using DataAccessAsync;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.Helper;
using Nexvis.NexPOS.LoadForm.User;
//using GMPMS.Helpers;

namespace Nexvis.NexPOS.User
{
    public partial class FrmUserPermission : DevExpress.XtraEditors.XtraForm
    {
        public FrmUserPermission()
        {
            InitializeComponent();
            btnExiting.Enabled = true;
        }
        #region********************Variable Declaration**********
        readonly Controls controls = new Controls();
        readonly FVLPermissionUser usersGroup = new FVLPermissionUser();
        //DataManager _dataManager = new DataManager();
        ZooEntity DB = new ZooEntity();
        private string PERTYPEDET;
        #endregion

        public string PERTYPEDET_
        {
            get { return PERTYPEDET; }
        }

       
        private void SYSTEMSETUPLVWMAIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (btnApplySystemSetup.Enabled)
                {
                    if (XtraMessageBox.Show("Are you sure you want to apply any changes ?", Application.ProductName, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        btnApplySystemSetup_Click(null, null);
                    }
                }
                Cursor = Cursors.WaitCursor;
                FVLPermissionUser.Action_Check = false;
                btnApplySystemSetup.Enabled = false;
                SYSTEMSETUPLVWDETAIL.Items.Clear();
                SYSTEMSETUPLVWDETAIL.Groups.Clear();
                var lvItem = new ListViewItem();
                lvItem = null;
                var lvGroup = new ListViewGroup();
                lvGroup = null;
                var is_Selected = false;
                foreach (ListViewItem lvwItem in SYSTEMSETUPLVWMAIN.Items)
                {
                    if (lvwItem.Selected == true)
                    {
                        lvItem = lvwItem;
                        is_Selected = true;
                        break;
                    }
                }
                if (is_Selected == false)
                {
                    Cursor = Cursors.Default;
                    return;
                }
                FVLPermissionUser.GR_DB_CODE = lvItem.Text;
                switch (FVLPermissionUser.GR_DB_CODE)
                {
                    // ************** User Permission. ************
                    case "User Management":
                        //PERTYPEDET = "URSMA";
                        FVLPermissionUser.SCREENID = "scrPermission";
                        lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "Permission", "Export");
                        break;
                    // ************** Company Setup. ************
                    case "Company Profile":
                        //PERTYPEDET = "CP";
                        FVLPermissionUser.SCREENID = "scrCompany";
                        lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                    case "Role Setup":
                        //PERTYPEDET = "PS";
                        PERTYPEDET = "scrRole";
                        FVLPermissionUser.SCREENID = PERTYPEDET;
                        lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                    case "Unit Of Measure":
                        //PERTYPEDET = "PS";
                        PERTYPEDET = "scrUOM";
                        FVLPermissionUser.SCREENID = PERTYPEDET;
                        lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                    case "Exchange Rate":
                        //PERTYPEDET = "PS";
                        PERTYPEDET = "scrCuryRate";
                        FVLPermissionUser.SCREENID = PERTYPEDET;
                        lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                    case "Cashier Amount List":
                        //PERTYPEDET = "PS";
                        PERTYPEDET = "scrCashierBalance";
                        FVLPermissionUser.SCREENID = PERTYPEDET;
                        lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "New", "Edit", "Close Balance", "Delete", "Export");
                        break;
                    case "User Setup":
                        //PERTYPEDET = "PS";
                        PERTYPEDET = "scrUser";
                        FVLPermissionUser.SCREENID = PERTYPEDET;
                        lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "New", "Edit", "Delete","Change Password", "Export");
                        break;

                    //case "PaymentMethod Setup":
                    //    //PERTYPEDET = "PS";
                    //    PERTYPEDET = "scrPayment";
                    //    FVLPermissionUser.SCREENID = PERTYPEDET;
                    //    lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                    //    controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                    //    break;

                    case "Employee Profile":
                        // PERTYPEDET = "EP";
                        FVLPermissionUser.SCREENID = "scrEmployee";
                        lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                    // ************** General Setup. ************
                    case "Customer Setup":
                        //PERTYPEDET = "CP";
                        FVLPermissionUser.SCREENID = "scrCustomer";
                        lvGroup = SYSTEMSETUPLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(SYSTEMSETUPLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                }
                if (SYSTEMSETUPLVWDETAIL.Items.Count > 0)
                {
                    bool isNoCheck = false;
                    foreach (ListViewItem item in SYSTEMSETUPLVWDETAIL.Items)
                    {
                        if (DB.CSRoleItems.Any(x => x.RoleId == FVLPermissionUser.SELECTED_ROLEID && x.ActionName == item.Text && x.ScreenId == FVLPermissionUser.SCREENID))
                        {
                            item.Checked = true;
                        }
                        else
                        {
                            item.Checked = false;
                            isNoCheck = true;
                        }
                    }
                    if (isNoCheck == true)
                    {
                        CheckBox7.CheckedChanged -= CheckBox7_CheckedChanged;
                        CheckBox7.Checked = false;
                        CheckBox7.CheckedChanged += CheckBox7_CheckedChanged;
                    }
                    else
                    {
                        CheckBox7.CheckedChanged -= CheckBox7_CheckedChanged;
                        CheckBox7.Checked = true;
                        CheckBox7.CheckedChanged += CheckBox7_CheckedChanged;
                    }
                    FVLPermissionUser.Action_Check = true;
                }
                Cursor = Cursors.Default;
            }
            catch
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnApplySystemSetup_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                //usersGroup.DeleteUserPermission();
                usersGroup.SaveUserPermission(SYSTEMSETUPLVWDETAIL);
                btnApplySystemSetup.Enabled = false;
                Cursor = Cursors.Default;
            }
            catch
            {
                Cursor = Cursors.Default;
            }
        }

        private void SYSTEMSETUPLVWDETAIL_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (FVLPermissionUser.Action_Check)
            {
                btnApplySystemSetup.Enabled = true;
            }
            if (isFullCheck == false)
            {
                int i = 0;
                foreach (ListViewItem l in SYSTEMSETUPLVWDETAIL.Items)
                {
                    if (l.Checked)
                        i += 1;
                }
                if (i == SYSTEMSETUPLVWDETAIL.Items.Count)
                {
                    CheckBox7.CheckedChanged -= CheckBox7_CheckedChanged;
                    CheckBox7.Checked = true;
                    CheckBox7.CheckedChanged += CheckBox7_CheckedChanged;
                }
                else
                {
                    CheckBox7.CheckedChanged -= CheckBox7_CheckedChanged;
                    CheckBox7.Checked = false;
                    CheckBox7.CheckedChanged += CheckBox7_CheckedChanged;
                }
            }
            isFullCheck = false;
        }
        private void PERMISSION_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                // first_load = false;
                var tab = TabGrant.TabPages[TabGrant.SelectedTabPageIndex];
                var lv = new ListView();
                int i = 0;
                switch (tab.Text)
                {
                    case "System":
                        lv = SYSTEMSETUPLVWMAIN;
                        SYSTEMSETUPLVWDETAIL.Items.Clear();
                        SYSTEMSETUPLVWDETAIL.Groups.Clear();
                        SYSTEMSETUPLVWMAIN.SelectedIndexChanged -= SYSTEMSETUPLVWMAIN_SelectedIndexChanged;
                        lv.SelectedItems.Clear();
                        SYSTEMSETUPLVWMAIN.SelectedIndexChanged += SYSTEMSETUPLVWMAIN_SelectedIndexChanged;
                        break;
                    case "Management":
                        lv = MANAGEMENTLVWMAIN;
                        MANAGEMENTLVWDETAIL.Items.Clear();
                        MANAGEMENTLVWDETAIL.Groups.Clear();
                        MANAGEMENTLVWMAIN.SelectedIndexChanged -= MANAGEMENTLVWMAIN_SelectedIndexChanged;
                        lv.SelectedItems.Clear();
                        MANAGEMENTLVWMAIN.SelectedIndexChanged += MANAGEMENTLVWMAIN_SelectedIndexChanged;
                        break;
                    
                    case "POS":
                        lv = POSLVWMAIN;
                        POSLVWDETAIL.Items.Clear();
                        POSLVWDETAIL.Groups.Clear();
                        POSLVWMAIN.SelectedIndexChanged -= POSLVWMAIN_SelectedIndexChanged;
                        lv.SelectedItems.Clear();
                        POSLVWMAIN.SelectedIndexChanged += POSLVWMAIN_SelectedIndexChanged;
                        break;
                }
                if (lv.Items.Count != 0)
                {
                    SYSTEMSETUPLVWMAIN.ItemChecked -= SYSTEMSETUPLVWMAIN_ItemChecked;
                    MANAGEMENTLVWMAIN.ItemChecked -= MANAGEMENTLVWMAIN_ItemChecked;
                    POSLVWMAIN.ItemChecked -= POSLVWMAIN_ItemChecked;
                    foreach (ListViewItem item in lv.Items)
                    {
                        if (DB.CSRoleItems.Any(x => x.RoleId == FVLPermissionUser.SELECTED_ROLEID && x.ScreenId == item.Tag))
                        {
                            item.Checked = true;
                        }
                        else
                        {
                            item.Checked = false;
                        }
                        if (item.Checked)
                            i += 1;
                        var dd = FVLPermissionUser.SELECTED_USERID;
                        if (DB.CSRoleItems.Any(x => x.RoleId == FVLPermissionUser.SELECTED_ROLEID && x.ScreenId == item.Tag))
                        {
                            item.Font = new Font(item.Font.Name, item.Font.Size, FontStyle.Bold);
                        }
                    }
                    if (i == lv.Items.Count)
                    {
                        if (lv.Name == "SYSTEMSETUPLVWMAIN")
                        {
                            CheckBox5.CheckedChanged -= CheckBox5_CheckedChanged;
                            CheckBox5.Checked = true;
                            CheckBox5.CheckedChanged += CheckBox5_CheckedChanged;
                        }
                        else if (lv.Name == "MANAGEMENTLVWMAIN")
                        {
                            CheckBox3.CheckedChanged -= CheckBox3_CheckedChanged;
                            CheckBox3.Checked = true;
                            CheckBox3.CheckedChanged += CheckBox3_CheckedChanged;
                        }
                        else if (lv.Name == "POSLVWMAIN")
                        {
                            CheckBox1.CheckedChanged -= CheckBox1_CheckedChanged;
                            CheckBox1.Checked = true;
                            CheckBox1.CheckedChanged += CheckBox1_CheckedChanged;
                        }
                    }
                    else
                    {
                        if (lv.Name == "SYSTEMSETUPLVWMAIN")
                        {
                            CheckBox5.CheckedChanged -= CheckBox5_CheckedChanged;
                            CheckBox5.Checked = false;
                            CheckBox5.CheckedChanged += CheckBox5_CheckedChanged;
                        }
                        else if (lv.Name == "MANAGEMENTLVWMAIN")
                        {
                            CheckBox3.CheckedChanged -= CheckBox3_CheckedChanged;
                            CheckBox3.Checked = false;
                            CheckBox3.CheckedChanged += CheckBox3_CheckedChanged;
                        }
                        else if (lv.Name == "POSLVWMAIN")
                        {
                            CheckBox1.CheckedChanged -= CheckBox1_CheckedChanged;
                            CheckBox1.Checked = false;
                            CheckBox1.CheckedChanged += CheckBox1_CheckedChanged;
                        }
                    }

                    SYSTEMSETUPLVWMAIN.ItemChecked += SYSTEMSETUPLVWMAIN_ItemChecked;
                    MANAGEMENTLVWMAIN.ItemChecked += MANAGEMENTLVWMAIN_ItemChecked;
                    POSLVWMAIN.ItemChecked += POSLVWMAIN_ItemChecked;

                }

                Cursor = Cursors.Default;
            }
            catch
            {
                Cursor = Cursors.Default;
            }
        }
        private void SYSTEMSETUPLVWMAIN_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            //usersGroup.LVWMainOnEachTab(e);
            if (isFullCheck == false)
            {
                int i = 0;
                foreach (ListViewItem l in SYSTEMSETUPLVWMAIN.Items)
                {
                    if (l.Checked)
                        i += 1;
                }
                if (i == SYSTEMSETUPLVWMAIN.Items.Count)
                {
                    CheckBox5.CheckedChanged -= CheckBox5_CheckedChanged;
                    CheckBox5.Checked = true;
                    CheckBox5.CheckedChanged += CheckBox5_CheckedChanged;
                }
                else
                {
                    CheckBox5.CheckedChanged -= CheckBox5_CheckedChanged;
                    CheckBox5.Checked = false;
                    CheckBox5.CheckedChanged += CheckBox5_CheckedChanged;
                }
            }
            isFullCheck = false;
            Cursor = Cursors.Default;
        }

        private void CheckBox7_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in SYSTEMSETUPLVWDETAIL.Items)
            {
                isFullCheck = true;
                item.Checked = CheckBox7.Checked;
            }
        }

        private void CheckBox5_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in SYSTEMSETUPLVWMAIN.Items)
            {
                isFullCheck = true;
                item.Checked = CheckBox5.Checked;
            }
        }

        private void frmUserPermission_Load(object sender, EventArgs e)
        {            
            if (SYSTEMSETUPLVWMAIN.Items.Count != 0)
            {
                int i = 0;
                SYSTEMSETUPLVWMAIN.SelectedIndexChanged -= SYSTEMSETUPLVWMAIN_SelectedIndexChanged;
                SYSTEMSETUPLVWMAIN.ItemChecked -= SYSTEMSETUPLVWMAIN_ItemChecked;
                foreach (ListViewItem item in SYSTEMSETUPLVWMAIN.Items)
                {
                    if (DB.CSRoleItems.Any(x => x.RoleId == FVLPermissionUser.SELECTED_ROLEID && x.ScreenId == item.Tag))
                    {
                        item.Checked = true;
                    }
                    else
                    {
                        item.Checked = false;
                    }
                    if (item.Checked)
                        i += 1;
                    var dd = FVLPermissionUser.SELECTED_USERID;
                    if (DB.CSRoleItems.Any(x => x.RoleId == FVLPermissionUser.SELECTED_ROLEID && x.ActionName == item.Text))
                    {
                        item.Font = new Font(item.Font.Name, item.Font.Size, FontStyle.Bold);
                    }
                }
                if (i == SYSTEMSETUPLVWMAIN.Items.Count)
                {
                    CheckBox5.CheckedChanged -= CheckBox5_CheckedChanged;
                    CheckBox5.Checked = true;
                    CheckBox5.CheckedChanged += CheckBox5_CheckedChanged;
                }
                else
                {
                    CheckBox5.CheckedChanged -= CheckBox5_CheckedChanged;
                    CheckBox5.Checked = false;
                    CheckBox5.CheckedChanged += CheckBox5_CheckedChanged;
                }
                SYSTEMSETUPLVWMAIN.SelectedIndexChanged += SYSTEMSETUPLVWMAIN_SelectedIndexChanged;
                SYSTEMSETUPLVWMAIN.ItemChecked += SYSTEMSETUPLVWMAIN_ItemChecked;
            }
        }
        bool isFullCheck = false;
        private void CheckBox3_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in MANAGEMENTLVWMAIN.Items)
            {
                isFullCheck = true;
                item.Checked = CheckBox3.Checked;
            }
        }
        private void chbMaintains_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in MANAGEMENTLVWDETAIL.Items)
            {
                isFullCheck = true;
                item.Checked = checkbox10.Checked;
            }
        }
        private void MANAGEMENTLVWMAIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (btnApplyManagement.Enabled)
                {
                    if (XtraMessageBox.Show("Are you sure you want to apply any changes ?", Application.ProductName, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        btnApplyManagement_Click(null, null);
                    }
                }
                Cursor = Cursors.WaitCursor;
                FVLPermissionUser.Action_Check = false;
                btnApplyManagement.Enabled = false;
                MANAGEMENTLVWDETAIL.Items.Clear();
                MANAGEMENTLVWDETAIL.Groups.Clear();
                var lvItem = new ListViewItem();
                lvItem = null;
                var lvGroup = new ListViewGroup();
                lvGroup = null;
                var is_Selected = false;
                foreach (ListViewItem lvwItem in MANAGEMENTLVWMAIN.Items)
                {
                    if (lvwItem.Selected == true)
                    {
                        lvItem = lvwItem;
                        is_Selected = true;
                        break;
                    }
                }
                if (is_Selected == false)
                {
                    Cursor = Cursors.Default;
                    return;
                }
                FVLPermissionUser.GR_DB_CODE = lvItem.Text;
                switch (FVLPermissionUser.GR_DB_CODE)
                {
                    //********* App ************
                    case "Dashboard":
                        //PERTYPEDET = "SC";
                        FVLPermissionUser.SCREENID = "scrDashboard";
                        lvGroup = MANAGEMENTLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(MANAGEMENTLVWDETAIL, lvGroup, "Preview");
                        break;
            
                    case "Store Information":
                        FVLPermissionUser.SCREENID = "scrStoreInfor";
                        lvGroup = MANAGEMENTLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(MANAGEMENTLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                  
                    //******* Report
                    case "Daily Invoice":
                        //PERTYPEDET = "FR";
                        FVLPermissionUser.SCREENID = "scrDailyInvoice";
                        lvGroup = MANAGEMENTLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(MANAGEMENTLVWDETAIL, lvGroup, "Preview");
                        break;

                    case "Details Invoice":
                        //PERTYPEDET = "CA";
                        FVLPermissionUser.SCREENID = "scrDetailsInvoice";
                        lvGroup = MANAGEMENTLVWDETAIL.Groups.Add("FVL", "General");
                        //controls.AddItemToLVGroup(MANAGEMENTLVWDETAIL, lvGroup, "Attendance Record", "Chat Room", "Refresh");
                        controls.AddItemToLVGroup(MANAGEMENTLVWDETAIL, lvGroup, "Preview");
                        break;
                    case "Barcode":
                        //PERTYPEDET = "CA";
                        FVLPermissionUser.SCREENID = "scrBarcode";
                        lvGroup = MANAGEMENTLVWDETAIL.Groups.Add("FVL", "General");
                        //controls.AddItemToLVGroup(MANAGEMENTLVWDETAIL, lvGroup, "Attendance Record", "Chat Room", "Refresh");
                        controls.AddItemToLVGroup(MANAGEMENTLVWDETAIL, lvGroup, "Preview");
                        break;

                }

                if (MANAGEMENTLVWDETAIL.Items.Count > 0)
                {
                    bool isNoCheck = false;
                    foreach (ListViewItem item in MANAGEMENTLVWDETAIL.Items)
                    {
                        if (DB.CSRoleItems.Any(x => x.RoleId == FVLPermissionUser.SELECTED_ROLEID && x.ActionName == item.Text && x.ScreenId == FVLPermissionUser.SCREENID))
                        {
                            item.Checked = true;
                        }
                        else
                        {
                            item.Checked = false;
                            isNoCheck = true;
                        }
                    }
                    if (isNoCheck == true)
                    {
                        checkbox10.CheckedChanged -= chbMaintains_CheckedChanged;
                        checkbox10.Checked = false;
                        checkbox10.CheckedChanged += chbMaintains_CheckedChanged;
                    }
                    else
                    {
                        checkbox10.CheckedChanged -= chbMaintains_CheckedChanged;
                        checkbox10.Checked = true;
                        checkbox10.CheckedChanged += chbMaintains_CheckedChanged;
                    }
                    FVLPermissionUser.Action_Check = true;
                }
                Cursor = Cursors.Default;
            }
            catch
            {
                Cursor = Cursors.Default;
            }
        }
        private void btnApplyManagement_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                //usersGroup.DeleteUserPermission();
                usersGroup.SaveUserPermission(MANAGEMENTLVWDETAIL);
                btnApplyManagement.Enabled = false;
                Cursor = Cursors.Default;
            }
            catch
            {
                Cursor = Cursors.Default;
            }
        }
        private void MANAGEMENTLVWDETAIL_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (FVLPermissionUser.Action_Check)
            {
                btnApplyManagement.Enabled = true;
            }
            if (isFullCheck == false)
            {
                int i = 0;
                foreach (ListViewItem l in MANAGEMENTLVWDETAIL.Items)
                {
                    if (l.Checked)
                        i += 1;
                }
                if (i == MANAGEMENTLVWDETAIL.Items.Count)
                {
                    checkbox10.CheckedChanged -= chbMaintains_CheckedChanged;
                    checkbox10.Checked = true;
                    checkbox10.CheckedChanged += chbMaintains_CheckedChanged;
                }
                else
                {
                    checkbox10.CheckedChanged -= chbMaintains_CheckedChanged;
                    checkbox10.Checked = false;
                    checkbox10.CheckedChanged += chbMaintains_CheckedChanged;
                }
            }
            isFullCheck = false;
        }
        private void chkAll1_CheckedChanged(object sender, EventArgs e)
        {
            //foreach (ListViewItem item in CLASSESTYPELVWMAIN.Items)
            //{
            //    isFullCheck = true;
            //    item.Checked = checkBox4.Checked;
            //}
        }
        private void chkAll2_CheckedChanged(object sender, EventArgs e)
        {
            //foreach (ListViewItem item in TRANSACTIONLVWDETAIL.Items)
            //{
            //    isFullCheck = true;
            //    item.Checked = CheckBox6.Checked;
            //}
        }   
        private void MANAGEMENTLVWMAIN_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            //usersGroup.LVWMainOnEachTab(e);
            if (isFullCheck == false)
            {
                int i = 0;
                foreach (ListViewItem l in MANAGEMENTLVWMAIN.Items)
                {
                    if (l.Checked)
                        i += 1;
                }
                if (i == MANAGEMENTLVWMAIN.Items.Count)
                {
                    CheckBox3.CheckedChanged -= CheckBox3_CheckedChanged;
                    CheckBox3.Checked = true;
                    CheckBox3.CheckedChanged += CheckBox3_CheckedChanged;
                }
                else
                {
                    CheckBox3.CheckedChanged -= CheckBox3_CheckedChanged;
                    CheckBox3.Checked = false;
                    CheckBox3.CheckedChanged += CheckBox3_CheckedChanged;
                }
            }
            isFullCheck = false;
            Cursor = Cursors.Default;
        } 
        private void TabGrant_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            PERMISSION_SelectedIndexChanged(null, null);
        }
        private void btnExiting_Click(object sender, EventArgs e)
        {
            // TODO: Comfirm before save.
            if (XtraMessageBox.Show("Are you sure you want to close ?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) 
                return;
            Close();
            //frmLogin frm = new frmLogin();
            ////FormShadow shadow = new FormShadow(frm);
            //this.Close();
            //frm.ShowDialog();

        }
        private void btnApplyPOS_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                //usersGroup.DeleteUserPermission();
                usersGroup.SaveUserPermission(POSLVWDETAIL);
                btnApplyPOS.Enabled = false;
                Cursor = Cursors.Default;
            }
            catch
            {
                Cursor = Cursors.Default;
            }
        }
        private void POSLVWMAIN_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
          //  usersGroup.LVWMainOnEachTab(e);
            if (isFullCheck == false)
            {
                int i = 0;
                foreach (ListViewItem l in POSLVWMAIN.Items)
                {
                    if (l.Checked)
                        i += 1;
                }
                if (i == POSLVWMAIN.Items.Count)
                {
                    CheckBox1.CheckedChanged -= CheckBox1_CheckedChanged;
                    CheckBox1.Checked = true;
                    CheckBox1.CheckedChanged += CheckBox1_CheckedChanged;
                }
                else
                {
                    CheckBox1.CheckedChanged -= CheckBox1_CheckedChanged;
                    CheckBox1.Checked = false;
                    CheckBox1.CheckedChanged += CheckBox1_CheckedChanged;
                }
            }
            isFullCheck = false;
            Cursor = Cursors.Default;
        }
        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in POSLVWMAIN.Items)
            {
                isFullCheck = true;
                item.Checked = CheckBox1.Checked;
            }
        }
        private void POSLVWMAIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (btnApplyPOS.Enabled)
                {
                    if (XtraMessageBox.Show("Are you sure you want to apply any changes ?", Application.ProductName, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        btnApplyPOS_Click(null, null);
                    }
                }
                Cursor = Cursors.WaitCursor;
                FVLPermissionUser.Action_Check = false;
                btnApplyPOS.Enabled = false;
                POSLVWDETAIL.Items.Clear();
                POSLVWDETAIL.Groups.Clear();
                var lvItem = new ListViewItem();
                lvItem = null;
                var lvGroup = new ListViewGroup();
                lvGroup = null;
                var is_Selected = false;
                foreach (ListViewItem lvwItem in POSLVWMAIN.Items)
                {
                    if (lvwItem.Selected == true)
                    {
                        lvItem = lvwItem;
                        is_Selected = true;
                        break;
                    }
                }
                if (is_Selected == false)
                {
                    Cursor = Cursors.Default;
                    return;
                }
                FVLPermissionUser.GR_DB_CODE = lvItem.Text;
                switch (FVLPermissionUser.GR_DB_CODE)
                {
                    //********* Product ************
                    case "Item Category":
                        //PERTYPEDET = "IC";
                        FVLPermissionUser.SCREENID = "scrCategory";
                        lvGroup = POSLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(POSLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                    case "Product":
                        //PERTYPEDET = "PB";
                        FVLPermissionUser.SCREENID = "scrItem";
                        lvGroup = POSLVWDETAIL.Groups.Add("FVL", "General");
                        //controls.AddItemToLVGroup(POSLVWDETAIL, lvGroup, "Export", "Print Barcode");
                        controls.AddItemToLVGroup(POSLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                    //********* Price ************
                    case "Price":
                        //PERTYPEDET = "PC";
                        FVLPermissionUser.SCREENID = "scrSalePrice";
                        lvGroup = POSLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(POSLVWDETAIL, lvGroup, "New", "Edit", "Delete", "Export");
                        break;
                    //********* Sale Management ************
                    case "Sale Management":
                        //PERTYPEDET = "SM";
                        FVLPermissionUser.SCREENID = "scrSaleOrder";
                        lvGroup = POSLVWDETAIL.Groups.Add("FVL", "General");
                        controls.AddItemToLVGroup(POSLVWDETAIL, lvGroup, "List Invoice");
                            //,"Receipt", "Pay Bill");
                        break;
                }

                if (POSLVWDETAIL.Items.Count > 0)
                {
                    bool isNoCheck = false;
                    foreach (ListViewItem item in POSLVWDETAIL.Items)
                    {
                        if (DB.CSRoleItems.Any(x => x.RoleId == FVLPermissionUser.SELECTED_ROLEID && x.ActionName == item.Text && x.ScreenId == FVLPermissionUser.SCREENID))
                        {
                            item.Checked = true;
                        }
                        else
                        {
                            item.Checked = false;
                            isNoCheck = true;
                        }
                    }
                    if (isNoCheck == true)
                    {
                        CheckBox11.CheckedChanged -= chbPOStains_CheckedChanged;
                        CheckBox11.Checked = false;
                        CheckBox11.CheckedChanged += chbPOStains_CheckedChanged;
                    }
                    else
                    {
                        CheckBox11.CheckedChanged -= chbPOStains_CheckedChanged;
                        CheckBox11.Checked = true;
                        CheckBox11.CheckedChanged += chbPOStains_CheckedChanged;
                    }
                    FVLPermissionUser.Action_Check = true;
                }
                Cursor = Cursors.Default;
            }
            catch
            {
                Cursor = Cursors.Default;
            }
        }
          private void chbPOStains_CheckedChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem item in POSLVWDETAIL.Items)
            {
                isFullCheck = true;
                item.Checked = CheckBox11.Checked;
            }
        }

        private void POSLVWDETAIL_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (FVLPermissionUser.Action_Check)
            {
                btnApplyPOS.Enabled = true;
            }
            if (isFullCheck == false)
            {
                int i = 0;
                foreach (ListViewItem l in POSLVWDETAIL.Items)
                {
                    if (l.Checked)
                        i += 1;
                }
                if (i == POSLVWDETAIL.Items.Count)
                {
                    CheckBox11.CheckedChanged -= chbPOStains_CheckedChanged;
                    CheckBox11.Checked = true;
                    CheckBox11.CheckedChanged += chbPOStains_CheckedChanged;
                }            
                else         
                {
                    CheckBox11.CheckedChanged -= chbPOStains_CheckedChanged;
                    CheckBox11.Checked = false;
                    CheckBox11.CheckedChanged += chbPOStains_CheckedChanged;
                }
            }
            isFullCheck = false;
        }


        #region Old Flow
        #region --- Variable Declaration ---
        private scrPermission parentForm;

        #endregion
        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //if (GlobalInitializer.GfCheckNullValue(txtRoleID) == false)
            
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close(); // this.Parent.Controls.Remove(this);
            parentForm.navRefresh_ElementClick(sender, e);
        }
        #endregion
    }
}
