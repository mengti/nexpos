﻿using DevExpress.XtraEditors;
using DgoStore.LoadForm.User;
using Nexvis.NexPOS.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nexvis.NexPOS.LoadForm.User
{
    public partial class ChangePassword : DevExpress.XtraEditors.XtraForm
    {
        public bool isExiting = false;
        private bool isNew = false;
        public string userText { get; set; }
        public int roleText { get; set; }
        private scrUser parentForm;
        public ChangePassword(string transactionType, scrUser objParent)
        {
            InitializeComponent();
            parentForm = objParent;
            txtCurrenPass.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            var result = XtraMessageBox.Show("Are you sure you want to cancel?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes) this.Close();
        }

        private void btnChangePass_Click(object sender, EventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtCurrenPass,txtNewPass,txtConfirmPass) == false)
            {
                parentForm.GsChangePas(this);

                // TODO: Validate which record exiting.
                if (isExiting && parentForm.isSuccessful)
                {
                    XtraMessageBox.Show("This password ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNewPass.Focus();
                    return;
                }
                this.Close();
            }
        }

        private void txtConfrimPass_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtConfirmPass_Validating(object sender, CancelEventArgs e)
        {
            if (txtConfirmPass.Text != txtNewPass.Text)
            {
                XtraMessageBox.Show("Password must match !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtConfirmPass.Text = null;
            }
        }
    }
}