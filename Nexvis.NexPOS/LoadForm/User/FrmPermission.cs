﻿using DevComponents.DotNetBar.Controls;
using DevExpress.XtraBars.Navigation;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DgoStore.LoadForm.User;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.Helper;
using Nexvis.NexPOS.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZooMangement;

namespace Nexvis.NexPOS.LoadForm.User
{
    public partial class scrPermission : DevExpress.XtraEditors.XtraForm
    {
        public scrPermission()
        {
            InitializeComponent();
        }
        ZooEntity DB = new ZooEntity();
        List<CSRole> userrole = new List<CSRole>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        public bool isSuccessful = true;
        private void FrmPermission_Load(object sender, EventArgs e)
        {
            userrole = DB.CSRoles.ToList();
            GC.DataSource = userrole.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
        }
        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }
        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FrmPermission_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }
        private void navPermission_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                var Permission = "Permission";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == Permission))
                {
                    CSRole result = new CSRole();
                    result = (CSRole)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        FVLPermissionUser.SELECTED_ROLEID = Convert.ToInt32(result.RoleID);
                    }

                    FrmUserPermission objForm = new FrmUserPermission();
                    FormShadow Shadow = new FormShadow(objForm);
                    Shadow.ShowDialog();

                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + Permission + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Permission"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navPermission_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Permission !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void navExport_ElementClick(object sender, NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}