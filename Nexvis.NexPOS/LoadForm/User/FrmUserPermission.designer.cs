﻿namespace Nexvis.NexPOS.User
{
    partial class FrmUserPermission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUserPermission));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Grant Access", "Grant.ico");
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Company Setup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("User Permission", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Membership Setup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("Currency", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup5 = new System.Windows.Forms.ListViewGroup("General", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("User Setup");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Role Setup");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Employee Profile");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("User Management");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("Unit Of Measure");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("Company Profile");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("Exchange Rate");
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("Customer Setup");
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem("Cashier Amount List");
            System.Windows.Forms.ListViewGroup listViewGroup6 = new System.Windows.Forms.ListViewGroup("App", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup7 = new System.Windows.Forms.ListViewGroup("Report", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem("Dashboard");
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem("Store Information");
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem("Daily Invoice");
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem("Details Invoice");
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem("Barcode");
            System.Windows.Forms.ListViewGroup listViewGroup8 = new System.Windows.Forms.ListViewGroup("Product", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup9 = new System.Windows.Forms.ListViewGroup("Price", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup10 = new System.Windows.Forms.ListViewGroup("Discount", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup11 = new System.Windows.Forms.ListViewGroup("Sales Management", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem("Item Category");
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem("Product");
            System.Windows.Forms.ListViewItem listViewItem18 = new System.Windows.Forms.ListViewItem("Price");
            System.Windows.Forms.ListViewItem listViewItem19 = new System.Windows.Forms.ListViewItem("Sale Management");
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.listViewEx1 = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnExiting = new DevExpress.XtraEditors.SimpleButton();
            this.TabGrant = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.CheckBox5 = new DevExpress.XtraEditors.CheckEdit();
            this.SYSTEMSETUPLVWMAIN = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CheckBox7 = new DevExpress.XtraEditors.CheckEdit();
            this.btnApplySystemSetup = new DevExpress.XtraEditors.SimpleButton();
            this.SYSTEMSETUPLVWDETAIL = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.CheckBox3 = new DevExpress.XtraEditors.CheckEdit();
            this.MANAGEMENTLVWMAIN = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.checkbox10 = new DevExpress.XtraEditors.CheckEdit();
            this.btnApplyManagement = new DevExpress.XtraEditors.SimpleButton();
            this.MANAGEMENTLVWDETAIL = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.CheckBox1 = new DevExpress.XtraEditors.CheckEdit();
            this.POSLVWMAIN = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CheckBox11 = new DevExpress.XtraEditors.CheckEdit();
            this.btnApplyPOS = new DevExpress.XtraEditors.SimpleButton();
            this.POSLVWDETAIL = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tileNavCategory1 = new DevExpress.XtraBars.Navigation.TileNavCategory();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navSave = new DevExpress.XtraBars.Navigation.NavButton();
            this.navNew = new DevExpress.XtraBars.Navigation.NavButton();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabGrant)).BeginInit();
            this.TabGrant.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox7.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkbox10.Properties)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "Permission.ico");
            this.ImageList1.Images.SetKeyName(1, "Grant.ico");
            this.ImageList1.Images.SetKeyName(2, "Deny.ico");
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.splitContainerControl1.Appearance.Options.UseFont = true;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.listViewEx1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.panelControl2);
            this.splitContainerControl1.Panel2.Controls.Add(this.TabGrant);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1399, 631);
            this.splitContainerControl1.SplitterPosition = 351;
            this.splitContainerControl1.TabIndex = 9;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // listViewEx1
            // 
            // 
            // 
            // 
            this.listViewEx1.Border.Class = "ListViewBorder";
            this.listViewEx1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.listViewEx1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9});
            this.listViewEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewEx1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.listViewEx1.HideSelection = false;
            listViewItem1.StateImageIndex = 0;
            this.listViewEx1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.listViewEx1.Location = new System.Drawing.Point(0, 0);
            this.listViewEx1.Margin = new System.Windows.Forms.Padding(4);
            this.listViewEx1.MultiSelect = false;
            this.listViewEx1.Name = "listViewEx1";
            this.listViewEx1.Size = new System.Drawing.Size(351, 631);
            this.listViewEx1.SmallImageList = this.ImageList1;
            this.listViewEx1.TabIndex = 0;
            this.listViewEx1.UseCompatibleStateImageBehavior = false;
            this.listViewEx1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "System Privilege";
            this.columnHeader9.Width = 262;
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.panelControl2.Controls.Add(this.btnExiting);
            this.panelControl2.Location = new System.Drawing.Point(997, 1);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(40, 23);
            this.panelControl2.TabIndex = 1;
            // 
            // btnExiting
            // 
            this.btnExiting.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExiting.Appearance.Options.UseFont = true;
            this.btnExiting.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnExiting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExiting.Enabled = false;
            this.btnExiting.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExiting.ImageOptions.Image")));
            this.btnExiting.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnExiting.Location = new System.Drawing.Point(2, 2);
            this.btnExiting.Margin = new System.Windows.Forms.Padding(4);
            this.btnExiting.Name = "btnExiting";
            this.btnExiting.Size = new System.Drawing.Size(36, 19);
            this.btnExiting.TabIndex = 10;
            this.btnExiting.Click += new System.EventHandler(this.btnExiting_Click);
            // 
            // TabGrant
            // 
            this.TabGrant.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TabGrant.Appearance.Options.UseFont = true;
            this.TabGrant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabGrant.Location = new System.Drawing.Point(0, 0);
            this.TabGrant.Margin = new System.Windows.Forms.Padding(4);
            this.TabGrant.Name = "TabGrant";
            this.TabGrant.SelectedTabPage = this.xtraTabPage1;
            this.TabGrant.Size = new System.Drawing.Size(1038, 631);
            this.TabGrant.TabIndex = 0;
            this.TabGrant.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage5});
            this.TabGrant.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.TabGrant_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.xtraTabPage1.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage1.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1036, 603);
            this.xtraTabPage1.Text = "System";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.CheckBox5);
            this.splitContainerControl2.Panel1.Controls.Add(this.SYSTEMSETUPLVWMAIN);
            this.splitContainerControl2.Panel2.Controls.Add(this.CheckBox7);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnApplySystemSetup);
            this.splitContainerControl2.Panel2.Controls.Add(this.SYSTEMSETUPLVWDETAIL);
            this.splitContainerControl2.Size = new System.Drawing.Size(1036, 603);
            this.splitContainerControl2.SplitterPosition = 495;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // CheckBox5
            // 
            this.CheckBox5.EnterMoveNextControl = true;
            this.CheckBox5.Location = new System.Drawing.Point(5, 4);
            this.CheckBox5.Margin = new System.Windows.Forms.Padding(4);
            this.CheckBox5.Name = "CheckBox5";
            this.CheckBox5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.CheckBox5.Properties.Appearance.Options.UseFont = true;
            this.CheckBox5.Properties.Caption = "";
            this.CheckBox5.Size = new System.Drawing.Size(17, 20);
            this.CheckBox5.TabIndex = 8;
            this.CheckBox5.CheckedChanged += new System.EventHandler(this.CheckBox5_CheckedChanged);
            // 
            // SYSTEMSETUPLVWMAIN
            // 
            // 
            // 
            // 
            this.SYSTEMSETUPLVWMAIN.Border.Class = "ListViewBorder";
            this.SYSTEMSETUPLVWMAIN.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SYSTEMSETUPLVWMAIN.CheckBoxes = true;
            this.SYSTEMSETUPLVWMAIN.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.SYSTEMSETUPLVWMAIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SYSTEMSETUPLVWMAIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            listViewGroup1.Header = "Company Setup";
            listViewGroup1.Name = "CompanySetup";
            listViewGroup2.Header = "User Permission";
            listViewGroup2.Name = "UserPermission";
            listViewGroup3.Header = "Membership Setup";
            listViewGroup3.Name = "MembershipSetup";
            listViewGroup4.Header = "Currency";
            listViewGroup4.Name = "Currency";
            listViewGroup5.Header = "General";
            listViewGroup5.Name = "General";
            this.SYSTEMSETUPLVWMAIN.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3,
            listViewGroup4,
            listViewGroup5});
            this.SYSTEMSETUPLVWMAIN.HideSelection = false;
            listViewItem2.Group = listViewGroup1;
            listViewItem2.StateImageIndex = 0;
            listViewItem2.Tag = "scrUser";
            listViewItem3.Group = listViewGroup1;
            listViewItem3.StateImageIndex = 0;
            listViewItem3.Tag = "scrRole";
            listViewItem4.Group = listViewGroup1;
            listViewItem4.StateImageIndex = 0;
            listViewItem4.Tag = "scrEmployee";
            listViewItem5.Group = listViewGroup2;
            listViewItem5.StateImageIndex = 0;
            listViewItem5.Tag = "scrPermission";
            listViewItem6.Group = listViewGroup1;
            listViewItem6.StateImageIndex = 0;
            listViewItem6.Tag = "scrUOM";
            listViewItem7.Group = listViewGroup1;
            listViewItem7.StateImageIndex = 0;
            listViewItem7.Tag = "scrCompany";
            listViewItem8.Group = listViewGroup4;
            listViewItem8.StateImageIndex = 0;
            listViewItem8.Tag = "scrCuryRate";
            listViewItem9.Group = listViewGroup5;
            listViewItem9.StateImageIndex = 0;
            listViewItem9.Tag = "scrCustomer";
            listViewItem10.Group = listViewGroup1;
            listViewItem10.StateImageIndex = 0;
            listViewItem10.Tag = "scrCashierBalance";
            this.SYSTEMSETUPLVWMAIN.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10});
            this.SYSTEMSETUPLVWMAIN.Location = new System.Drawing.Point(0, 0);
            this.SYSTEMSETUPLVWMAIN.Margin = new System.Windows.Forms.Padding(4);
            this.SYSTEMSETUPLVWMAIN.MultiSelect = false;
            this.SYSTEMSETUPLVWMAIN.Name = "SYSTEMSETUPLVWMAIN";
            this.SYSTEMSETUPLVWMAIN.Size = new System.Drawing.Size(495, 603);
            this.SYSTEMSETUPLVWMAIN.TabIndex = 0;
            this.SYSTEMSETUPLVWMAIN.UseCompatibleStateImageBehavior = false;
            this.SYSTEMSETUPLVWMAIN.View = System.Windows.Forms.View.Details;
            this.SYSTEMSETUPLVWMAIN.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.SYSTEMSETUPLVWMAIN_ItemChecked);
            this.SYSTEMSETUPLVWMAIN.SelectedIndexChanged += new System.EventHandler(this.SYSTEMSETUPLVWMAIN_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "    GENERAL SETUP";
            this.columnHeader1.Width = 368;
            // 
            // CheckBox7
            // 
            this.CheckBox7.EnterMoveNextControl = true;
            this.CheckBox7.Location = new System.Drawing.Point(5, 4);
            this.CheckBox7.Margin = new System.Windows.Forms.Padding(4);
            this.CheckBox7.Name = "CheckBox7";
            this.CheckBox7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.CheckBox7.Properties.Appearance.Options.UseFont = true;
            this.CheckBox7.Properties.Caption = "";
            this.CheckBox7.Size = new System.Drawing.Size(21, 20);
            this.CheckBox7.TabIndex = 11;
            this.CheckBox7.CheckedChanged += new System.EventHandler(this.CheckBox7_CheckedChanged);
            // 
            // btnApplySystemSetup
            // 
            this.btnApplySystemSetup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplySystemSetup.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.btnApplySystemSetup.Appearance.Options.UseFont = true;
            this.btnApplySystemSetup.Enabled = false;
            this.btnApplySystemSetup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnApplySystemSetup.ImageOptions.Image")));
            this.btnApplySystemSetup.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnApplySystemSetup.Location = new System.Drawing.Point(238, 513);
            this.btnApplySystemSetup.Margin = new System.Windows.Forms.Padding(4);
            this.btnApplySystemSetup.Name = "btnApplySystemSetup";
            this.btnApplySystemSetup.Size = new System.Drawing.Size(109, 42);
            this.btnApplySystemSetup.TabIndex = 10;
            this.btnApplySystemSetup.Text = "&APPLY";
            this.btnApplySystemSetup.Click += new System.EventHandler(this.btnApplySystemSetup_Click);
            // 
            // SYSTEMSETUPLVWDETAIL
            // 
            // 
            // 
            // 
            this.SYSTEMSETUPLVWDETAIL.Border.Class = "ListViewBorder";
            this.SYSTEMSETUPLVWDETAIL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.SYSTEMSETUPLVWDETAIL.CheckBoxes = true;
            this.SYSTEMSETUPLVWDETAIL.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.SYSTEMSETUPLVWDETAIL.Dock = System.Windows.Forms.DockStyle.Top;
            this.SYSTEMSETUPLVWDETAIL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.SYSTEMSETUPLVWDETAIL.HideSelection = false;
            this.SYSTEMSETUPLVWDETAIL.Location = new System.Drawing.Point(0, 0);
            this.SYSTEMSETUPLVWDETAIL.Margin = new System.Windows.Forms.Padding(4);
            this.SYSTEMSETUPLVWDETAIL.MultiSelect = false;
            this.SYSTEMSETUPLVWDETAIL.Name = "SYSTEMSETUPLVWDETAIL";
            this.SYSTEMSETUPLVWDETAIL.Size = new System.Drawing.Size(531, 500);
            this.SYSTEMSETUPLVWDETAIL.TabIndex = 1;
            this.SYSTEMSETUPLVWDETAIL.UseCompatibleStateImageBehavior = false;
            this.SYSTEMSETUPLVWDETAIL.View = System.Windows.Forms.View.Details;
            this.SYSTEMSETUPLVWDETAIL.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.SYSTEMSETUPLVWDETAIL_ItemChecked);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "    System Setup Permission Detail";
            this.columnHeader2.Width = 368;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.xtraTabPage2.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage2.Controls.Add(this.splitContainerControl3);
            this.xtraTabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1036, 603);
            this.xtraTabPage2.Text = "Management";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.CheckBox3);
            this.splitContainerControl3.Panel1.Controls.Add(this.MANAGEMENTLVWMAIN);
            this.splitContainerControl3.Panel2.Controls.Add(this.checkbox10);
            this.splitContainerControl3.Panel2.Controls.Add(this.btnApplyManagement);
            this.splitContainerControl3.Panel2.Controls.Add(this.MANAGEMENTLVWDETAIL);
            this.splitContainerControl3.Size = new System.Drawing.Size(1036, 603);
            this.splitContainerControl3.SplitterPosition = 495;
            this.splitContainerControl3.TabIndex = 1;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // CheckBox3
            // 
            this.CheckBox3.EnterMoveNextControl = true;
            this.CheckBox3.Location = new System.Drawing.Point(5, 4);
            this.CheckBox3.Margin = new System.Windows.Forms.Padding(4);
            this.CheckBox3.Name = "CheckBox3";
            this.CheckBox3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.CheckBox3.Properties.Appearance.Options.UseFont = true;
            this.CheckBox3.Properties.Caption = "";
            this.CheckBox3.Size = new System.Drawing.Size(21, 20);
            this.CheckBox3.TabIndex = 8;
            this.CheckBox3.CheckedChanged += new System.EventHandler(this.CheckBox3_CheckedChanged);
            // 
            // MANAGEMENTLVWMAIN
            // 
            // 
            // 
            // 
            this.MANAGEMENTLVWMAIN.Border.Class = "ListViewBorder";
            this.MANAGEMENTLVWMAIN.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.MANAGEMENTLVWMAIN.CheckBoxes = true;
            this.MANAGEMENTLVWMAIN.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3});
            this.MANAGEMENTLVWMAIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MANAGEMENTLVWMAIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            listViewGroup6.Header = "App";
            listViewGroup6.Name = "App";
            listViewGroup7.Header = "Report";
            listViewGroup7.Name = "Report";
            this.MANAGEMENTLVWMAIN.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup6,
            listViewGroup7});
            this.MANAGEMENTLVWMAIN.HideSelection = false;
            listViewItem11.Group = listViewGroup6;
            listViewItem11.StateImageIndex = 0;
            listViewItem11.Tag = "scrDashboard";
            listViewItem12.Group = listViewGroup6;
            listViewItem12.StateImageIndex = 0;
            listViewItem12.Tag = "scrStoreInfor";
            listViewItem13.Group = listViewGroup7;
            listViewItem13.StateImageIndex = 0;
            listViewItem13.Tag = "scrDailyInvoice";
            listViewItem14.Group = listViewGroup7;
            listViewItem14.StateImageIndex = 0;
            listViewItem14.Tag = "scrDetailsInvoice";
            listViewItem15.Group = listViewGroup7;
            listViewItem15.StateImageIndex = 0;
            listViewItem15.Tag = "scrBarcode";
            this.MANAGEMENTLVWMAIN.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15});
            this.MANAGEMENTLVWMAIN.Location = new System.Drawing.Point(0, 0);
            this.MANAGEMENTLVWMAIN.Margin = new System.Windows.Forms.Padding(4);
            this.MANAGEMENTLVWMAIN.MultiSelect = false;
            this.MANAGEMENTLVWMAIN.Name = "MANAGEMENTLVWMAIN";
            this.MANAGEMENTLVWMAIN.Size = new System.Drawing.Size(495, 603);
            this.MANAGEMENTLVWMAIN.TabIndex = 0;
            this.MANAGEMENTLVWMAIN.UseCompatibleStateImageBehavior = false;
            this.MANAGEMENTLVWMAIN.View = System.Windows.Forms.View.Details;
            this.MANAGEMENTLVWMAIN.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.MANAGEMENTLVWMAIN_ItemChecked);
            this.MANAGEMENTLVWMAIN.SelectedIndexChanged += new System.EventHandler(this.MANAGEMENTLVWMAIN_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "    MEMBERSHIP";
            this.columnHeader3.Width = 368;
            // 
            // checkbox10
            // 
            this.checkbox10.EnterMoveNextControl = true;
            this.checkbox10.Location = new System.Drawing.Point(5, 4);
            this.checkbox10.Margin = new System.Windows.Forms.Padding(4);
            this.checkbox10.Name = "checkbox10";
            this.checkbox10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.checkbox10.Properties.Appearance.Options.UseFont = true;
            this.checkbox10.Properties.Caption = "";
            this.checkbox10.Size = new System.Drawing.Size(21, 20);
            this.checkbox10.TabIndex = 12;
            this.checkbox10.CheckedChanged += new System.EventHandler(this.chbMaintains_CheckedChanged);
            // 
            // btnApplyManagement
            // 
            this.btnApplyManagement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyManagement.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.btnApplyManagement.Appearance.Options.UseFont = true;
            this.btnApplyManagement.Enabled = false;
            this.btnApplyManagement.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnApplyManagement.ImageOptions.Image")));
            this.btnApplyManagement.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnApplyManagement.Location = new System.Drawing.Point(238, 513);
            this.btnApplyManagement.Margin = new System.Windows.Forms.Padding(4);
            this.btnApplyManagement.Name = "btnApplyManagement";
            this.btnApplyManagement.Size = new System.Drawing.Size(109, 42);
            this.btnApplyManagement.TabIndex = 10;
            this.btnApplyManagement.Text = "&APPLY";
            this.btnApplyManagement.Click += new System.EventHandler(this.btnApplyManagement_Click);
            // 
            // MANAGEMENTLVWDETAIL
            // 
            // 
            // 
            // 
            this.MANAGEMENTLVWDETAIL.Border.Class = "ListViewBorder";
            this.MANAGEMENTLVWDETAIL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.MANAGEMENTLVWDETAIL.CheckBoxes = true;
            this.MANAGEMENTLVWDETAIL.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4});
            this.MANAGEMENTLVWDETAIL.Dock = System.Windows.Forms.DockStyle.Top;
            this.MANAGEMENTLVWDETAIL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.MANAGEMENTLVWDETAIL.HideSelection = false;
            this.MANAGEMENTLVWDETAIL.Location = new System.Drawing.Point(0, 0);
            this.MANAGEMENTLVWDETAIL.Margin = new System.Windows.Forms.Padding(4);
            this.MANAGEMENTLVWDETAIL.MultiSelect = false;
            this.MANAGEMENTLVWDETAIL.Name = "MANAGEMENTLVWDETAIL";
            this.MANAGEMENTLVWDETAIL.Size = new System.Drawing.Size(531, 500);
            this.MANAGEMENTLVWDETAIL.TabIndex = 1;
            this.MANAGEMENTLVWDETAIL.UseCompatibleStateImageBehavior = false;
            this.MANAGEMENTLVWDETAIL.View = System.Windows.Forms.View.Details;
            this.MANAGEMENTLVWDETAIL.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.MANAGEMENTLVWDETAIL_ItemChecked);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "    Management Permission Detail";
            this.columnHeader4.Width = 368;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Appearance.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.xtraTabPage5.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage5.Controls.Add(this.splitContainerControl6);
            this.xtraTabPage5.Margin = new System.Windows.Forms.Padding(4);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1036, 603);
            this.xtraTabPage5.Text = "POS";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl6.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl6.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.CheckBox1);
            this.splitContainerControl6.Panel1.Controls.Add(this.POSLVWMAIN);
            this.splitContainerControl6.Panel2.Controls.Add(this.CheckBox11);
            this.splitContainerControl6.Panel2.Controls.Add(this.btnApplyPOS);
            this.splitContainerControl6.Panel2.Controls.Add(this.POSLVWDETAIL);
            this.splitContainerControl6.Size = new System.Drawing.Size(1036, 603);
            this.splitContainerControl6.SplitterPosition = 495;
            this.splitContainerControl6.TabIndex = 1;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // CheckBox1
            // 
            this.CheckBox1.EnterMoveNextControl = true;
            this.CheckBox1.Location = new System.Drawing.Point(5, 4);
            this.CheckBox1.Margin = new System.Windows.Forms.Padding(4);
            this.CheckBox1.Name = "CheckBox1";
            this.CheckBox1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.CheckBox1.Properties.Appearance.Options.UseFont = true;
            this.CheckBox1.Properties.Caption = "";
            this.CheckBox1.Size = new System.Drawing.Size(15, 20);
            this.CheckBox1.TabIndex = 8;
            // 
            // POSLVWMAIN
            // 
            // 
            // 
            // 
            this.POSLVWMAIN.Border.Class = "ListViewBorder";
            this.POSLVWMAIN.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.POSLVWMAIN.CheckBoxes = true;
            this.POSLVWMAIN.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10});
            this.POSLVWMAIN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.POSLVWMAIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            listViewGroup8.Header = "Product";
            listViewGroup8.Name = "Product";
            listViewGroup9.Header = "Price";
            listViewGroup9.Name = "Price";
            listViewGroup10.Header = "Discount";
            listViewGroup10.Name = "Discount";
            listViewGroup11.Header = "Sales Management";
            listViewGroup11.Name = "SaleManagement";
            this.POSLVWMAIN.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup8,
            listViewGroup9,
            listViewGroup10,
            listViewGroup11});
            this.POSLVWMAIN.HideSelection = false;
            listViewItem16.Group = listViewGroup8;
            listViewItem16.StateImageIndex = 0;
            listViewItem16.Tag = "scrCategory";
            listViewItem17.Group = listViewGroup8;
            listViewItem17.StateImageIndex = 0;
            listViewItem17.Tag = "scrItem";
            listViewItem18.Group = listViewGroup9;
            listViewItem18.StateImageIndex = 0;
            listViewItem18.Tag = "scrSalePrice";
            listViewItem19.Group = listViewGroup11;
            listViewItem19.StateImageIndex = 0;
            listViewItem19.Tag = "scrSaleOrder";
            this.POSLVWMAIN.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem16,
            listViewItem17,
            listViewItem18,
            listViewItem19});
            this.POSLVWMAIN.Location = new System.Drawing.Point(0, 0);
            this.POSLVWMAIN.Margin = new System.Windows.Forms.Padding(4);
            this.POSLVWMAIN.MultiSelect = false;
            this.POSLVWMAIN.Name = "POSLVWMAIN";
            this.POSLVWMAIN.Size = new System.Drawing.Size(495, 603);
            this.POSLVWMAIN.TabIndex = 0;
            this.POSLVWMAIN.UseCompatibleStateImageBehavior = false;
            this.POSLVWMAIN.View = System.Windows.Forms.View.Details;
            this.POSLVWMAIN.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.POSLVWMAIN_ItemChecked);
            this.POSLVWMAIN.SelectedIndexChanged += new System.EventHandler(this.POSLVWMAIN_SelectedIndexChanged);
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "    Point of Sale Managment";
            this.columnHeader10.Width = 368;
            // 
            // CheckBox11
            // 
            this.CheckBox11.EnterMoveNextControl = true;
            this.CheckBox11.Location = new System.Drawing.Point(5, 4);
            this.CheckBox11.Margin = new System.Windows.Forms.Padding(4);
            this.CheckBox11.Name = "CheckBox11";
            this.CheckBox11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.CheckBox11.Properties.Appearance.Options.UseFont = true;
            this.CheckBox11.Properties.Caption = "";
            this.CheckBox11.Size = new System.Drawing.Size(21, 20);
            this.CheckBox11.TabIndex = 11;
            // 
            // btnApplyPOS
            // 
            this.btnApplyPOS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyPOS.Appearance.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.btnApplyPOS.Appearance.Options.UseFont = true;
            this.btnApplyPOS.Enabled = false;
            this.btnApplyPOS.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnApplyPOS.ImageOptions.Image")));
            this.btnApplyPOS.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnApplyPOS.Location = new System.Drawing.Point(238, 513);
            this.btnApplyPOS.Margin = new System.Windows.Forms.Padding(4);
            this.btnApplyPOS.Name = "btnApplyPOS";
            this.btnApplyPOS.Size = new System.Drawing.Size(109, 42);
            this.btnApplyPOS.TabIndex = 10;
            this.btnApplyPOS.Text = "&APPLY";
            this.btnApplyPOS.Click += new System.EventHandler(this.btnApplyPOS_Click);
            // 
            // POSLVWDETAIL
            // 
            // 
            // 
            // 
            this.POSLVWDETAIL.Border.Class = "ListViewBorder";
            this.POSLVWDETAIL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.POSLVWDETAIL.CheckBoxes = true;
            this.POSLVWDETAIL.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11});
            this.POSLVWDETAIL.Dock = System.Windows.Forms.DockStyle.Top;
            this.POSLVWDETAIL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.POSLVWDETAIL.HideSelection = false;
            this.POSLVWDETAIL.Location = new System.Drawing.Point(0, 0);
            this.POSLVWDETAIL.Margin = new System.Windows.Forms.Padding(4);
            this.POSLVWDETAIL.MultiSelect = false;
            this.POSLVWDETAIL.Name = "POSLVWDETAIL";
            this.POSLVWDETAIL.Size = new System.Drawing.Size(531, 500);
            this.POSLVWDETAIL.TabIndex = 1;
            this.POSLVWDETAIL.UseCompatibleStateImageBehavior = false;
            this.POSLVWDETAIL.View = System.Windows.Forms.View.Details;
            this.POSLVWDETAIL.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.POSLVWDETAIL_ItemChecked);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "    Poin of Sale Permission Detail";
            this.columnHeader11.Width = 368;
            // 
            // btnExit
            // 
            this.btnExit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Appearance.Options.UseFont = true;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExit.Enabled = false;
            this.btnExit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.ImageOptions.Image")));
            this.btnExit.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(2, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(43, 17);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "&APPLY";
            this.btnExit.Click += new System.EventHandler(this.btnApplySystemSetup_Click);
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "System Setup Permission Detail";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 1;
            this.treeListColumn3.Width = 612;
            // 
            // tileNavCategory1
            // 
            this.tileNavCategory1.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.tileNavCategory1.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Edit";
            this.navEdit.Name = "navEdit";
            // 
            // navSave
            // 
            this.navSave.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navSave.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navSave.AppearanceHovered.Options.UseForeColor = true;
            this.navSave.Caption = "Save";
            this.navSave.Name = "navSave";
            // 
            // navNew
            // 
            this.navNew.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navNew.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navNew.AppearanceHovered.Options.UseForeColor = true;
            this.navNew.Caption = "New";
            this.navNew.Name = "navNew";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "User Management Modification";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navHeader
            // 
            // 
            // tileNavCategory2
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory2";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(500, 40);
            this.navHeader.TabIndex = 0;
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.Location = new System.Drawing.Point(0, 0);
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Size = new System.Drawing.Size(100, 20);
            this.buttonEdit1.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnExit);
            this.panelControl1.Location = new System.Drawing.Point(730, 1);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(47, 21);
            this.panelControl1.TabIndex = 4;
            // 
            // FrmUserPermission
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1399, 631);
            this.Controls.Add(this.splitContainerControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Glow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FrmUserPermission.IconOptions.Icon")));
            this.IconOptions.ShowIcon = false;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FrmUserPermission";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Permission Information";
            this.Load += new System.EventHandler(this.frmUserPermission_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabGrant)).EndInit();
            this.TabGrant.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox7.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkbox10.Properties)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBox11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ImageList ImageList1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        internal DevComponents.DotNetBar.Controls.ListViewEx listViewEx1;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private DevExpress.XtraBars.Navigation.TileNavCategory tileNavCategory1;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navSave;
        private DevExpress.XtraBars.Navigation.NavButton navNew;
        private DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        internal DevExpress.XtraEditors.SimpleButton btnExit;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        internal DevExpress.XtraEditors.SimpleButton btnExiting;
        internal DevExpress.XtraTab.XtraTabControl TabGrant;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        internal DevExpress.XtraEditors.CheckEdit CheckBox5;
        internal DevComponents.DotNetBar.Controls.ListViewEx SYSTEMSETUPLVWMAIN;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        internal DevExpress.XtraEditors.CheckEdit CheckBox7;
        internal DevExpress.XtraEditors.SimpleButton btnApplySystemSetup;
        internal DevComponents.DotNetBar.Controls.ListViewEx SYSTEMSETUPLVWDETAIL;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        internal DevExpress.XtraEditors.CheckEdit CheckBox3;
        internal DevComponents.DotNetBar.Controls.ListViewEx MANAGEMENTLVWMAIN;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        internal DevExpress.XtraEditors.CheckEdit checkbox10;
        internal DevExpress.XtraEditors.SimpleButton btnApplyManagement;
        internal DevComponents.DotNetBar.Controls.ListViewEx MANAGEMENTLVWDETAIL;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        internal DevExpress.XtraEditors.CheckEdit CheckBox1;
        internal DevComponents.DotNetBar.Controls.ListViewEx POSLVWMAIN;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        internal DevExpress.XtraEditors.CheckEdit CheckBox11;
        internal DevExpress.XtraEditors.SimpleButton btnApplyPOS;
        internal DevComponents.DotNetBar.Controls.ListViewEx POSLVWDETAIL;
        private System.Windows.Forms.ColumnHeader columnHeader11;
    }
}