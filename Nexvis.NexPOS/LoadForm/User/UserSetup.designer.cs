﻿namespace DgoStore.LoadForm.User
{
    partial class UserSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navNew = new DevExpress.XtraBars.Navigation.NavButton();
            this.navSave = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.luRoleID = new DevExpress.XtraEditors.LookUpEdit();
            this.txtUserID = new DevExpress.XtraEditors.TextEdit();
            this.txtUserName = new DevExpress.XtraEditors.TextEdit();
            this.txtRoleName = new DevExpress.XtraEditors.TextEdit();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.ckStatus = new DevExpress.XtraEditors.CheckEdit();
            this.luUserEmployee = new DevExpress.XtraEditors.LookUpEdit();
            this.txtConfirmPass = new DevExpress.XtraEditors.TextEdit();
            this.ckShow1 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlRePassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luRoleID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRoleName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luUserEmployee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConfirmPass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckShow1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRePassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navNew);
            this.navHeader.Buttons.Add(this.navSave);
            this.navHeader.Buttons.Add(this.navEdit);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(562, 40);
            this.navHeader.TabIndex = 4;
            this.navHeader.Text = "tileNavPane1";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Add User";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navNew
            // 
            this.navNew.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navNew.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navNew.AppearanceHovered.Options.UseForeColor = true;
            this.navNew.Caption = "New";
            this.navNew.Name = "navNew";
            this.navNew.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navSave
            // 
            this.navSave.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navSave.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navSave.AppearanceHovered.Options.UseForeColor = true;
            this.navSave.Caption = "Save";
            this.navSave.Name = "navSave";
            this.navSave.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navSave_ElementClick);
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Update";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.luRoleID);
            this.layoutControl1.Controls.Add(this.txtUserID);
            this.layoutControl1.Controls.Add(this.txtUserName);
            this.layoutControl1.Controls.Add(this.txtRoleName);
            this.layoutControl1.Controls.Add(this.txtPassword);
            this.layoutControl1.Controls.Add(this.ckStatus);
            this.layoutControl1.Controls.Add(this.luUserEmployee);
            this.layoutControl1.Controls.Add(this.txtConfirmPass);
            this.layoutControl1.Controls.Add(this.ckShow1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem1});
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(716, 246, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(562, 279);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // luRoleID
            // 
            this.luRoleID.Location = new System.Drawing.Point(152, 12);
            this.luRoleID.Name = "luRoleID";
            this.luRoleID.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luRoleID.Properties.Appearance.Options.UseFont = true;
            this.luRoleID.Properties.Appearance.Options.UseTextOptions = true;
            this.luRoleID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luRoleID.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.luRoleID.Properties.AppearanceDropDown.Options.UseFont = true;
            this.luRoleID.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luRoleID.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.luRoleID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luRoleID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("RoleID", "RoleID"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")});
            this.luRoleID.Properties.NullText = "--Select--";
            this.luRoleID.Properties.PopupSizeable = false;
            this.luRoleID.Properties.ShowFooter = false;
            this.luRoleID.Size = new System.Drawing.Size(387, 28);
            this.luRoleID.StyleController = this.layoutControl1;
            this.luRoleID.TabIndex = 0;
            this.luRoleID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luRoleID_KeyDown);
            // 
            // txtUserID
            // 
            this.txtUserID.Location = new System.Drawing.Point(12, 49);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.txtUserID.Properties.Appearance.Options.UseFont = true;
            this.txtUserID.Size = new System.Drawing.Size(761, 24);
            this.txtUserID.StyleController = this.layoutControl1;
            this.txtUserID.TabIndex = 4;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(152, 98);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtUserName.Properties.Appearance.Options.UseFont = true;
            this.txtUserName.Properties.MaxLength = 50;
            this.txtUserName.Size = new System.Drawing.Size(387, 28);
            this.txtUserName.StyleController = this.layoutControl1;
            this.txtUserName.TabIndex = 2;
            this.txtUserName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luRoleID_KeyDown);
            // 
            // txtRoleName
            // 
            this.txtRoleName.Location = new System.Drawing.Point(12, 235);
            this.txtRoleName.Name = "txtRoleName";
            this.txtRoleName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.txtRoleName.Properties.Appearance.Options.UseFont = true;
            this.txtRoleName.Size = new System.Drawing.Size(761, 24);
            this.txtRoleName.StyleController = this.layoutControl1;
            this.txtRoleName.TabIndex = 7;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(152, 180);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtPassword.Properties.Appearance.Options.UseFont = true;
            this.txtPassword.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.txtPassword.Properties.MaxLength = 18;
            this.txtPassword.Properties.UseSystemPasswordChar = true;
            this.txtPassword.Size = new System.Drawing.Size(310, 28);
            this.txtPassword.StyleController = this.layoutControl1;
            this.txtPassword.TabIndex = 4;
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luRoleID_KeyDown);
            this.txtPassword.Validating += new System.ComponentModel.CancelEventHandler(this.txtPassword_Validating);
            // 
            // ckStatus
            // 
            this.ckStatus.Location = new System.Drawing.Point(23, 141);
            this.ckStatus.Name = "ckStatus";
            this.ckStatus.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.ckStatus.Properties.Appearance.Options.UseFont = true;
            this.ckStatus.Properties.Caption = "Status";
            this.ckStatus.Size = new System.Drawing.Size(516, 25);
            this.ckStatus.StyleController = this.layoutControl1;
            this.ckStatus.TabIndex = 3;
            this.ckStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luRoleID_KeyDown);
            // 
            // luUserEmployee
            // 
            this.luUserEmployee.Location = new System.Drawing.Point(152, 55);
            this.luUserEmployee.Name = "luUserEmployee";
            this.luUserEmployee.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luUserEmployee.Properties.Appearance.Options.UseFont = true;
            this.luUserEmployee.Properties.Appearance.Options.UseTextOptions = true;
            this.luUserEmployee.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luUserEmployee.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.luUserEmployee.Properties.AppearanceDropDown.Options.UseFont = true;
            this.luUserEmployee.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luUserEmployee.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.luUserEmployee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luUserEmployee.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EmployeeID", "Employee ID"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EmployeeName", "Employee Name")});
            this.luUserEmployee.Properties.NullText = "---Select---";
            this.luUserEmployee.Properties.PopupSizeable = false;
            this.luUserEmployee.Properties.ShowFooter = false;
            this.luUserEmployee.Size = new System.Drawing.Size(387, 28);
            this.luUserEmployee.StyleController = this.layoutControl1;
            this.luUserEmployee.TabIndex = 1;
            this.luUserEmployee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luRoleID_KeyDown);
            // 
            // txtConfirmPass
            // 
            this.txtConfirmPass.Location = new System.Drawing.Point(152, 222);
            this.txtConfirmPass.Name = "txtConfirmPass";
            this.txtConfirmPass.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtConfirmPass.Properties.Appearance.Options.UseFont = true;
            this.txtConfirmPass.Properties.MaxLength = 18;
            this.txtConfirmPass.Properties.UseSystemPasswordChar = true;
            this.txtConfirmPass.Size = new System.Drawing.Size(310, 28);
            this.txtConfirmPass.StyleController = this.layoutControl1;
            this.txtConfirmPass.TabIndex = 5;
            this.txtConfirmPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtConfirmPass_KeyDown);
            this.txtConfirmPass.Validating += new System.ComponentModel.CancelEventHandler(this.txtConfirmPass_Validating);
            // 
            // ckShow1
            // 
            this.ckShow1.Location = new System.Drawing.Point(472, 182);
            this.ckShow1.Name = "ckShow1";
            this.ckShow1.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.ckShow1.Properties.Appearance.Options.UseFont = true;
            this.ckShow1.Properties.Caption = "Show";
            this.ckShow1.Size = new System.Drawing.Size(67, 25);
            this.ckShow1.StyleController = this.layoutControl1;
            this.ckShow1.TabIndex = 6;
            this.ckShow1.Visible = false;
            this.ckShow1.CheckedChanged += new System.EventHandler(this.ckShow1_CheckedChanged);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11F);
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.txtRoleName;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 202);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(765, 49);
            this.layoutControlItem4.Text = "Role Name";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(93, 18);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtUserID;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 16);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(765, 49);
            this.layoutControlItem1.Text = "User ID";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(93, 18);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.emptySpaceItem7,
            this.layoutControlPassword,
            this.layoutControlItem3,
            this.emptySpaceItem10,
            this.emptySpaceItem1,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.emptySpaceItem3,
            this.layoutControlRePassword,
            this.layoutControlItem2,
            this.emptySpaceItem8,
            this.emptySpaceItem2,
            this.emptySpaceItem9});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(562, 279);
            this.Root.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(11, 75);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(520, 11);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(11, 200);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(520, 10);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlPassword
            // 
            this.layoutControlPassword.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.layoutControlPassword.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlPassword.Control = this.txtPassword;
            this.layoutControlPassword.Location = new System.Drawing.Point(11, 168);
            this.layoutControlPassword.Name = "layoutControlPassword";
            this.layoutControlPassword.Size = new System.Drawing.Size(443, 32);
            this.layoutControlPassword.Text = "Password             :";
            this.layoutControlPassword.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlPassword.TextSize = new System.Drawing.Size(126, 20);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.luRoleID;
            this.layoutControlItem3.Location = new System.Drawing.Point(11, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(520, 32);
            this.layoutControlItem3.Text = "User Role             :";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(126, 20);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(11, 32);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(520, 11);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(11, 158);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(520, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.ckStatus;
            this.layoutControlItem7.Location = new System.Drawing.Point(11, 129);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(520, 29);
            this.layoutControlItem7.Text = "Status";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.luUserEmployee;
            this.layoutControlItem6.Location = new System.Drawing.Point(11, 43);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(520, 32);
            this.layoutControlItem6.Text = "User Employee      :";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(126, 20);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(11, 259);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(11, 242);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(520, 17);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(531, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(11, 259);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlRePassword
            // 
            this.layoutControlRePassword.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.layoutControlRePassword.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlRePassword.Control = this.txtConfirmPass;
            this.layoutControlRePassword.Location = new System.Drawing.Point(11, 210);
            this.layoutControlRePassword.Name = "layoutControlRePassword";
            this.layoutControlRePassword.Size = new System.Drawing.Size(443, 32);
            this.layoutControlRePassword.Text = "RePassword :";
            this.layoutControlRePassword.TextSize = new System.Drawing.Size(126, 20);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.txtUserName;
            this.layoutControlItem2.Location = new System.Drawing.Point(11, 86);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(520, 32);
            this.layoutControlItem2.Text = "User Name           :";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(126, 20);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(11, 118);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(520, 11);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(454, 168);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(77, 32);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(454, 210);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(77, 32);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.ckShow1;
            this.layoutControlItem5.Location = new System.Drawing.Point(429, 145);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(61, 26);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // UserSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 319);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Name = "UserSetup";
            this.Text = "UserSetup";
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.luRoleID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRoleName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luUserEmployee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtConfirmPass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ckShow1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRePassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navNew;
        private DevExpress.XtraBars.Navigation.NavButton navSave;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        public DevExpress.XtraEditors.TextEdit txtUserID;
        public DevExpress.XtraEditors.TextEdit txtUserName;
        public DevExpress.XtraEditors.TextEdit txtRoleName;
        public DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        public DevExpress.XtraEditors.CheckEdit ckStatus;
        public DevExpress.XtraEditors.LookUpEdit luUserEmployee;
        public DevExpress.XtraEditors.LookUpEdit luRoleID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        internal DevExpress.XtraEditors.TextEdit txtConfirmPass;
        internal DevExpress.XtraLayout.LayoutControlItem layoutControlPassword;
        internal DevExpress.XtraLayout.LayoutControlItem layoutControlRePassword;
        internal DevExpress.XtraEditors.CheckEdit ckShow1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        internal DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}