﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;

namespace DgoStore.LoadForm.User
{
    public partial class UserSetup : DevExpress.XtraEditors.XtraForm
    {
        #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        private scrUser parentForm;

        #endregion

        #region --- Form Action ---
        public UserSetup(string transactionType, scrUser objParent)
        {
            InitializeComponent();
            parentForm = objParent;

            if (transactionType == "NEW")
            {
                navEdit.Visible = false;
                isNew = true;
            }
            else if (transactionType == "EDIT")
            {
                navSave.Visible = false;
            }

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;
            

        }

        #endregion

        #region --- Header Action ---

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Clear value for new
            GlobalInitializer.GsClearValue(txtUserID, txtUserName, luRoleID, txtRoleName, txtPassword, txtConfirmPass, luUserEmployee);
            navEdit.Visible = false;
            navSave.Visible = true;
            isNew = true;
            txtUserID.Enabled = true;
            txtUserID.Focus();
            luRoleID.Enabled = true;
            luRoleID.Focus();

            ckStatus.Checked = false;
        }
        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtUserName) == false)
            {
                //    // TODO: Save value to DB.
                //if (txtPassword.Text.Length < 8)
                //{
                //    MessageBox.Show("Password must be at least 8 characters long");
                //    txtConfirmPass.Focus();
                //    return;
                //}
                parentForm.GsSaveData(this);

                // TODO: Validate which record exiting.
                if (isExiting && parentForm.isSuccessful)
                {
                    XtraMessageBox.Show("This record ready existing !", "POS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtUserName.Focus();
                    return;
                }

                if (parentForm.isSuccessful)
                    navNew_ElementClick(sender, e);
                this.Close();
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtUserName) == false)
                parentForm.GsUpdateData(this);
            this.Close();
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close(); // this.Parent.Controls.Remove(this);
            parentForm.navRefresh_ElementClick(sender, e);
        }

        #endregion


        private void txtPassword_Validating(object sender, CancelEventArgs e)
        {
            //if (txtPassword.Text.Length < 8)
            //{

            //    MessageBox.Show("Password must be at least 8 characters long");
            //    txtConfirmPass.Text = null;
            //}
        }

        private void txtConfirmPass_Validating(object sender, CancelEventArgs e)
        {
            if (txtConfirmPass.Text != txtPassword.Text)
            {
                XtraMessageBox.Show("Password must match !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtConfirmPass.Text = null;
            }
        }

        private void luRoleID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtConfirmPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && navSave.Visible == true)
            {
                navSave_ElementClick(null, null);
            }
            else if (e.KeyCode == Keys.Enter && navSave.Visible == false)
            {
                navEdit_ElementClick(null, null);
            }
        }

        private void ckShow1_CheckedChanged(object sender, EventArgs e)
        {
            txtPassword.Properties.UseSystemPasswordChar = !ckShow1.Checked;
        }

        private void ckShow2_CheckedChanged(object sender, EventArgs e)
        {
            //txtConfirmPass.Properties.UseSystemPasswordChar = !ckShow2.Checked;
        }
    }
}