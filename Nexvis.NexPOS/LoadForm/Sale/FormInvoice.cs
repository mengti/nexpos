﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using ZooMangement;
using DevExpress.XtraBars.Navigation;
using Nexvis.NexPOS.Helper;

namespace DgoStore.LoadForm.Sale
{
    public partial class scrListInvoice : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrListInvoice()
        {
            InitializeComponent();
        }
      //  FormInvoice invoice = new FormInvoice();
        List<ARInvoicesDetail> invoceDe = new List<ARInvoicesDetail>();

        List<getStatus1_Result> getStatus1_Results = new List<getStatus1_Result>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        public bool isSuccessful = true;

        private void FormInvoice_Load(object sender, EventArgs e)
        {
            //invoceDe = DB.ARInvoicesDetails.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();

            getStatus1_Results = DB.getStatus1().ToList();
            Gc.DataSource = getStatus1_Results.ToList();

            _RoleItems = DB.CSRoleItems.ToList();
        }
        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
           //if()  this.Parent.Controls.Remove(this);
           this.Close();
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormInvoice_Load(sender, e);

            //TODO : Clear in field for filter.
            Gv.ClearColumnsFilter();
        }

        private void Gc_Click(object sender, EventArgs e)
        {

        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
           // NavButton navigator = (NavButton)sender;
           // if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
           // {
                if (Gv.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(Gc);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

           // }
           // else
           // {
             //   XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
        }
    }
}