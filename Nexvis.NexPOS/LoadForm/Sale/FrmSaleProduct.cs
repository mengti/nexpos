﻿using DevExpress.DataProcessing;
using DevExpress.Utils;
using DevExpress.Utils.Extensions;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using DevExpress.XtraSplashScreen;
using Nexvis.NexPOS.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Nexvis.NexPOS.Data;
using System.Linq;
using System.Windows.Forms;
using ZooManagement;
using ZooMangement;
using ZooManagement.Report;
using ZooManagement.Helper;
using Nexvis.NexPOS;
using Nexvis.NexPOS.LoadForm;
using System.Globalization;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;
using Nexvis.NexPOS.Report;

namespace DgoStore.LoadForm.Sale
{
    public partial class scrSaleOrder : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
     
        List<INItem> _Products = new List<INItem>();
    
        public List<SaleProductDomain> _SaleProducts = new List<SaleProductDomain>();
        List<INItemCategory> _ItemCategories = new List<INItemCategory>();
        ClsGlobal _obj = new ClsGlobal();
        List<CSCuryRate> _USDExchangeRates = new List<CSCuryRate>();
        
        List<ZooInfo> _Zoo = new List<ZooInfo>();

        List<ARCustomer> _Customers = new List<ARCustomer>();
        List<ARInvoice> _Invoice = new List<ARInvoice>();

        List<MDSalesPrice> _SalePrice = new List<MDSalesPrice>();
        List<CSUOM> _Units = new List<CSUOM>();
      //  List<ARPaymentDetail> _ARPaymentDetails = new List<ARPaymentDetail>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        List<CashierBalance> _CashierBalance = new List<CashierBalance>();



        frmLogin login = new frmLogin();
           
        public string invoice;
        public string PaymentRef;
        public string CusNo;
        Decimal totalGrand = 0;
        Decimal balanceUSD = 0;
        Decimal cashInD = 0;

        public Decimal CHANGE = 0;
        public Decimal CASHINUSD = 0;
        public Decimal DisAm = 0;
        public Decimal DisPer = 0;
        public Decimal TotalPay = 0;

        Decimal TMoney = 0;
        //  public Decimal CASHIREIL = 0;
        public bool isSuccessful = true;
        public bool isBalanceAvailable;
        LookUpEdit ledMyControl;
        #region --- Form Action ---

        //LabelControl lbContact;
        public scrSaleOrder()
        {
            InitializeComponent();

           
            _Zoo = DB.ZooInfoes.ToList();
            _Units = DB.CSUOMs.ToList();

            lbShop.Text ="Shop : " + _Zoo.FirstOrDefault().ZooInfoName;
            lbContact.Text = "Contact : " + _Zoo.FirstOrDefault().Contact;
            lblLocation.Text =  _Zoo.FirstOrDefault().Address;

            var shopID = _Zoo.FirstOrDefault().ZooInfoID;
            CSShopModel.ZooID = shopID;
            CSShopModel.ZooName = _Zoo.FirstOrDefault().ZooInfoName;
            CSShopModel.ZooAddress = _Zoo.FirstOrDefault().Address;
            CSShopModel.ZooPhone = _Zoo.FirstOrDefault().Contact;

            _Invoice = DB.ARInvoices.Where(w => w.Reverse == false).ToList();
            _RoleItems = DB.CSRoleItems.ToList();
            _Customers = DB.ARCustomers.ToList();
            _CashierBalance = DB.CashierBalances.ToList();


            _SaleTemporary = DB.ARInvoices.Where(y => y.Status == "H").ToList();

           GlobalInitializer.GsFillLookUpEdit(
             _SaleTemporary.ToList(), "InvoiceRefNbr", "InvoiceRefNbr", luBrowse, (int) _SaleTemporary.ToList().Count);
          
            GlobalInitializer.GsFillLookUpEdit(_Invoice.Where(x=>x.Status=="C").ToList(), "InvoiceRefNbr", "InvoiceRefNbr", luInvoice, (int)_Invoice.Where(x => x.Status == "C").ToList().Count);
           
         
            //luCustomer.Text = _Customers.FirstOrDefault().CustomerName;
            labelControl1.Text = CSUserModel.LoginName.ToUpper();

            //var balanceCheck = DB.CashierBalances.Where(x => x.StartDateTime == DateTime.Today.Date && x.Status == "Open");
            if (_CashierBalance.Any(x => x.StartDate <= DateTime.Today && x.Status == "Open" && x.UserID == CSUserModel.User))
                isBalanceAvailable = true;
            else 
               isBalanceAvailable = false;


        }

        private void FrmSaleProduct_Load(object sender, EventArgs e)
        {
            SplashScreenManager.ShowForm(typeof(WaitForm1));
            // TODO: Retieve All Corresponding Data
            try
            {
                if (isBalanceAvailable == false)
                {
                    XtraMessageBox.Show("The balance is not available.", "NEXPOS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
                _ItemCategories = DB.INItemCategories.Where(w => w.CompanyID == 2).ToList();
                _Products = DB.INItems.ToList();
                _Products = _Products.Where(w => _ItemCategories.Where(x => x.CategoryID == w.CategoryID && x.CompanyID == x.CompanyID).Any()).ToList();
                //_Products = _Products.Where(w => _ItemBalance.Where(x => x.InventoryID == w.InventoryID).Any()).ToList();
                _ItemCategories = _ItemCategories.Where(w => _Products.Where(x => x.CategoryID == w.CategoryID).Any()).ToList();
                _USDExchangeRates = DB.CSCuryRates.ToList();

                _USDExchangeRates = _USDExchangeRates.Where(x => x.EffectiveDate <= DateTime.Today.Date && x.FromCuryID == "KHR" && x.IsActive == true).ToList();

                if (_USDExchangeRates.Count == 0)
                {
                    XtraMessageBox.Show("Please check exchange rate!", "NEXPOS", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                }
                else
                {
                    lblUSDExchangeRate.Tag = _USDExchangeRates.FirstOrDefault().CuryRate;

                    lblUSDExchangeRate.Text = string.Format("{0} : {1:N0} {2}", "Exchange Rate  1$", lblUSDExchangeRate.Tag, "៛");
                }

                _SalePrice = DB.MDSalesPrices.Where(a => a.EffectiveDate <= DateTime.Today.Date && a.ExpirationDate >= DateTime.Today.Date).ToList();

                GlobalInitializer.GsFillLookUpEdit(_Customers.ToList(), "CustomerID", "CustomerName", luCustomer, (int)_Customers.ToList().Count);

                lblCashier.Text = "Cashier    : " + CSUserModel.LoginName;
                lblCashier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));

              
                //lblLocation.Text = UserLogOn.LocationAddress;

                _Customers = DB.ARCustomers.ToList();
                GlobalInitializer.GsFillLookUpEdit(_Customers.ToList(), "CustomerID", "CustomerName", luCustomer, (int)_Customers.ToList().Count);
                luCustomer.EditValue = _Customers.FirstOrDefault().CustomerID;

                //   luCustomer_EditValueChanged(sender,e);

                //// TODO: Format header GC
                Gv.CustomDrawRowIndicator += GlobalEvent.objGridView_CustomDrawRowIndicator;
                _SaleProducts = new List<SaleProductDomain>();
                GC.DataSource = _SaleProducts;
                txtBarcode.Enabled = true;
                btnReturn.Enabled = false;

                //// TODO: Initial Category
                InitialCategory(new List<INItemCategory>(_ItemCategories.OrderByDescending(x => x.CategoryCD).ThenBy(y => y.CategoryName)));

                //// TODO: Initial Product List
                InitialProduct(new List<INItem>(_Products.OrderByDescending(z => z.InventoryCD).ThenBy(x => x.Descr)));


              //  _Invoice = DB.ARInvoices.Where(w => w.Reverse == false).ToList();
             //   GlobalInitializer.GsFillLookUpEdit(_Invoice.ToList(), "InvoiceRefNbr", "InvoiceRefNbr", luInvoice, (int)_Invoice.ToList().Count);


                Gv.RefreshData();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            SplashScreenManager.CloseForm();
        }

        #endregion

        #region --- Component Action ---

        private void tmCurrentDateTime_Tick(object sender, EventArgs e)
        {
            lblCurrentDateTime.Text = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt");

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            TextEdit obj = sender as TextEdit;
            (from TileItem ti in screenProduct.Groups[0].Items
             select ti).ToList().ForEach(t => t.Visible = t.Text.ToLower().Contains(obj.Text.ToLower()));
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            var result = XtraMessageBox.Show("Are you sure you want to close this sale process?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes) this.Close();
        }

        private void picFilter_Click(object sender, EventArgs e)
        {
            if (panelSearch.Visible == true) panelSearch.Visible = false;
            else panelSearch.Visible = true;
        }

        private void picClear_Click(object sender, EventArgs e)
        {
            txtSearch.EditValue = null;
            txtSearch.Text = "";
        }

        #endregion

        #region --- Private Method ---

        public void TotalInvoiceAmount()
        {
            Gv.RefreshData();
            DevExpress.XtraGrid.GridSummaryItem summaryItem = Gv.Columns["Amount"].SummaryItem;
            //DevExpress.XtraGrid.GridSummaryItem summaryItemDis = Gv.Columns["DisSalePrice"].SummaryItem;
            Decimal totalAmount = (Decimal)summaryItem.SummaryValue;
            //Decimal disamount = (Decimal)summaryItemDis.SummaryValue;
            string summaryText = String.Format("{0} {1:N2} {2}", "Total :", totalAmount, "$");
            lblTotalAmount.Text = summaryText;
            lblTotalAmount.Tag = totalAmount;
            //  lblVATAmount.Text = String.Format("{0} {1:N2} {2}", "VAT Amount (10%) :", totalAmount - (totalAmount / 1.1), "$");
            // lblDiscountAmount.Text = "Discount Amount     : 0.0 $";
        }

        #endregion

        #region--- Product ---
        private void screenProduct_ItemClick(object sender, TileItemEventArgs e)
        {
       
            Decimal? price = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString() && x.Status == "AC").Any()
                ? _Products.Where(x =>
                 x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().SalePrice : 0;

            Decimal? dis = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString() && x.Status == "AC").Any()
                ? _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().DisSalePrice : 0;

            //get id (inventoryID) to ProductTypeID
            var t = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault();
            int ProductTypeID = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().InventoryID;
            var priceList =  _SalePrice.Where(a => a.EffectiveDate <= DateTime.Today.Date && a.ExpirationDate >= DateTime.Today.Date).
                FirstOrDefault(x => x.InventoryID == ProductTypeID && x.UOM == t.BaseUnit);


            if (priceList != null )
            { 
                if (_SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text && x.Price == priceList.SalesPrice).Any())
                {
                    var sale = _SaleProducts[_SaleProducts.IndexOf(_SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text && x.Price == priceList.SalesPrice).FirstOrDefault())];
                    //sale.Barcode = _SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text).FirstOrDefault().Barcode;
                    sale.ItemID = e.Item.Tag.ToString();
                    sale.ItemCode = e.Item.Elements[3].Text;
                    sale.ItemDescriptionEn = e.Item.Elements[0].Text;
                    sale.Altermate = priceList.AlternateID.Trim();
                    sale.Price = Convert.ToDecimal(priceList.SalesPrice);
                    sale.UOM = priceList.UOM;

                    sale.DiscountPrice = Convert.ToDecimal(dis);
                    sale.Quantity = _SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text && x.Price == priceList.SalesPrice).FirstOrDefault().Quantity + 1;
                    sale.Amount = (_SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text && x.Price == priceList.SalesPrice).FirstOrDefault().Quantity) * (Convert.ToDecimal(priceList.SalesPrice) - (Convert.ToDecimal(dis)));

                    Gv.FocusedRowHandle = _SaleProducts.IndexOf(_SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text && x.Price == priceList.SalesPrice).FirstOrDefault());
                }
                else
                {
                    //TODO: Initial Invoice Date and Time
                    if (_SaleProducts.Count == 0)
                    {
                        lblIssuedDate.Text = "Date  :" + DateTime.Now.ToString("dd-MMM-yyyy");
                        lblIssuedDate.Tag = DateTime.Now;
                        lblIssuedTime.Text = "Time :" + DateTime.Now.ToString("hh:mm tt");
                    }

                    _SaleProducts.Insert(0, new SaleProductDomain()
                    {
                        ProductTypeID = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().InventoryID,
                        ProductType = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().Descr,
                        //Barcode = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().Barcode,
                        Altermate = priceList.AlternateID.Trim(),
                        ItemID = e.Item.Tag.ToString(),
                        Currency = Convert.ToDecimal(lblUSDExchangeRate.Tag),
                        ItemCode = e.Item.Elements[3].Text,
                        ItemDescriptionEn = e.Item.Elements[0].Text,
                        Price = Convert.ToDecimal(priceList.SalesPrice),
                        DiscountPrice = Convert.ToDecimal(dis),
                        Quantity = 1,
                        UOM = priceList.UOM,
                        CustomerCode = luCustomer.EditValue.ToString(),      
                        CustomerNameEn = luCustomer.Text.ToString(),
                        Status = "O",
                        Amount = Convert.ToDecimal(priceList.SalesPrice) - Convert.ToDecimal(dis),
                    });
                    Gv.FocusedRowHandle = 0;
       
                }
            }
            else
            {
                if (_SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text && x.Barcode != null).Any())
                {
                    var sale = _SaleProducts[_SaleProducts.IndexOf(_SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text).FirstOrDefault())];
                    sale.Barcode = _SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text).FirstOrDefault().Barcode.Trim();
                    sale.ItemID = e.Item.Tag.ToString();
                    sale.ItemCode = e.Item.Elements[3].Text;
                    sale.ItemDescriptionEn = e.Item.Elements[0].Text;
                    sale.Price = Convert.ToDecimal(price);
                    sale.UOM = _SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text).FirstOrDefault().UOM;
                    sale.DiscountPrice = Convert.ToDecimal(dis);
                    sale.Quantity = _SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text).FirstOrDefault().Quantity + 1;
                    sale.Amount = (_SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text).FirstOrDefault().Quantity) * (Convert.ToDecimal(price) - (Convert.ToDecimal(dis)));

                    Gv.FocusedRowHandle = _SaleProducts.IndexOf(_SaleProducts.Where(x => x.ItemCode == e.Item.Elements[3].Text).FirstOrDefault());
                }
                else
                {
                    //TODO: Initial Invoice Date and Time
                    if (_SaleProducts.Count == 0)
                    {
                        lblIssuedDate.Text = "Date  :" + DateTime.Now.ToString("dd-MMM-yyyy");
                        lblIssuedDate.Tag = DateTime.Now;
                        lblIssuedTime.Text = "Time :" + DateTime.Now.ToString("hh:mm tt");
                    }

                    _SaleProducts.Insert(0, new SaleProductDomain()
                    {
                        ProductTypeID = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().InventoryID,
                        ProductType = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().Descr,
                        Barcode = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().Barcode.Trim(),
                        ItemID = e.Item.Tag.ToString(),
                        ItemCode = e.Item.Elements[3].Text,
                        ItemDescriptionEn = e.Item.Elements[0].Text,
                        Currency = Convert.ToDecimal(lblUSDExchangeRate.Tag),
                        Price = Convert.ToDecimal(price),
                        DiscountPrice = Convert.ToDecimal(dis),
                        Quantity = 1,
                        UOM = _Products.Where(x => x.InventoryCD == e.Item.Tag.ToString()).FirstOrDefault().BaseUnit,
                        CustomerCode = luCustomer.EditValue.ToString(),
                        CustomerNameEn = luCustomer.Text.ToString(),
                        Status = "O",
                        Amount = Convert.ToDecimal(price) - Convert.ToDecimal(dis),
                    });
                    Gv.FocusedRowHandle = 0;
                    
                }
            }
           
            //TODO: Recalculate CashChange
            if (_SaleProducts.Sum(x => x.CashInUSD) + _SaleProducts.Sum(x => x.CashInRiel) > 0)
                CashChange();
            else
            {
                lblCashInUSD.Text = "Cash In ($) : 0 $";
                lblCashInRiel.Text = "Cash In (៛) : 0 ៛";
                lblChangeUSD.Text = "Change ($) : 0 $";
                lblChangeRiel.Text = "Change (៛) : 0 ៛";
            }
            //   }

            GC.DataSource = null;
            GC.DataSource = _SaleProducts.ToList();
            TotalInvoiceAmount();
            txtBarcode.Focus();
        }
        private void screenProduct_MouseMove(object sender, MouseEventArgs e)
        {
            BeginInvoke((MethodInvoker)delegate
            {
                //toolTipController1.HideHint();
                TileControl control = sender as TileControl;
                TileControlHitInfo hi = control.CalcHitInfo(control.PointToClient(MousePosition));
                if (hi.InItem)
                    toolTipProduct.ShowHint(hi.ItemInfo.Item.Text, MousePosition);
                else
                    toolTipProduct.HideHint();
            });
        }
        #endregion


        #region --- Internal Function ---
        private Font getTitleAppearanceFont(float size)
        {
            AppearanceObject a = new AppearanceObject();
            a.Font = new System.Drawing.Font("Segoe UI Light", size,
                System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            return a.Font;
        }
        private Color getGlobalColor(int R, int G, int B)
        {
            return System.Drawing.Color.FromArgb(R, G, B);
        }
        #endregion

        #region --- Internal Sub Procedure ---
        private void InitialCategory(List<INItemCategory> list)
        {
            tileGroupCategory.Items.Clear();
            for (int i = 0; i < list.ToList().Count; i++)
            {
                var tileItem = new TileItem();
                var category = list.ToList()[i];

                var tieCategory = new TileItemElement() { Image = ((category.ImagesUrl == null || category.ImagesUrl.Length <= 0) ? global::Nexvis.NexPOS.Properties.Resources.noimageCopy : GlobalInitializer.GfConvertByteArrayToImage(category.ImagesUrl)), Text = category.CategoryName, TextAlignment = TileItemContentAlignment.Manual, ImageAlignment = TileItemContentAlignment.MiddleLeft, TextLocation = new Point(50, 15), ImageSize = new Size(50, 50), ImageScaleMode = TileItemImageScaleMode.ZoomOutside };
                tieCategory.Appearance.Normal.Font = getTitleAppearanceFont(11);
                tieCategory.Appearance.Normal.TextOptions.Trimming = Trimming.EllipsisCharacter;
                tieCategory.Appearance.Normal.TextOptions.WordWrap = WordWrap.NoWrap;
                tileItem.Elements.Add(tieCategory);

                tileItem.Tag = category.CategoryID;
                tileItem.ItemSize = TileItemSize.Wide;
                tileItem.Checked = true;

                tileItem.Tag = category.CategoryID;
                tileGroupCategory.Items.Add(tileItem);
            }
        }
        private void InitialProduct(List<INItem> list)
        {
            var checkSalePirce = _SalePrice.Where(x => _Products.Where(y => y.InventoryID == x.InventoryID && y.BaseUnit == x.UOM).Any() &&  x.EffectiveDate <= DateTime.Today.Date && x.ExpirationDate >= DateTime.Today.Date).ToList();
            tileGroupProductList.Items.Clear();
            for (int i = 0; i < list.Where(x => x.Status.Trim() == "AC").ToList().Count; i++)
            {
                var tileItem = new TileItem();
                var item = list.Where(x => x.Status.Trim() == "AC").ToList()[i];

                decimal? price;

                price = checkSalePirce.Where(x => x.InventoryID == item.InventoryID).Any() ? checkSalePirce.Where(x => x.InventoryID == item.InventoryID).FirstOrDefault().SalesPrice : 0;
            
                if(price == 0)
                    price = _Products.Where(x => x.InventoryCD == item.InventoryCD && x.Status.Trim() == "AC").Any() ? _Products.Where(x => x.InventoryCD == item.InventoryCD).FirstOrDefault().SalePrice : 0;

                var tieItemDescriptionEn = new TileItemElement() { Text = item.Descr, MaxWidth = 160, TextAlignment = TileItemContentAlignment.Manual, TextLocation = new Point(100, 0) };
                tieItemDescriptionEn.Appearance.Normal.Font = getTitleAppearanceFont(17);
                tieItemDescriptionEn.Appearance.Normal.ForeColor = item.InventoryID == 1 ? Color.DarkRed : Color.DarkBlue;
                tieItemDescriptionEn.Appearance.Normal.TextOptions.Trimming = Trimming.EllipsisCharacter;
                tieItemDescriptionEn.Appearance.Normal.TextOptions.WordWrap = WordWrap.NoWrap;
                tileItem.Elements.Add(tieItemDescriptionEn);

                var tieItemPrice = new TileItemElement() { Text = price.Value.ToString("C2"), MaxWidth = 160, TextAlignment = TileItemContentAlignment.Manual, TextLocation = new Point(100, 30) };
                tieItemPrice.Appearance.Normal.Font = getTitleAppearanceFont(15.75F);
                tieItemPrice.Appearance.Normal.ForeColor = price == 0 ? Color.Red : Color.Green;
                tieItemPrice.Appearance.Normal.TextOptions.Trimming = Trimming.EllipsisCharacter;
                tieItemPrice.Appearance.Normal.TextOptions.WordWrap = WordWrap.NoWrap;
                tileItem.Elements.Add(tieItemPrice);

                var tieUOM = new TileItemElement() { Text = item.BaseUnit, MaxWidth = 160, TextAlignment = TileItemContentAlignment.Manual, TextLocation = new Point(100, 60) };
                tieUOM.Appearance.Normal.Font = getTitleAppearanceFont(12);
                tieUOM.Appearance.Normal.ForeColor = Color.Gray;
                tieUOM.Appearance.Normal.TextOptions.Trimming = Trimming.EllipsisCharacter;
                tieUOM.Appearance.Normal.TextOptions.WordWrap = WordWrap.NoWrap;
                tileItem.Elements.Add(tieUOM);

                var tieItemCode = new TileItemElement() { Text = item.InventoryCD, MaxWidth = 160, TextAlignment = TileItemContentAlignment.Manual, TextLocation = new Point(100, 84) };
                tieItemCode.Appearance.Normal.Font = getTitleAppearanceFont(12);
                tieItemCode.Appearance.Normal.ForeColor = Color.Gray;
                tieItemCode.Appearance.Normal.TextOptions.Trimming = Trimming.EllipsisCharacter;
                tieItemCode.Appearance.Normal.TextOptions.WordWrap = WordWrap.NoWrap;
                tileItem.Elements.Add(tieItemCode);

                var tieImage = new TileItemElement() { Image = ((item.ImageUrl == null || item.ImageUrl.Length <= 0) ? global::Nexvis.NexPOS.Properties.Resources.noimageCopy : GlobalInitializer.GfConvertByteArrayToImage(item.ImageUrl)), MaxWidth = 160, ImageAlignment = TileItemContentAlignment.Manual, ImageLocation = new Point(0, 2) };
                tieImage.ImageScaleMode = TileItemImageScaleMode.ZoomOutside;
                tieImage.ImageSize = new System.Drawing.Size(100, 100);
                tileItem.Elements.Add(tieImage);

                tileItem.Tag = item.InventoryCD;
                tileItem.ItemSize = TileItemSize.Wide;
                tileItem.TextAlignment = TileItemContentAlignment.Manual;
                tileItem.AppearanceItem.Normal.BackColor = Color.AliceBlue;
                tileItem.AppearanceItem.Normal.BorderColor = Color.Green;

                tileGroupProductList.Items.Add(tileItem);
            }
        }
        public void ClearSaleProduct()
        {
            // GetServerDateTime(null, null);
            _SaleProducts.Clear();
            lblIssuedDate.Text = "Date  :";
            lblIssuedDate.Tag = "";
            lblIssuedTime.Text = "Time :";
            //  lblCustomer.Text = "Customer :" + _Customers.FirstOrDefault().CustomerName;
            // lblCustomer.Tag = _Customers.FirstOrDefault().CustomerID;
            //lblCashier.Text = "Cashier    :" + UserLogOn.EmployeeName;
            lblTotalAmount.Text = "Total : 0.0 $";
            lblCashInUSD.Text = "Cash In ($) : 0 $";
            lblCashInRiel.Text = "Cash In (៛) : 0 ៛";
            lblChangeUSD.Text = "Change ($) : 0 $";
            lblChangeRiel.Text = "Change (៛) : 0 ៛";
            // lblUSDExchangeRate.Text = string.Format("{0} : {1:N0} {2}", "Exchange Rate  1$", _USDExchangeRates.FirstOrDefault().ExchangeRate, "៛");
            if (_USDExchangeRates.Count != 0)
            {
                lblUSDExchangeRate.Text = string.Format("{0} : {1:N0} {2}", "Exchange Rate  1$", _USDExchangeRates.FirstOrDefault().CuryRate, "៛");
            }
            
            lblDiscountAmount.Text = "Discount Amount     : 0.0 $";
            lblVATAmount.Text = "VAT Amount (10%) : 0.0 $";

            invoice = null;
            PaymentRef = null;

            _SaleProducts.Clear();
            Gv.RefreshData();
            GC.DataSource = null;
            _Invoice = DB.ARInvoices.Where(w => w.Reverse == false).ToList();
            GlobalInitializer.GsFillLookUpEdit(_Invoice.Where(x => x.Status == "C").ToList(), "InvoiceRefNbr", "InvoiceRefNbr", luInvoice, (int)_Invoice.Where(x => x.Status == "C").ToList().Count);

            //MessageBox.Show(luInvoice.CanSelect.ToString());

            //luInvoice.EditValue =null ;
          

        }
        #endregion

        #region --- Global Sub Procedure ---
        //TODO: Change Cash Remaining with RIEL Only
      
        public void CashChange()
        {
            try
            {
                Decimal ExchangeRate = Convert.ToDecimal(_USDExchangeRates.FirstOrDefault().CuryRate); //_SaleProducts.Where(x => x.ExchangeRate != 0).FirstOrDefault().ExchangeRate;
                Decimal CashInUSD = _SaleProducts.Where(x => x.CashInUSD != 0).Any() ? _SaleProducts.Where(x => x.CashInUSD != 0).FirstOrDefault().CashInUSD : 0;
                Decimal CashInRiel = _SaleProducts.Where(x => x.CashInRiel != 0).Any() ? _SaleProducts.Where(x => x.CashInRiel != 0).FirstOrDefault().CashInRiel : 0;
                Decimal TotalPayment = _SaleProducts.Sum(x => x.Amount);
                Decimal CashInUSDRemaining = 0;
                Decimal ChangeUSD = 0;
               

                if ((CashInUSD + CashInRiel) > 0)
                {
                    CashInUSDRemaining = CashInUSD + (CashInRiel / ExchangeRate) - TotalPayment;
                    ChangeUSD = Convert.ToInt32(CashInUSDRemaining.ToString().Split('.')[0]);
                   // ChangeUSD = Convert.ToDecimal(CashInUSDRemaining.ToString());

                    lblChangeUSD.Text = string.Format("{0} : {1:N0} {2}", "Change ($)", ChangeUSD, "$");
                    _SaleProducts.ForEach(x => x.ChangeUSD = ChangeUSD);
                    _SaleProducts.ForEach(x => x.ChangeRiel = (Decimal)(CashInUSDRemaining - ChangeUSD) * ExchangeRate);

                    if (_SaleProducts.FirstOrDefault().ChangeRiel >= 0)
                    {
                        lblChangeRiel.Text = string.Format("{0} : {1:N0} {2}", "Change (៛)", _obj.RoundRielAmountDown(Math.Round(_SaleProducts.FirstOrDefault().ChangeRiel, 0)), "៛");
                        _SaleProducts.ForEach(x => x.ChangeRiel = _obj.RoundRielAmountDown(Math.Round(_SaleProducts.FirstOrDefault().ChangeRiel, 0)));
                    }
                    else
                    {
                        lblChangeRiel.Text = string.Format("{0} : {1:N0} {2}", "Change (៛)", _obj.RoundRielAmountUp(Math.Round(_SaleProducts.FirstOrDefault().ChangeRiel, 0) * -1) * -1, "៛");
                        _SaleProducts.ForEach(x => x.ChangeRiel = _obj.RoundRielAmountUp(Math.Round(_SaleProducts.FirstOrDefault().ChangeRiel, 0) * -1) * -1);
                    }
                }
                else
                {
                    lblChangeUSD.Text = string.Format("{0} : {1:N0} {2}", "Change ($)", 0, "$");
                    lblChangeRiel.Text = string.Format("{0} : {1:N0} {2}", "Change (៛)", 0, "៛");
                    _SaleProducts.ForEach(x => x.ChangeUSD = 0);
                    _SaleProducts.ForEach(x => x.ChangeRiel = 0);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Invoice Payment Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion


        #region --- Category ---
        private void tileBarCategory_ItemClick(object sender, TileItemEventArgs e)
        {
            e.Item.Checked = e.Item.Checked ? false : true;
            var categoryCode = tileGroupCategory.Items.Where(x => x.Checked == true).Select(y => y.Tag.ToString()).ToArray();
            var ProductList = new List<INItem>(_Products.Where(x => categoryCode.Contains(x.CategoryID.ToString())).OrderBy(x => x.Descr).ThenBy(y => y.Descr));
            // TODO: Initial Product List
            InitialProduct(ProductList);
        }
        #endregion

        #region --- Invoice ---

        private void btnPayBill_Click(object sender, EventArgs e)
        {

            if (_SaleProducts.Count == 0)
                XtraMessageBox.Show("Please pick up product item you want to sell first !", "Sale Product Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                FrmPayBill objPayBill = new FrmPayBill(this);
                FormShadow Shadow = new FormShadow(objPayBill);


                _SaleProducts.ForEach(y => y.ExchangeRate = Convert.ToDecimal(_USDExchangeRates.FirstOrDefault().CuryRate));
                objPayBill.ExchangeRate = _SaleProducts.Where(x => x.ExchangeRate != 0).Any() ? _SaleProducts.Where(x => x.ExchangeRate != 0).FirstOrDefault().ExchangeRate : 0;
                objPayBill.txtCashInUSD.EditValue = _SaleProducts.Where(x => x.CashInUSD != 0).Any() ? _SaleProducts.Where(x => x.CashInUSD != 0).FirstOrDefault().CashInUSD : 0;
                objPayBill.txtCashInRiel.EditValue = _SaleProducts.Where(x => x.CashInRiel != 0).Any() ? _SaleProducts.Where(x => x.CashInRiel != 0).FirstOrDefault().CashInRiel : 0;
                objPayBill.txtChangeUSD.EditValue = _SaleProducts.Where(x => x.ChangeUSD != 0).Any() ? _SaleProducts.Where(x => x.ChangeUSD != 0).FirstOrDefault().ChangeUSD : 0;
                objPayBill.txtChangeRiel.EditValue = _SaleProducts.Where(x => x.ChangeRiel != 0).Any() ? _SaleProducts.Where(x => x.ChangeRiel != 0).FirstOrDefault().ChangeRiel : 0;

                objPayBill.txtGrandTotalUSD.EditValue = _SaleProducts.Sum(w => w.Amount);
                objPayBill.txtGrandTotalRiel.EditValue = _SaleProducts.Sum(w => w.Amount) * _USDExchangeRates.FirstOrDefault().CuryRate;

                objPayBill.TotalPayment = (Decimal)Gv.Columns["Amount"].SummaryItem.SummaryValue;


        

                if (luCustomer.EditValue == null)
                {
                    XtraMessageBox.Show("Please Select Customer!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;

                }
               // DialogResult dr = Shadow.ShowDialog(this);
                 Shadow.ShowDialog();

                if (objPayBill.IsLogg == false)
                {
                   // e.Cancel = true;
                    Shadow.Close();
                    return;
                }

                if ((decimal)objPayBill.txtCashInRiel.EditValue != 0)
                {
                    if (objPayBill.txtDisGrandD.EditValue.ToString() != "0")
                    {
                        if ((decimal)objPayBill.txtCashInUSD.EditValue < (decimal)objPayBill.txtDisGrandD.EditValue)
                        {
                            CASHINUSD = (decimal)objPayBill.txtCashInUSD.EditValue;
                            objPayBill.txtCashInUSD.EditValue = Convert.ToDecimal(objPayBill.txtCashInUSD.EditValue) + (Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue) / (_SaleProducts.FirstOrDefault().ExchangeRate));
                            // CASHINUSD = Convert.ToDecimal(objPayBill.txtCashInUSD.EditValue) + (Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue) / (_SaleProducts.FirstOrDefault().ExchangeRate));

                            if ((decimal)objPayBill.txtGrandTotalUSD.EditValue > (decimal)objPayBill.txtCashInUSD.EditValue)
                            {
                                objPayBill.txtChangeUSD.EditValue = (decimal)objPayBill.txtGrandTotalUSD.EditValue - (decimal)objPayBill.txtCashInUSD.EditValue;
                            }
                            else objPayBill.txtChangeUSD.EditValue = (decimal)objPayBill.txtCashInUSD.EditValue - (decimal)objPayBill.txtGrandTotalUSD.EditValue;

                            // CHANGE = (decimal)objPayBill.txtCashInUSD.EditValue - (decimal)objPayBill.txtGrandTotalUSD.EditValue;


                            _SaleProducts.ForEach(y => y.CashInUSD = Convert.ToDecimal(objPayBill.txtCashInUSD.EditValue));
                        }
                        else
                        {
                            CASHINUSD = (decimal)objPayBill.txtCashInUSD.EditValue;
                            objPayBill.txtChangeUSD.EditValue = (Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue) / (_SaleProducts.FirstOrDefault().ExchangeRate));
                        }
                    }
                    else
                    {
                        if ((decimal)objPayBill.txtCashInUSD.EditValue < (decimal)objPayBill.txtGrandTotalUSD.EditValue)
                        {
                            CASHINUSD = (decimal)objPayBill.txtCashInUSD.EditValue;
                            objPayBill.txtCashInUSD.EditValue = Convert.ToDecimal(objPayBill.txtCashInUSD.EditValue) + (Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue) / (_SaleProducts.FirstOrDefault().ExchangeRate));
                            // CASHINUSD = Convert.ToDecimal(objPayBill.txtCashInUSD.EditValue) + (Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue) / (_SaleProducts.FirstOrDefault().ExchangeRate));

                            objPayBill.txtChangeUSD.EditValue = (decimal)objPayBill.txtCashInUSD.EditValue - (decimal)objPayBill.txtGrandTotalUSD.EditValue;

                            // CHANGE = (decimal)objPayBill.txtCashInUSD.EditValue - (decimal)objPayBill.txtGrandTotalUSD.EditValue;


                            _SaleProducts.ForEach(y => y.CashInUSD = Convert.ToDecimal(objPayBill.txtCashInUSD.EditValue));

                            objPayBill.txtDisGrandD.EditValue = objPayBill.txtGrandTotalUSD.EditValue;
                        }
                        else
                        {
                            CASHINUSD = (decimal)objPayBill.txtCashInUSD.EditValue;
                            objPayBill.txtChangeUSD.EditValue = (Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue) / (_SaleProducts.FirstOrDefault().ExchangeRate));

                            objPayBill.txtDisGrandD.EditValue = objPayBill.txtGrandTotalUSD.EditValue;
                        }
                        //  }      

                    }
                }
                else
                {
                    if (objPayBill.txtDisGrandR.EditValue.ToString() != "0")
                    {
                        if ((decimal)objPayBill.txtCashInUSD.EditValue >= (decimal)objPayBill.txtDisGrandD.EditValue)
                        {
                            CASHINUSD = (decimal)objPayBill.txtCashInUSD.EditValue;
                            // objPayBill.txtChangeUSD.EditValue = (Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue) / (_SaleProducts.FirstOrDefault().ExchangeRate));
                            objPayBill.txtChangeUSD.EditValue = (decimal)objPayBill.txtCashInUSD.EditValue - (decimal)objPayBill.txtDisGrandD.EditValue;
                        }
                    }
                    else
                    {

                        if ((decimal)objPayBill.txtCashInUSD.EditValue >= (decimal)objPayBill.txtGrandTotalUSD.EditValue)
                        {
                            CASHINUSD = (decimal)objPayBill.txtCashInUSD.EditValue;
                            objPayBill.txtChangeUSD.EditValue = (Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue) / (_SaleProducts.FirstOrDefault().ExchangeRate));
                            objPayBill.txtChangeUSD.EditValue = (decimal)objPayBill.txtCashInUSD.EditValue - (decimal)objPayBill.txtGrandTotalUSD.EditValue;
                           
                            objPayBill.txtDisGrandD.EditValue = (decimal)objPayBill.txtGrandTotalUSD.EditValue;

                        }
                    }
                }

                DisAm = Convert.ToDecimal(objPayBill.txtDisAmount.EditValue.ToString());
                DisPer = Convert.ToDecimal(objPayBill.txtDiscPercent.EditValue.ToString());
                TotalPay = (decimal)objPayBill.txtDisGrandD.EditValue;

                TMoney = CASHINUSD + Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue) / (_SaleProducts.FirstOrDefault().ExchangeRate);

                if (TMoney < (decimal)objPayBill.txtDisGrandD.EditValue)
                {
                    //MessageBox.Show("Money is not enough");
                    XtraMessageBox.Show("Your money is not enough.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                try
                 {


                    if (luBrowse.EditValue  != null)
                    {
                        invoice = luBrowse.EditValue.ToString();
                        var checkdup = DB.ARInvoices.FirstOrDefault(w => w.InvoiceRefNbr == invoice
                            && w.CompanyID == DACClasses.CompanyID);

                        if (checkdup != null)
                        {
                            var checkdupList = DB.ARInvoicesDetails.Where(w => w.InvoiceRefNbr.ToString() == invoice && w.CompanyID == DACClasses.CompanyID).ToList();
                            foreach (var read in checkdupList)
                            {
                                DB.ARInvoicesDetails.Remove(read);
                                DB.SaveChanges();
                            }
                            //_SaleProducts.ForEach(x => x.InvoiceID = number.ToString());
                            foreach (var sale in _SaleProducts.ToList())
                            {

                                ARInvoicesDetail item = new ARInvoicesDetail()
                                {
                                    CompanyID = DACClasses.CompanyID,
                                    InventoryID = sale.ProductTypeID,
                                    ItemCode = sale.ItemCode,
                                    Barcode = sale.Barcode,
                                    InvoiceRefNbr = invoice,
                                    Descr = sale.ItemDescriptionEn,
                                    InvoiceTypes = "INV",
                                    Status = "C",
                                    DisAmount = sale.DiscountPrice,
                                    InvoiceDate = DateTime.Now,
                                    UOM = sale.UOM,
                                    Qty = sale.Quantity,
                                    UnitPrice = sale.Price,
                                    CustomerID = luCustomer.EditValue.ToString(),
                                    CustomerName = luCustomer.Text,
                                    SalePersonID = CSUserModel.LoginName,
                                    Reverse = false,
                                    CuryAmount = sale.Amount * (decimal)lblUSDExchangeRate.Tag,
                                    TotalPayment = Convert.ToDecimal(objPayBill.txtDisGrandD.EditValue),
                                    Amount = sale.Amount

                                };
                                DB.ARInvoicesDetails.Add(item);
                            }


                            checkdup.SalePersonID = CSUserModel.LoginName;
                            checkdup.Status = "C";
                            checkdup.TotalQty = _SaleProducts.Sum(x => x.Quantity);
                            checkdup.CustomerID = luCustomer.EditValue.ToString();
                            checkdup.CustomerName = luCustomer.Text.ToString();
                            checkdup.TotalAmount = objPayBill.TotalPayment;
                            checkdup.TotalPayment = Convert.ToDecimal(objPayBill.txtDisGrandD.EditValue);
                            checkdup.LastModifiedBy = CSUserModel.User;
                            checkdup.LastModifiedDateTime = DateTime.Now;
                            checkdup.IsCloseBalan = false;
                            checkdup.CheckDiscount = objPayBill.CheckDiscount;

                            if (checkdup.CheckDiscount == true)
                            {
                                checkdup.Descr = "Have Discount";
                            }
                            else
                            {
                                checkdup.Descr = "Don't have Discount";
                            }

                            DB.Entry(checkdup).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                    else
                    {
                        //auto number invocie
                        var resul = new CFNumberRank("INV", DocConfType.Normal, true);
                        invoice = resul.NextNumberRank.Trim();
                        foreach (var sale in _SaleProducts.ToList())
                        {

                            ARInvoicesDetail item = new ARInvoicesDetail()
                            {
                                CompanyID = DACClasses.CompanyID,
                                InventoryID = sale.ProductTypeID,
                                ItemCode = sale.ItemCode,
                                Barcode = sale.Barcode,
                                InvoiceRefNbr = invoice,
                                Descr = sale.ItemDescriptionEn,
                                InvoiceTypes = "INV",
                                Status = "C",
                                DisAmount = sale.DiscountPrice,
                                InvoiceDate = DateTime.Now,
                                UOM = sale.UOM,
                                Qty = sale.Quantity,
                                UnitPrice = sale.Price,
                                CustomerID = sale.CustomerCode,
                                CustomerName = sale.CustomerNameEn,
                                SalePersonID = CSUserModel.LoginName,
                                Reverse = false,
                                Amount = sale.Amount,
                                TotalPayment = Convert.ToDecimal(objPayBill.txtDisGrandD.EditValue),
                                CuryAmount = sale.Amount * (decimal)lblUSDExchangeRate.Tag,
                                CheckDiscount = objPayBill.CheckDiscount,


                            };
                            sale.InvoiceID = invoice;
                            sale.ExchangeRate = (decimal)lblUSDExchangeRate.Tag;

                            if (item.CheckDiscount == true)
                            {
                                item.DescrDiscount = "Have Discount";
                            }
                            else
                            {
                                item.DescrDiscount = "Don't have Discount";
                            }


                            DB.ARInvoicesDetails.Add(item);

                        }


                        ARInvoice aRInvoice = new ARInvoice()
                        {
                            CompanyID = DACClasses.CompanyID,
                            InvoiceRefNbr = invoice,
                            Status = "C",
                            // Descr = descr,
                            InvoiceTypes = "INV",
                            CuryID = "USD",
                            CuryInfoID = 2,
                            Reverse = false,
                            CheckDiscount = objPayBill.CheckDiscount,
                            // Dis = sale.DiscountAmount,
                            InvoiceDate = DateTime.Today,
                            CustomerID = luCustomer.EditValue.ToString(),
                            CustomerName = luCustomer.Text,
                            SalePersonID = CSUserModel.LoginName,
                            TotalQty = _SaleProducts.Sum(x => x.Quantity),
                            TotalAmount = objPayBill.TotalPayment,
                            TotalPayment = Convert.ToDecimal(objPayBill.txtDisGrandD.EditValue),
                            CreatedBy = CSUserModel.User,
                            CreatedDateTime = DateTime.Now,
                            IsCloseBalan = false

                        };

                        if (aRInvoice.CheckDiscount == true)
                        {
                            aRInvoice.Descr = "Have Discount";
                        }
                        else
                        {
                            aRInvoice.Descr = "Don't have Discount";
                        }

                        DB.ARInvoices.Add(aRInvoice);
                    }

                    var resul1 = new CFNumberRank("PAY", DocConfType.Normal, true);
                    PaymentRef = resul1.NextNumberRank.Trim();

                    ARPaymentDetail pay = new ARPaymentDetail()
                    {
                        CompanyID = DACClasses.CompanyID,
                        PaymentRefNbr = PaymentRef,
                        InvoiceRefNbr = invoice,
                        InvoiceTypes = "PMT",
                        Status = "R",
                        //PaymentTypes = "PMT",
                        PaymentTypes = objPayBill.cboPaymentType.EditValue.ToString(),
                        CuryID = "USD",
                        CuryInfoID = 2,
                        PaymentDate = DateTime.Today.Date,
                        CustomerID = luCustomer.EditValue.ToString(),
                        CustomerName = luCustomer.Text,
                        //  Balance = (decimal)(objPayBill.txtChangeUSD.EditValue),
                        BalanceUSD = _SaleProducts.First().ChangeUSD,
                        BalanceKHR = _SaleProducts.First().ChangeRiel,
                        ReceiveAmountUSD = CASHINUSD,
                        ReceiveAmountKHR = (decimal)(objPayBill.txtCashInRiel.EditValue),
                        DiscountAmount = Convert.ToDecimal(objPayBill.txtDisAmount.EditValue.ToString()),
                        DiscountPercent = Convert.ToDecimal(objPayBill.txtDiscPercent.EditValue.ToString()),
                        PaymentAmount = Convert.ToDecimal(objPayBill.txtDisGrandD.EditValue),
                        OriginalAmount = Convert.ToDecimal(objPayBill.txtGrandTotalUSD.EditValue.ToString()),
                        CreatedBy = CSUserModel.User,
                        CreatedDateTime = DateTime.Now,
                        IsCloseBalan = false
                    };



                    DB.ARPaymentDetails.Add(pay);



                    ARPayment payment = new ARPayment()
                    {
                        CompanyID = DACClasses.CompanyID,
                        PaymentRefNbr = PaymentRef,
                        PaymentTypes = "PMT",
                        PaymentMethodID = objPayBill.cboPaymentType.Text,
                        CuryID = "USD",
                        CuryInfoID = 2,
                        PaymentDate = DateTime.Today.Date,
                        CustomerID = luCustomer.EditValue.ToString(),
                        ReceiveAmount = (decimal)(objPayBill.txtCashInUSD.EditValue),
                        DiscountAmount = Convert.ToDecimal(objPayBill.txtDisAmount.EditValue.ToString()),
                        DiscountPercent = Convert.ToDecimal(objPayBill.txtDiscPercent.EditValue.ToString()),
                        Balance = (decimal)(objPayBill.txtChangeUSD.EditValue),
                        PaymentAmount = Convert.ToDecimal(objPayBill.txtDisGrandD.EditValue),
                        CreatedBy = CSUserModel.User,
                        CreatedDateTime = DateTime.Now,
                        IsCloseBalan = false
                    };




                    DB.ARPayments.Add(payment);


                    var Row = DB.SaveChanges();

                    _SaleProducts.ForEach(y => y.CashInUSD = CASHINUSD);
                    _SaleProducts.ForEach(y => y.DisAmount = DisAm);
                    _SaleProducts.ForEach(y => y.DisPercent = DisPer);
                    _SaleProducts.ForEach(y => y.TotalPayment = TotalPay);

                    _SaleTemporary = DB.ARInvoices.Where(x => x.Status == "H").ToList();
                    GlobalInitializer.GsFillLookUpEdit(
                       _SaleTemporary.ToList(), "InvoiceRefNbr", "InvoiceRefNbr", luBrowse, (int)_SaleTemporary.ToList().Count);

                    if (cboprint.EditValue =="Print Receipt")
                    {
                        rptReceipt rptA5 = new rptReceipt();

                        rptA5.DataSource = _SaleProducts;
                        rptA5.Parameters["Cashier"].Value = CSUserModel.LoginName;
                        rptA5.RequestParameters = false;
                        ReportPrintTool printTool = new ReportPrintTool(rptA5);
                        printTool.ShowPreview();
                    }
                    else if (cboprint.EditValue == "Print A5")
                    {
                        ReportA5 rptA5 = new ReportA5();

                        rptA5.DataSource = _SaleProducts;
                        rptA5.Parameters["Cashier"].Value = CSUserModel.LoginName;
                        rptA5.RequestParameters = false;
                        ReportPrintTool printTool = new ReportPrintTool(rptA5);
                        printTool.ShowPreview();
                    }
                    else if (cboprint.EditValue == "Print A3")
                    {
                        rptReceiptA3 rptA5 = new rptReceiptA3();

                        rptA5.DataSource = _SaleProducts;
                        rptA5.Parameters["Cashier"].Value = CSUserModel.LoginName;
                        rptA5.RequestParameters = false;
                        ReportPrintTool printTool = new ReportPrintTool(rptA5);
                        printTool.ShowPreview();
                    }
                    else if (cboprint.EditValue == "Print A4")
                    {
                        rptReceiptA4 rptA5 = new rptReceiptA4();

                        rptA5.DataSource = _SaleProducts;
                        rptA5.Parameters["Cashier"].Value = CSUserModel.LoginName;
                        rptA5.RequestParameters = false;
                        ReportPrintTool printTool = new ReportPrintTool(rptA5);
                        printTool.ShowPreview();
                    }

                    lblChangeUSD.Tag = _SaleProducts.First().ChangeUSD;
                    lblChangeUSD.Text = string.Format("{0} : {1:F} ", "Change USD ($)", lblChangeUSD.Tag, "$");

                    lblDiscountAmount.Tag = _SaleProducts.First().DisAmount;
                    lblDiscountAmount.Text = "Discount Amount     : " + string.Format("{0:N2}", lblDiscountAmount.Tag) + "$";

                    btnPayBill.Enabled = false;
                    screenProduct.Enabled = false;
                    btnSkip.Enabled = false;
                    btnDeleteBrowse.Enabled = false;
                    luBrowse.Enabled = false;
                    GC.Enabled = false;
                    txtBarcode.Text = "";
                    luBrowse.EditValue = null;

                }
                catch(Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void GV_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            if (e.HitInfo.InRow)
                popMenu.ShowPopup(Control.MousePosition);
        }

        private void ppmUnPickupItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to unpick up this item?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                _SaleProducts.RemoveAt(_SaleProducts.IndexOf((SaleProductDomain)Gv.GetRow(Gv.FocusedRowHandle)));
                TotalInvoiceAmount();
                //TODO: Recalculate CashChange
                if (_SaleProducts.Sum(x => x.CashInUSD) + _SaleProducts.Sum(x => x.CashInRiel) > 0)
                    CashChange();
                else
                {
                    lblCashInUSD.Text = "Cash In ($) : 0 $";
                    lblCashInRiel.Text = "Cash In (៛) : 0 ៛";
                    lblChangeUSD.Text = "Change ($) : 0 $";
                    lblChangeRiel.Text = "Change (៛) : 0 ៛";
                }
            }
        }

        private void ppmEditQuantity_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //if (_SaleProducts.Where(x => x.ProductTypeID == 1).Any())
            //    XtraMessageBox.Show("Please clear Membership Payment first!", "Sale Product Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //else
            //{
            //    FrmEditQuantity objEditQuantity = new FrmEditQuantity(this);
            //    FormShadow Shadow = new FormShadow(objEditQuantity);
            //    objEditQuantity.txtQuantity.EditValue = ((SaleProductDomain)GV.GetRow(GV.FocusedRowHandle)).Quantity;
            //    Shadow.ShowDialog();
            //}
        }

        private void GV_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            // e.Appearance.BackColor = CommonSkins.GetSkin(LookAndFeel.ActiveLookAndFeel).Colors.GetColor(CommonColors.Window);
            // e.Appearance.ForeColor = CommonSkins.GetSkin(LookAndFeel.ActiveLookAndFeel).Colors.GetColor(CommonColors.ControlText);
        }



        private void repositoryTransaction_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var result = (SaleProductDomain)Gv.GetRow(Gv.FocusedRowHandle);
            Decimal quantity;
            Decimal price;
            Decimal disprice;
            if (result != null)
            {
                quantity = result.Quantity;
                price = result.Price;
                disprice = result.DiscountPrice;
                if (e.Button.Index == 0)
                {
                    //if (_SaleProducts.Where(x => x.ProductTypeID == 1).Any())
                    //    XtraMessageBox.Show("Please clear Membership Payment first!", "Sale Product Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //else
                    //{
                        var increaseValue = quantity + 1;
                        Gv.SetRowCellValue(Gv.FocusedRowHandle, Gv.Columns["Quantity"], increaseValue);
                        Gv.SetRowCellValue(Gv.FocusedRowHandle, Gv.Columns["Amount"], increaseValue * (price - disprice));
                   // }
                }
                else
                {
                    var decreaseValue = quantity - 1;
                    if (decreaseValue <= 0)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to remove this item?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            _SaleProducts = _SaleProducts.Where(w => w.ItemCode != result.ItemCode).ToList();
                            Gv.DeleteSelectedRows(); }
                    }
                    else
                    {
                        Gv.SetRowCellValue(Gv.FocusedRowHandle, Gv.Columns["Quantity"], decreaseValue);
                        Gv.SetRowCellValue(Gv.FocusedRowHandle, Gv.Columns["Amount"], decreaseValue * (price - disprice));
                    }
                }
                TotalInvoiceAmount();
                //TODO: Recalculate CashChange
                if (_SaleProducts.Sum(x => x.CashInUSD) + _SaleProducts.Sum(x => x.CashInRiel) > 0)
                    CashChange();
                else
                {
                    lblCashInUSD.Text = "Cash In ($) : 0 $";
                    lblCashInRiel.Text = "Cash In (៛) : 0 ៛";
                    lblChangeUSD.Text = "Change ($) : 0 $";
                    lblChangeRiel.Text = "Change (៛) : 0 ៛";
                }
            }
        }

        #endregion
        private void txtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (GC.Enabled == false)
                {
                    return;
                }
                var LocalBarcode = txtBarcode.Text.Trim();
                var ProCount = _Products.Where(w => w.Barcode.TrimEnd() == LocalBarcode).ToList();
                var checkPrice = new MDSalesPrice();


                if (LocalBarcode == "" || LocalBarcode == null)
                {
                    return;
                }
                //Check Barcode if have SalePrice on Base Unit
                if (ProCount.Count == 0)
                {
                    checkPrice = _SalePrice.Where(a => a.EffectiveDate <= DateTime.Today.Date && a.ExpirationDate >= DateTime.Today.Date)
                        .FirstOrDefault(w => w.AlternateID.Trim() == LocalBarcode);
                }
                else
                {
                    checkPrice = _SalePrice.Where(a => a.EffectiveDate <= DateTime.Today.Date && a.ExpirationDate >= DateTime.Today.Date).
                        FirstOrDefault(w => w.InventoryID == ProCount.FirstOrDefault().InventoryID && w.UOM == ProCount.FirstOrDefault().BaseUnit);
                }
                

                if(checkPrice != null) 
                    ProCount = _Products.Where(w => w.InventoryID == checkPrice.InventoryID).ToList();

               
                if (ProCount.Count == 0)
                {
                    XtraMessageBox.Show("Invalid Product", "Sale Product Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBarcode.Focus();
                    txtBarcode.SelectAll();
                }
                else
                {
                    if (checkPrice != null)
                    {
                        //Altermate
                        if (_SaleProducts.Where(w => w.Altermate == LocalBarcode && w.ProductTypeID == checkPrice.InventoryID).Any())
                        {
                            var sale = _SaleProducts[_SaleProducts.IndexOf(_SaleProducts.Where(x => x.Altermate == LocalBarcode || x.UOM == checkPrice.UOM).FirstOrDefault())];

                            sale.Quantity = _SaleProducts.Where(x => x.Altermate == LocalBarcode || x.UOM == checkPrice.UOM).FirstOrDefault().Quantity + 1;
                            sale.Amount = (_SaleProducts.Where(x => x.Altermate == LocalBarcode || x.UOM == checkPrice.UOM).FirstOrDefault().Quantity) * sale.Price;
                            Gv.FocusedRowHandle = _SaleProducts.IndexOf(_SaleProducts.Where(x => x.Altermate == LocalBarcode || x.UOM == checkPrice.UOM).FirstOrDefault());
                        }
                        else
                        {
                            if (_SaleProducts.Count == 0)
                            {
                                lblIssuedDate.Text = "Date  :" + DateTime.Now.ToString("dd-MMM-yyyy");
                                lblIssuedDate.Tag = DateTime.Now;
                                lblIssuedTime.Text = "Time :" + DateTime.Now.ToString("hh:mm tt");
                            }
                            var Product = _Products.SingleOrDefault(w => w.InventoryID == checkPrice.InventoryID);
                            
                            var ProAltermate = new MDSalesPrice();
                            if (checkPrice.UOM == Product.BaseUnit)
                            
                                ProAltermate = _SalePrice.Where(w => w.AlternateID.Trim() == LocalBarcode || w.UOM == Product.BaseUnit).FirstOrDefault();
                            else
                                ProAltermate = _SalePrice.Where(w => w.AlternateID.Trim() == LocalBarcode).FirstOrDefault();

                            _SaleProducts.Insert(0, new SaleProductDomain()
                            {
                                ProductTypeID = _Products.Where(x => x.InventoryID == Product.InventoryID).FirstOrDefault().InventoryID,
                                ProductType = _Products.Where(x => x.InventoryCD == Product.InventoryCD).FirstOrDefault().Descr,
                                ItemID = Product.InventoryCD,
                                ItemCode = Product.InventoryCD,
                                //Barcode = Product.Barcode,
                                Currency = Convert.ToDecimal(lblUSDExchangeRate.Tag),
                                ItemDescriptionEn = Product.Descr,
                                Price = Convert.ToDecimal(ProAltermate.SalesPrice),
                                Altermate = ProAltermate.AlternateID.Trim(),
                                UOM = ProAltermate.UOM,
                                DiscountPrice = Convert.ToDecimal(Product.DisSalePrice),
                                Quantity = 1,
                                DiscountAmount = 0,
                                CustomerCode = luCustomer.EditValue.ToString(),
                                CustomerNameEn = luCustomer.Text.ToString(),
                                Status = "O",
                                Amount = Convert.ToDecimal(ProAltermate.SalesPrice),
                            });

                            Gv.FocusedRowHandle = 0;
                        }
                    }
                    else
                    {
                        //Barcode
                        if (_SaleProducts.Where(w => w.Barcode == LocalBarcode).Any())
                        {
                            var sale = _SaleProducts[_SaleProducts.IndexOf(_SaleProducts.Where(x => x.Barcode == LocalBarcode).FirstOrDefault())];

                            sale.Quantity = _SaleProducts.Where(x => x.Barcode == LocalBarcode).FirstOrDefault().Quantity + 1;
                            sale.Amount = (_SaleProducts.Where(x => x.Barcode == LocalBarcode).FirstOrDefault().Quantity) * sale.Price;
                            Gv.FocusedRowHandle = _SaleProducts.IndexOf(_SaleProducts.Where(x => x.Barcode == LocalBarcode).FirstOrDefault());
                        }
                        else
                        {
                            if (_SaleProducts.Count == 0)
                            {
                                lblIssuedDate.Text = "Date  :" + DateTime.Now.ToString("dd-MMM-yyyy");
                                lblIssuedDate.Tag = DateTime.Now;
                                lblIssuedTime.Text = "Time :" + DateTime.Now.ToString("hh:mm tt");
                            }
                            var Product = _Products.SingleOrDefault(w => w.Barcode.Trim() == LocalBarcode);
                            _SaleProducts.Insert(0, new SaleProductDomain()
                            {
                                ProductTypeID = _Products.Where(x => x.InventoryID == Product.InventoryID).FirstOrDefault().InventoryID,
                                ProductType = _Products.Where(x => x.InventoryCD == Product.InventoryCD).FirstOrDefault().Descr,
                                ItemID = Product.InventoryCD,
                                ItemCode = Product.InventoryCD,
                                Barcode = Product.Barcode.Trim(),
                                // Altermate = "",
                                Currency = Convert.ToDecimal(lblUSDExchangeRate.Tag),
                                ItemDescriptionEn = Product.Descr,
                                UOM = _Products.Where(x => x.InventoryCD == Product.InventoryCD).FirstOrDefault().BaseUnit,
                                Price = Convert.ToDecimal(Product.SalePrice),
                                DiscountPrice = Convert.ToDecimal(Product.DisSalePrice),
                                Quantity = 1,
                                DiscountAmount = 0,
                                CustomerCode = luCustomer.EditValue.ToString(),
                                CustomerNameEn = luCustomer.Text.ToString(),
                                Status = "O",
                                Amount = Convert.ToDecimal(Product.SalePrice),
                            });

                            Gv.FocusedRowHandle = 0;
                        }
                    }
                }
                GC.DataSource = null;
                GC.DataSource = _SaleProducts.ToList();
                TotalInvoiceAmount();
                CashChange();
                txtBarcode.Focus();
                txtBarcode.SelectAll();
                txtBarcode.Text = "";
            }
        }
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
        private void GC_Click(object sender, EventArgs e)
        {
        }
        private void btnReturn_Click(object sender, EventArgs e)
        {      
            try
            {
                if (XtraMessageBox.Show("Are you sure you want to Return this invoice?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
                _SaleProducts = new List<SaleProductDomain>();

                //  FrmPayBill objPayBill = new FrmPayBill(this);
                var objMatch = DB.ARInvoices.FirstOrDefault(w => w.InvoiceRefNbr == luInvoice.Text);
                if(objMatch==null)
                {
                    //meass
                    return;
                }
                objMatch.Status = "R";
                objMatch.Reverse = true;
           
                DB.ARInvoices.Attach(objMatch);
                DB.Entry(objMatch).Property(x => x.Status).IsModified = true;
                DB.Entry(objMatch).Property(x => x.Reverse).IsModified = true;
             
                var lstInvoiceDetail = DB.ARInvoicesDetails.Where(w => w.InvoiceRefNbr == luInvoice.Text).ToList();
                /////////Update_Inoice///////////
                foreach (var sale in lstInvoiceDetail.ToList())
                {
                    sale.Status = "R";
                    sale.Reverse = true;
                    DB.ARInvoicesDetails.Attach(sale);
                    DB.Entry(sale).Property(w => w.Status).IsModified = true;
                    DB.Entry(sale).Property(w => w.Reverse).IsModified = true;

                }
         
                var objPayDe = DB.ARPaymentDetails.FirstOrDefault(w => w.InvoiceRefNbr == luInvoice.Text);
                if (objPayDe == null)
                {
                    //meass
                    return;
                }
            
                    objPayDe.Status = "R";
               
                    DB.ARPaymentDetails.Attach(objPayDe);
                    DB.Entry(objPayDe).Property(w => w.Status).IsModified = true;
                    

                var Row = DB.SaveChanges();

                //  ClearSaleProduct();
                btnClear_Click(sender,e);
                screenProduct.Enabled = true;


            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    private void txtSearch_EditValueChanged(object sender, EventArgs e)
    {

    }
    private void lblTotalAmount_Click(object sender, EventArgs e)
    {
    }
    private void lblDiscountAmount_Click(object sender, EventArgs e)
    {

    }

    private void luInvoice_Properties_EditValueChanged(object sender, EventArgs e)
    {

    }

    private void luInvoice_EditValueChanged(object sender, EventArgs e)
    {
            IsSelectAuto = true;
          //  luCustomer.EditValue = null;
           _SaleProducts = new List<SaleProductDomain>();
            var PaymentList = DB.ARPaymentDetails.ToList();
           _Invoice = DB.ARInvoices.Where(w => w.Reverse == false).ToList();
           GlobalInitializer.GsFillLookUpEdit(_Invoice.Where(w => w.Status == "C").ToList(),
                 "InvoiceRefNbr", "InvoiceRefNbr", luInvoice, (int)_Invoice.Where(w => w.Status == "C").ToList().Count);    
        try
        {
                
                var refInvoice = luInvoice.EditValue;
                if (luInvoice.EditValue == null) return;
                var lstInvoiceDetail = DB.ARInvoicesDetails.Where(w => w.InvoiceRefNbr == refInvoice && w.Reverse == false).ToList();   
               
                foreach (var row in lstInvoiceDetail.ToList())
                {
                    var itemPayment = PaymentList.Where(x => x.InvoiceRefNbr == row.InvoiceRefNbr).FirstOrDefault();
                    SaleProductDomain invoice = new SaleProductDomain()
                    {
                        ItemCode = row.ItemCode,
                        InvoiceID = row.InvoiceRefNbr,
                        Barcode = row.Barcode,
                        ItemDescriptionEn = row.Descr,
                        Price = (decimal)row.UnitPrice,
                        Quantity = (int)row.Qty,
                        UOM = row.UOM,
                        //ReceiveAmount = row.Re
                        DiscountPrice = (decimal)row.DisAmount,
                        ExchangeRate = (decimal)lblUSDExchangeRate.Tag,
                        CustomerCode = row.CustomerID,
                        Status="C",
                        Amount = (decimal)row.Amount,
                        ChangeRiel = (decimal)itemPayment.BalanceKHR,
                        ChangeUSD = (decimal)itemPayment.BalanceUSD,
                        CashInRiel = (decimal)itemPayment.ReceiveAmountKHR,
                        CashInUSD = (decimal)itemPayment.ReceiveAmountUSD,
                        DisAmount = (decimal)itemPayment.DiscountAmount
                        
                    };

                    _SaleProducts.Add(invoice);
                    lblTotalAmount.Tag = _SaleProducts.Sum(x => x.Amount);

                    if (luCustomer.EditValue.ToString() == invoice.CustomerCode) {
                        IsSelectAuto = false;
                    }
                    luCustomer.EditValue = invoice.CustomerCode;

                    //_SaleProducts.FirstOrDefault().CustomerCode;

                    string summaryText = String.Format("{0} {1:N2} {2}", "Total :", lblTotalAmount.Tag, "$");
                    lblTotalAmount.Text = summaryText;


                    lblCashInUSD.Tag = itemPayment.ReceiveAmountUSD;
                    lblCashInUSD.Text = string.Format("{0} : {1:N0} {2}", "Cash In ($)", lblCashInUSD.Tag, "$");

                    lblCashInRiel.Tag = itemPayment.ReceiveAmountKHR;
                    lblCashInRiel.Text = string.Format("{0} : {1:N0} {2}", "Cash In (៛)", lblCashInRiel.Tag, "៛");

                    lblChangeUSD.Tag = itemPayment.BalanceUSD;
                    lblChangeUSD.Text = string.Format("{0} : {1:N2} {2}", "Change ($)", lblChangeUSD.Tag, "$");

                    lblChangeRiel.Tag = itemPayment.BalanceKHR;
                    lblChangeRiel.Text = string.Format("{0} : {1:N0} {2}", "Change (៛)", lblChangeRiel.Tag, "៛");

                    lblDiscountAmount.Tag = itemPayment.DiscountAmount;
                    lblDiscountAmount.Text = "Discount Amount     : " + string.Format("{0:N2}", lblDiscountAmount.Tag) + "$"; 
                }
         
              totalGrand = (decimal)lblTotalAmount.Tag;

              balanceUSD = (decimal)lblChangeUSD.Tag;

              cashInD = (decimal)lblCashInUSD.Tag;

              GC.DataSource = null;
              GC.DataSource = _SaleProducts.ToList();

             GC.Enabled = false;
                btnReturn.Enabled = true;
                screenProduct.Enabled = false;
             btnPayBill.Enabled = false;
             btnSkip.Enabled = false;
             btnDeleteBrowse.Enabled = false;
                luBrowse.EditValue = null;
                luBrowse.Enabled = false;
         }
         catch (Exception ex)
        {
            XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
           
    }
    public bool IsSelectAuto { get;set; }
    private void luCustomer_EditValueChanged(object sender, EventArgs e)
    {
            //_SaleProducts = new List<SaleProductDomain>();//Need Fix
            //btnClear_Click(sender, e);
            if (IsSelectAuto)
            {
                GC.Enabled = true;
                screenProduct.Enabled = true;
                btnPayBill.Enabled = true;
                //  txtBarcode.Enabled = false;
                txtBarcode.Text = "";
            }
            else
            {
                _SaleProducts.ForEach(x => x.CustomerCode = luCustomer.EditValue.ToString());
                _SaleProducts.ForEach(x => x.CustomerNameEn = luCustomer.Text);
            }
            IsSelectAuto = false;

            //    btnClear_Click(sender, e);
            //    GC.Enabled = true;
            //    screenProduct.Enabled = true;
            //    btnPayBill.Enabled = true;
            ////    txtBarcode.Enabled = false;
            //    txtBarcode.Text = "";

        }

    private void btnClear_Click(object sender, EventArgs e)
        {
            ClearSaleProduct();
            luBrowse.EditValue = null;
            GC.Enabled = true;
            screenProduct.Enabled = true;
            btnPayBill.Enabled = true;
           // txtBarcode.Enabled = true;
            txtBarcode.Text = "";
            btnReturn.Enabled = false;
            luInvoice.EditValue = null;
            btnSkip.Enabled = true;
            btnDeleteBrowse.Enabled = false;
            luBrowse.Enabled = true;
        }

    private void btnLogout_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to Logout?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) { }
                return;
            //frmLogin frmLogin = new frmLogin();
            //FormShadow Shadow = new FormShadow(frmLogin);

            //Shadow.ShowDialog();

        }
    private void btnPayBill_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Alt + P", btnPayBill);
        }
    private void btnClose_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Alt + C", btnClose);
        }
    private void btnHistory_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Alt + H", btnListInvoice);
        }
        private void btnReturn_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Alt + R", btnReturn);
        }
        private void btnLogout_Click_1(object sender, EventArgs e)
        {
            var result = XtraMessageBox.Show("Are you sure you want to Logout?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                frmLogin frm = new frmLogin();
                //FormShadow shadow = new FormShadow(frm);
                this.Close();
                frm.ShowDialog();

            }
        }

        List<ARInvoice> _SaleTemporary = new List<ARInvoice>();
        int CInvoice = 0;
        private void btnSkip_Click(object sender, EventArgs e)
        {
            if(luBrowse.EditValue != null && _SaleTemporary.Count != 0)
            {
                if (_SaleProducts.Count == 0)
                {
                    //var number = luBrowse.EditValue;
                    //_SaleTemporary.RemoveAll(x => x.InvoiceID == number.ToString());
                    //_SaleProducts.Clear();
                    XtraMessageBox.Show("This invoices can't empty!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnClear_Click(sender, e);
                    return;
                }
                else
                {
                    var number = luBrowse.EditValue;
                    //_SaleTemporary.RemoveAll(x => x.InvoiceID == number.ToString());
                    //_SaleProducts.ForEach(x => x.InvoiceID = number.ToString());
                    //_SaleTemporary.AddRange(_SaleProducts);
                    //_SaleProducts.Clear();
                    //CInvoice = Convert.ToInt32(number);

                    var checkdup = DB.ARInvoices.FirstOrDefault(w => w.InvoiceRefNbr.ToString() == number && w.CompanyID == DACClasses.CompanyID);

                    if (checkdup != null)
                    {
                        var checkdupList = DB.ARInvoicesDetails.Where(w => w.InvoiceRefNbr.ToString() == number && w.CompanyID == DACClasses.CompanyID).ToList();
                        foreach (var read in checkdupList)
                        {
                            DB.ARInvoicesDetails.Remove(read);
                            DB.SaveChanges();
                        }
                        _SaleProducts.ForEach(x => x.InvoiceID = number.ToString());
                        foreach (var sale in _SaleProducts.ToList())
                        {

                            ARInvoicesDetail item = new ARInvoicesDetail()
                            {
                                CompanyID = DACClasses.CompanyID,
                                InventoryID = sale.ProductTypeID,
                                ItemCode = sale.ItemCode,
                                Barcode = sale.Barcode,
                                InvoiceRefNbr = number.ToString(),
                                Descr = sale.ItemDescriptionEn,
                                InvoiceTypes = "INV",
                                Status = "H",
                                DisAmount = sale.DiscountPrice,
                                InvoiceDate = DateTime.Now,
                                UOM = sale.UOM,
                                Qty = sale.Quantity,
                                UnitPrice = sale.Price,
                                CustomerID = luCustomer.EditValue.ToString(),
                                CustomerName = luCustomer.Text.ToString(),
                                SalePersonID = CSUserModel.LoginName,
                                Reverse = false,
                                Amount = sale.Amount

                            };
                            DB.ARInvoicesDetails.Add(item);
                        }


                        checkdup.SalePersonID = CSUserModel.LoginName;
                        checkdup.TotalQty = _SaleProducts.Sum(x => x.Quantity);
                        checkdup.CustomerID = _SaleProducts.FirstOrDefault().CustomerCode;
                        checkdup.CustomerName = _SaleProducts.FirstOrDefault().CustomerNameEn;
                        checkdup.LastModifiedBy = CSUserModel.User;
                        checkdup.LastModifiedDateTime = DateTime.Now;

                        DB.Entry(checkdup).State = System.Data.Entity.EntityState.Modified;
                    }

                    DB.SaveChanges();

                    _SaleTemporary = DB.ARInvoices.Where(x => x.Status == "H").ToList();

                }
               
            }
            else
            {


                if (_SaleProducts.Count == 0)
                {
                    //var number = luBrowse.EditValue;
                    //_SaleTemporary.RemoveAll(x => x.InvoiceID == number.ToString());
                    //_SaleProducts.Clear();
                    XtraMessageBox.Show("This invoices cannot empty!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnClear_Click(sender, e);
                    return;
                }
                // var row = CInvoice + 1;
                // _SaleProducts.ForEach(x => x.InvoiceID = row.ToString());
                // _SaleTemporary.AddRange(_SaleProducts);
                // _SaleProducts.Clear();    
                // CInvoice = row;

                //auto number invocie

                var resul = new CFNumberRank("S", DocConfType.Normal, true);
                string yrmm = DateTime.Now.Year.ToString()+ "-" + DateTime.Now.Month.ToString();
                invoice = resul.NextNumberRank.Trim();
                foreach (var sale in _SaleProducts.ToList())
                {

                    ARInvoicesDetail item = new ARInvoicesDetail()
                    {
                        CompanyID = DACClasses.CompanyID,
                        InventoryID = sale.ProductTypeID,
                        ItemCode = sale.ItemCode,
                        Barcode = sale.Barcode,
                        InvoiceRefNbr =  invoice ,
                        Descr = sale.ItemDescriptionEn,
                        InvoiceTypes = "INV",
                        Status = "H",
                        DisAmount = sale.DiscountPrice,
                        InvoiceDate = DateTime.Now,
                        UOM = sale.UOM,
                        Qty = sale.Quantity,
                        UnitPrice = sale.Price,
                        CustomerID = sale.CustomerCode,
                        CustomerName = sale.CustomerNameEn,
                        SalePersonID = CSUserModel.LoginName,
                        Reverse = false,
                        Amount = sale.Amount

                    };
                    //sale.InvoiceID = invoice;
                    //sale.ExchangeRate = (decimal)lblUSDExchangeRate.Tag;
                    DB.ARInvoicesDetails.Add(item);

                }


                ARInvoice aRInvoice = new ARInvoice()
                {
                    CompanyID = DACClasses.CompanyID,
                    InvoiceRefNbr = invoice,
                    Status = "H",
                    // Descr = descr,
                    InvoiceTypes = "INV",
                    CuryID = "USD",
                    CuryInfoID = 2,
                    Reverse = false,
                    // Dis = sale.DiscountAmount,
                    InvoiceDate = DateTime.Today,
                    CustomerID = luCustomer.EditValue.ToString(),
                    CustomerName = luCustomer.Text,
                    SalePersonID = CSUserModel.LoginName,
                    TotalQty = _SaleProducts.Sum(x => x.Quantity),
                    CreatedBy = CSUserModel.User,
                    CreatedDateTime = DateTime.Now,
                    IsCloseBalan = false

                };

                DB.ARInvoices.Add(aRInvoice);

                var Row = DB.SaveChanges();

               // _SaleTemporary = DB.ARInvoicesDetails.Where(x => DB.ARInvoices.Where(y => y.InvoiceRefNbr == x.InvoiceRefNbr && y.Status == "H").Any()).ToList();
                _SaleTemporary = DB.ARInvoices.Where(y => y.Status == "H").ToList();
            }

            btnClear_Click(sender, e);
            //var groupBy = _SaleTemporary.GroupBy(x => x.InvoiceRefNbr)
            //    .ToDictionary(x => x.Key, x => x.Select(y => y.InvoiceRefNbr + y.InvoiceDate.ToString("yy-mm")));

            GlobalInitializer.GsFillLookUpEdit(
               _SaleTemporary.ToList(), "InvoiceRefNbr", "InvoiceRefNbr", luBrowse, (int)_SaleTemporary.ToList().Count);


        }
        private void btnListInvoice_Click(object sender, EventArgs e)
        {
            var nameAction = "List Invoice";
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
            {
                scrListInvoice objPayBill = new scrListInvoice();
                FormShadow Shadow = new FormShadow(objPayBill);
                Shadow.ShowDialog();
            }
            else
            {
                XtraMessageBox.Show("You cannot access list invoices !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void GV_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            // var CellValueRecordID = Gv.GetRowCellValue(view.FocusedRowHandle, "ProductTypeID");
            if (e.Column == Gv.Columns["UOM"])
            {
                //var b = _SalePrice.Where(x => x.InventoryID == Convert.ToInt32(CellValueRecordID));

                //Gv.SetRowCellValue(Gv.FocusedRowHandle, Gv.Columns["Amount"], b.FirstOrDefault().SalesPrice);
                //repositoryItemGridLookUpEdit1.DataSource = b.ToList();
            }
        }
        private void luBrowse_EditValueChanged(object sender, EventArgs e)
        {
            if (luBrowse.EditValue == null)
            {
                return;
            }
            _SaleProducts.Clear();
            //var listby = _SaleTemporary.Where(x => x.InvoiceRefNbr == luBrowse.EditValue.ToString()).ToList();
            var listby = DB.ARInvoicesDetails.Where(x => x.InvoiceRefNbr == luBrowse.EditValue.ToString()).ToList();
            //_SaleProducts.AddRange(listby);
            foreach (var sale in listby.ToList())
            {

                SaleProductDomain item = new SaleProductDomain()
                {
                    ProductTypeID = sale.InventoryID,
                    ProductType = sale.Descr,
                    ItemID = sale.InventoryID.ToString(),
                    Currency = Convert.ToDecimal(lblUSDExchangeRate.Tag),
                    ItemCode = sale.ItemCode,
                    Barcode = sale.Barcode,
                    InvoiceID = sale.InvoiceRefNbr,
                    ItemDescriptionEn = sale.Descr,
                    Status = "H",
                    InvoiceDate = (DateTime)sale.InvoiceDate,
                    UOM = sale.UOM,
                    Quantity = Convert.ToInt32(sale.Qty),
                    Price = Convert.ToDecimal(sale.UnitPrice),
                    CustomerCode = sale.CustomerID,
                    CustomerNameEn = sale.CustomerName,
                    EmployeeNameEn = sale.SalePersonID,
                    Reverse = false,
                    Amount = Convert.ToDecimal(sale.Amount)

                };
                //sale.InvoiceID = invoice;
                //sale.ExchangeRate = (decimal)lblUSDExchangeRate.Tag;
               luCustomer.EditValue = sale.CustomerID;
              // luCustomer.Text = sale.CustomerName;
                _SaleProducts.Add(item);

            }

            GC.DataSource = null;
            GC.DataSource = _SaleProducts;
            btnDeleteBrowse.Enabled = true;


            lblTotalAmount.Tag = _SaleProducts.Sum(x => x.Amount);
            //_SaleProducts.FirstOrDefault(w => w.InvoiceID == luBrowse.EditValue).TotalAmount;
            string summaryText = String.Format("{0} {1:N2} {2}", "Total :", lblTotalAmount.Tag, "$");
            lblTotalAmount.Text = summaryText;

        }
        private void btnDeleteBrowse_Click(object sender, EventArgs e)
        {
            var result = XtraMessageBox.Show("Do you want to delete this invoice?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes) 
            {
                if (_SaleTemporary.Count != 0)
                {
                    var number = luBrowse.EditValue;
                    //_SaleTemporary.RemoveAll(x => x.InvoiceRefNbr == number.ToString());

                    var checkdup = DB.ARInvoices.FirstOrDefault(w => w.InvoiceRefNbr.ToString() == number && w.CompanyID == DACClasses.CompanyID);

                    if (checkdup != null)
                    {
                        var checkdupList = DB.ARInvoicesDetails.Where(w => w.InventoryID.ToString() == number && w.CompanyID == DACClasses.CompanyID).ToList();
                        foreach (var read in checkdupList)
                        {
                            DB.ARInvoicesDetails.Remove(read);
                            DB.SaveChanges();
                        }


                        DB.ARInvoices.Remove(checkdup);
                        //DB.Entry(checkdup).State = System.Data.Entity.EntityState.Modified;
                    }

                    DB.SaveChanges();
                    _SaleProducts.Clear();
                    btnClear_Click(sender, e);

                    _SaleTemporary.RemoveAll(x => x.InvoiceRefNbr == number.ToString());
                    GlobalInitializer.GsFillLookUpEdit(
                   _SaleTemporary.ToList(), "InvoiceRefNbr", "InvoiceRefNbr", luBrowse, (int)_SaleTemporary.ToList().Count);

                    return;
                }          
            }
            else
            {
                return;
            }
        }

        private void Gv_FocusedRowLoaded(object sender, DevExpress.XtraGrid.Views.Base.RowEventArgs e)
        {
            //GridView view = (GridView)sender;
            //var CellValueRecord = Gv.GetRowCellValue(view.FocusedRowHandle, "UOM");
            //var CellValueRecordID = Gv.GetRowCellValue(view.FocusedRowHandle, "InventoryID");

            //if (_SalePrice.Where(x => x.InventoryID.ToString() == CellValueRecordID).Any())
            //{
            //    //repositoryItemGridLookUpEdit1.DataSource = _SalePrice.Where(x => x.InventoryID == ProductTypeID).Select(person => person.UOM);
            //    var b = _SalePrice.Where(x => x.InventoryID == Convert.ToInt32(CellValueRecordID));

            //    Gv.SetRowCellValue(Gv.FocusedRowHandle, Gv.Columns["Amount"], b.FirstOrDefault().SalesPrice);
            //    repositoryItemLookUpEdit1.DataSource = b.ToList();
            //    repositoryItemLookUpEdit1.DisplayMember = "UOM";
            //    repositoryItemLookUpEdit1.ValueMember = "UOM";
            //    gridColUnit.OptionsColumn.AllowEdit = true;
            //    repositoryItemLookUpEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            //    gridColUnit.ColumnEdit = repositoryItemLookUpEdit1;
            //}
            //else
            //{
            //    gridColUnit.ColumnEdit = null;
            //    gridColUnit.OptionsColumn.AllowEdit = false;
            //    // gridColUnit.OptionsColumn.Allo = false;
            //}
        }


        private void Gv_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            GridView view = (GridView)sender;
            var CellValueRecord = Gv.GetRowCellValue(view.FocusedRowHandle, "UOM");
            var CellValueRecordID = Gv.GetRowCellValue(view.FocusedRowHandle, "ProductTypeID");

            if (_SalePrice.Where(x => x.InventoryID == Convert.ToInt32(CellValueRecordID)).Any())
            {
                //repositoryItemGridLookUpEdit1.DataSource = _SalePrice.Where(x => x.InventoryID == ProductTypeID).Select(person => person.UOM);
                var list = _SalePrice.Where(x => x.InventoryID == Convert.ToInt32(CellValueRecordID));

                repositoryItemLookUpEdit1.DataSource = null;
                repositoryItemLookUpEdit1.DataSource = _SalePrice.Where(x => x.InventoryID == Convert.ToInt32(CellValueRecordID)).Select(i => new { i.UOM, i.SalesPrice });
                //_Units.Where(x => list.Where(y => x.UOMID == x.UOMID).Any()).ToList();
                repositoryItemLookUpEdit1.DisplayMember = "UOM";
                repositoryItemLookUpEdit1.ValueMember = "UOM";
                gridColUnit.OptionsColumn.AllowEdit = true;
                repositoryItemLookUpEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                gridColUnit.ColumnEdit = repositoryItemLookUpEdit1;
            }
            else
            {
                gridColUnit.ColumnEdit = null;
                gridColUnit.OptionsColumn.AllowEdit = false;
            }
        }

        private void Gv_FocusedColumnChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs e)
        {
            //GridView view = (GridView)sender;
            //var CellValueRecord = Gv.GetRowCellValue(view.FocusedRowHandle, "UOM");
            //var CellValueRecordID = Gv.GetRowCellValue(view.FocusedRowHandle, "ProductTypeID");

            //if (_SalePrice.Where(x => x.InventoryID == Convert.ToInt32(CellValueRecordID)).Any())
            //{
            //    repositoryItemGridLookUpEdit1.DataSource = _SalePrice.Where(x => x.InventoryID == ProductTypeID).Select(person => person.UOM);
            //    var b = _SalePrice.Where(x => x.InventoryID == Convert.ToInt32(CellValueRecordID));

            //    Gv.SetRowCellValue(Gv.FocusedRowHandle, Gv.Columns["Amount"], b.FirstOrDefault().SalesPrice);
            //    repositoryItemGridLookUpEdit1.DataSource = b.ToList();
            //    repositoryItemGridLookUpEdit1.DisplayMember = "UOM";
            //    repositoryItemGridLookUpEdit1.ValueMember = "UOM";
            //    repositoryItemGridLookUpEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            //    gridColUnit.OptionsColumn.AllowEdit = true;
            //    gridColUnit.ColumnEdit = repositoryItemGridLookUpEdit1;
            //    Gv.RefreshData();

            //}
            //else
            //{
            //    gridColUnit.ColumnEdit = null;
            //    gridColUnit.OptionsColumn.AllowEdit = false;
            //    gridColUnit.OptionsColumn.Allo = false;
            //}
        }

       

        private void Gv_ColumnChanged(object sender, EventArgs e)
        {
        }

        private void repositoryItemLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit textEditor = (TextEdit)sender;
          //  var CellValueRecord = Gv.GetRowCellValue(Gv.FocusedRowHandle, Gv.Columns["UOM"]);
            var CellValueRecordID = Gv.GetRowCellValue(Gv.FocusedRowHandle, "ProductTypeID");
            var CellValueQty = Gv.GetRowCellValue(Gv.FocusedRowHandle, "Quantity");

            var listprice = _SalePrice.Where(x => x.InventoryID == Convert.ToInt32(CellValueRecordID) && x.UOM == textEditor.EditValue).ToList();
            if (listprice.Count == 0)
            {
                return;
            }
            var saleprice = listprice.FirstOrDefault().SalesPrice;
            Gv.SetRowCellValue(Gv.FocusedRowHandle, Gv.Columns["Amount"], saleprice * Convert.ToDecimal(CellValueQty));
            Gv.RefreshData();
            _SaleProducts.Where(x => x.ProductTypeID == Convert.ToInt32(CellValueRecordID)).ToList().ForEach(x => x.Price = (decimal)listprice.FirstOrDefault().SalesPrice);
            _SaleProducts.Where(x => x.ProductTypeID == Convert.ToInt32(CellValueRecordID)).ToList().ForEach(x => x.Altermate = listprice.FirstOrDefault().AlternateID.Trim()); 
            _SaleProducts.Where(x => x.ProductTypeID == Convert.ToInt32(CellValueRecordID)).ToList().ForEach(x => x.Barcode = null); 
            TotalInvoiceAmount();
           

        }

        private void repositoryItemGridLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void btnPrintNow_Click(object sender, EventArgs e)
        {
             if (luInvoice.EditValue != null || GC.Enabled == false)
             {
                if (cboprint.EditValue == "Print Receipt")
                {
                    rptReceipt rptA5 = new rptReceipt();

                    rptA5.DataSource = _SaleProducts;
                    rptA5.Parameters["Cashier"].Value = CSUserModel.LoginName;
                    rptA5.RequestParameters = false;
                    ReportPrintTool printTool = new ReportPrintTool(rptA5);

                    printTool.ShowPreview();
                }
                else if (cboprint.EditValue == "Print A5")
                {
                    ReportA5 rptA5 = new ReportA5();

                    rptA5.DataSource = _SaleProducts;
                    rptA5.Parameters["Cashier"].Value = CSUserModel.LoginName;
                    rptA5.RequestParameters = false;
                    ReportPrintTool printTool = new ReportPrintTool(rptA5);
                    printTool.ShowPreview();
                }
                else if (cboprint.EditValue == "Print A3")
                {
                    rptReceiptA3 rptA5 = new rptReceiptA3();

                    rptA5.DataSource = _SaleProducts;
                    rptA5.Parameters["Cashier"].Value = CSUserModel.LoginName;
                    rptA5.RequestParameters = false;
                    ReportPrintTool printTool = new ReportPrintTool(rptA5);
                    printTool.ShowPreview();
                }
                else if (cboprint.EditValue == "Print A4")
                {
                    rptReceiptA4 rptA5 = new rptReceiptA4();

                    rptA5.DataSource = _SaleProducts;
                    rptA5.Parameters["Cashier"].Value = CSUserModel.LoginName;
                    rptA5.RequestParameters = false;
                    ReportPrintTool printTool = new ReportPrintTool(rptA5);
                    //   report.DataSource = rptA5;
                    printTool.ShowPreview();
                }
            }
            else
            {
                XtraMessageBox.Show("You cannot print invoices!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
  
        }
    }
}