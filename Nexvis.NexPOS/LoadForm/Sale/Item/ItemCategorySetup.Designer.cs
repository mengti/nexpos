﻿namespace DgoStore.LoadForm.Sale
{
    partial class ItemCategorySetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navNew = new DevExpress.XtraBars.Navigation.NavButton();
            this.navSave = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtCategoryID = new DevExpress.XtraEditors.TextEdit();
            this.txtCategoryCD = new DevExpress.XtraEditors.TextEdit();
            this.txtCategoryName = new DevExpress.XtraEditors.TextEdit();
            this.txtDescr = new DevExpress.XtraEditors.TextEdit();
            this.txtImagesUrl = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryCD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImagesUrl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navNew);
            this.navHeader.Buttons.Add(this.navSave);
            this.navHeader.Buttons.Add(this.navEdit);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(703, 40);
            this.navHeader.TabIndex = 5;
            this.navHeader.Text = "q";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Add Category";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navNew
            // 
            this.navNew.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navNew.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navNew.AppearanceHovered.Options.UseForeColor = true;
            this.navNew.Caption = "New";
            this.navNew.Name = "navNew";
            this.navNew.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navSave
            // 
            this.navSave.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navSave.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navSave.AppearanceHovered.Options.UseForeColor = true;
            this.navSave.Caption = "Save";
            this.navSave.Name = "navSave";
            this.navSave.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navSave_ElementClick);
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Update";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtCategoryID);
            this.layoutControl1.Controls.Add(this.txtCategoryCD);
            this.layoutControl1.Controls.Add(this.txtCategoryName);
            this.layoutControl1.Controls.Add(this.txtDescr);
            this.layoutControl1.Controls.Add(this.txtImagesUrl);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(751, 29, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(703, 251);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtCategoryID
            // 
            this.txtCategoryID.Enabled = false;
            this.txtCategoryID.Location = new System.Drawing.Point(45, 62);
            this.txtCategoryID.Name = "txtCategoryID";
            this.txtCategoryID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.txtCategoryID.Properties.Appearance.Options.UseFont = true;
            this.txtCategoryID.Size = new System.Drawing.Size(340, 24);
            this.txtCategoryID.StyleController = this.layoutControl1;
            this.txtCategoryID.TabIndex = 4;
            // 
            // txtCategoryCD
            // 
            this.txtCategoryCD.Location = new System.Drawing.Point(40, 51);
            this.txtCategoryCD.Name = "txtCategoryCD";
            this.txtCategoryCD.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtCategoryCD.Properties.Appearance.Options.UseFont = true;
            this.txtCategoryCD.Properties.MaxLength = 30;
            this.txtCategoryCD.Size = new System.Drawing.Size(289, 28);
            this.txtCategoryCD.StyleController = this.layoutControl1;
            this.txtCategoryCD.TabIndex = 0;
            this.txtCategoryCD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCategoryCD_KeyDown);
            this.txtCategoryCD.MouseHover += new System.EventHandler(this.txtCategoryCD_MouseHover);
            // 
            // txtCategoryName
            // 
            this.txtCategoryName.Location = new System.Drawing.Point(40, 114);
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtCategoryName.Properties.Appearance.Options.UseFont = true;
            this.txtCategoryName.Properties.MaxLength = 50;
            this.txtCategoryName.Size = new System.Drawing.Size(289, 28);
            this.txtCategoryName.StyleController = this.layoutControl1;
            this.txtCategoryName.TabIndex = 1;
            this.txtCategoryName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCategoryName_KeyDown);
            this.txtCategoryName.MouseHover += new System.EventHandler(this.txtCategoryCD_MouseHover);
            // 
            // txtDescr
            // 
            this.txtDescr.Location = new System.Drawing.Point(40, 183);
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtDescr.Properties.Appearance.Options.UseFont = true;
            this.txtDescr.Properties.MaxLength = 50;
            this.txtDescr.Size = new System.Drawing.Size(289, 28);
            this.txtDescr.StyleController = this.layoutControl1;
            this.txtDescr.TabIndex = 2;
            this.txtDescr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDescr_KeyDown);
            this.txtDescr.MouseHover += new System.EventHandler(this.txtCategoryCD_MouseHover);
            // 
            // txtImagesUrl
            // 
            this.txtImagesUrl.Location = new System.Drawing.Point(353, 46);
            this.txtImagesUrl.Name = "txtImagesUrl";
            this.txtImagesUrl.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImagesUrl.Properties.Appearance.Options.UseFont = true;
            this.txtImagesUrl.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.txtImagesUrl.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.txtImagesUrl.Size = new System.Drawing.Size(338, 193);
            this.txtImagesUrl.StyleController = this.layoutControl1;
            this.txtImagesUrl.TabIndex = 3;
            this.txtImagesUrl.TabStop = true;
            this.txtImagesUrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtImagesUrl_KeyDown);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 11F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtCategoryID;
            this.layoutControlItem1.Location = new System.Drawing.Point(33, 29);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(344, 49);
            this.layoutControlItem1.Text = "CategoryID";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(98, 18);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.txtCategoryName;
            this.layoutControlItem3.Location = new System.Drawing.Point(28, 83);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(293, 51);
            this.layoutControlItem3.Text = "Category Name";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(95, 16);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.emptySpaceItem4,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.emptySpaceItem8,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.layoutControlItem3});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(703, 251);
            this.Root.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(28, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(293, 20);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(28, 71);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(293, 12);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.txtCategoryCD;
            this.layoutControlItem2.CustomizationFormText = "Category Code";
            this.layoutControlItem2.Location = new System.Drawing.Point(28, 20);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(293, 51);
            this.layoutControlItem2.Text = "Category Code";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(95, 16);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.txtDescr;
            this.layoutControlItem4.Location = new System.Drawing.Point(28, 152);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(293, 51);
            this.layoutControlItem4.Text = "Description";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(95, 16);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(28, 134);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(293, 18);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(28, 231);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(341, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(342, 15);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(321, 0);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(20, 231);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtImagesUrl;
            this.layoutControlItem5.Location = new System.Drawing.Point(341, 15);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(342, 216);
            this.layoutControlItem5.Text = "Images Url";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(95, 16);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(28, 203);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(293, 28);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemCategorySetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 291);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Name = "ItemCategorySetup";
            this.Text = "ItemCategorySetup";
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryCD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategoryName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImagesUrl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navNew;
        private DevExpress.XtraBars.Navigation.NavButton navSave;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        public DevExpress.XtraEditors.TextEdit txtCategoryID;
        public DevExpress.XtraEditors.TextEdit txtCategoryCD;
        public DevExpress.XtraEditors.TextEdit txtCategoryName;
        public DevExpress.XtraEditors.TextEdit txtDescr;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        public DevExpress.XtraEditors.PictureEdit txtImagesUrl;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}