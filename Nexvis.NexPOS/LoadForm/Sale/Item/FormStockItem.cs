﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ZooMangement;
using Nexvis.NexPOS.Helper;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.Data.ODataLinq.Helpers;
using Nexvis.NexPOS.Data;
using DevExpress.XtraGrid;
using Nexvis.NexPOS.Helper;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.Sale
{
    public partial class scrItem : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrItem()
        {
            InitializeComponent();
        }


        List<CSCurrency> _CSCurrency = new List<CSCurrency>();
        List<INItemCategory> iNItemCategories = new List<INItemCategory>();
        List<INLotSerClass> iNLotSerClasses = new List<INLotSerClass>();
        List<CSUOM> cSUOMs = new List<CSUOM>();
        List<INItem> _Initem = new List<INItem>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        List<MDSalesPrice> _SalePrices = new List<MDSalesPrice>();
        
        public List<INUnitConversion> _INUnitConversion = new List<INUnitConversion>();
        public int rowUnit { get; set; } 

        public bool isSuccessful = true;
        private void FormStockItem_Load(object sender, EventArgs e)
        {
            _INUnitConversion = new List<INUnitConversion>();
            _Initem = DB.INItems.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            GC.DataSource = _Initem.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
             _CSCurrency = DB.CSCurrencies.ToList();
            iNItemCategories = DB.INItemCategories.ToList();
            iNLotSerClasses = DB.INLotSerClasses.ToList();
            cSUOMs = DB.CSUOMs.ToList();
            _SalePrices = DB.MDSalesPrices.ToList();
           _INUnitConversion = DB.INUnitConversions.ToList();
        }
        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }
        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }
        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormStockItem_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }
        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    INItem result = new INItem();
                    result = (INItem)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {
                            INItem item = DB.INItems.FirstOrDefault(st => st.CompanyID.Equals(result.CompanyID) && st.InventoryID.Equals(result.InventoryID));
                            INUnitConversion unit = DB.INUnitConversions.FirstOrDefault(st => st.CompanyID.Equals(result.CompanyID) && st.InventoryID.Equals(result.InventoryID));

                            if (unit != null)
                            {
                                DB.INUnitConversions.Remove(unit);
                            }
                            
                            DB.INItems.Remove(item);
                            DB.SaveChanges();
                            FormStockItem_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }   
        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            DB = new ZooEntity();
            try
            {
                var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    StockItemSetup objForm = new StockItemSetup("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);
                    GlobalInitializer.GsFillLookUpEdit(_CSCurrency.ToList(), "CuryID", "CuryID", objForm.luCuryID, (int)_CSCurrency.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(iNLotSerClasses.ToList(), "LotSerClassID", "LotSerClassID", objForm.luLotSerClassID, (int)iNLotSerClasses.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(iNItemCategories.ToList(), "CategoryID", "CategoryName", objForm.luCateCD, (int)iNItemCategories.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luBaseUnit, (int)cSUOMs.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luSaleUnit, (int)cSUOMs.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luPurchaseUnit, (int)cSUOMs.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luFromUnit, (int)cSUOMs.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luToUnit, (int)cSUOMs.ToList().Count);

                    INItem result = new INItem();
                    result = (INItem)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        objForm.txtBarcode.Enabled = false;
                        //objForm.btnRandomCode.Enabled = false;
                        objForm.txtInventoryID.Text = result.InventoryID.ToString();
                        objForm.txtBarcode.Text = result.Barcode;
                        objForm.txtInventoryCD.Text = result.InventoryCD;
                        objForm.txtInventoryCD.Focus();
                        objForm.txtDescr.Text = result.Descr;
                        objForm.cmbItemType.Text = result.ItemType;
                        objForm.cmbStatus.Text = result.Status;
                        objForm.luLotSerClassID.Text = result.LotSerClassID;
                        objForm.luCateCD.EditValue = result.CategoryID.ToString();
                        objForm.luCuryID.EditValue = result.CuryID;
                        objForm.txtSalePrice.Text = result.SalePrice.ToString();
                        objForm.txtImageUrl.Image = result.ImageUrl == null ? objForm.txtImageUrl.Image : DACClasses.ReadImage(result.ImageUrl);
                        objForm.luBaseUnit.EditValue = result.BaseUnit;
                        objForm.luSaleUnit.EditValue = result.SaleUnit;
                        objForm.luPurchaseUnit.EditValue = result.PurchaseUnit;
                        objForm.cmbIndicator.Text = result.Indicator;
                        objForm.cmbValuationMethod.Text = result.ValuationMethod;
                        objForm.txtCuryUnitCost.EditValue = result.CuryUnitCost;
                        objForm.txtUnitCost.EditValue = result.UnitCost;
                        objForm.dtStartDate.EditValue = result.ItemStartDate;
                        objForm.dtItemExpDate.EditValue = result.ItemExpDate;

                        objForm.txtInventoryID.Enabled = false;
                        objForm.luLotSerClassID.Enabled = false;
                        objForm.btnUpdateUnit.Enabled = false;
                        _INUnitConversion = new List<INUnitConversion>();
                        _INUnitConversion = DB.INUnitConversions.ToList();

                        objForm.gcUnit.DataSource = null;
                        objForm.gcUnit.DataSource = _INUnitConversion.Where(x => x.InventoryID.ToString() == objForm.txtInventoryID.Text).ToList();
                        objForm.gvUnit.RefreshData();
                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    navRefresh_ElementClick(sender, e);


                    StockItemSetup objForm = new StockItemSetup("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);
                    objForm.txtImageUrl.Image = null;
                    objForm.txtImageUrl.Invalidate();

                    objForm.btnUpdateUnit.Enabled = false;


                    //TODO: Binding value to comboBox or LookupEdit.
                    GlobalInitializer.GsFillLookUpEdit(_CSCurrency.ToList(), "CuryID", "CuryID", objForm.luCuryID, (int)_CSCurrency.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(iNLotSerClasses.ToList(), "LotSerClassID", "LotSerClassID", objForm.luLotSerClassID, (int)iNLotSerClasses.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(iNItemCategories.ToList(), "CategoryID", "CategoryName", objForm.luCateCD, (int)iNItemCategories.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luBaseUnit, (int)cSUOMs.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luSaleUnit, (int)cSUOMs.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luPurchaseUnit, (int)cSUOMs.ToList().Count);

                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luFromUnit, (int)cSUOMs.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luToUnit, (int)cSUOMs.ToList().Count);

                    objForm.dtStartDate.EditValue = DateTime.Today;

                    Shadow.ShowDialog();
                    // objForm.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #region Actions
        public void GsSaveData(StockItemSetup objChild)
        {
            objChild.isExiting = false;
            if (_Initem.Any(x => x.InventoryID.ToString() == objChild.txtInventoryID.Text.Trim().ToLower()))
            {
                objChild.isExiting = true;
                return;
            }

            if (_Initem.Any(x => x.Barcode.Trim().ToString() == objChild.txtBarcode.Text.Trim()) || _SalePrices.Any(x => x.AlternateID.Trim().ToString() == objChild.txtBarcode.Text.Trim())
                || _INUnitConversion.Any(x => x.AltermateID.Trim().ToString() == objChild.txtBarcode.Text.Trim()))
            {
                objChild.isExistingBarcode = true;
                return;
            }
            try
            {
                INItem item = new INItem()
                {
                    CompanyID = DACClasses.CompanyID,
                    InventoryCD = objChild.txtInventoryCD.Text,
                    ItemCode = "",
                    Descr = objChild.txtDescr.Text,
                    Barcode = objChild.txtBarcode.Text,
                    ItemType = objChild.cmbItemType.Text,
                    Status = objChild.cmbStatus.Text,

                    // Tax_Exemption = objChild.ckTax.Checked,
                    CategoryID = Convert.ToInt32(objChild.luCateCD.EditValue),
                    LotSerClassID = objChild.luLotSerClassID.Text,
                    CuryInfoID = 2,
                    CuryID = objChild.luCuryID.EditValue.ToString(),
                    SalePrice = Convert.ToDecimal(objChild.txtSalePrice.EditValue),
                    //   DisSalePrice = Convert.ToDecimal(objChild.txtdiscountSalePrie.EditValue),
                    BaseUnit = objChild.luBaseUnit.Text,
                    SaleUnit = objChild.luSaleUnit.Text,
                    PurchaseUnit = objChild.luPurchaseUnit.Text,
                    Indicator = objChild.cmbIndicator.Text,
                    ValuationMethod = objChild.cmbValuationMethod.Text,
                    CuryUnitCost = Convert.ToDecimal(objChild.txtCuryUnitCost.EditValue),
                    UnitCost = Convert.ToDecimal(objChild.txtUnitCost.EditValue),
                    ImageUrl = DACClasses.ReturnImage(objChild.txtImageUrl),
                    ItemStartDate = DateTimeConvert.ConvertStringToDate(objChild.dtStartDate.Text),
                    ItemExpDate = DateTimeConvert.ConvertStringToDate(objChild.dtItemExpDate.Text)
                };
                DB.INItems.Add(item);
  
                List<INUnitConversion> GetArticlesList = (objChild.gcUnit.DataSource as IEnumerable<INUnitConversion>).ToList();
                if (DB.INItems.Count() != 0)
                {
                    item.InventoryID = DB.INItems.Max(p => p.InventoryID) + 1;
                }
                else
                    item.InventoryID = 1;

                GetArticlesList.ForEach(x => x.InventoryID = item.InventoryID);
                DB.INUnitConversions.AddRange(GetArticlesList);
                var Row = DB.SaveChanges();

                if (Row == 1)
                {
                    isSuccessful = true;
                }
                GV.FocusedRowHandle = 0;
                XtraMessageBox.Show("This field has been saved", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                navRefresh_ElementClick(null,null);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public void GsUpdateData(StockItemSetup objChild)
        {
            try
            {
                if (_Initem.Any(x => x.InventoryID.ToString() != objChild.txtInventoryID.Text.Trim().ToLower()
                && x.InventoryCD.Trim().ToLower() == objChild.txtInventoryCD.Text.Trim().ToLower()) && objChild.txtUnitRate.Text=="0")
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtInventoryCD.Focus();
                    return;
                }
                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return; 
                }

                DB = new ZooEntity();
                var checkdup = DB.INItems.FirstOrDefault(w => w.InventoryID.ToString() == objChild.txtInventoryID.Text && w.CompanyID == DACClasses.CompanyID);

                if (checkdup != null)
                {
                    var checkdupList = DB.INUnitConversions.Where(w => w.InventoryID.ToString() == objChild.txtInventoryID.Text && w.CompanyID == DACClasses.CompanyID).ToList();
                    foreach (var read in checkdupList)
                    {
                        DB.INUnitConversions.Remove(read);
                        DB.SaveChanges();
                    }
                   // _INUnitConversion = _INUnitConversion.Where(w => w.InventoryID.ToString() == objChild.txtInventoryID.Text && w.CompanyID == DACClasses.CompanyID).ToList();

                    foreach (var read in _INUnitConversion.Where(x => x.InventoryID.ToString() == objChild.txtInventoryID.Text).ToList())
                    {
                        read.CompanyID = checkdup.CompanyID;
                        read.InventoryID = checkdup.InventoryID;
                        //read.RecordId =
                        DB.INUnitConversions.Add(read);
                        DB.SaveChanges();
                    }

                    checkdup.InventoryID = Convert.ToInt32(objChild.txtInventoryID.Text);
                    checkdup.InventoryCD = objChild.txtInventoryCD.Text;
                    checkdup.Descr = objChild.txtDescr.Text;
                    checkdup.ItemType = objChild.cmbItemType.Text;
                    checkdup.Status = objChild.cmbStatus.Text;
                    checkdup.LotSerClassID = objChild.luLotSerClassID.Text;
                    checkdup.CategoryID = Convert.ToInt32(objChild.luCateCD.EditValue);
                    checkdup.CuryID = objChild.luCuryID.Text.ToString();
                    checkdup.SalePrice = Convert.ToDecimal(objChild.txtSalePrice.EditValue);
                    checkdup.ImageUrl = DACClasses.ReturnImage(objChild.txtImageUrl);
                    checkdup.BaseUnit = objChild.luBaseUnit.EditValue.ToString();
                    checkdup.PurchaseUnit = objChild.luPurchaseUnit.Text;
                    checkdup.Indicator = objChild.cmbIndicator.Text;
                    checkdup.ValuationMethod = objChild.cmbValuationMethod.Text;
                    checkdup.CuryUnitCost = Convert.ToDecimal(objChild.txtCuryUnitCost.EditValue);
                    checkdup.UnitCost = Convert.ToDecimal(objChild.txtUnitCost.EditValue);
                    checkdup.ItemExpDate = DateTimeConvert.ConvertStringToDate(objChild.dtItemExpDate.Text);

                    DB.Entry(checkdup).State = System.Data.Entity.EntityState.Modified;
                }

                DB.SaveChanges();
                navRefresh_ElementClick(null,null);
                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "POS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
        #region Unit Conversion
        public void AddUnitConvert(StockItemSetup objChild)
        {
            if (GlobalInitializer.GfCheckNullValue(objChild.txtAltermateID, objChild.luFromUnit, objChild.luToUnit, objChild.txtUnitRate, objChild.cboMultiply) == false)
            {
                try
                {
                    if (_SalePrices.Any(x => x.AlternateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim())
                      || _INUnitConversion.Any(x => x.AltermateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim()))
                    {
                        XtraMessageBox.Show("The unit conversion already exists altermate.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        objChild.txtAltermateID.Focus();
                        return;
                    }



                    //int c = DB.INUnitConversions.Max(p => p.RecordId) + 1;
                    int numInven = 99;
                    //= DB.INItems.Max(p => p.InventoryID) + 1;
                    INUnitConversion iunit = new INUnitConversion()
                    {
                        CompanyID = DACClasses.CompanyID,
                        //RecordId = c++,
                        InventoryID = numInven,
                        FromUnit = objChild.luFromUnit.EditValue.ToString(),
                        ToUnit = objChild.luToUnit.EditValue.ToString(),
                        UnitMulDiv = objChild.cboMultiply.Text,
                        UnitRate = Convert.ToDecimal(objChild.txtUnitRate.EditValue),
                        AltermateID = objChild.txtAltermateID.Text
                    };
                    //DB.INUnitConversions.Add(iunit);
                    _INUnitConversion.Add(iunit);

                    objChild.gcUnit.DataSource = _INUnitConversion.Where(x => x.InventoryID == numInven).ToList();
                    objChild.gvUnit.RefreshData();

                    objChild.ClearUnitConversion();
                    //  var Row = DB.SaveChanges();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
               

        }
        public void EditAddUnitConvert(StockItemSetup objChild)
        {
            if (GlobalInitializer.GfCheckNullValue(objChild.txtAltermateID, objChild.luFromUnit, objChild.luToUnit, objChild.txtUnitRate, objChild.cboMultiply) == false)
            {
                try
                {

                    if (_SalePrices.Any(x => x.AlternateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim())
                        || _INUnitConversion.Any(x => x.AltermateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim()))
                    {
                        XtraMessageBox.Show("The unit conversion already exists altermate.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }


                    var rowC = objChild.gvUnit.DataRowCount;
                    INUnitConversion result = new INUnitConversion();
                    //result = (INUnitConversion)objChild.gvUnit.Get(rowC);
                    var CellValueRecord = objChild.gvUnit.GetRowCellValue(rowC - 1, "RecordId");

                    INUnitConversion iunit = new INUnitConversion()
                    {
                        CompanyID = DACClasses.CompanyID,
                        RecordId = Convert.ToInt32(CellValueRecord) + 1,
                        InventoryID = Convert.ToInt32(objChild.txtInventoryID.Text),
                        FromUnit = objChild.luFromUnit.EditValue.ToString(),
                        ToUnit = objChild.luToUnit.EditValue.ToString(),
                        UnitMulDiv = objChild.cboMultiply.Text,
                        UnitRate = Convert.ToDecimal(objChild.txtUnitRate.EditValue),
                        AltermateID = objChild.txtAltermateID.Text
                    };
                    DB.INUnitConversions.Add(iunit);
                    _INUnitConversion.Add(iunit);

                    //List<INUnitConversion> GetArticlesList = (objChild.gcUnit.DataSource as IEnumerable<INUnitConversion>).ToList();
                    //_INUnitConTemp = GetArticlesList;
                    //_INUnitConTemp.Add(iunit);


                    objChild.gcUnit.DataSource = _INUnitConversion.Where(x => x.InventoryID == Convert.ToInt32(objChild.txtInventoryID.Text)).ToList();
                    objChild.gvUnit.RefreshData();
                    objChild.ClearUnitConversion();
                    //  var Row = DB.SaveChanges();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "POS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
           

        }
        public void UpdateUnitConvert(StockItemSetup objChild)
        {      
            try
            {
                if (objChild.luFromUnit.EditValue == null || objChild.luToUnit.EditValue == null || objChild.txtUnitRate.Text == "0" || objChild.cboMultiply.Text == "")
                {
                    return;
                }


                if (_SalePrices.Any(x => x.AlternateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim())
                      || _INUnitConversion.Any(x => x.AltermateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim()))
                {
                    XtraMessageBox.Show("The unit conversion already exists altermate.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (_SalePrices.Any(x => x.AlternateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim())
                      || _INUnitConversion.Any(x => x.AltermateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim()))
                {
                    XtraMessageBox.Show("The unit conversion already exists altermate.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                objChild.gvUnit.SetRowCellValue(rowUnit, "FromUnit", objChild.luFromUnit.EditValue);
                objChild.gvUnit.SetRowCellValue(rowUnit, "ToUnit", objChild.luToUnit.EditValue);
                objChild.gvUnit.SetRowCellValue(rowUnit, "UnitMulDiv", objChild.cboMultiply.Text);
                objChild.gvUnit.SetRowCellValue(rowUnit, "UnitRate", objChild.txtUnitRate.Text);
                objChild.gvUnit.SetRowCellValue(rowUnit, "AltermateID", objChild.txtAltermateID.Text);

                objChild.gvUnit.RefreshData();
                List<INUnitConversion> GetArticlesList = (objChild.gcUnit.DataSource as IEnumerable<INUnitConversion>).ToList();

                DB.INUnitConversions.AddRange(GetArticlesList);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "POS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void EditUpdateUnitConvert(StockItemSetup objChild)
        {
            try
            {
                //if (_SalePrices.Any(x => x.AlternateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim())
                //      || _INUnitConversion.Any(x => x.AltermateID.Trim().ToString() == objChild.txtAltermateID.Text.Trim()))
                //{
                //    XtraMessageBox.Show("The unit conversion already exists altermate.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return;
                //}

                _INUnitConversion.Where(w => w.RecordId.ToString() == objChild.txtRecordID.Text).ToList().ForEach(x =>x.UnitMulDiv=objChild.cboMultiply.EditValue.ToString());
                _INUnitConversion.Where(w => w.RecordId.ToString() == objChild.txtRecordID.Text).ToList().ForEach(x => x.FromUnit = objChild.luFromUnit.EditValue.ToString());
                _INUnitConversion.Where(w => w.RecordId.ToString() == objChild.txtRecordID.Text).ToList().ForEach(x => x.ToUnit = objChild.luToUnit.EditValue.ToString());
                _INUnitConversion.Where(w => w.RecordId.ToString() == objChild.txtRecordID.Text).ToList().ForEach(x => x.UnitRate = Convert.ToDecimal(objChild.txtUnitRate.EditValue));
                _INUnitConversion.Where(w => w.RecordId.ToString() == objChild.txtRecordID.Text).ToList().ForEach(x => x.AltermateID = objChild.txtAltermateID.Text);
                objChild.gcUnit.DataSource = null;
                objChild.gcUnit.DataSource = _INUnitConversion.Where(x => x.InventoryID == Convert.ToInt32(objChild.txtInventoryID.Text)).ToList();

                objChild.gvUnit.RefreshData();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public void RemoveUnit(StockItemSetup objChild)
        {
            try
            {
                if (objChild.btnUpdateUnit.Enabled == true)
                {
                    objChild.ClearUnitConversion();
                    objChild.btnAddUnit.Enabled = true;
                    objChild.btnUpdateUnit.Enabled = false;
                }

                INUnitConversion result = new INUnitConversion();
                result = (INUnitConversion)objChild.gvUnit.GetRow(objChild.gvUnit.FocusedRowHandle);

                if (result != null)
                {
                    objChild.gvUnit.DeleteRow(objChild.gvUnit.FocusedRowHandle);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "POS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void EditRemoveUnit(StockItemSetup objChild)
        {
            try
            {
                if (objChild.btnUpdateUnit.Enabled == true)
                {
                    objChild.ClearUnitConversion();
                    objChild.btnAddUnit.Enabled = true;
                    objChild.btnUpdateUnit.Enabled = false;
                }
                INUnitConversion result = new INUnitConversion();
                result = (INUnitConversion)objChild.gvUnit.GetRow(objChild.gvUnit.FocusedRowHandle);
                
                if (result != null)
                {
                    INUnitConversion unit = DB.INUnitConversions.FirstOrDefault(st => st.CompanyID.Equals(result.CompanyID) && st.RecordId.Equals(result.RecordId));
                    if (unit == null)
                    {
                        DB.INUnitConversions.Remove(result);
                        _INUnitConversion.Remove(result);
                    }
                    else
                    {
                        DB.INUnitConversions.Remove(unit);
                        _INUnitConversion.Remove(unit);
                    }
                        
                    objChild.gcUnit.DataSource = null;
                    objChild.gcUnit.DataSource = _INUnitConversion.Where(x => x.InventoryID.ToString() == objChild.txtInventoryID.Text).ToList();
                    objChild.gvUnit.RefreshData();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "POS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
        private void GV_DoubleClick(object sender, EventArgs  e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }

}