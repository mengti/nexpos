﻿using ZooManagement;

namespace DgoStore.LoadForm.Sale
{
    partial class StockItemSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StockItemSetup));
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navNew = new DevExpress.XtraBars.Navigation.NavButton();
            this.navSave = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gcUnit = new DevExpress.XtraGrid.GridControl();
            this.gvUnit = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnRefreshUnit = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddUnit = new DevExpress.XtraEditors.SimpleButton();
            this.txtInventoryCD = new DevExpress.XtraEditors.TextEdit();
            this.txtDescr = new DevExpress.XtraEditors.TextEdit();
            this.txtImageUrl = new DevExpress.XtraEditors.PictureEdit();
            this.cmbItemType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.luBaseUnit = new DevExpress.XtraEditors.LookUpEdit();
            this.luSaleUnit = new DevExpress.XtraEditors.LookUpEdit();
            this.cmbIndicator = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbValuationMethod = new DevExpress.XtraEditors.ComboBoxEdit();
            this.luLotSerClassID = new DevExpress.XtraEditors.LookUpEdit();
            this.luCuryID = new DevExpress.XtraEditors.LookUpEdit();
            this.luCateCD = new DevExpress.XtraEditors.LookUpEdit();
            this.luPurchaseUnit = new DevExpress.XtraEditors.LookUpEdit();
            this.txtInventoryID = new DevExpress.XtraEditors.TextEdit();
            this.txtSalePrice = new DevExpress.XtraEditors.TextEdit();
            this.txtCuryUnitCost = new DevExpress.XtraEditors.TextEdit();
            this.txtUnitCost = new DevExpress.XtraEditors.TextEdit();
            this.txtBarcode = new DevExpress.XtraEditors.TextEdit();
            this.txtProfit = new DevExpress.XtraEditors.TextEdit();
            this.dtItemExpDate = new DevExpress.XtraEditors.DateEdit();
            this.dtStartDate = new DevExpress.XtraEditors.DateEdit();
            this.cboMultiply = new DevExpress.XtraEditors.ComboBoxEdit();
            this.luFromUnit = new DevExpress.XtraEditors.LookUpEdit();
            this.luToUnit = new DevExpress.XtraEditors.LookUpEdit();
            this.btnUpdateUnit = new DevExpress.XtraEditors.SimpleButton();
            this.txtRecordID = new DevExpress.XtraEditors.TextEdit();
            this.btnRemoveUnit = new DevExpress.XtraEditors.SimpleButton();
            this.txtAltermateID = new DevExpress.XtraEditors.TextEdit();
            this.txtUnitRate = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.iNItemCategoryBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.iNItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.zooManagementDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iNItemsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.iNItemCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInventoryCD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImageUrl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbItemType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luBaseUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luSaleUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIndicator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbValuationMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luLotSerClassID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luCuryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luCateCD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luPurchaseUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInventoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCuryUnitCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProfit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtItemExpDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtItemExpDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboMultiply.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luFromUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luToUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRecordID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAltermateID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNItemCategoryBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNItemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zooManagementDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNItemsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNItemCategoryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navNew);
            this.navHeader.Buttons.Add(this.navSave);
            this.navHeader.Buttons.Add(this.navEdit);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(1125, 40);
            this.navHeader.TabIndex = 5;
            this.navHeader.Text = "q";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Add Item ";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navNew
            // 
            this.navNew.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navNew.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navNew.AppearanceHovered.Options.UseForeColor = true;
            this.navNew.Caption = "New";
            this.navNew.Name = "navNew";
            this.navNew.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navSave
            // 
            this.navSave.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navSave.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navSave.AppearanceHovered.Options.UseForeColor = true;
            this.navSave.Caption = "Save";
            this.navSave.Name = "navSave";
            this.navSave.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navSave_ElementClick);
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Update";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gcUnit);
            this.layoutControl1.Controls.Add(this.btnRefreshUnit);
            this.layoutControl1.Controls.Add(this.btnAddUnit);
            this.layoutControl1.Controls.Add(this.txtInventoryCD);
            this.layoutControl1.Controls.Add(this.txtDescr);
            this.layoutControl1.Controls.Add(this.txtImageUrl);
            this.layoutControl1.Controls.Add(this.cmbItemType);
            this.layoutControl1.Controls.Add(this.cmbStatus);
            this.layoutControl1.Controls.Add(this.luBaseUnit);
            this.layoutControl1.Controls.Add(this.luSaleUnit);
            this.layoutControl1.Controls.Add(this.cmbIndicator);
            this.layoutControl1.Controls.Add(this.cmbValuationMethod);
            this.layoutControl1.Controls.Add(this.luLotSerClassID);
            this.layoutControl1.Controls.Add(this.luCuryID);
            this.layoutControl1.Controls.Add(this.luCateCD);
            this.layoutControl1.Controls.Add(this.luPurchaseUnit);
            this.layoutControl1.Controls.Add(this.txtInventoryID);
            this.layoutControl1.Controls.Add(this.txtSalePrice);
            this.layoutControl1.Controls.Add(this.txtCuryUnitCost);
            this.layoutControl1.Controls.Add(this.txtUnitCost);
            this.layoutControl1.Controls.Add(this.txtBarcode);
            this.layoutControl1.Controls.Add(this.txtProfit);
            this.layoutControl1.Controls.Add(this.dtItemExpDate);
            this.layoutControl1.Controls.Add(this.dtStartDate);
            this.layoutControl1.Controls.Add(this.cboMultiply);
            this.layoutControl1.Controls.Add(this.luFromUnit);
            this.layoutControl1.Controls.Add(this.luToUnit);
            this.layoutControl1.Controls.Add(this.btnUpdateUnit);
            this.layoutControl1.Controls.Add(this.txtRecordID);
            this.layoutControl1.Controls.Add(this.btnRemoveUnit);
            this.layoutControl1.Controls.Add(this.txtAltermateID);
            this.layoutControl1.Controls.Add(this.txtUnitRate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.layoutControlItem17,
            this.layoutControlItem16,
            this.layoutControlItem13,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem31});
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(901, 485, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1125, 766);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gcUnit
            // 
            this.gcUnit.Location = new System.Drawing.Point(24, 604);
            this.gcUnit.MainView = this.gvUnit;
            this.gcUnit.Name = "gcUnit";
            this.gcUnit.Size = new System.Drawing.Size(516, 138);
            this.gcUnit.TabIndex = 27;
            this.gcUnit.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvUnit});
            // 
            // gvUnit
            // 
            this.gvUnit.Appearance.GroupRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.gvUnit.Appearance.GroupRow.Options.UseFont = true;
            this.gvUnit.Appearance.HeaderPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvUnit.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvUnit.Appearance.Row.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.gvUnit.Appearance.Row.Options.UseFont = true;
            this.gvUnit.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn6});
            this.gvUnit.GridControl = this.gcUnit;
            this.gvUnit.Name = "gvUnit";
            this.gvUnit.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvUnit.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvUnit.OptionsBehavior.Editable = false;
            this.gvUnit.OptionsView.EnableAppearanceEvenRow = true;
            this.gvUnit.OptionsView.ShowGroupPanel = false;
            this.gvUnit.DoubleClick += new System.EventHandler(this.gvUnit_DoubleClick);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "RecordId";
            this.gridColumn5.FieldName = "RecordId";
            this.gridColumn5.Name = "gridColumn5";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "From Unit";
            this.gridColumn1.FieldName = "FromUnit";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "To Unit";
            this.gridColumn2.FieldName = "ToUnit";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Conversion Factor";
            this.gridColumn3.FieldName = "UnitRate";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Multiply / Divid";
            this.gridColumn4.FieldName = "UnitMulDiv";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "AltermateID";
            this.gridColumn6.FieldName = "AltermateID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // btnRefreshUnit
            // 
            this.btnRefreshUnit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefreshUnit.ImageOptions.Image")));
            this.btnRefreshUnit.Location = new System.Drawing.Point(73, 342);
            this.btnRefreshUnit.Name = "btnRefreshUnit";
            this.btnRefreshUnit.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnRefreshUnit.Size = new System.Drawing.Size(45, 36);
            this.btnRefreshUnit.StyleController = this.layoutControl1;
            this.btnRefreshUnit.TabIndex = 26;
            this.btnRefreshUnit.Click += new System.EventHandler(this.btnRefreshUnit_Click);
            // 
            // btnAddUnit
            // 
            this.btnAddUnit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUnit.ImageOptions.Image")));
            this.btnAddUnit.Location = new System.Drawing.Point(24, 342);
            this.btnAddUnit.Name = "btnAddUnit";
            this.btnAddUnit.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnAddUnit.Size = new System.Drawing.Size(45, 36);
            this.btnAddUnit.StyleController = this.layoutControl1;
            this.btnAddUnit.TabIndex = 25;
            this.btnAddUnit.Click += new System.EventHandler(this.btnAddUnit_Click);
            // 
            // txtInventoryCD
            // 
            this.txtInventoryCD.Location = new System.Drawing.Point(124, 113);
            this.txtInventoryCD.Name = "txtInventoryCD";
            this.txtInventoryCD.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInventoryCD.Properties.Appearance.Options.UseFont = true;
            this.txtInventoryCD.Properties.MaxLength = 15;
            this.txtInventoryCD.Size = new System.Drawing.Size(421, 22);
            this.txtInventoryCD.StyleController = this.layoutControl1;
            this.txtInventoryCD.TabIndex = 2;
            this.txtInventoryCD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // txtDescr
            // 
            this.txtDescr.Location = new System.Drawing.Point(124, 161);
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Properties.Appearance.Options.UseFont = true;
            this.txtDescr.Properties.MaxLength = 200;
            this.txtDescr.Size = new System.Drawing.Size(421, 22);
            this.txtDescr.StyleController = this.layoutControl1;
            this.txtDescr.TabIndex = 5;
            this.txtDescr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // txtImageUrl
            // 
            this.txtImageUrl.EditValue = ((object)(resources.GetObject("txtImageUrl.EditValue")));
            this.txtImageUrl.Location = new System.Drawing.Point(556, 505);
            this.txtImageUrl.Name = "txtImageUrl";
            this.txtImageUrl.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtImageUrl.Properties.Appearance.Options.UseFont = true;
            this.txtImageUrl.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.txtImageUrl.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.txtImageUrl.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.txtImageUrl.Size = new System.Drawing.Size(557, 249);
            this.txtImageUrl.StyleController = this.layoutControl1;
            this.txtImageUrl.TabIndex = 18;
            this.txtImageUrl.TabStop = true;
            // 
            // cmbItemType
            // 
            this.cmbItemType.EditValue = "Stock";
            this.cmbItemType.Location = new System.Drawing.Point(127, 193);
            this.cmbItemType.Name = "cmbItemType";
            this.cmbItemType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cmbItemType.Properties.Appearance.Options.UseFont = true;
            this.cmbItemType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbItemType.Properties.Items.AddRange(new object[] {
            "Stock",
            "Non Stock"});
            this.cmbItemType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbItemType.Size = new System.Drawing.Size(342, 20);
            this.cmbItemType.StyleController = this.layoutControl1;
            this.cmbItemType.TabIndex = 4;
            this.cmbItemType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // cmbStatus
            // 
            this.cmbStatus.EditValue = "AC";
            this.cmbStatus.Location = new System.Drawing.Point(127, 147);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cmbStatus.Properties.Appearance.Options.UseFont = true;
            this.cmbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbStatus.Properties.Items.AddRange(new object[] {
            "AC",
            "IN"});
            this.cmbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbStatus.Size = new System.Drawing.Size(342, 20);
            this.cmbStatus.StyleController = this.layoutControl1;
            this.cmbStatus.TabIndex = 3;
            this.cmbStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // luBaseUnit
            // 
            this.luBaseUnit.Location = new System.Drawing.Point(136, 305);
            this.luBaseUnit.Name = "luBaseUnit";
            this.luBaseUnit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luBaseUnit.Properties.Appearance.Options.UseFont = true;
            this.luBaseUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luBaseUnit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMID", "UOMID")});
            this.luBaseUnit.Properties.NullText = "";
            this.luBaseUnit.Properties.PopupSizeable = false;
            this.luBaseUnit.Properties.ShowFooter = false;
            this.luBaseUnit.Properties.ShowHeader = false;
            this.luBaseUnit.Size = new System.Drawing.Size(404, 22);
            this.luBaseUnit.StyleController = this.layoutControl1;
            this.luBaseUnit.TabIndex = 11;
            this.luBaseUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // luSaleUnit
            // 
            this.luSaleUnit.Location = new System.Drawing.Point(139, 335);
            this.luSaleUnit.Name = "luSaleUnit";
            this.luSaleUnit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luSaleUnit.Properties.Appearance.Options.UseFont = true;
            this.luSaleUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luSaleUnit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMID", "UOMID")});
            this.luSaleUnit.Properties.NullText = "";
            this.luSaleUnit.Properties.PopupSizeable = false;
            this.luSaleUnit.Properties.ShowFooter = false;
            this.luSaleUnit.Properties.ShowHeader = false;
            this.luSaleUnit.Size = new System.Drawing.Size(324, 22);
            this.luSaleUnit.StyleController = this.layoutControl1;
            this.luSaleUnit.TabIndex = 12;
            this.luSaleUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // cmbIndicator
            // 
            this.cmbIndicator.EditValue = "M";
            this.cmbIndicator.Location = new System.Drawing.Point(191, 441);
            this.cmbIndicator.Name = "cmbIndicator";
            this.cmbIndicator.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cmbIndicator.Properties.Appearance.Options.UseFont = true;
            this.cmbIndicator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbIndicator.Properties.Items.AddRange(new object[] {
            "M",
            "D"});
            this.cmbIndicator.Size = new System.Drawing.Size(239, 22);
            this.cmbIndicator.StyleController = this.layoutControl1;
            this.cmbIndicator.TabIndex = 16;
            // 
            // cmbValuationMethod
            // 
            this.cmbValuationMethod.EditValue = "AVG";
            this.cmbValuationMethod.Location = new System.Drawing.Point(191, 433);
            this.cmbValuationMethod.Name = "cmbValuationMethod";
            this.cmbValuationMethod.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cmbValuationMethod.Properties.Appearance.Options.UseFont = true;
            this.cmbValuationMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbValuationMethod.Properties.Items.AddRange(new object[] {
            "Statistic"});
            this.cmbValuationMethod.Size = new System.Drawing.Size(239, 22);
            this.cmbValuationMethod.StyleController = this.layoutControl1;
            this.cmbValuationMethod.TabIndex = 22;
            // 
            // luLotSerClassID
            // 
            this.luLotSerClassID.EditValue = "LREX";
            this.luLotSerClassID.Location = new System.Drawing.Point(600, 193);
            this.luLotSerClassID.Name = "luLotSerClassID";
            this.luLotSerClassID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.luLotSerClassID.Properties.Appearance.Options.UseFont = true;
            this.luLotSerClassID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luLotSerClassID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LotSerClassID", "LotSerClassID")});
            this.luLotSerClassID.Properties.NullText = "LREX";
            this.luLotSerClassID.Properties.PopupSizeable = false;
            this.luLotSerClassID.Properties.ShowFooter = false;
            this.luLotSerClassID.Properties.ShowHeader = false;
            this.luLotSerClassID.Size = new System.Drawing.Size(344, 20);
            this.luLotSerClassID.StyleController = this.layoutControl1;
            this.luLotSerClassID.TabIndex = 8;
            this.luLotSerClassID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // luCuryID
            // 
            this.luCuryID.EditValue = "USD";
            this.luCuryID.Enabled = false;
            this.luCuryID.Location = new System.Drawing.Point(673, 184);
            this.luCuryID.Name = "luCuryID";
            this.luCuryID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luCuryID.Properties.Appearance.Options.UseFont = true;
            this.luCuryID.Properties.Appearance.Options.UseTextOptions = true;
            this.luCuryID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luCuryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luCuryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CuryID", "CuryID")});
            this.luCuryID.Properties.NullText = "USD";
            this.luCuryID.Properties.PopupSizeable = false;
            this.luCuryID.Properties.ShowFooter = false;
            this.luCuryID.Properties.ShowHeader = false;
            this.luCuryID.Size = new System.Drawing.Size(428, 22);
            this.luCuryID.StyleController = this.layoutControl1;
            this.luCuryID.TabIndex = 9;
            this.luCuryID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // luCateCD
            // 
            this.luCateCD.Location = new System.Drawing.Point(673, 135);
            this.luCateCD.Name = "luCateCD";
            this.luCateCD.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.luCateCD.Properties.Appearance.Options.UseFont = true;
            this.luCateCD.Properties.Appearance.Options.UseTextOptions = true;
            this.luCateCD.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luCateCD.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luCateCD.Properties.AppearanceDropDown.Options.UseFont = true;
            this.luCateCD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luCateCD.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CategoryCD", "CategoryCD")});
            this.luCateCD.Properties.NullText = "Chose Category";
            this.luCateCD.Properties.PopupSizeable = false;
            this.luCateCD.Properties.ShowFooter = false;
            this.luCateCD.Properties.ShowHeader = false;
            this.luCateCD.Size = new System.Drawing.Size(428, 22);
            this.luCateCD.StyleController = this.layoutControl1;
            this.luCateCD.TabIndex = 7;
            this.luCateCD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // luPurchaseUnit
            // 
            this.luPurchaseUnit.Location = new System.Drawing.Point(139, 371);
            this.luPurchaseUnit.Name = "luPurchaseUnit";
            this.luPurchaseUnit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luPurchaseUnit.Properties.Appearance.Options.UseFont = true;
            this.luPurchaseUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luPurchaseUnit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMID", "UOMID")});
            this.luPurchaseUnit.Properties.NullText = "";
            this.luPurchaseUnit.Properties.PopupSizeable = false;
            this.luPurchaseUnit.Properties.ShowFooter = false;
            this.luPurchaseUnit.Properties.ShowHeader = false;
            this.luPurchaseUnit.Size = new System.Drawing.Size(324, 22);
            this.luPurchaseUnit.StyleController = this.layoutControl1;
            this.luPurchaseUnit.TabIndex = 13;
            this.luPurchaseUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // txtInventoryID
            // 
            this.txtInventoryID.Enabled = false;
            this.txtInventoryID.Location = new System.Drawing.Point(200, 22);
            this.txtInventoryID.Name = "txtInventoryID";
            this.txtInventoryID.Size = new System.Drawing.Size(270, 20);
            this.txtInventoryID.StyleController = this.layoutControl1;
            this.txtInventoryID.TabIndex = 23;
            // 
            // txtSalePrice
            // 
            this.txtSalePrice.EditValue = "0";
            this.txtSalePrice.Location = new System.Drawing.Point(680, 383);
            this.txtSalePrice.Name = "txtSalePrice";
            this.txtSalePrice.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.txtSalePrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalePrice.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtSalePrice.Properties.Appearance.Options.UseFont = true;
            this.txtSalePrice.Properties.Appearance.Options.UseForeColor = true;
            this.txtSalePrice.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSalePrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtSalePrice.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtSalePrice.Properties.DisplayFormat.FormatString = "n2";
            this.txtSalePrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSalePrice.Properties.Mask.EditMask = "n";
            this.txtSalePrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSalePrice.Properties.MaxLength = 10;
            this.txtSalePrice.Size = new System.Drawing.Size(421, 26);
            this.txtSalePrice.StyleController = this.layoutControl1;
            this.txtSalePrice.TabIndex = 15;
            this.txtSalePrice.TextChanged += new System.EventHandler(this.txtSalePrice_TextChanged);
            this.txtSalePrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSalePrice_KeyDown);
            // 
            // txtCuryUnitCost
            // 
            this.txtCuryUnitCost.EditValue = "00.00";
            this.txtCuryUnitCost.Location = new System.Drawing.Point(625, 357);
            this.txtCuryUnitCost.Name = "txtCuryUnitCost";
            this.txtCuryUnitCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtCuryUnitCost.Properties.Appearance.Options.UseFont = true;
            this.txtCuryUnitCost.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCuryUnitCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCuryUnitCost.Properties.Mask.EditMask = "d";
            this.txtCuryUnitCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCuryUnitCost.Size = new System.Drawing.Size(204, 26);
            this.txtCuryUnitCost.StyleController = this.layoutControl1;
            this.txtCuryUnitCost.TabIndex = 20;
            this.txtCuryUnitCost.EditValueChanged += new System.EventHandler(this.txtCuryUnitCost_EditValueChanged);
            this.txtCuryUnitCost.TextChanged += new System.EventHandler(this.txtCuryUnitCost_TextChanged);
            // 
            // txtUnitCost
            // 
            this.txtUnitCost.EditValue = "0";
            this.txtUnitCost.Location = new System.Drawing.Point(680, 305);
            this.txtUnitCost.Name = "txtUnitCost";
            this.txtUnitCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtUnitCost.Properties.Appearance.Options.UseFont = true;
            this.txtUnitCost.Properties.Appearance.Options.UseTextOptions = true;
            this.txtUnitCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtUnitCost.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtUnitCost.Properties.DisplayFormat.FormatString = "n2";
            this.txtUnitCost.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtUnitCost.Properties.Mask.EditMask = "n";
            this.txtUnitCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtUnitCost.Properties.MaxLength = 10;
            this.txtUnitCost.Properties.NullText = "00.00";
            this.txtUnitCost.Properties.NullValuePrompt = "00.00";
            this.txtUnitCost.Size = new System.Drawing.Size(421, 26);
            this.txtUnitCost.StyleController = this.layoutControl1;
            this.txtUnitCost.TabIndex = 14;
            this.txtUnitCost.TextChanged += new System.EventHandler(this.txtUnitCost_TextChanged);
            this.txtUnitCost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(24, 64);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Properties.Appearance.Options.UseFont = true;
            this.txtBarcode.Properties.MaxLength = 13;
            this.txtBarcode.Size = new System.Drawing.Size(1077, 22);
            this.txtBarcode.StyleController = this.layoutControl1;
            this.txtBarcode.TabIndex = 0;
            this.txtBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // txtProfit
            // 
            this.txtProfit.EditValue = "0";
            this.txtProfit.Enabled = false;
            this.txtProfit.Location = new System.Drawing.Point(680, 435);
            this.txtProfit.Name = "txtProfit";
            this.txtProfit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProfit.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtProfit.Properties.Appearance.Options.UseFont = true;
            this.txtProfit.Properties.Appearance.Options.UseForeColor = true;
            this.txtProfit.Properties.Appearance.Options.UseTextOptions = true;
            this.txtProfit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtProfit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txtProfit.Properties.DisplayFormat.FormatString = "n2";
            this.txtProfit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtProfit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.txtProfit.Properties.Mask.EditMask = "n";
            this.txtProfit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtProfit.Size = new System.Drawing.Size(421, 26);
            this.txtProfit.StyleController = this.layoutControl1;
            this.txtProfit.TabIndex = 16;
            this.txtProfit.TextChanged += new System.EventHandler(this.txtProfit_TextChanged);
            this.txtProfit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // dtItemExpDate
            // 
            this.dtItemExpDate.EditValue = null;
            this.dtItemExpDate.Location = new System.Drawing.Point(124, 236);
            this.dtItemExpDate.Name = "dtItemExpDate";
            this.dtItemExpDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtItemExpDate.Properties.Appearance.Options.UseFont = true;
            this.dtItemExpDate.Properties.Appearance.Options.UseTextOptions = true;
            this.dtItemExpDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dtItemExpDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtItemExpDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtItemExpDate.Properties.DisplayFormat.FormatString = "g";
            this.dtItemExpDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtItemExpDate.Properties.EditFormat.FormatString = "g";
            this.dtItemExpDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtItemExpDate.Properties.Mask.EditMask = "g";
            this.dtItemExpDate.Properties.NullDate = new System.DateTime(9999, 12, 31, 23, 59, 0, 0);
            this.dtItemExpDate.Properties.NullDateCalendarValue = new System.DateTime(9999, 12, 31, 23, 59, 0, 0);
            this.dtItemExpDate.Size = new System.Drawing.Size(421, 22);
            this.dtItemExpDate.StyleController = this.layoutControl1;
            this.dtItemExpDate.TabIndex = 10;
            this.dtItemExpDate.InvalidValue += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.dtItemExpDate_InvalidValue);
            this.dtItemExpDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            this.dtItemExpDate.Validating += new System.ComponentModel.CancelEventHandler(this.dtItemExpDate_Validating);
            // 
            // dtStartDate
            // 
            this.dtStartDate.EditValue = null;
            this.dtStartDate.Enabled = false;
            this.dtStartDate.Location = new System.Drawing.Point(124, 198);
            this.dtStartDate.Name = "dtStartDate";
            this.dtStartDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtStartDate.Properties.Appearance.Options.UseFont = true;
            this.dtStartDate.Properties.Appearance.Options.UseTextOptions = true;
            this.dtStartDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dtStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtStartDate.Size = new System.Drawing.Size(421, 22);
            this.dtStartDate.StyleController = this.layoutControl1;
            this.dtStartDate.TabIndex = 6;
            this.dtStartDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // cboMultiply
            // 
            this.cboMultiply.EditValue = "Multiply";
            this.cboMultiply.Enabled = false;
            this.cboMultiply.Location = new System.Drawing.Point(24, 578);
            this.cboMultiply.Name = "cboMultiply";
            this.cboMultiply.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.cboMultiply.Properties.Appearance.Options.UseFont = true;
            this.cboMultiply.Properties.Appearance.Options.UseTextOptions = true;
            this.cboMultiply.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cboMultiply.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboMultiply.Properties.Items.AddRange(new object[] {
            "Multiply",
            "Divid"});
            this.cboMultiply.Properties.NullText = "---Select---";
            this.cboMultiply.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboMultiply.Size = new System.Drawing.Size(516, 22);
            this.cboMultiply.StyleController = this.layoutControl1;
            this.cboMultiply.TabIndex = 30;
            // 
            // luFromUnit
            // 
            this.luFromUnit.Location = new System.Drawing.Point(24, 468);
            this.luFromUnit.Name = "luFromUnit";
            this.luFromUnit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luFromUnit.Properties.Appearance.Options.UseFont = true;
            this.luFromUnit.Properties.Appearance.Options.UseTextOptions = true;
            this.luFromUnit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luFromUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luFromUnit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMID", "UOMID")});
            this.luFromUnit.Properties.NullText = "---Select---";
            this.luFromUnit.Properties.PopupSizeable = false;
            this.luFromUnit.Properties.ShowFooter = false;
            this.luFromUnit.Properties.ShowHeader = false;
            this.luFromUnit.Size = new System.Drawing.Size(233, 22);
            this.luFromUnit.StyleController = this.layoutControl1;
            this.luFromUnit.TabIndex = 29;
            // 
            // luToUnit
            // 
            this.luToUnit.Location = new System.Drawing.Point(261, 468);
            this.luToUnit.Name = "luToUnit";
            this.luToUnit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luToUnit.Properties.Appearance.Options.UseFont = true;
            this.luToUnit.Properties.Appearance.Options.UseTextOptions = true;
            this.luToUnit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luToUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luToUnit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMID", "UOMID")});
            this.luToUnit.Properties.NullText = "---Select---";
            this.luToUnit.Properties.PopupSizeable = false;
            this.luToUnit.Properties.ShowFooter = false;
            this.luToUnit.Properties.ShowHeader = false;
            this.luToUnit.Size = new System.Drawing.Size(279, 22);
            this.luToUnit.StyleController = this.layoutControl1;
            this.luToUnit.TabIndex = 28;
            // 
            // btnUpdateUnit
            // 
            this.btnUpdateUnit.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnUpdateUnit.ImageOptions.SvgImage")));
            this.btnUpdateUnit.Location = new System.Drawing.Point(122, 342);
            this.btnUpdateUnit.Name = "btnUpdateUnit";
            this.btnUpdateUnit.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnUpdateUnit.Size = new System.Drawing.Size(51, 36);
            this.btnUpdateUnit.StyleController = this.layoutControl1;
            this.btnUpdateUnit.TabIndex = 32;
            this.btnUpdateUnit.Click += new System.EventHandler(this.btnUpdateUnit_Click);
            // 
            // txtRecordID
            // 
            this.txtRecordID.Location = new System.Drawing.Point(346, 351);
            this.txtRecordID.Name = "txtRecordID";
            this.txtRecordID.Size = new System.Drawing.Size(117, 20);
            this.txtRecordID.StyleController = this.layoutControl1;
            this.txtRecordID.TabIndex = 33;
            // 
            // btnRemoveUnit
            // 
            this.btnRemoveUnit.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnRemoveUnit.ImageOptions.SvgImage")));
            this.btnRemoveUnit.Location = new System.Drawing.Point(177, 342);
            this.btnRemoveUnit.Name = "btnRemoveUnit";
            this.btnRemoveUnit.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnRemoveUnit.Size = new System.Drawing.Size(45, 36);
            this.btnRemoveUnit.StyleController = this.layoutControl1;
            this.btnRemoveUnit.TabIndex = 34;
            this.btnRemoveUnit.Click += new System.EventHandler(this.btnRemoveUnit_Click);
            // 
            // txtAltermateID
            // 
            this.txtAltermateID.Location = new System.Drawing.Point(24, 413);
            this.txtAltermateID.Name = "txtAltermateID";
            this.txtAltermateID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtAltermateID.Properties.Appearance.Options.UseFont = true;
            this.txtAltermateID.Properties.MaxLength = 13;
            this.txtAltermateID.Size = new System.Drawing.Size(516, 22);
            this.txtAltermateID.StyleController = this.layoutControl1;
            this.txtAltermateID.TabIndex = 35;
            // 
            // txtUnitRate
            // 
            this.txtUnitRate.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtUnitRate.Location = new System.Drawing.Point(24, 523);
            this.txtUnitRate.Name = "txtUnitRate";
            this.txtUnitRate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtUnitRate.Properties.Appearance.Options.UseFont = true;
            this.txtUnitRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtUnitRate.Properties.DisplayFormat.FormatString = "n0";
            this.txtUnitRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtUnitRate.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.txtUnitRate.Properties.Mask.EditMask = "n0";
            this.txtUnitRate.Properties.MaxLength = 10;
            this.txtUnitRate.Size = new System.Drawing.Size(516, 22);
            this.txtUnitRate.StyleController = this.layoutControl1;
            this.txtUnitRate.TabIndex = 31;
            this.txtUnitRate.EditValueChanged += new System.EventHandler(this.txtUnitRate_EditValueChanged);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txtInventoryID;
            this.layoutControlItem19.Location = new System.Drawing.Point(44, 10);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(418, 24);
            this.layoutControlItem19.Text = "InventoryID";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.Control = this.txtCuryUnitCost;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(375, 30);
            this.layoutControlItem17.Text = "CuryUnit Cost";
            this.layoutControlItem17.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(164, 16);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.Control = this.cmbValuationMethod;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 110);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(410, 26);
            this.layoutControlItem16.Text = "Valuation Method";
            this.layoutControlItem16.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(164, 16);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.cmbIndicator;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(410, 26);
            this.layoutControlItem13.Text = "Indicator";
            this.layoutControlItem13.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(164, 16);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.luLotSerClassID;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(463, 24);
            this.layoutControlItem5.Text = "LotSerClass ID";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(120, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.cmbItemType;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 181);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem3.Text = "Item Type :";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(120, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.cmbStatus;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 135);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(461, 24);
            this.layoutControlItem4.Text = "Status :";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(120, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.Control = this.luSaleUnit;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 36);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(443, 26);
            this.layoutControlItem11.Text = "Sale Unit";
            this.layoutControlItem11.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(112, 16);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.Control = this.luPurchaseUnit;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(443, 26);
            this.layoutControlItem12.Text = "Purchase Unit";
            this.layoutControlItem12.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(112, 16);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.txtRecordID;
            this.layoutControlItem31.Location = new System.Drawing.Point(207, 52);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem31.Text = "RecordID";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(50, 20);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem13,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlGroup4,
            this.layoutControlGroup6,
            this.emptySpaceItem7,
            this.layoutControlGroup1,
            this.emptySpaceItem5,
            this.layoutControlGroup2,
            this.emptySpaceItem8,
            this.layoutControlItem9,
            this.layoutControlItem2,
            this.emptySpaceItem14,
            this.layoutControlItem25,
            this.layoutControlItem14});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1125, 766);
            this.Root.TextVisible = false;
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 175);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(537, 11);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtInventoryCD;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 101);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(537, 26);
            this.layoutControlItem1.Text = "Item Name :";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(109, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 127);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(537, 11);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 138);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(537, 11);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18,
            this.emptySpaceItem11,
            this.simpleSeparator1,
            this.simpleLabelItem1,
            this.layoutControlItem8,
            this.emptySpaceItem17,
            this.layoutControlItem22,
            this.emptySpaceItem15});
            this.layoutControlGroup4.Location = new System.Drawing.Point(544, 260);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(561, 205);
            this.layoutControlGroup4.Text = "Sale";
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.Control = this.txtUnitCost;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(537, 30);
            this.layoutControlItem18.Text = "Unit Cost :";
            this.layoutControlItem18.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(109, 16);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 30);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(537, 11);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 41);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(537, 1);
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.BackColor = System.Drawing.Color.DimGray;
            this.simpleLabelItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleLabelItem1.AppearanceItemCaption.ForeColor = System.Drawing.Color.SpringGreen;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 42);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(537, 24);
            this.simpleLabelItem1.Text = "Selling";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(109, 20);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.Control = this.txtSalePrice;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(537, 30);
            this.layoutControlItem8.Text = "Sale Price :";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(109, 20);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 66);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(537, 12);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.Control = this.txtProfit;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 130);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(537, 30);
            this.layoutControlItem22.Text = "Profit :";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(109, 20);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 108);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(537, 22);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1105, 90);
            this.layoutControlGroup6.Text = "Enter the Number/BarCode then press Enter";
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.Control = this.txtBarcode;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(1081, 45);
            this.layoutControlItem20.Text = "BarCode";
            this.layoutControlItem20.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(109, 16);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 90);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(537, 11);
            this.emptySpaceItem7.Text = "Discount ";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.emptySpaceItem12,
            this.layoutControlItem6,
            this.emptySpaceItem16,
            this.emptySpaceItem18});
            this.layoutControlGroup1.Location = new System.Drawing.Point(537, 90);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(568, 160);
            this.layoutControlGroup1.Text = "Group";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.luCateCD;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(544, 26);
            this.layoutControlItem7.Text = "Category Code :";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(109, 14);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 85);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(544, 30);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.luCuryID;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(544, 26);
            this.layoutControlItem6.Text = "Currency :";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(109, 14);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 75);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(544, 10);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 26);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(544, 23);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 250);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1105, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.emptySpaceItem3,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem15,
            this.emptySpaceItem19,
            this.emptySpaceItem20,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem26,
            this.emptySpaceItem22,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.emptySpaceItem21,
            this.simpleLabelItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 260);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(544, 486);
            this.layoutControlGroup2.Text = "Unit Of Measure";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.luBaseUnit;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(520, 26);
            this.layoutControlItem10.Text = "Base Unit";
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(109, 16);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 26);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(520, 11);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.btnAddUnit;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 37);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(49, 40);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.btnRefreshUnit;
            this.layoutControlItem24.Location = new System.Drawing.Point(49, 37);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(49, 40);
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.gcUnit;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 299);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(520, 142);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(0, 77);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(520, 12);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(0, 189);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(520, 10);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.Control = this.luFromUnit;
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(237, 45);
            this.layoutControlItem27.Text = "From Unit";
            this.layoutControlItem27.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(109, 16);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.Control = this.cboMultiply;
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 254);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(520, 45);
            this.layoutControlItem28.Text = "Multiply / Divid";
            this.layoutControlItem28.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(109, 16);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem26.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem26.Control = this.luToUnit;
            this.layoutControlItem26.Location = new System.Drawing.Point(237, 144);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(283, 45);
            this.layoutControlItem26.Text = "To Unit";
            this.layoutControlItem26.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(109, 16);
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.Location = new System.Drawing.Point(0, 244);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(520, 10);
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.Control = this.txtUnitRate;
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 199);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(520, 45);
            this.layoutControlItem29.Text = "Conversion Factor";
            this.layoutControlItem29.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(109, 16);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.btnUpdateUnit;
            this.layoutControlItem30.Location = new System.Drawing.Point(98, 37);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(55, 40);
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.btnRemoveUnit;
            this.layoutControlItem32.Location = new System.Drawing.Point(153, 37);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(49, 40);
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.Control = this.txtAltermateID;
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(520, 45);
            this.layoutControlItem33.Text = "Altermate ID";
            this.layoutControlItem33.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(109, 16);
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.Location = new System.Drawing.Point(0, 134);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(520, 10);
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.simpleLabelItem2.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem2.Location = new System.Drawing.Point(202, 37);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Size = new System.Drawing.Size(318, 40);
            this.simpleLabelItem2.Text = "(Optional)";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(109, 25);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(544, 465);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(561, 11);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.txtImageUrl;
            this.layoutControlItem9.Location = new System.Drawing.Point(544, 476);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(561, 270);
            this.layoutControlItem9.Text = "ImageUrl";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(109, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.txtDescr;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 149);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(537, 26);
            this.layoutControlItem2.Text = "Description :";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(109, 14);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 212);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(537, 12);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.Control = this.dtStartDate;
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 186);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(537, 26);
            this.layoutControlItem25.Text = "Start Date :";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(109, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.Control = this.dtItemExpDate;
            this.layoutControlItem14.CustomizationFormText = " Item Expried Date : ";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 224);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(537, 26);
            this.layoutControlItem14.Text = "Expried Date : ";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(109, 14);
            // 
            // iNItemCategoryBindingSource1
            // 
            this.iNItemCategoryBindingSource1.DataMember = "INItemCategory";
            // 
            // StockItemSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1125, 806);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Name = "StockItemSetup";
            this.Text = "Item";
            this.Load += new System.EventHandler(this.StockItemSetup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInventoryCD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImageUrl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbItemType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luBaseUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luSaleUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIndicator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbValuationMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luLotSerClassID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luCuryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luCateCD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luPurchaseUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInventoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCuryUnitCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProfit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtItemExpDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtItemExpDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboMultiply.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luFromUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luToUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRecordID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAltermateID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNItemCategoryBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNItemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zooManagementDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNItemsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iNItemCategoryBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navNew;
        private DevExpress.XtraBars.Navigation.NavButton navSave;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        public DevExpress.XtraEditors.TextEdit txtInventoryCD;
        public DevExpress.XtraEditors.TextEdit txtDescr;
        public DevExpress.XtraEditors.PictureEdit txtImageUrl;
        public DevExpress.XtraEditors.ComboBoxEdit cmbItemType;
        public DevExpress.XtraEditors.ComboBoxEdit cmbStatus;
        public DevExpress.XtraEditors.LookUpEdit luBaseUnit;
        public DevExpress.XtraEditors.LookUpEdit luSaleUnit;
        public DevExpress.XtraEditors.ComboBoxEdit cmbIndicator;
        public DevExpress.XtraEditors.ComboBoxEdit cmbValuationMethod;
        public DevExpress.XtraEditors.LookUpEdit luLotSerClassID;
        public DevExpress.XtraEditors.LookUpEdit luCateCD;
        public DevExpress.XtraEditors.LookUpEdit luCuryID;
        public DevExpress.XtraEditors.LookUpEdit luPurchaseUnit;
        public DevExpress.XtraEditors.TextEdit txtInventoryID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        public DevExpress.XtraEditors.TextEdit txtSalePrice;
        public DevExpress.XtraEditors.TextEdit txtCuryUnitCost;
        public DevExpress.XtraEditors.TextEdit txtUnitCost;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit txtProfit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        public DevExpress.XtraEditors.TextEdit txtBarcode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
   //   private ZooManagementDataSet zooManagementDataSet;
        private System.Windows.Forms.BindingSource iNItemsBindingSource;
    //    private ZooManagementDataSetTableAdapters.INItemsTableAdapter iNItemsTableAdapter;
        private System.Windows.Forms.BindingSource zooManagementDataSetBindingSource;
        private System.Windows.Forms.BindingSource iNItemsBindingSource1;
   //   private ZooManagementDataSet1 zooManagementDataSet1;
        private System.Windows.Forms.BindingSource iNItemCategoryBindingSource;
      //  private ZooManagementDataSet zooManagementDataSet;
        private System.Windows.Forms.BindingSource iNItemCategoryBindingSource1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        internal DevExpress.XtraEditors.DateEdit dtStartDate;
        internal DevExpress.XtraEditors.DateEdit dtItemExpDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        internal DevExpress.XtraEditors.LookUpEdit luFromUnit;
        internal DevExpress.XtraEditors.LookUpEdit luToUnit;
        internal DevExpress.XtraEditors.ComboBoxEdit cboMultiply;
        internal DevExpress.XtraGrid.GridControl gcUnit;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvUnit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        internal DevExpress.XtraEditors.TextEdit txtRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        internal DevExpress.XtraEditors.TextEdit txtAltermateID;
        internal DevExpress.XtraEditors.SimpleButton btnRefreshUnit;
        internal DevExpress.XtraEditors.SimpleButton btnAddUnit;
        internal DevExpress.XtraEditors.SimpleButton btnUpdateUnit;
        internal DevExpress.XtraEditors.SimpleButton btnRemoveUnit;
        internal DevExpress.XtraEditors.SpinEdit txtUnitRate;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        //   private ZooManagementDataSet1TableAdapters.INItemCategoryTableAdapter iNItemCategoryTableAdapter;
    }
}