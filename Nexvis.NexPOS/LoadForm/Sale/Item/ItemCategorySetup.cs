﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;

namespace DgoStore.LoadForm.Sale
{
    public partial class ItemCategorySetup : DevExpress.XtraEditors.XtraForm
    {
        #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        private scrCategory parentForm;

        #endregion

        #region --- Form Action ---
        public ItemCategorySetup(string transactionType, scrCategory objParent)
        {
            InitializeComponent();
            parentForm = objParent;


            if (transactionType == "NEW")
            {
                navEdit.Visible = false;
                isNew = true;
            }
            else if (transactionType == "EDIT")
            {
                navSave.Visible = false;
            }

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;

        }

        #endregion

        #region --- Header Action ---

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Clear value for new
            GlobalInitializer.GsClearValue(txtCategoryID, txtCategoryCD,
                txtCategoryName, txtDescr);

            navEdit.Visible = false;
            navSave.Visible = true;
            isNew = true;

            txtImagesUrl.Image = null;
            txtImagesUrl.Invalidate();
           
            txtCategoryID.Enabled = false;
            txtCategoryCD.Focus();

        }

        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtCategoryCD,
                txtCategoryName) == false)
            {
                //    // TODO: Save value to DB.
                parentForm.GsSaveData(this);


                // TODO: Validate which record exiting.
                if (isExiting && parentForm.isSuccessful)
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCategoryID.Focus();

                    return;
                }

                if (parentForm.isSuccessful)
                    navNew_ElementClick(sender, e);
                this.Close();
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue( txtCategoryCD) == false)
                parentForm.GsUpdateData(this);
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close(); // this.Parent.Controls.Remove(this);
            try
            {
                //parentForm.navRefresh_ElementClick(sender, e);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void txtCategoryCD_MouseHover(object sender, EventArgs e)
        {

        }

        private void txtCategoryCD_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");     
            }
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                navSave_ElementClick(null, null);
            }
        }

        private void txtCategoryName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtImagesUrl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                navSave_ElementClick(null, null);
            }
        }
    }
}