﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using ZooMangement;
using Nexvis.NexPOS.Helper;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.Sale
{
    public partial class scrCategory : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrCategory()
        {
            InitializeComponent();
      
        }

        List<INItemCategory> _ItemCategory = new List<INItemCategory>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        public bool isSuccessful = true;

        private void FormItemCategory_Load(object sender, EventArgs e)
        {
            _ItemCategory = DB.INItemCategories.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            GC.DataSource = _ItemCategory.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

           
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormItemCategory_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    if (GV.RowCount > 0)
                    {
                        INItemCategory result = new INItemCategory();
                        result = (INItemCategory)GV.GetRow(GV.FocusedRowHandle);

                        if (result != null)
                        {
                            if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                            {
                                INItemCategory customer = DB.INItemCategories.FirstOrDefault(st => st.CompanyID.Equals(result.CompanyID) && st.CategoryID.Equals(result.CategoryID));

                                DB.INItemCategories.Remove(customer);
                                DB.SaveChanges();
                                FormItemCategory_Load(sender, e);
                            }
                        }
                    }
                    else
                        XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                //NavButton navigator = (NavButton)sender;
                var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    ItemCategorySetup objForm = new ItemCategorySetup("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    // GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    INItemCategory result = new INItemCategory();

                    result = (INItemCategory)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        objForm.txtCategoryID.Text = result.CategoryID.ToString();
                        objForm.txtCategoryID.Enabled = false;
                        objForm.txtCategoryCD.Focus();
                        objForm.txtDescr.Text = result.Descr;
                        objForm.txtCategoryCD.Text = result.CategoryCD;
                        objForm.txtCategoryName.Text = result.CategoryName;
                        objForm.txtImagesUrl.Image = result.ImagesUrl == null ? objForm.txtImagesUrl.Image : DACClasses.ReadImage(result.ImagesUrl);


                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

             

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Args_Showing(object sender, XtraMessageShowingArgs e)
        {
            //bold message caption
            e.Form.Appearance.FontStyleDelta = FontStyle.Bold;
            //increased button height and font size
            MessageButtonCollection buttons = e.Buttons as MessageButtonCollection;
            SimpleButton btn = buttons[System.Windows.Forms.DialogResult.OK] as SimpleButton;
            if (btn != null)
            {
                btn.Appearance.FontSizeDelta = 5;
                btn.Height += 10;
            }
        }
        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    ItemCategorySetup objForm = new ItemCategorySetup("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    objForm.txtCategoryID.Enabled = false;
                    objForm.txtImagesUrl.Image = null;
                    objForm.txtImagesUrl.Invalidate();

                    //TODO: Binding value to comboBox or LookupEdit.
                    //Global Initializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    // objForm._cities = _cities;  
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

              
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Actions
        public void GsSaveData(ItemCategorySetup objChild)
        {
            objChild.isExiting = false;
            if (_ItemCategory.Any(x => x.CategoryID.ToString() == objChild.txtCategoryID.Text.Trim().ToLower()))
            {
                objChild.isExiting = true;
                return;
            }

           // if (XtraMessageBox.Show("Are you sure you want to save this record?", "POS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            INItemCategory itemCategory = new INItemCategory()
            {
                CompanyID = DACClasses.CompanyID,
                //CategoryID = DB.INItemCategories.Max(x => x.CategoryID) + 1,
                CategoryCD = objChild.txtCategoryCD.Text,
                CategoryName = objChild.txtCategoryName.Text,
                Descr = objChild.txtDescr.Text,
                ImagesUrl = DACClasses.ReturnImage(objChild.txtImagesUrl)

            };

            DB.INItemCategories.Add(itemCategory);
            var Row = DB.SaveChanges();
            if (Row == 1)
            {
                isSuccessful = true;
            }
            GV.FocusedRowHandle = 0;
            navRefresh_ElementClick(null,null);
            XtraMessageBox.Show("This field has been Saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public void GsUpdateData(ItemCategorySetup objChild)
        {
            try
            {
                if (_ItemCategory.Any(x => x.CategoryID.ToString() != objChild.txtCategoryID.Text.Trim().ToLower()
                && x.CategoryCD.Trim().ToLower() == objChild.txtCategoryCD.Text.Trim().ToLower()))
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtCategoryCD.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                var _obj = DB.INItemCategories.SingleOrDefault(w => w.CategoryID.ToString() == objChild.txtCategoryID.Text && w.CompanyID.Equals(DACClasses.CompanyID));

                _obj.CategoryName = objChild.txtCategoryName.Text;
                _obj.CategoryCD = objChild.txtCategoryCD.Text;
                _obj.Descr = objChild.txtDescr.Text;
                _obj.ImagesUrl = DACClasses.ReturnImage(objChild.txtImagesUrl);

                DB.INItemCategories.Attach(_obj);
                DB.Entry(_obj).Property(w => w.CategoryName).IsModified = true;
                DB.Entry(_obj).Property(w => w.CategoryCD).IsModified = true;
                DB.Entry(_obj).Property(w => w.Descr).IsModified = true;
                DB.Entry(_obj).Property(w => w.ImagesUrl).IsModified = true;

                var Row = DB.SaveChanges();

                XtraMessageBoxArgs args = new XtraMessageBoxArgs();
                args.Caption = "Message";
                args.Text = "This field has been upadated.";
                args.Buttons = new DialogResult[] { DialogResult.OK };
                args.Showing += Args_Showing;
                XtraMessageBox.Show(args).ToString();
                objChild.Close();

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion
        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}