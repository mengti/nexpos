﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;
using DgoStore.LoadForm.Sale;
using DevExpress.XtraReports.UI;
using ZooMangement;
using DgoStore.LoadForm.Organizations;
using Nexvis.NexPOS.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS;

namespace DgoStore.LoadForm.Sale
{
    public partial class StockItemSetup : DevExpress.XtraEditors.XtraForm
    {
        #region --- Variable Declaration ---
        ZooEntity DB = new ZooEntity();

        public bool isExiting = false;
        public bool isExistingBarcode = false;

        private bool isNew = false;
        ClsGlobal _obj = new ClsGlobal();
        private scrItem parentForm;
        public List<INItemCategory> _Category = new List<INItemCategory>();

        #endregion

        #region --- Form Action ---
        public StockItemSetup(string transactionType, scrItem objParent)
        {
            InitializeComponent();
            parentForm = objParent;
            gcUnit.DataSource = null;
            gcUnit.DataSource = objParent._INUnitConversion.Where(x => x.InventoryID.ToString() == txtInventoryID.Text) ;
           // btnUpdateUnit.Enabled = false;
            if (transactionType == "NEW")
            {
                navEdit.Visible = false;
                // luCuryID.Enabled = false;
              //  dtStartDate.Visible = true;
                isNew = true;
            }
            else if (transactionType == "EDIT")
            {
                navSave.Visible = false;
            }

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;


          //  luCateCD.EditValueChanged += new EventHandler(luCateCD_EditValueChanged);
        }
        #endregion
        #region --- Header Action ---
        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {

            //TODO: Clear value for new
            GlobalInitializer.GsClearValue(txtBarcode,txtInventoryCD, txtDescr,
                luCateCD,luBaseUnit, luSaleUnit, luPurchaseUnit, txtSalePrice, txtCuryUnitCost, txtUnitCost, luToUnit,
                luFromUnit,cboMultiply,txtUnitRate,txtAltermateID);
            
            navEdit.Visible = false;;
            navSave.Visible = true;
            isNew = true;
           // dtStartDate.Visible = true;
            txtImageUrl.Image = null;
            txtImageUrl.Invalidate();
            //txtUnitCost.Enabled = false;
            txtInventoryID.Enabled = false;
            txtInventoryCD.Focus();
            //parentForm._INUnitConversion.Clear();
            gcUnit.DataSource = null;
           // txtAltermateID.Enabled = true;
            txtInventoryID.Text = "";

            btnUpdateUnit.Enabled = false;

        }
        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {   
            if (GlobalInitializer.GfCheckNullValue(txtInventoryCD,txtBarcode, luBaseUnit, luCateCD,  luCuryID) == false)
            {
                //    // TODO: Save value to DB.
                parentForm.GsSaveData(this);
                if (dtStartDate.DateTime > dtItemExpDate.DateTime)
                {
                    XtraMessageBox.Show("Your Application is Expired.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                if (isExistingBarcode)
                {
                    XtraMessageBox.Show("The Stock item already exists barcode.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtInventoryCD.Focus();

                    return;
                }


                // TODO: Validate which record exiting.
                if (isExiting && parentForm.isSuccessful)
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtInventoryCD.Focus();

                    return;
                }

                if (parentForm.isSuccessful)
                {
                  //  dtStartDate.Visible = false;
                    navNew_ElementClick(sender, e);
                    gcUnit.DataSource = null;
                   
                }
                   
            }
        }
        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            txtUnitRate.Text = null;
            txtUnitRate.Text = "0";
            if (GlobalInitializer.GfCheckNullValue(txtInventoryCD, cmbItemType, cmbStatus,
                luCuryID,luCateCD, txtSalePrice, luBaseUnit, cmbIndicator, 
                cmbValuationMethod, txtCuryUnitCost, txtUnitCost, txtUnitRate) == false)
            {
                btnUpdateUnit.Enabled = false;
                btnAddUnit.Enabled = true;
              //  txtAltermateID.Enabled = false;
                if (dtStartDate.DateTime > dtItemExpDate.DateTime)
                {
                    MessageBox.Show("Your Application is Expired");
                }
                parentForm.GsUpdateData(this);
                
            }
            this.Close();
        }
        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close(); // this.Parent.Controls.Remove(this);
            parentForm.navRefresh_ElementClick(sender, e);
        }
        #endregion
        private void txtCuryUnitCost_TextChanged(object sender, EventArgs e)
        {
            ChangeText();
        }

        private void txtSalePrice_TextChanged(object sender, EventArgs e)
        {

            ChangeText();
        }

        private void btnRandomCode_Click(object sender, EventArgs e)
        {
            //XRBarCode xrBarCode1;
            //Random g = new Random(Environment.TickCount);
            //for (int i = 0; i < 120; i++)
            //{
            //    xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            //    //txtBarcode.Text = xrBarCode1.Text;
            //    xrBarCode1.Name = i.ToString();
            //    xrBarCode1.SizeF = new System.Drawing.SizeF(258.3333F, 100F);
            //    xrBarCode1.Symbology = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            //    xrBarCode1.Text = g.Next(10000000, 434243242).ToString();
            //    txtBarcode.Text = xrBarCode1.Text;
            //}
        }
        void ChangeText()
        {
            decimal UnitCost = Convert.ToDecimal(txtUnitCost.Text == "" ? 0 : txtUnitCost.EditValue);
            decimal SalePrice = Convert.ToDecimal(txtSalePrice.Text == "" ? 0 : txtSalePrice.EditValue);
            decimal Profit = 0;
            Profit = SalePrice - UnitCost;
            txtProfit.EditValue = (decimal)Profit;
            txtCuryUnitCost.EditValue = txtUnitCost.EditValue;
            //if ((decimal)txtProfit.EditValue >= 0)
            //    txtProfit.EditValue = _obj.RoundRielAmountDown(Math.Round((decimal)txtProfit.EditValue, 0));
            //else
            //    txtProfit.EditValue = _obj.RoundRielAmountUp(Math.Round((decimal)txtProfit.EditValue, 0) * -1) * -1;
        }
        private void txtProfit_TextChanged(object sender, EventArgs e)
        {
            ChangeText();
        }

        private void txtUnitCost_TextChanged(object sender, EventArgs e)
        {
            ChangeText();
        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            DgoStore.LoadForm.Organizations.AddCategory objPayCate = new DgoStore.LoadForm.Organizations.AddCategory();
            FormShadow Shadow = new FormShadow(objPayCate);
            Shadow.ShowDialog();
        }

        private void txtCuryUnitCost_EditValueChanged(object sender, EventArgs e)
        {

        }
        private void StockItemSetup_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'zooManagementDataSet.INItemCategory' table. You can move, or remove it, as needed.
        //   this.iNItemCategoryTableAdapter.Fill(this.zooManagementDataSet.INItemCategory);
        //    luCateCD.EditValueChanged += new EventHandler(luCateCD_EditValueChanged);

        }
        private void dtItemExpDate_InvalidValue(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {
            MessageBox.Show("Enter a date within the current month", "Error");
        }
        private void dtItemExpDate_Validating(object sender, CancelEventArgs e)
        {
            //DateTime currentValue = (sender as DateEdit).DateTime;
            //if (currentValue.Date != DateTime.Month || currentValue.Year != DateTime..Year)
            //    e.Cancel = true;
            if(dtStartDate.DateTime > dtItemExpDate.DateTime)
            {
                MessageBox.Show("Your Application is Expired");
            }
        }
        private void txtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }
        private void txtSalePrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && navSave.Visible == true)
            {
                navSave_ElementClick(null, null);
            }
            else if (e.KeyCode == Keys.Enter && navSave.Visible == false)
            {
                navEdit_ElementClick(null, null);
            }
        }
        private void txtUnitRate_EditValueChanged(object sender, EventArgs e)
        {

        }
        private void btnRefreshUnit_Click(object sender, EventArgs e)
        {
            gvUnit.RefreshData();
            ClearUnitConversion();
            btnUpdateUnit.Enabled = false;
            btnAddUnit.Enabled = true;
            txtAltermateID.Enabled = true;
        }
        private void gvUnit_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            Point point = view.GridControl.PointToClient(Control.MousePosition);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
            if (info.InRow || info.InRowCell)
            {

                INUnitConversion result = new INUnitConversion();

                result = (INUnitConversion)gvUnit.GetRow(gvUnit.FocusedRowHandle);
                if (result != null)
                {
                    txtUnitRate.Text = result.UnitRate.ToString();
                    cboMultiply.Text = result.UnitMulDiv;
                    luFromUnit.EditValue = result.FromUnit;
                    luToUnit.EditValue = result.ToUnit;
                    txtRecordID.Text = result.RecordId.ToString();
                    txtAltermateID.Text = result.AltermateID;
                }
                btnAddUnit.Enabled = false;
                btnUpdateUnit.Enabled = true;
                parentForm.rowUnit = info.RowHandle;
                txtAltermateID.Enabled = false;
            }
        }
        #region Action Unit Conversion 
        private void btnAddUnit_Click(object sender, EventArgs e)
        {
            if (isNew == true)
            {
                parentForm.AddUnitConvert(this);
            }
            else
            {
                parentForm.EditAddUnitConvert(this);
            }
            //if (parentForm.isSuccessful)
            //{
            //    ClearUnitConversion();
            //  //  btnUpdateUnit.Enabled = false;
            //  //  btnAddUnit.Enabled = true;
            //}
        }
        private void btnUpdateUnit_Click(object sender, EventArgs e)
        {
            if (isNew == true)
            {
                parentForm.UpdateUnitConvert(this);
            }
            else
            {
                txtAltermateID.Enabled = true;
                parentForm.EditUpdateUnitConvert(this);
            }
            if (parentForm.isSuccessful)
            {
                ClearUnitConversion();
                btnUpdateUnit.Enabled = false;
                btnAddUnit.Enabled = true;
            }

        }
        public void ClearUnitConversion()
        {
            GlobalInitializer.GsClearValue(txtRecordID, luToUnit,
            luFromUnit, cboMultiply, txtUnitRate,txtAltermateID);
            txtUnitRate.Text = "0";
        }
        private void btnRemoveUnit_Click(object sender, EventArgs e)
        {
            if (isNew == true)
            {
                parentForm.RemoveUnit(this);
            }
            else
            {
                parentForm.EditRemoveUnit(this);
            }
        }
        #endregion
        //public void ValidateAltermate()
        //{
        //    if (txtAltermateID.Text == "") txtAltermateID.Enabled = true;
        //    else txtAltermateID.Enabled = false;
        //}
    }
}