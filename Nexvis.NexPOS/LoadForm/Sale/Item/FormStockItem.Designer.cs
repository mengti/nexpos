﻿namespace DgoStore.LoadForm.Sale
{
    partial class scrItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navNew = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navDelete = new DevExpress.XtraBars.Navigation.NavButton();
            this.navExport = new DevExpress.XtraBars.Navigation.NavButton();
            this.navRefresh = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.GC = new DevExpress.XtraGrid.GridControl();
            this.GV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.InventoryCD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Descr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemExpDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LotSerClassID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SalePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ImageUrl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BaseUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SaleUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PurchaseUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UnitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Status = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navNew);
            this.navHeader.Buttons.Add(this.navEdit);
            this.navHeader.Buttons.Add(this.navDelete);
            this.navHeader.Buttons.Add(this.navExport);
            this.navHeader.Buttons.Add(this.navRefresh);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(848, 40);
            this.navHeader.TabIndex = 8;
            this.navHeader.Text = "tileNavPane1";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Stock Items List";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navNew
            // 
            this.navNew.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navNew.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navNew.AppearanceHovered.Options.UseForeColor = true;
            this.navNew.Caption = "New";
            this.navNew.Name = "navNew";
            this.navNew.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Edit";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navDelete
            // 
            this.navDelete.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navDelete.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navDelete.AppearanceHovered.Options.UseForeColor = true;
            this.navDelete.Caption = "Delete";
            this.navDelete.Name = "navDelete";
            this.navDelete.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navDelete_ElementClick);
            // 
            // navExport
            // 
            this.navExport.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navExport.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navExport.AppearanceHovered.Options.UseForeColor = true;
            this.navExport.Caption = "Export";
            this.navExport.Name = "navExport";
            this.navExport.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navExport_ElementClick);
            // 
            // navRefresh
            // 
            this.navRefresh.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navRefresh.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navRefresh.AppearanceHovered.Options.UseForeColor = true;
            this.navRefresh.Caption = "Refresh";
            this.navRefresh.Name = "navRefresh";
            this.navRefresh.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navRefresh_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // GC
            // 
            this.GC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GC.Location = new System.Drawing.Point(0, 40);
            this.GC.MainView = this.GV;
            this.GC.Name = "GC";
            this.GC.Size = new System.Drawing.Size(848, 456);
            this.GC.TabIndex = 9;
            this.GC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GV});
            // 
            // GV
            // 
            this.GV.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.GV.Appearance.GroupPanel.Options.UseFont = true;
            this.GV.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.GV.Appearance.HeaderPanel.Options.UseFont = true;
            this.GV.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GV.Appearance.Row.Options.UseFont = true;
            this.GV.ColumnPanelRowHeight = 30;
            this.GV.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.InventoryCD,
            this.Descr,
            this.ItemType,
            this.ItemExpDate,
            this.LotSerClassID,
            this.SalePrice,
            this.gridColumn1,
            this.ImageUrl,
            this.BaseUnit,
            this.SaleUnit,
            this.PurchaseUnit,
            this.UnitCost,
            this.Status});
            this.GV.CustomizationFormBounds = new System.Drawing.Rectangle(1156, 368, 210, 242);
            this.GV.GridControl = this.GC;
            this.GV.IndicatorWidth = 35;
            this.GV.Name = "GV";
            this.GV.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV.OptionsBehavior.Editable = false;
            this.GV.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GV.OptionsView.EnableAppearanceEvenRow = true;
            this.GV.OptionsView.ShowGroupPanel = false;
            this.GV.RowHeight = 30;
            this.GV.DoubleClick += new System.EventHandler(this.GV_DoubleClick);
            // 
            // InventoryCD
            // 
            this.InventoryCD.Caption = "Inventory Name";
            this.InventoryCD.FieldName = "InventoryCD";
            this.InventoryCD.Name = "InventoryCD";
            this.InventoryCD.Visible = true;
            this.InventoryCD.VisibleIndex = 0;
            // 
            // Descr
            // 
            this.Descr.Caption = "Description";
            this.Descr.FieldName = "Descr";
            this.Descr.Name = "Descr";
            this.Descr.Visible = true;
            this.Descr.VisibleIndex = 1;
            // 
            // ItemType
            // 
            this.ItemType.Caption = "Item Type";
            this.ItemType.FieldName = "ItemType";
            this.ItemType.Name = "ItemType";
            this.ItemType.Visible = true;
            this.ItemType.VisibleIndex = 2;
            // 
            // ItemExpDate
            // 
            this.ItemExpDate.Caption = "Item ExpiredDate";
            this.ItemExpDate.DisplayFormat.FormatString = "D";
            this.ItemExpDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ItemExpDate.FieldName = "ItemExpDate";
            this.ItemExpDate.Name = "ItemExpDate";
            this.ItemExpDate.Visible = true;
            this.ItemExpDate.VisibleIndex = 7;
            // 
            // LotSerClassID
            // 
            this.LotSerClassID.Caption = "Class";
            this.LotSerClassID.FieldName = "LotSerClassID";
            this.LotSerClassID.Name = "LotSerClassID";
            // 
            // SalePrice
            // 
            this.SalePrice.Caption = "Sale Price";
            this.SalePrice.FieldName = "SalePrice";
            this.SalePrice.Name = "SalePrice";
            this.SalePrice.Visible = true;
            this.SalePrice.VisibleIndex = 3;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Discount Price";
            this.gridColumn1.FieldName = "DisSalePrice";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // ImageUrl
            // 
            this.ImageUrl.Caption = "Image";
            this.ImageUrl.FieldName = "ImageUrl";
            this.ImageUrl.Name = "ImageUrl";
            this.ImageUrl.Visible = true;
            this.ImageUrl.VisibleIndex = 4;
            // 
            // BaseUnit
            // 
            this.BaseUnit.Caption = "Base Unit";
            this.BaseUnit.FieldName = "BaseUnit";
            this.BaseUnit.Name = "BaseUnit";
            this.BaseUnit.Visible = true;
            this.BaseUnit.VisibleIndex = 5;
            // 
            // SaleUnit
            // 
            this.SaleUnit.Caption = "Sale Unit";
            this.SaleUnit.FieldName = "SaleUnit";
            this.SaleUnit.Name = "SaleUnit";
            // 
            // PurchaseUnit
            // 
            this.PurchaseUnit.Caption = "Purchase Unit";
            this.PurchaseUnit.FieldName = "PurchaseUnit";
            this.PurchaseUnit.Name = "PurchaseUnit";
            // 
            // UnitCost
            // 
            this.UnitCost.Caption = "Unit Cost";
            this.UnitCost.FieldName = "UnitCost";
            this.UnitCost.Name = "UnitCost";
            this.UnitCost.Visible = true;
            this.UnitCost.VisibleIndex = 6;
            // 
            // Status
            // 
            this.Status.Caption = "Status";
            this.Status.FieldName = "Status";
            this.Status.Name = "Status";
            // 
            // scrItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 496);
            this.Controls.Add(this.GC);
            this.Controls.Add(this.navHeader);
            this.Name = "scrItem";
            this.Text = "FormStockItem";
            this.Load += new System.EventHandler(this.FormStockItem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        private DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navNew;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navDelete;
        private DevExpress.XtraBars.Navigation.NavButton navExport;
        private DevExpress.XtraBars.Navigation.NavButton navRefresh;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraGrid.GridControl GC;
        private DevExpress.XtraGrid.Views.Grid.GridView GV;
        private DevExpress.XtraGrid.Columns.GridColumn InventoryCD;
        private DevExpress.XtraGrid.Columns.GridColumn Descr;
        private DevExpress.XtraGrid.Columns.GridColumn ItemType;
        private DevExpress.XtraGrid.Columns.GridColumn Status;
        private DevExpress.XtraGrid.Columns.GridColumn LotSerClassID;
        private DevExpress.XtraGrid.Columns.GridColumn SalePrice;
        private DevExpress.XtraGrid.Columns.GridColumn ImageUrl;
        private DevExpress.XtraGrid.Columns.GridColumn BaseUnit;
        private DevExpress.XtraGrid.Columns.GridColumn SaleUnit;
        private DevExpress.XtraGrid.Columns.GridColumn PurchaseUnit;
        private DevExpress.XtraGrid.Columns.GridColumn UnitCost;
        private DevExpress.XtraGrid.Columns.GridColumn ItemExpDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}