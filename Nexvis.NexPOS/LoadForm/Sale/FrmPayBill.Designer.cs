﻿namespace DgoStore.LoadForm.Sale
{
    partial class FrmPayBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPayBill));
            this.layoutKey = new System.Windows.Forms.FlowLayoutPanel();
            this.btnNumberOne = new DevExpress.XtraEditors.SimpleButton();
            this.btnNumberTwo = new DevExpress.XtraEditors.SimpleButton();
            this.btnNumberThree = new DevExpress.XtraEditors.SimpleButton();
            this.btnNumberFour = new DevExpress.XtraEditors.SimpleButton();
            this.btnNumberFive = new DevExpress.XtraEditors.SimpleButton();
            this.btnNumberSix = new DevExpress.XtraEditors.SimpleButton();
            this.btnNumberSeven = new DevExpress.XtraEditors.SimpleButton();
            this.btnNumberEight = new DevExpress.XtraEditors.SimpleButton();
            this.btnNumberNine = new DevExpress.XtraEditors.SimpleButton();
            this.btnNumberZero = new DevExpress.XtraEditors.SimpleButton();
            this.btnDot = new DevExpress.XtraEditors.SimpleButton();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnEnter = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtCashInUSD = new DevExpress.XtraEditors.TextEdit();
            this.cboPaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtChangeRiel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtCashInRiel = new DevExpress.XtraEditors.TextEdit();
            this.txtChangeUSD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtGrandTotalUSD = new DevExpress.XtraEditors.TextEdit();
            this.txtGrandTotalRiel = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelBackground = new DevExpress.XtraEditors.PanelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.txtDiscPercent = new DevExpress.XtraEditors.TextEdit();
            this.txtDisAmount = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtDisGrandR = new DevExpress.XtraEditors.TextEdit();
            this.txtDisGrandD = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.layoutKey.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashInUSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChangeRiel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashInRiel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChangeUSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotalUSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotalRiel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelBackground)).BeginInit();
            this.panelBackground.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscPercent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisGrandR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisGrandD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutKey
            // 
            this.layoutKey.Controls.Add(this.btnNumberOne);
            this.layoutKey.Controls.Add(this.btnNumberTwo);
            this.layoutKey.Controls.Add(this.btnNumberThree);
            this.layoutKey.Controls.Add(this.btnNumberFour);
            this.layoutKey.Controls.Add(this.btnNumberFive);
            this.layoutKey.Controls.Add(this.btnNumberSix);
            this.layoutKey.Controls.Add(this.btnNumberSeven);
            this.layoutKey.Controls.Add(this.btnNumberEight);
            this.layoutKey.Controls.Add(this.btnNumberNine);
            this.layoutKey.Controls.Add(this.btnNumberZero);
            this.layoutKey.Controls.Add(this.btnDot);
            this.layoutKey.Controls.Add(this.btnClear);
            this.layoutKey.Controls.Add(this.btnClose);
            this.layoutKey.Controls.Add(this.btnEnter);
            this.layoutKey.Location = new System.Drawing.Point(15, 405);
            this.layoutKey.Name = "layoutKey";
            this.layoutKey.Size = new System.Drawing.Size(937, 167);
            this.layoutKey.TabIndex = 0;
            // 
            // btnNumberOne
            // 
            this.btnNumberOne.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberOne.Appearance.Options.UseFont = true;
            this.btnNumberOne.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberOne.Location = new System.Drawing.Point(3, 3);
            this.btnNumberOne.Name = "btnNumberOne";
            this.btnNumberOne.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberOne.Size = new System.Drawing.Size(111, 72);
            this.btnNumberOne.TabIndex = 1;
            this.btnNumberOne.Text = "1";
            this.btnNumberOne.Click += new System.EventHandler(this.btnNumberOne_Click);
            // 
            // btnNumberTwo
            // 
            this.btnNumberTwo.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberTwo.Appearance.Options.UseFont = true;
            this.btnNumberTwo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberTwo.Location = new System.Drawing.Point(120, 3);
            this.btnNumberTwo.Name = "btnNumberTwo";
            this.btnNumberTwo.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberTwo.Size = new System.Drawing.Size(111, 72);
            this.btnNumberTwo.TabIndex = 2;
            this.btnNumberTwo.Text = "2";
            this.btnNumberTwo.Click += new System.EventHandler(this.btnNumberTwo_Click);
            // 
            // btnNumberThree
            // 
            this.btnNumberThree.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberThree.Appearance.Options.UseFont = true;
            this.btnNumberThree.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberThree.Location = new System.Drawing.Point(237, 3);
            this.btnNumberThree.Name = "btnNumberThree";
            this.btnNumberThree.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberThree.Size = new System.Drawing.Size(111, 72);
            this.btnNumberThree.TabIndex = 3;
            this.btnNumberThree.Text = "3";
            this.btnNumberThree.Click += new System.EventHandler(this.btnNumberThree_Click);
            // 
            // btnNumberFour
            // 
            this.btnNumberFour.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberFour.Appearance.Options.UseFont = true;
            this.btnNumberFour.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberFour.Location = new System.Drawing.Point(354, 3);
            this.btnNumberFour.Name = "btnNumberFour";
            this.btnNumberFour.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberFour.Size = new System.Drawing.Size(111, 72);
            this.btnNumberFour.TabIndex = 4;
            this.btnNumberFour.Text = "4";
            this.btnNumberFour.Click += new System.EventHandler(this.btnNumberFour_Click);
            // 
            // btnNumberFive
            // 
            this.btnNumberFive.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberFive.Appearance.Options.UseFont = true;
            this.btnNumberFive.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberFive.Location = new System.Drawing.Point(471, 3);
            this.btnNumberFive.Name = "btnNumberFive";
            this.btnNumberFive.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberFive.Size = new System.Drawing.Size(111, 72);
            this.btnNumberFive.TabIndex = 5;
            this.btnNumberFive.Text = "5";
            this.btnNumberFive.Click += new System.EventHandler(this.btnNumberFive_Click);
            // 
            // btnNumberSix
            // 
            this.btnNumberSix.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberSix.Appearance.Options.UseFont = true;
            this.btnNumberSix.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberSix.Location = new System.Drawing.Point(588, 3);
            this.btnNumberSix.Name = "btnNumberSix";
            this.btnNumberSix.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberSix.Size = new System.Drawing.Size(111, 72);
            this.btnNumberSix.TabIndex = 6;
            this.btnNumberSix.Text = "6";
            this.btnNumberSix.Click += new System.EventHandler(this.btnNumberSix_Click);
            // 
            // btnNumberSeven
            // 
            this.btnNumberSeven.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberSeven.Appearance.Options.UseFont = true;
            this.btnNumberSeven.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberSeven.Location = new System.Drawing.Point(705, 3);
            this.btnNumberSeven.Name = "btnNumberSeven";
            this.btnNumberSeven.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberSeven.Size = new System.Drawing.Size(111, 72);
            this.btnNumberSeven.TabIndex = 7;
            this.btnNumberSeven.Text = "7";
            this.btnNumberSeven.Click += new System.EventHandler(this.btnNumberSeven_Click);
            // 
            // btnNumberEight
            // 
            this.btnNumberEight.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberEight.Appearance.Options.UseFont = true;
            this.btnNumberEight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberEight.Location = new System.Drawing.Point(822, 3);
            this.btnNumberEight.Name = "btnNumberEight";
            this.btnNumberEight.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberEight.Size = new System.Drawing.Size(111, 72);
            this.btnNumberEight.TabIndex = 8;
            this.btnNumberEight.Text = "8";
            this.btnNumberEight.Click += new System.EventHandler(this.btnNumberEight_Click);
            // 
            // btnNumberNine
            // 
            this.btnNumberNine.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberNine.Appearance.Options.UseFont = true;
            this.btnNumberNine.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberNine.Location = new System.Drawing.Point(3, 81);
            this.btnNumberNine.Name = "btnNumberNine";
            this.btnNumberNine.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberNine.Size = new System.Drawing.Size(111, 72);
            this.btnNumberNine.TabIndex = 9;
            this.btnNumberNine.Text = "9";
            this.btnNumberNine.Click += new System.EventHandler(this.btnNumberNine_Click);
            // 
            // btnNumberZero
            // 
            this.btnNumberZero.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumberZero.Appearance.Options.UseFont = true;
            this.btnNumberZero.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNumberZero.Location = new System.Drawing.Point(120, 81);
            this.btnNumberZero.Name = "btnNumberZero";
            this.btnNumberZero.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnNumberZero.Size = new System.Drawing.Size(111, 72);
            this.btnNumberZero.TabIndex = 10;
            this.btnNumberZero.Text = "0";
            this.btnNumberZero.Click += new System.EventHandler(this.btnNumberZero_Click);
            // 
            // btnDot
            // 
            this.btnDot.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDot.Appearance.Options.UseFont = true;
            this.btnDot.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDot.Location = new System.Drawing.Point(237, 81);
            this.btnDot.Name = "btnDot";
            this.btnDot.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDot.Size = new System.Drawing.Size(111, 72);
            this.btnDot.TabIndex = 11;
            this.btnDot.Text = ".";
            this.btnDot.Click += new System.EventHandler(this.btnDot_Click);
            // 
            // btnClear
            // 
            this.btnClear.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Appearance.Options.UseFont = true;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Location = new System.Drawing.Point(354, 81);
            this.btnClear.Name = "btnClear";
            this.btnClear.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnClear.Size = new System.Drawing.Size(111, 72);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "C";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnClose
            // 
            this.btnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.ImageOptions.Image")));
            this.btnClose.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnClose.Location = new System.Drawing.Point(471, 81);
            this.btnClose.Name = "btnClose";
            this.btnClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnClose.Size = new System.Drawing.Size(111, 72);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "$C";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnEnter
            // 
            this.btnEnter.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnter.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnEnter.Appearance.Options.UseFont = true;
            this.btnEnter.Appearance.Options.UseForeColor = true;
            this.btnEnter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEnter.Location = new System.Drawing.Point(588, 81);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnEnter.Size = new System.Drawing.Size(345, 72);
            this.btnEnter.TabIndex = 0;
            this.btnEnter.Text = "&ENTER";
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.txtCashInUSD);
            this.panelControl1.Controls.Add(this.cboPaymentType);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.txtChangeRiel);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.txtCashInRiel);
            this.panelControl1.Controls.Add(this.txtChangeUSD);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Location = new System.Drawing.Point(15, 155);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(470, 227);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(7, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(123, 36);
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "CASH IN ($)";
            // 
            // txtCashInUSD
            // 
            this.txtCashInUSD.EditValue = "0";
            this.txtCashInUSD.Location = new System.Drawing.Point(136, 53);
            this.txtCashInUSD.Name = "txtCashInUSD";
            this.txtCashInUSD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtCashInUSD.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtCashInUSD.Properties.Appearance.Options.UseFont = true;
            this.txtCashInUSD.Properties.Appearance.Options.UseForeColor = true;
            this.txtCashInUSD.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtCashInUSD.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtCashInUSD.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtCashInUSD.Properties.EditFormat.FormatString = "N0";
            this.txtCashInUSD.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCashInUSD.Properties.Mask.EditMask = "N0";
            this.txtCashInUSD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCashInUSD.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCashInUSD.Properties.MaxLength = 10;
            this.txtCashInUSD.Properties.NullText = "0";
            this.txtCashInUSD.Size = new System.Drawing.Size(331, 36);
            this.txtCashInUSD.TabIndex = 0;
            this.txtCashInUSD.EditValueChanged += new System.EventHandler(this.txtCashInUSD_EditValueChanged);
            this.txtCashInUSD.TextChanged += new System.EventHandler(this.txtCashInUSD_TextChanged);
            this.txtCashInUSD.Enter += new System.EventHandler(this.txtCashInUSD_Enter);
            this.txtCashInUSD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCashInUSD_KeyDown);
            // 
            // cboPaymentType
            // 
            this.cboPaymentType.EditValue = "CASH";
            this.cboPaymentType.EnterMoveNextControl = true;
            this.cboPaymentType.Location = new System.Drawing.Point(134, 8);
            this.cboPaymentType.Name = "cboPaymentType";
            this.cboPaymentType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cboPaymentType.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 11F);
            this.cboPaymentType.Properties.Appearance.Options.UseFont = true;
            this.cboPaymentType.Properties.AppearanceDropDown.BorderColor = System.Drawing.Color.Transparent;
            this.cboPaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPaymentType.Properties.AppearanceDropDown.Options.UseBorderColor = true;
            this.cboPaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboPaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.cboPaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboPaymentType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PaymentMethodID", "PaymentMethod")});
            this.cboPaymentType.Properties.NullText = "CASH";
            this.cboPaymentType.Properties.PopupSizeable = false;
            this.cboPaymentType.Properties.ShowFooter = false;
            this.cboPaymentType.Properties.ShowHeader = false;
            this.cboPaymentType.Size = new System.Drawing.Size(328, 40);
            this.cboPaymentType.TabIndex = 0;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F);
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(10, 11);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(123, 36);
            this.labelControl8.TabIndex = 15;
            this.labelControl8.Text = "Payment Type:";
            // 
            // txtChangeRiel
            // 
            this.txtChangeRiel.EditValue = "0";
            this.txtChangeRiel.Location = new System.Drawing.Point(136, 175);
            this.txtChangeRiel.Name = "txtChangeRiel";
            this.txtChangeRiel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtChangeRiel.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtChangeRiel.Properties.Appearance.Options.UseFont = true;
            this.txtChangeRiel.Properties.Appearance.Options.UseForeColor = true;
            this.txtChangeRiel.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtChangeRiel.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtChangeRiel.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtChangeRiel.Properties.EditFormat.FormatString = "N0";
            this.txtChangeRiel.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtChangeRiel.Properties.Mask.EditMask = "N0";
            this.txtChangeRiel.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChangeRiel.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtChangeRiel.Properties.NullText = "0";
            this.txtChangeRiel.Properties.ReadOnly = true;
            this.txtChangeRiel.Size = new System.Drawing.Size(331, 36);
            this.txtChangeRiel.TabIndex = 4;
            this.txtChangeRiel.Enter += new System.EventHandler(this.txtChangeRiel_Enter);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F);
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(7, 93);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(123, 34);
            this.labelControl3.TabIndex = 16;
            this.labelControl3.Text = "CASH IN (៛)";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(7, 173);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(123, 36);
            this.labelControl5.TabIndex = 19;
            this.labelControl5.Text = "CHANGE (៛)";
            // 
            // txtCashInRiel
            // 
            this.txtCashInRiel.EditValue = "0";
            this.txtCashInRiel.Location = new System.Drawing.Point(136, 95);
            this.txtCashInRiel.Name = "txtCashInRiel";
            this.txtCashInRiel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtCashInRiel.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtCashInRiel.Properties.Appearance.Options.UseFont = true;
            this.txtCashInRiel.Properties.Appearance.Options.UseForeColor = true;
            this.txtCashInRiel.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtCashInRiel.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtCashInRiel.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtCashInRiel.Properties.EditFormat.FormatString = "N0";
            this.txtCashInRiel.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCashInRiel.Properties.Mask.EditMask = "N0";
            this.txtCashInRiel.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCashInRiel.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtCashInRiel.Properties.MaxLength = 10;
            this.txtCashInRiel.Properties.NullText = "0";
            this.txtCashInRiel.Size = new System.Drawing.Size(331, 36);
            this.txtCashInRiel.TabIndex = 2;
            this.txtCashInRiel.TextChanged += new System.EventHandler(this.txtCashInRiel_TextChanged);
            this.txtCashInRiel.Enter += new System.EventHandler(this.txtCashInRiel_Enter);
            this.txtCashInRiel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCashInRiel_KeyDown);
            // 
            // txtChangeUSD
            // 
            this.txtChangeUSD.EditValue = "0";
            this.txtChangeUSD.Location = new System.Drawing.Point(136, 135);
            this.txtChangeUSD.Name = "txtChangeUSD";
            this.txtChangeUSD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtChangeUSD.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtChangeUSD.Properties.Appearance.Options.UseFont = true;
            this.txtChangeUSD.Properties.Appearance.Options.UseForeColor = true;
            this.txtChangeUSD.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtChangeUSD.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtChangeUSD.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtChangeUSD.Properties.EditFormat.FormatString = "N0";
            this.txtChangeUSD.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtChangeUSD.Properties.Mask.EditMask = "f";
            this.txtChangeUSD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtChangeUSD.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtChangeUSD.Properties.NullText = "0";
            this.txtChangeUSD.Properties.ReadOnly = true;
            this.txtChangeUSD.Size = new System.Drawing.Size(331, 36);
            this.txtChangeUSD.TabIndex = 3;
            this.txtChangeUSD.Enter += new System.EventHandler(this.txtChangeUSD_Enter);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(7, 133);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(123, 36);
            this.labelControl4.TabIndex = 17;
            this.labelControl4.Text = "CHANGE ($)";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F);
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(10, 43);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(123, 36);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Grand Total (៛)";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F);
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(10, 3);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(123, 36);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Grand Total ($)";
            // 
            // txtGrandTotalUSD
            // 
            this.txtGrandTotalUSD.EditValue = "0";
            this.txtGrandTotalUSD.Location = new System.Drawing.Point(139, 5);
            this.txtGrandTotalUSD.Name = "txtGrandTotalUSD";
            this.txtGrandTotalUSD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtGrandTotalUSD.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtGrandTotalUSD.Properties.Appearance.Options.UseFont = true;
            this.txtGrandTotalUSD.Properties.Appearance.Options.UseForeColor = true;
            this.txtGrandTotalUSD.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtGrandTotalUSD.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtGrandTotalUSD.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtGrandTotalUSD.Properties.EditFormat.FormatString = "N0";
            this.txtGrandTotalUSD.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGrandTotalUSD.Properties.Mask.EditMask = "f";
            this.txtGrandTotalUSD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGrandTotalUSD.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGrandTotalUSD.Properties.NullText = "0";
            this.txtGrandTotalUSD.Properties.ReadOnly = true;
            this.txtGrandTotalUSD.Properties.ValidateOnEnterKey = true;
            this.txtGrandTotalUSD.Size = new System.Drawing.Size(323, 36);
            this.txtGrandTotalUSD.TabIndex = 0;
            this.txtGrandTotalUSD.Enter += new System.EventHandler(this.txtChangeUSD_Enter);
            // 
            // txtGrandTotalRiel
            // 
            this.txtGrandTotalRiel.EditValue = "0";
            this.txtGrandTotalRiel.Location = new System.Drawing.Point(139, 47);
            this.txtGrandTotalRiel.Name = "txtGrandTotalRiel";
            this.txtGrandTotalRiel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtGrandTotalRiel.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtGrandTotalRiel.Properties.Appearance.Options.UseFont = true;
            this.txtGrandTotalRiel.Properties.Appearance.Options.UseForeColor = true;
            this.txtGrandTotalRiel.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtGrandTotalRiel.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtGrandTotalRiel.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtGrandTotalRiel.Properties.EditFormat.FormatString = "N0";
            this.txtGrandTotalRiel.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtGrandTotalRiel.Properties.Mask.EditMask = "N0";
            this.txtGrandTotalRiel.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtGrandTotalRiel.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtGrandTotalRiel.Properties.NullText = "0";
            this.txtGrandTotalRiel.Properties.ReadOnly = true;
            this.txtGrandTotalRiel.Size = new System.Drawing.Size(323, 36);
            this.txtGrandTotalRiel.TabIndex = 1;
            this.txtGrandTotalRiel.EditValueChanged += new System.EventHandler(this.txtGrandTotalRiel_EditValueChanged);
            this.txtGrandTotalRiel.Enter += new System.EventHandler(this.txtChangeUSD_Enter);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Stencil", 20.25F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Green;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl1.LineVisible = true;
            this.labelControl1.Location = new System.Drawing.Point(2, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(968, 43);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "INVOICE PAYMENT";
            // 
            // panelBackground
            // 
            this.panelBackground.Controls.Add(this.labelControl13);
            this.panelBackground.Controls.Add(this.panelControl4);
            this.panelBackground.Controls.Add(this.panelControl2);
            this.panelBackground.Controls.Add(this.panelControl3);
            this.panelBackground.Controls.Add(this.panelControl1);
            this.panelBackground.Controls.Add(this.labelControl1);
            this.panelBackground.Controls.Add(this.layoutKey);
            this.panelBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBackground.Location = new System.Drawing.Point(0, 0);
            this.panelBackground.Name = "panelBackground";
            this.panelBackground.Size = new System.Drawing.Size(972, 585);
            this.panelBackground.TabIndex = 2;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.LineVisible = true;
            this.labelControl13.Location = new System.Drawing.Point(515, 89);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(118, 39);
            this.labelControl13.TabIndex = 20;
            this.labelControl13.Text = "Discount";
            this.labelControl13.UseMnemonic = false;
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Controls.Add(this.txtDiscPercent);
            this.panelControl4.Controls.Add(this.txtDisAmount);
            this.panelControl4.Controls.Add(this.labelControl11);
            this.panelControl4.Controls.Add(this.labelControl12);
            this.panelControl4.Location = new System.Drawing.Point(493, 157);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(474, 87);
            this.panelControl4.TabIndex = 19;
            // 
            // txtDiscPercent
            // 
            this.txtDiscPercent.EditValue = "0";
            this.txtDiscPercent.Location = new System.Drawing.Point(168, 40);
            this.txtDiscPercent.Name = "txtDiscPercent";
            this.txtDiscPercent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtDiscPercent.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtDiscPercent.Properties.Appearance.Options.UseFont = true;
            this.txtDiscPercent.Properties.Appearance.Options.UseForeColor = true;
            this.txtDiscPercent.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtDiscPercent.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtDiscPercent.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtDiscPercent.Properties.EditFormat.FormatString = "N0";
            this.txtDiscPercent.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDiscPercent.Properties.Mask.EditMask = "N0";
            this.txtDiscPercent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDiscPercent.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDiscPercent.Properties.MaxLength = 3;
            this.txtDiscPercent.Properties.NullText = "0";
            this.txtDiscPercent.Size = new System.Drawing.Size(291, 36);
            this.txtDiscPercent.TabIndex = 1;
            this.txtDiscPercent.EditValueChanged += new System.EventHandler(this.txtDiscPercent_EditValueChanged);
            // 
            // txtDisAmount
            // 
            this.txtDisAmount.EditValue = "0";
            this.txtDisAmount.Location = new System.Drawing.Point(168, 3);
            this.txtDisAmount.Name = "txtDisAmount";
            this.txtDisAmount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtDisAmount.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtDisAmount.Properties.Appearance.Options.UseFont = true;
            this.txtDisAmount.Properties.Appearance.Options.UseForeColor = true;
            this.txtDisAmount.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtDisAmount.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtDisAmount.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtDisAmount.Properties.DisplayFormat.FormatString = "n2";
            this.txtDisAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDisAmount.Properties.EditFormat.FormatString = "N2";
            this.txtDisAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDisAmount.Properties.Mask.EditMask = "n2";
            this.txtDisAmount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDisAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDisAmount.Properties.MaxLength = 10;
            this.txtDisAmount.Properties.NullText = "0";
            this.txtDisAmount.Size = new System.Drawing.Size(291, 36);
            this.txtDisAmount.TabIndex = 0;
            this.txtDisAmount.EditValueChanged += new System.EventHandler(this.txtDisAmount_EditValueChanged);
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F);
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Location = new System.Drawing.Point(10, 43);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(161, 36);
            this.labelControl11.TabIndex = 15;
            this.labelControl11.Text = "Discount Percent %";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F);
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Location = new System.Drawing.Point(8, 5);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(161, 36);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "Discount Total ($)";
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.txtDisGrandR);
            this.panelControl2.Controls.Add(this.txtDisGrandD);
            this.panelControl2.Controls.Add(this.labelControl9);
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Location = new System.Drawing.Point(493, 295);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(474, 87);
            this.panelControl2.TabIndex = 18;
            // 
            // txtDisGrandR
            // 
            this.txtDisGrandR.EditValue = "0";
            this.txtDisGrandR.Enabled = false;
            this.txtDisGrandR.Location = new System.Drawing.Point(168, 40);
            this.txtDisGrandR.Name = "txtDisGrandR";
            this.txtDisGrandR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtDisGrandR.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtDisGrandR.Properties.Appearance.Options.UseFont = true;
            this.txtDisGrandR.Properties.Appearance.Options.UseForeColor = true;
            this.txtDisGrandR.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtDisGrandR.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtDisGrandR.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtDisGrandR.Properties.EditFormat.FormatString = "N0";
            this.txtDisGrandR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDisGrandR.Properties.Mask.EditMask = "N0";
            this.txtDisGrandR.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDisGrandR.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDisGrandR.Properties.NullText = "0";
            this.txtDisGrandR.Size = new System.Drawing.Size(291, 36);
            this.txtDisGrandR.TabIndex = 19;
            // 
            // txtDisGrandD
            // 
            this.txtDisGrandD.EditValue = "0";
            this.txtDisGrandD.Enabled = false;
            this.txtDisGrandD.Location = new System.Drawing.Point(168, 3);
            this.txtDisGrandD.Name = "txtDisGrandD";
            this.txtDisGrandD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 17F);
            this.txtDisGrandD.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.txtDisGrandD.Properties.Appearance.Options.UseFont = true;
            this.txtDisGrandD.Properties.Appearance.Options.UseForeColor = true;
            this.txtDisGrandD.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Orange;
            this.txtDisGrandD.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtDisGrandD.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtDisGrandD.Properties.EditFormat.FormatString = "N0";
            this.txtDisGrandD.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtDisGrandD.Properties.Mask.EditMask = "f";
            this.txtDisGrandD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtDisGrandD.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txtDisGrandD.Properties.NullText = "0";
            this.txtDisGrandD.Size = new System.Drawing.Size(291, 36);
            this.txtDisGrandD.TabIndex = 19;
            this.txtDisGrandD.TextChanged += new System.EventHandler(this.txtDisGrandD_TextChanged);
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F, System.Drawing.FontStyle.Bold);
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Location = new System.Drawing.Point(3, 40);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(161, 36);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "Total Payment (៛)";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Khmer OS Freehand", 13F, System.Drawing.FontStyle.Bold);
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Location = new System.Drawing.Point(3, 3);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(161, 36);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "Total Payment ($)";
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.labelControl7);
            this.panelControl3.Controls.Add(this.txtGrandTotalUSD);
            this.panelControl3.Controls.Add(this.labelControl6);
            this.panelControl3.Controls.Add(this.txtGrandTotalRiel);
            this.panelControl3.Location = new System.Drawing.Point(15, 51);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(470, 87);
            this.panelControl3.TabIndex = 17;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(0, 0);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // FrmPayBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 585);
            this.Controls.Add(this.panelBackground);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPayBill";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Invoice Payment";
            this.Load += new System.EventHandler(this.FrmPayment_Load);
            this.layoutKey.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCashInUSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChangeRiel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCashInRiel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtChangeUSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotalUSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGrandTotalRiel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelBackground)).EndInit();
            this.panelBackground.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscPercent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDisGrandR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisGrandD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel layoutKey;
        private DevExpress.XtraEditors.SimpleButton btnNumberOne;
        private DevExpress.XtraEditors.SimpleButton btnNumberTwo;
        private DevExpress.XtraEditors.SimpleButton btnNumberThree;
        private DevExpress.XtraEditors.SimpleButton btnNumberFour;
        private DevExpress.XtraEditors.SimpleButton btnNumberFive;
        private DevExpress.XtraEditors.SimpleButton btnNumberSix;
        private DevExpress.XtraEditors.SimpleButton btnNumberSeven;
        private DevExpress.XtraEditors.SimpleButton btnNumberEight;
        private DevExpress.XtraEditors.SimpleButton btnNumberNine;
        private DevExpress.XtraEditors.SimpleButton btnNumberZero;
        private DevExpress.XtraEditors.SimpleButton btnDot;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnEnter;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelBackground;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        internal DevExpress.XtraEditors.TextEdit txtCashInUSD;
        internal DevExpress.XtraEditors.TextEdit txtCashInRiel;
        internal DevExpress.XtraEditors.TextEdit txtChangeUSD;
        internal DevExpress.XtraEditors.TextEdit txtChangeRiel;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        internal DevExpress.XtraEditors.TextEdit txtGrandTotalRiel;
        public DevExpress.XtraEditors.LookUpEdit cboPaymentType;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        public DevExpress.XtraEditors.TextEdit txtGrandTotalUSD;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        internal DevExpress.XtraEditors.TextEdit txtDisGrandR;
        internal DevExpress.XtraEditors.TextEdit txtDisGrandD;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        internal DevExpress.XtraEditors.TextEdit txtDiscPercent;
        internal DevExpress.XtraEditors.TextEdit txtDisAmount;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private DevExpress.XtraEditors.LabelControl labelControl13;
    }
}