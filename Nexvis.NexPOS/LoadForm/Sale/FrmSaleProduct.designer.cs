﻿namespace DgoStore.LoadForm.Sale
{
    partial class scrSaleOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(scrSaleOrder));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement20 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement21 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement22 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement23 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement24 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement25 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement26 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement27 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement28 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement29 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement30 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            this.repositoryTransaction = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelHeader = new DevExpress.XtraEditors.PanelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.cboprint = new DevExpress.XtraEditors.ComboBoxEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnAction = new DevExpress.XtraBars.BarButtonItem();
            this.popMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.ppmUnPickupItem = new DevExpress.XtraBars.BarButtonItem();
            this.ppmEditQuantity = new DevExpress.XtraBars.BarButtonItem();
            this.ppmAction = new DevExpress.XtraBars.BarLinkContainerItem();
            this.ppmNew = new DevExpress.XtraBars.BarButtonItem();
            this.ppmEdit = new DevExpress.XtraBars.BarButtonItem();
            this.ppmDelete = new DevExpress.XtraBars.BarButtonItem();
            this.ppmViewLog = new DevExpress.XtraBars.BarLinkContainerItem();
            this.ppmLogOn = new DevExpress.XtraBars.BarButtonItem();
            this.ppmLogOut = new DevExpress.XtraBars.BarButtonItem();
            this.ppmViewStatus = new DevExpress.XtraBars.BarLinkContainerItem();
            this.ppmActiveView = new DevExpress.XtraBars.BarButtonItem();
            this.ppResetPassword = new DevExpress.XtraBars.BarButtonItem();
            this.ppPermission = new DevExpress.XtraBars.BarButtonItem();
            this.ppmAdvanceFilter = new DevExpress.XtraBars.BarButtonItem();
            this.ppmExport = new DevExpress.XtraBars.BarButtonItem();
            this.ppmViewAllLog = new DevExpress.XtraBars.BarButtonItem();
            this.ppmViewAllStatus = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.ppmInactiveView = new DevExpress.XtraBars.BarButtonItem();
            this.ppmActive = new DevExpress.XtraBars.BarButtonItem();
            this.ppmInactive = new DevExpress.XtraBars.BarButtonItem();
            this.ppmDesignReport = new DevExpress.XtraBars.BarButtonItem();
            this.btnClearDesignReport = new DevExpress.XtraBars.BarButtonItem();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.lblCurrentDateTime = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelFilter = new DevExpress.XtraEditors.PanelControl();
            this.picFilter = new DevExpress.XtraEditors.PictureEdit();
            this.panelProduct = new DevExpress.XtraEditors.PanelControl();
            this.panelSearch = new DevExpress.XtraEditors.PanelControl();
            this.picClear = new DevExpress.XtraEditors.PictureEdit();
            this.txtSearch = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.screenProduct = new DevExpress.XtraEditors.TileControl();
            this.tileGroupProductList = new DevExpress.XtraEditors.TileGroup();
            this.tileBarCategory = new DevExpress.XtraBars.Navigation.TileBar();
            this.tileGroupCategory = new DevExpress.XtraBars.Navigation.TileBarGroup();
            this.tmCurrentDateTime = new System.Windows.Forms.Timer(this.components);
            this.tileItem9 = new DevExpress.XtraEditors.TileItem();
            this.tileItem10 = new DevExpress.XtraEditors.TileItem();
            this.tileItem11 = new DevExpress.XtraEditors.TileItem();
            this.tileBarGroup1 = new DevExpress.XtraBars.Navigation.TileBarGroup();
            this.tileBarGroup2 = new DevExpress.XtraBars.Navigation.TileBarGroup();
            this.tileBarGroup3 = new DevExpress.XtraBars.Navigation.TileBarGroup();
            this.toolTipProduct = new DevExpress.Utils.ToolTipController(this.components);
            this.tileNavCategory1 = new DevExpress.XtraBars.Navigation.TileNavCategory();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lblIssuedDate = new DevExpress.XtraEditors.LabelControl();
            this.lblIssuedTime = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblInvoiceNo = new DevExpress.XtraEditors.LabelControl();
            this.lblCustomer = new DevExpress.XtraEditors.LabelControl();
            this.txtBarcode = new DevExpress.XtraEditors.TextEdit();
            this.GC = new DevExpress.XtraGrid.GridControl();
            this.Gv = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemDescriptionEn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Quantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DisSalePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.luInvoice = new DevExpress.XtraEditors.LookUpEdit();
            this.luCustomer = new DevExpress.XtraEditors.LookUpEdit();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.panelCustomer = new DevExpress.XtraEditors.PanelControl();
            this.lbContact = new DevExpress.XtraEditors.LabelControl();
            this.lblLocation = new DevExpress.XtraEditors.LabelControl();
            this.sidePanel1 = new DevExpress.XtraEditors.SidePanel();
            this.panelButton = new DevExpress.XtraEditors.PanelControl();
            this.btnListInvoice = new DevExpress.XtraEditors.SimpleButton();
            this.btnReturn = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnPayBill = new DevExpress.XtraEditors.SimpleButton();
            this.lblChangeRiel = new DevExpress.XtraEditors.LabelControl();
            this.lblUSDExchangeRate = new DevExpress.XtraEditors.LabelControl();
            this.lblCashInRiel = new DevExpress.XtraEditors.LabelControl();
            this.lblDiscountAmount = new DevExpress.XtraEditors.LabelControl();
            this.lblVATAmount = new DevExpress.XtraEditors.LabelControl();
            this.lblChangeUSD = new DevExpress.XtraEditors.LabelControl();
            this.lblCashInUSD = new DevExpress.XtraEditors.LabelControl();
            this.lblTotalAmount = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lbShop = new DevExpress.XtraEditors.LabelControl();
            this.lblCashier = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.sidePanel2 = new DevExpress.XtraEditors.SidePanel();
            this.btnDeleteBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.luBrowse = new DevExpress.XtraEditors.LookUpEdit();
            this.btnSkip = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrintNow = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransaction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelHeader)).BeginInit();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboprint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).BeginInit();
            this.panelFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelProduct)).BeginInit();
            this.panelProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSearch)).BeginInit();
            this.panelSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picClear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCustomer)).BeginInit();
            this.panelCustomer.SuspendLayout();
            this.sidePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelButton)).BeginInit();
            this.panelButton.SuspendLayout();
            this.sidePanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.luBrowse.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryTransaction
            // 
            this.repositoryTransaction.AutoHeight = false;
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            editorButtonImageOptions4.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions4.Image")));
            this.repositoryTransaction.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryTransaction.Name = "repositoryTransaction";
            this.repositoryTransaction.NullText = "";
            this.repositoryTransaction.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryTransaction.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryTransaction_ButtonClick);
            // 
            // panelHeader
            // 
            this.panelHeader.Appearance.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.panelHeader.Appearance.Options.UseBackColor = true;
            this.panelHeader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelHeader.Controls.Add(this.btnPrintNow);
            this.panelHeader.Controls.Add(this.labelControl9);
            this.panelHeader.Controls.Add(this.cboprint);
            this.panelHeader.Controls.Add(this.picLogo);
            this.panelHeader.Controls.Add(this.lblCurrentDateTime);
            this.panelHeader.Controls.Add(this.labelControl1);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1470, 90);
            this.panelHeader.TabIndex = 3;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Appearance.Options.UseForeColor = true;
            this.labelControl9.Location = new System.Drawing.Point(1200, 42);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(118, 21);
            this.labelControl9.TabIndex = 31;
            this.labelControl9.Text = "Print paper size : ";
            // 
            // cboprint
            // 
            this.cboprint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboprint.EditValue = "Print Receipt";
            this.cboprint.Location = new System.Drawing.Point(1324, 39);
            this.cboprint.MenuManager = this.barManager1;
            this.cboprint.Name = "cboprint";
            this.cboprint.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cboprint.Properties.Appearance.Options.UseFont = true;
            this.cboprint.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cboprint.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboprint.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboprint.Properties.Items.AddRange(new object[] {
            "Print Receipt",
            "Print A3",
            "Print A4",
            "Print A5"});
            this.cboprint.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboprint.Size = new System.Drawing.Size(138, 28);
            this.cboprint.TabIndex = 30;
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnAction,
            this.ppmAction,
            this.ppmNew,
            this.ppmEdit,
            this.ppmDelete,
            this.ppmViewLog,
            this.ppmLogOn,
            this.ppmLogOut,
            this.ppmViewStatus,
            this.ppmActiveView,
            this.ppResetPassword,
            this.ppPermission,
            this.ppmAdvanceFilter,
            this.ppmExport,
            this.ppmUnPickupItem,
            this.ppmViewAllLog,
            this.ppmViewAllStatus,
            this.barSubItem1,
            this.barButtonItem2,
            this.barSubItem2,
            this.barSubItem3,
            this.ppmActive,
            this.ppmInactive,
            this.ppmInactiveView,
            this.ppmDesignReport,
            this.btnClearDesignReport,
            this.ppmEditQuantity});
            this.barManager1.MaxItemId = 31;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1470, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 772);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1470, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 772);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1470, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 772);
            // 
            // btnAction
            // 
            this.btnAction.Caption = "&Action";
            this.btnAction.DropDownControl = this.popMenu;
            this.btnAction.Id = 0;
            this.btnAction.Name = "btnAction";
            // 
            // popMenu
            // 
            this.popMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ppmUnPickupItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.ppmEditQuantity)});
            this.popMenu.Manager = this.barManager1;
            this.popMenu.Name = "popMenu";
            // 
            // ppmUnPickupItem
            // 
            this.ppmUnPickupItem.Caption = "&Unpick up item";
            this.ppmUnPickupItem.Id = 17;
            this.ppmUnPickupItem.ImageOptions.ImageIndex = 5;
            this.ppmUnPickupItem.Name = "ppmUnPickupItem";
            this.ppmUnPickupItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ppmUnPickupItem_ItemClick);
            // 
            // ppmEditQuantity
            // 
            this.ppmEditQuantity.Caption = "Edit Quantity";
            this.ppmEditQuantity.Id = 30;
            this.ppmEditQuantity.Name = "ppmEditQuantity";
            this.ppmEditQuantity.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ppmEditQuantity_ItemClick);
            // 
            // ppmAction
            // 
            this.ppmAction.Caption = "&Action";
            this.ppmAction.Id = 3;
            this.ppmAction.Name = "ppmAction";
            // 
            // ppmNew
            // 
            this.ppmNew.Caption = "&New";
            this.ppmNew.Id = 4;
            this.ppmNew.ImageOptions.ImageIndex = 0;
            this.ppmNew.Name = "ppmNew";
            // 
            // ppmEdit
            // 
            this.ppmEdit.Caption = "&Edit";
            this.ppmEdit.Id = 5;
            this.ppmEdit.ImageOptions.ImageIndex = 2;
            this.ppmEdit.Name = "ppmEdit";
            // 
            // ppmDelete
            // 
            this.ppmDelete.Caption = "&Delete";
            this.ppmDelete.Id = 6;
            this.ppmDelete.ImageOptions.ImageIndex = 6;
            this.ppmDelete.Name = "ppmDelete";
            // 
            // ppmViewLog
            // 
            this.ppmViewLog.Caption = "View &Log";
            this.ppmViewLog.Id = 7;
            this.ppmViewLog.Name = "ppmViewLog";
            // 
            // ppmLogOn
            // 
            this.ppmLogOn.Caption = "Log &On";
            this.ppmLogOn.Id = 8;
            this.ppmLogOn.ImageOptions.ImageIndex = 12;
            this.ppmLogOn.Name = "ppmLogOn";
            // 
            // ppmLogOut
            // 
            this.ppmLogOut.Caption = "Log O&ut";
            this.ppmLogOut.Id = 9;
            this.ppmLogOut.ImageOptions.ImageIndex = 13;
            this.ppmLogOut.Name = "ppmLogOut";
            // 
            // ppmViewStatus
            // 
            this.ppmViewStatus.Caption = "View &Status";
            this.ppmViewStatus.Id = 10;
            this.ppmViewStatus.Name = "ppmViewStatus";
            // 
            // ppmActiveView
            // 
            this.ppmActiveView.Caption = "&Active";
            this.ppmActiveView.Id = 11;
            this.ppmActiveView.ImageOptions.ImageIndex = 12;
            this.ppmActiveView.Name = "ppmActiveView";
            // 
            // ppResetPassword
            // 
            this.ppResetPassword.Caption = "&Reset Password";
            this.ppResetPassword.Id = 13;
            this.ppResetPassword.ImageOptions.ImageIndex = 1;
            this.ppResetPassword.Name = "ppResetPassword";
            // 
            // ppPermission
            // 
            this.ppPermission.Caption = "&Permission";
            this.ppPermission.Id = 14;
            this.ppPermission.ImageOptions.ImageIndex = 7;
            this.ppPermission.Name = "ppPermission";
            // 
            // ppmAdvanceFilter
            // 
            this.ppmAdvanceFilter.Caption = "Advance &Filter";
            this.ppmAdvanceFilter.Id = 15;
            this.ppmAdvanceFilter.ImageOptions.ImageIndex = 3;
            this.ppmAdvanceFilter.Name = "ppmAdvanceFilter";
            // 
            // ppmExport
            // 
            this.ppmExport.Caption = "&Export";
            this.ppmExport.Id = 16;
            this.ppmExport.ImageOptions.ImageIndex = 4;
            this.ppmExport.Name = "ppmExport";
            // 
            // ppmViewAllLog
            // 
            this.ppmViewAllLog.Caption = "&View All";
            this.ppmViewAllLog.Id = 18;
            this.ppmViewAllLog.ImageOptions.ImageIndex = 14;
            this.ppmViewAllLog.Name = "ppmViewAllLog";
            // 
            // ppmViewAllStatus
            // 
            this.ppmViewAllStatus.Caption = "&View All";
            this.ppmViewAllStatus.Id = 19;
            this.ppmViewAllStatus.ImageOptions.ImageIndex = 14;
            this.ppmViewAllStatus.Name = "ppmViewAllStatus";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "&Action";
            this.barSubItem1.Id = 21;
            this.barSubItem1.ImageOptions.ImageIndex = 11;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Test";
            this.barButtonItem2.Id = 22;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "&View Log";
            this.barSubItem2.Id = 23;
            this.barSubItem2.ImageOptions.ImageIndex = 10;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ppmViewAllLog),
            new DevExpress.XtraBars.LinkPersistInfo(this.ppmLogOn),
            new DevExpress.XtraBars.LinkPersistInfo(this.ppmLogOut)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "&View Status";
            this.barSubItem3.Id = 24;
            this.barSubItem3.ImageOptions.ImageIndex = 10;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ppmViewAllStatus),
            new DevExpress.XtraBars.LinkPersistInfo(this.ppmActiveView),
            new DevExpress.XtraBars.LinkPersistInfo(this.ppmInactiveView)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // ppmInactiveView
            // 
            this.ppmInactiveView.Caption = "&Inactive";
            this.ppmInactiveView.Id = 27;
            this.ppmInactiveView.ImageOptions.ImageIndex = 16;
            this.ppmInactiveView.Name = "ppmInactiveView";
            // 
            // ppmActive
            // 
            this.ppmActive.Caption = "&Active";
            this.ppmActive.Id = 25;
            this.ppmActive.ImageOptions.ImageIndex = 17;
            this.ppmActive.Name = "ppmActive";
            // 
            // ppmInactive
            // 
            this.ppmInactive.Caption = "&Inactive";
            this.ppmInactive.Id = 26;
            this.ppmInactive.ImageOptions.ImageIndex = 13;
            this.ppmInactive.Name = "ppmInactive";
            // 
            // ppmDesignReport
            // 
            this.ppmDesignReport.Caption = "&Design Report";
            this.ppmDesignReport.Id = 28;
            this.ppmDesignReport.ImageOptions.ImageIndex = 18;
            this.ppmDesignReport.Name = "ppmDesignReport";
            // 
            // btnClearDesignReport
            // 
            this.btnClearDesignReport.Caption = "&Clear Design Report";
            this.btnClearDesignReport.Id = 29;
            this.btnClearDesignReport.ImageOptions.ImageIndex = 19;
            this.btnClearDesignReport.Name = "btnClearDesignReport";
            // 
            // picLogo
            // 
            this.picLogo.BackColor = System.Drawing.Color.Transparent;
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(12, 3);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(156, 81);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 2;
            this.picLogo.TabStop = false;
            // 
            // lblCurrentDateTime
            // 
            this.lblCurrentDateTime.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentDateTime.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblCurrentDateTime.Appearance.Options.UseFont = true;
            this.lblCurrentDateTime.Appearance.Options.UseForeColor = true;
            this.lblCurrentDateTime.Location = new System.Drawing.Point(189, 48);
            this.lblCurrentDateTime.Name = "lblCurrentDateTime";
            this.lblCurrentDateTime.Size = new System.Drawing.Size(183, 19);
            this.lblCurrentDateTime.TabIndex = 1;
            this.lblCurrentDateTime.Text = "22-02-2017 09:50:18 AM";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(189, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(130, 19);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "ADMINISTRATOR";
            // 
            // panelFilter
            // 
            this.panelFilter.Controls.Add(this.picFilter);
            this.panelFilter.Location = new System.Drawing.Point(16, 139);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(40, 36);
            this.panelFilter.TabIndex = 9;
            // 
            // picFilter
            // 
            this.picFilter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picFilter.EditValue = ((object)(resources.GetObject("picFilter.EditValue")));
            this.picFilter.Location = new System.Drawing.Point(5, 5);
            this.picFilter.Name = "picFilter";
            this.picFilter.Properties.AllowFocused = false;
            this.picFilter.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picFilter.Properties.Appearance.Options.UseBackColor = true;
            this.picFilter.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picFilter.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picFilter.Size = new System.Drawing.Size(30, 26);
            this.picFilter.TabIndex = 2;
            this.picFilter.Click += new System.EventHandler(this.picFilter_Click);
            // 
            // panelProduct
            // 
            this.panelProduct.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelProduct.Controls.Add(this.panelSearch);
            this.panelProduct.Controls.Add(this.panelFilter);
            this.panelProduct.Controls.Add(this.labelControl5);
            this.panelProduct.Controls.Add(this.labelControl4);
            this.panelProduct.Controls.Add(this.screenProduct);
            this.panelProduct.Controls.Add(this.tileBarCategory);
            this.panelProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelProduct.Location = new System.Drawing.Point(0, 90);
            this.panelProduct.Name = "panelProduct";
            this.panelProduct.Size = new System.Drawing.Size(875, 682);
            this.panelProduct.TabIndex = 4;
            // 
            // panelSearch
            // 
            this.panelSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSearch.Controls.Add(this.picClear);
            this.panelSearch.Controls.Add(this.txtSearch);
            this.panelSearch.Location = new System.Drawing.Point(57, 139);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(812, 36);
            this.panelSearch.TabIndex = 8;
            // 
            // picClear
            // 
            this.picClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picClear.EditValue = ((object)(resources.GetObject("picClear.EditValue")));
            this.picClear.Location = new System.Drawing.Point(761, 5);
            this.picClear.Name = "picClear";
            this.picClear.Properties.AllowFocused = false;
            this.picClear.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picClear.Properties.Appearance.Options.UseBackColor = true;
            this.picClear.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.picClear.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picClear.Size = new System.Drawing.Size(30, 26);
            this.picClear.TabIndex = 1;
            this.picClear.Click += new System.EventHandler(this.picClear_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(5, 5);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Properties.Appearance.Options.UseFont = true;
            this.txtSearch.Properties.NullText = "SEARCH ...";
            this.txtSearch.Size = new System.Drawing.Size(746, 26);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.EditValueChanged += new System.EventHandler(this.txtSearch_EditValueChanged);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.LineVisible = true;
            this.labelControl5.Location = new System.Drawing.Point(12, 120);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(857, 16);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "PRODUCTS";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.LineVisible = true;
            this.labelControl4.Location = new System.Drawing.Point(12, 11);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(857, 16);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "CATEGORIES";
            // 
            // screenProduct
            // 
            this.screenProduct.AllowItemHover = true;
            this.screenProduct.AppearanceItem.Hovered.BackColor = System.Drawing.Color.LightCyan;
            this.screenProduct.AppearanceItem.Hovered.BorderColor = System.Drawing.Color.LightCyan;
            this.screenProduct.AppearanceItem.Hovered.Options.UseBackColor = true;
            this.screenProduct.AppearanceItem.Hovered.Options.UseBorderColor = true;
            this.screenProduct.AppearanceText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.screenProduct.AppearanceText.Options.UseFont = true;
            this.screenProduct.Cursor = System.Windows.Forms.Cursors.Default;
            this.screenProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.screenProduct.Groups.Add(this.tileGroupProductList);
            this.screenProduct.Location = new System.Drawing.Point(0, 100);
            this.screenProduct.MaxId = 13;
            this.screenProduct.Name = "screenProduct";
            this.screenProduct.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.screenProduct.Padding = new System.Windows.Forms.Padding(18, 48, 18, 18);
            this.screenProduct.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons;
            this.screenProduct.ShowText = true;
            this.screenProduct.Size = new System.Drawing.Size(875, 582);
            this.screenProduct.TabIndex = 6;
            this.screenProduct.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.screenProduct_ItemClick);
            this.screenProduct.MouseMove += new System.Windows.Forms.MouseEventHandler(this.screenProduct_MouseMove);
            // 
            // tileGroupProductList
            // 
            this.tileGroupProductList.Name = "tileGroupProductList";
            this.tileGroupProductList.Text = "PRODUCT";
            // 
            // tileBarCategory
            // 
            this.tileBarCategory.AllowGroupHighlighting = true;
            this.tileBarCategory.AppearanceItem.Hovered.BackColor = System.Drawing.Color.Gray;
            this.tileBarCategory.AppearanceItem.Hovered.BorderColor = System.Drawing.Color.Gray;
            this.tileBarCategory.AppearanceItem.Hovered.Options.UseBackColor = true;
            this.tileBarCategory.AppearanceItem.Hovered.Options.UseBorderColor = true;
            this.tileBarCategory.AppearanceItem.Normal.BackColor = System.Drawing.Color.DimGray;
            this.tileBarCategory.AppearanceItem.Normal.BorderColor = System.Drawing.Color.DimGray;
            this.tileBarCategory.AppearanceItem.Normal.ForeColor = System.Drawing.Color.White;
            this.tileBarCategory.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileBarCategory.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tileBarCategory.AppearanceItem.Normal.Options.UseForeColor = true;
            this.tileBarCategory.AppearanceText.Font = new System.Drawing.Font("Tahoma", 9F);
            this.tileBarCategory.AppearanceText.Options.UseFont = true;
            this.tileBarCategory.AppearanceText.Options.UseForeColor = true;
            this.tileBarCategory.Dock = System.Windows.Forms.DockStyle.Top;
            this.tileBarCategory.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.tileBarCategory.Groups.Add(this.tileGroupCategory);
            this.tileBarCategory.GroupTextToItemsIndent = 0;
            this.tileBarCategory.Location = new System.Drawing.Point(0, 0);
            this.tileBarCategory.MaxId = 8;
            this.tileBarCategory.Name = "tileBarCategory";
            this.tileBarCategory.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons;
            this.tileBarCategory.ShowGroupText = false;
            this.tileBarCategory.ShowText = true;
            this.tileBarCategory.Size = new System.Drawing.Size(875, 100);
            this.tileBarCategory.TabIndex = 4;
            this.tileBarCategory.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileBarCategory_ItemClick);
            // 
            // tileGroupCategory
            // 
            this.tileGroupCategory.Name = "tileGroupCategory";
            this.tileGroupCategory.Text = "CATEGORY";
            // 
            // tmCurrentDateTime
            // 
            this.tmCurrentDateTime.Enabled = true;
            this.tmCurrentDateTime.Interval = 1000;
            this.tmCurrentDateTime.Tick += new System.EventHandler(this.tmCurrentDateTime_Tick);
            // 
            // tileItem9
            // 
            this.tileItem9.AppearanceItem.Normal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tileItem9.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement16.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement16.Appearance.Hovered.Options.UseFont = true;
            tileItemElement16.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement16.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement16.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement16.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement16.Appearance.Normal.ForeColor = System.Drawing.Color.Black;
            tileItemElement16.Appearance.Normal.Options.UseFont = true;
            tileItemElement16.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement16.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement16.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement16.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement16.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement16.Appearance.Selected.Options.UseFont = true;
            tileItemElement16.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement16.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement16.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement16.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement16.MaxWidth = 160;
            tileItemElement16.Text = "S - Glass";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement16.TextLocation = new System.Drawing.Point(100, 0);
            tileItemElement17.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 12F);
            tileItemElement17.Appearance.Hovered.Options.UseFont = true;
            tileItemElement17.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement17.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement17.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement17.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement17.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            tileItemElement17.Appearance.Normal.Options.UseFont = true;
            tileItemElement17.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement17.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement17.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement17.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement17.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 12F);
            tileItemElement17.Appearance.Selected.Options.UseFont = true;
            tileItemElement17.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement17.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement17.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement17.MaxWidth = 160;
            tileItemElement17.Text = "12.00 $";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement17.TextLocation = new System.Drawing.Point(100, 30);
            tileItemElement18.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement18.Appearance.Hovered.Options.UseFont = true;
            tileItemElement18.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement18.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement18.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement18.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement18.Appearance.Normal.ForeColor = System.Drawing.SystemColors.ControlDark;
            tileItemElement18.Appearance.Normal.Options.UseFont = true;
            tileItemElement18.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement18.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement18.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement18.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement18.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement18.Appearance.Selected.Options.UseFont = true;
            tileItemElement18.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement18.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement18.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement18.MaxWidth = 160;
            tileItemElement18.Text = "Thailand";
            tileItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement18.TextLocation = new System.Drawing.Point(100, 60);
            tileItemElement19.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement19.Appearance.Hovered.Options.UseFont = true;
            tileItemElement19.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement19.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement19.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement19.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement19.Appearance.Normal.ForeColor = System.Drawing.SystemColors.ControlDark;
            tileItemElement19.Appearance.Normal.Options.UseFont = true;
            tileItemElement19.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement19.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement19.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement19.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement19.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement19.Appearance.Selected.Options.UseFont = true;
            tileItemElement19.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement19.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement19.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement19.MaxWidth = 160;
            tileItemElement19.Text = "# 28176493";
            tileItemElement19.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement19.TextLocation = new System.Drawing.Point(100, 84);
            tileItemElement20.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement20.ImageOptions.ImageLocation = new System.Drawing.Point(0, 2);
            tileItemElement20.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement20.ImageOptions.ImageSize = new System.Drawing.Size(100, 100);
            this.tileItem9.Elements.Add(tileItemElement16);
            this.tileItem9.Elements.Add(tileItemElement17);
            this.tileItem9.Elements.Add(tileItemElement18);
            this.tileItem9.Elements.Add(tileItemElement19);
            this.tileItem9.Elements.Add(tileItemElement20);
            this.tileItem9.Id = 1;
            this.tileItem9.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItem9.Name = "tileItem9";
            // 
            // tileItem10
            // 
            this.tileItem10.AppearanceItem.Normal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tileItem10.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement21.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement21.Appearance.Hovered.Options.UseFont = true;
            tileItemElement21.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement21.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement21.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement21.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement21.Appearance.Normal.ForeColor = System.Drawing.Color.Black;
            tileItemElement21.Appearance.Normal.Options.UseFont = true;
            tileItemElement21.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement21.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement21.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement21.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement21.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement21.Appearance.Selected.Options.UseFont = true;
            tileItemElement21.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement21.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement21.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement21.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement21.MaxWidth = 160;
            tileItemElement21.Text = "S - Glass";
            tileItemElement21.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement21.TextLocation = new System.Drawing.Point(100, 0);
            tileItemElement22.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 12F);
            tileItemElement22.Appearance.Hovered.Options.UseFont = true;
            tileItemElement22.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement22.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement22.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement22.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement22.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            tileItemElement22.Appearance.Normal.Options.UseFont = true;
            tileItemElement22.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement22.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement22.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement22.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement22.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 12F);
            tileItemElement22.Appearance.Selected.Options.UseFont = true;
            tileItemElement22.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement22.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement22.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement22.MaxWidth = 160;
            tileItemElement22.Text = "12.00 $";
            tileItemElement22.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement22.TextLocation = new System.Drawing.Point(100, 30);
            tileItemElement23.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement23.Appearance.Hovered.Options.UseFont = true;
            tileItemElement23.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement23.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement23.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement23.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement23.Appearance.Normal.ForeColor = System.Drawing.SystemColors.ControlDark;
            tileItemElement23.Appearance.Normal.Options.UseFont = true;
            tileItemElement23.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement23.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement23.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement23.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement23.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement23.Appearance.Selected.Options.UseFont = true;
            tileItemElement23.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement23.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement23.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement23.MaxWidth = 160;
            tileItemElement23.Text = "Thailand";
            tileItemElement23.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement23.TextLocation = new System.Drawing.Point(100, 60);
            tileItemElement24.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement24.Appearance.Hovered.Options.UseFont = true;
            tileItemElement24.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement24.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement24.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement24.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement24.Appearance.Normal.ForeColor = System.Drawing.SystemColors.ControlDark;
            tileItemElement24.Appearance.Normal.Options.UseFont = true;
            tileItemElement24.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement24.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement24.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement24.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement24.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement24.Appearance.Selected.Options.UseFont = true;
            tileItemElement24.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement24.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement24.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement24.MaxWidth = 160;
            tileItemElement24.Text = "# 28176493";
            tileItemElement24.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement24.TextLocation = new System.Drawing.Point(100, 84);
            tileItemElement25.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement25.ImageOptions.ImageLocation = new System.Drawing.Point(0, 2);
            tileItemElement25.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement25.ImageOptions.ImageSize = new System.Drawing.Size(100, 100);
            this.tileItem10.Elements.Add(tileItemElement21);
            this.tileItem10.Elements.Add(tileItemElement22);
            this.tileItem10.Elements.Add(tileItemElement23);
            this.tileItem10.Elements.Add(tileItemElement24);
            this.tileItem10.Elements.Add(tileItemElement25);
            this.tileItem10.Id = 1;
            this.tileItem10.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItem10.Name = "tileItem10";
            // 
            // tileItem11
            // 
            this.tileItem11.AppearanceItem.Normal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tileItem11.AppearanceItem.Normal.Options.UseBackColor = true;
            tileItemElement26.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement26.Appearance.Hovered.Options.UseFont = true;
            tileItemElement26.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement26.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement26.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement26.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement26.Appearance.Normal.ForeColor = System.Drawing.Color.Black;
            tileItemElement26.Appearance.Normal.Options.UseFont = true;
            tileItemElement26.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement26.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement26.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement26.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement26.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement26.Appearance.Selected.Options.UseFont = true;
            tileItemElement26.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement26.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement26.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement26.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement26.MaxWidth = 160;
            tileItemElement26.Text = "S - Glass";
            tileItemElement26.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement26.TextLocation = new System.Drawing.Point(100, 0);
            tileItemElement27.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 12F);
            tileItemElement27.Appearance.Hovered.Options.UseFont = true;
            tileItemElement27.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement27.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement27.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement27.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement27.Appearance.Normal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            tileItemElement27.Appearance.Normal.Options.UseFont = true;
            tileItemElement27.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement27.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement27.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement27.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement27.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 12F);
            tileItemElement27.Appearance.Selected.Options.UseFont = true;
            tileItemElement27.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement27.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement27.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement27.MaxWidth = 160;
            tileItemElement27.Text = "12.00 $";
            tileItemElement27.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement27.TextLocation = new System.Drawing.Point(100, 30);
            tileItemElement28.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement28.Appearance.Hovered.Options.UseFont = true;
            tileItemElement28.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement28.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement28.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement28.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement28.Appearance.Normal.ForeColor = System.Drawing.SystemColors.ControlDark;
            tileItemElement28.Appearance.Normal.Options.UseFont = true;
            tileItemElement28.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement28.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement28.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement28.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement28.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement28.Appearance.Selected.Options.UseFont = true;
            tileItemElement28.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement28.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement28.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement28.MaxWidth = 160;
            tileItemElement28.Text = "Thailand";
            tileItemElement28.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement28.TextLocation = new System.Drawing.Point(100, 60);
            tileItemElement29.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement29.Appearance.Hovered.Options.UseFont = true;
            tileItemElement29.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement29.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement29.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement29.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemElement29.Appearance.Normal.ForeColor = System.Drawing.SystemColors.ControlDark;
            tileItemElement29.Appearance.Normal.Options.UseFont = true;
            tileItemElement29.Appearance.Normal.Options.UseForeColor = true;
            tileItemElement29.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement29.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement29.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement29.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement29.Appearance.Selected.Options.UseFont = true;
            tileItemElement29.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement29.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement29.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement29.MaxWidth = 160;
            tileItemElement29.Text = "# 28176493";
            tileItemElement29.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement29.TextLocation = new System.Drawing.Point(100, 84);
            tileItemElement30.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement30.ImageOptions.ImageLocation = new System.Drawing.Point(0, 2);
            tileItemElement30.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement30.ImageOptions.ImageSize = new System.Drawing.Size(100, 100);
            this.tileItem11.Elements.Add(tileItemElement26);
            this.tileItem11.Elements.Add(tileItemElement27);
            this.tileItem11.Elements.Add(tileItemElement28);
            this.tileItem11.Elements.Add(tileItemElement29);
            this.tileItem11.Elements.Add(tileItemElement30);
            this.tileItem11.Id = 1;
            this.tileItem11.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItem11.Name = "tileItem11";
            // 
            // tileBarGroup1
            // 
            this.tileBarGroup1.Name = "tileBarGroup1";
            // 
            // tileBarGroup2
            // 
            this.tileBarGroup2.Name = "tileBarGroup2";
            // 
            // tileBarGroup3
            // 
            this.tileBarGroup3.Name = "tileBarGroup3";
            // 
            // toolTipProduct
            // 
            this.toolTipProduct.ToolTipType = DevExpress.Utils.ToolTipType.SuperTip;
            // 
            // tileNavCategory1
            // 
            this.tileNavCategory1.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.tileNavCategory1.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Appearance.Options.UseTextOptions = true;
            this.labelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl12.LineColor = System.Drawing.Color.Black;
            this.labelControl12.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.labelControl12.LineVisible = true;
            this.labelControl12.Location = new System.Drawing.Point(9, 234);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(577, 10);
            this.labelControl12.TabIndex = 0;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseTextOptions = true;
            this.labelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl13.LineColor = System.Drawing.Color.Black;
            this.labelControl13.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.labelControl13.LineVisible = true;
            this.labelControl13.Location = new System.Drawing.Point(9, 473);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(577, 10);
            this.labelControl13.TabIndex = 0;
            // 
            // lblIssuedDate
            // 
            this.lblIssuedDate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIssuedDate.Appearance.Options.UseFont = true;
            this.lblIssuedDate.Location = new System.Drawing.Point(13, 145);
            this.lblIssuedDate.Name = "lblIssuedDate";
            this.lblIssuedDate.Size = new System.Drawing.Size(39, 16);
            this.lblIssuedDate.TabIndex = 9;
            this.lblIssuedDate.Tag = " ";
            this.lblIssuedDate.Text = "Date  :";
            // 
            // lblIssuedTime
            // 
            this.lblIssuedTime.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIssuedTime.Appearance.Options.UseFont = true;
            this.lblIssuedTime.Location = new System.Drawing.Point(13, 167);
            this.lblIssuedTime.Name = "lblIssuedTime";
            this.lblIssuedTime.Size = new System.Drawing.Size(38, 16);
            this.lblIssuedTime.TabIndex = 9;
            this.lblIssuedTime.Text = "Time :";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(13, 189);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(51, 16);
            this.labelControl2.TabIndex = 9;
            this.labelControl2.Text = "Barcode:";
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInvoiceNo.Appearance.Options.UseFont = true;
            this.lblInvoiceNo.Location = new System.Drawing.Point(13, 123);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(40, 16);
            this.lblInvoiceNo.TabIndex = 9;
            this.lblInvoiceNo.Text = "No     :";
            // 
            // lblCustomer
            // 
            this.lblCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCustomer.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lblCustomer.Appearance.Options.UseFont = true;
            this.lblCustomer.Appearance.Options.UseTextOptions = true;
            this.lblCustomer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblCustomer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCustomer.Location = new System.Drawing.Point(307, 147);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(104, 26);
            this.lblCustomer.TabIndex = 12;
            this.lblCustomer.Text = "Customer :";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(71, 186);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.txtBarcode.Properties.Appearance.Options.UseFont = true;
            this.txtBarcode.Properties.MaxLength = 13;
            this.txtBarcode.Size = new System.Drawing.Size(177, 24);
            this.txtBarcode.TabIndex = 17;
            this.txtBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // GC
            // 
            this.GC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GC.EmbeddedNavigator.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.GC.EmbeddedNavigator.Appearance.Options.UseBackColor = true;
            gridLevelNode2.RelationName = "Level1";
            this.GC.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.GC.Location = new System.Drawing.Point(14, 250);
            this.GC.MainView = this.Gv;
            this.GC.Name = "GC";
            this.GC.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemLookUpEdit1});
            this.GC.Size = new System.Drawing.Size(570, 228);
            this.GC.TabIndex = 18;
            this.GC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.Gv,
            this.gridView1});
            // 
            // Gv
            // 
            this.Gv.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Gv.Appearance.HeaderPanel.Options.UseFont = true;
            this.Gv.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.Gv.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Gv.Appearance.Row.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Gv.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Gv.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.Gv.Appearance.Row.Options.UseBorderColor = true;
            this.Gv.Appearance.Row.Options.UseFont = true;
            this.Gv.Appearance.Row.Options.UseForeColor = true;
            this.Gv.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.Gv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.ItemCode,
            this.ItemDescriptionEn,
            this.Price,
            this.Quantity,
            this.DisSalePrice,
            this.gridColUnit,
            this.Amount,
            this.gridColumn5,
            this.gridColumn3,
            this.gridColumn4});
            this.Gv.GridControl = this.GC;
            this.Gv.IndicatorWidth = 30;
            this.Gv.Name = "Gv";
            this.Gv.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.Gv.OptionsSelection.MultiSelect = true;
            this.Gv.OptionsView.EnableAppearanceEvenRow = true;
            this.Gv.OptionsView.ShowGroupPanel = false;
            this.Gv.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.Gv.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.GV_CustomDrawCell);
            this.Gv.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GV_PopupMenuShowing);
            this.Gv.ColumnChanged += new System.EventHandler(this.Gv_ColumnChanged);
            this.Gv.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.Gv_FocusedRowChanged);
            this.Gv.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.Gv_FocusedColumnChanged);
            this.Gv.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.GV_CellValueChanged);
            this.Gv.FocusedRowLoaded += new DevExpress.XtraGrid.Views.Base.RowEventHandler(this.Gv_FocusedRowLoaded);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "ItemID";
            this.gridColumn2.FieldName = "ProductTypeID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 49;
            // 
            // ItemCode
            // 
            this.ItemCode.Caption = "Item Code";
            this.ItemCode.FieldName = "ItemCode";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.OptionsColumn.AllowEdit = false;
            this.ItemCode.Visible = true;
            this.ItemCode.VisibleIndex = 1;
            this.ItemCode.Width = 77;
            // 
            // ItemDescriptionEn
            // 
            this.ItemDescriptionEn.Caption = "Item Description";
            this.ItemDescriptionEn.FieldName = "ItemDescriptionEn";
            this.ItemDescriptionEn.Name = "ItemDescriptionEn";
            this.ItemDescriptionEn.OptionsColumn.AllowEdit = false;
            this.ItemDescriptionEn.Visible = true;
            this.ItemDescriptionEn.VisibleIndex = 2;
            this.ItemDescriptionEn.Width = 111;
            // 
            // Price
            // 
            this.Price.Caption = "Price";
            this.Price.DisplayFormat.FormatString = "n2";
            this.Price.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Price.FieldName = "Price";
            this.Price.Name = "Price";
            this.Price.OptionsColumn.AllowEdit = false;
            this.Price.Visible = true;
            this.Price.VisibleIndex = 3;
            this.Price.Width = 62;
            // 
            // Quantity
            // 
            this.Quantity.Caption = "Qty";
            this.Quantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Quantity.FieldName = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.OptionsColumn.AllowEdit = false;
            this.Quantity.Visible = true;
            this.Quantity.VisibleIndex = 4;
            this.Quantity.Width = 39;
            // 
            // DisSalePrice
            // 
            this.DisSalePrice.Caption = "Discount Price";
            this.DisSalePrice.DisplayFormat.FormatString = "n2";
            this.DisSalePrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DisSalePrice.FieldName = "DiscountPrice";
            this.DisSalePrice.Name = "DisSalePrice";
            this.DisSalePrice.Width = 89;
            // 
            // gridColUnit
            // 
            this.gridColUnit.AppearanceCell.Options.UseTextOptions = true;
            this.gridColUnit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColUnit.Caption = "Unit";
            this.gridColUnit.FieldName = "UOM";
            this.gridColUnit.Name = "gridColUnit";
            this.gridColUnit.Visible = true;
            this.gridColUnit.VisibleIndex = 5;
            this.gridColUnit.Width = 82;
            // 
            // Amount
            // 
            this.Amount.Caption = "Amount";
            this.Amount.DisplayFormat.FormatString = "n2";
            this.Amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Amount.FieldName = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.OptionsColumn.AllowEdit = false;
            this.Amount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Amount", "SUM={0:C2}")});
            this.Amount.Visible = true;
            this.Amount.VisibleIndex = 6;
            this.Amount.Width = 63;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Action";
            this.gridColumn5.ColumnEdit = this.repositoryTransaction;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 7;
            this.gridColumn5.Width = 57;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ChangeRiel";
            this.gridColumn3.FieldName = "ChangeRiel";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ChangeUSD";
            this.gridColumn4.FieldName = "ChangeUSD";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.PopupSizeable = false;
            this.repositoryItemGridLookUpEdit1.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEdit1.ShowFooter = false;
            this.repositoryItemGridLookUpEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEdit1_EditValueChanged);
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 10F);
            this.repositoryItemLookUpEdit1.AppearanceDropDown.Options.UseFont = true;
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.PopupSizeable = false;
            this.repositoryItemLookUpEdit1.ShowFooter = false;
            this.repositoryItemLookUpEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemLookUpEdit1_EditValueChanged);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.GC;
            this.gridView1.Name = "gridView1";
            // 
            // luInvoice
            // 
            this.luInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.luInvoice.Location = new System.Drawing.Point(417, 184);
            this.luInvoice.MenuManager = this.barManager1;
            this.luInvoice.Name = "luInvoice";
            this.luInvoice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luInvoice.Properties.Appearance.Options.UseFont = true;
            this.luInvoice.Properties.Appearance.Options.UseTextOptions = true;
            this.luInvoice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luInvoice.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luInvoice.Properties.AppearanceDropDown.Options.UseFont = true;
            this.luInvoice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luInvoice.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("InvoiceRefNbr", "InvoiceRefNbr")});
            this.luInvoice.Properties.NullText = "";
            this.luInvoice.Properties.NullValuePrompt = "--Select--";
            this.luInvoice.Properties.PopupSizeable = false;
            this.luInvoice.Properties.ShowFooter = false;
            this.luInvoice.Properties.ShowHeader = false;
            this.luInvoice.Properties.EditValueChanged += new System.EventHandler(this.luInvoice_Properties_EditValueChanged);
            this.luInvoice.Size = new System.Drawing.Size(169, 28);
            this.luInvoice.TabIndex = 19;
            this.luInvoice.EditValueChanged += new System.EventHandler(this.luInvoice_EditValueChanged);
            // 
            // luCustomer
            // 
            this.luCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.luCustomer.EditValue = "General Customer";
            this.luCustomer.Location = new System.Drawing.Point(417, 147);
            this.luCustomer.MenuManager = this.barManager1;
            this.luCustomer.Name = "luCustomer";
            this.luCustomer.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luCustomer.Properties.Appearance.Options.UseFont = true;
            this.luCustomer.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luCustomer.Properties.AppearanceDropDown.Options.UseFont = true;
            this.luCustomer.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luCustomer.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.luCustomer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luCustomer.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CustomerName", "CustomerName")});
            this.luCustomer.Properties.MaxLength = 30;
            this.luCustomer.Properties.NullText = "General Customer";
            this.luCustomer.Properties.NullValuePrompt = "General Customer";
            this.luCustomer.Properties.PopupSizeable = false;
            this.luCustomer.Properties.ShowFooter = false;
            this.luCustomer.Properties.ShowHeader = false;
            this.luCustomer.Size = new System.Drawing.Size(169, 28);
            this.luCustomer.TabIndex = 21;
            this.luCustomer.EditValueChanged += new System.EventHandler(this.luCustomer_EditValueChanged);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
            this.btnClear.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnClear.Appearance.Options.UseBackColor = true;
            this.btnClear.Appearance.Options.UseFont = true;
            this.btnClear.Location = new System.Drawing.Point(515, 0);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(80, 43);
            this.btnClear.TabIndex = 22;
            this.btnClear.Text = "Clear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // panelCustomer
            // 
            this.panelCustomer.Appearance.BackColor = System.Drawing.Color.White;
            this.panelCustomer.Appearance.Options.UseBackColor = true;
            this.panelCustomer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelCustomer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelCustomer.Controls.Add(this.lbContact);
            this.panelCustomer.Controls.Add(this.lblLocation);
            this.panelCustomer.Controls.Add(this.sidePanel1);
            this.panelCustomer.Controls.Add(this.labelControl3);
            this.panelCustomer.Controls.Add(this.lbShop);
            this.panelCustomer.Controls.Add(this.luCustomer);
            this.panelCustomer.Controls.Add(this.luInvoice);
            this.panelCustomer.Controls.Add(this.GC);
            this.panelCustomer.Controls.Add(this.txtBarcode);
            this.panelCustomer.Controls.Add(this.lblCustomer);
            this.panelCustomer.Controls.Add(this.lblCashier);
            this.panelCustomer.Controls.Add(this.lblInvoiceNo);
            this.panelCustomer.Controls.Add(this.labelControl2);
            this.panelCustomer.Controls.Add(this.lblIssuedTime);
            this.panelCustomer.Controls.Add(this.lblIssuedDate);
            this.panelCustomer.Controls.Add(this.labelControl7);
            this.panelCustomer.Controls.Add(this.labelControl13);
            this.panelCustomer.Controls.Add(this.labelControl12);
            this.panelCustomer.Controls.Add(this.labelControl8);
            this.panelCustomer.Controls.Add(this.sidePanel2);
            this.panelCustomer.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelCustomer.Location = new System.Drawing.Point(875, 90);
            this.panelCustomer.Name = "panelCustomer";
            this.panelCustomer.Size = new System.Drawing.Size(595, 682);
            this.panelCustomer.TabIndex = 6;
            // 
            // lbContact
            // 
            this.lbContact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbContact.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbContact.Appearance.Options.UseFont = true;
            this.lbContact.Appearance.Options.UseTextOptions = true;
            this.lbContact.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lbContact.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbContact.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbContact.Location = new System.Drawing.Point(188, 118);
            this.lbContact.Name = "lbContact";
            this.lbContact.Size = new System.Drawing.Size(399, 18);
            this.lbContact.TabIndex = 29;
            this.lbContact.Text = "Contact";
            // 
            // lblLocation
            // 
            this.lblLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLocation.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocation.Appearance.Options.UseFont = true;
            this.lblLocation.Appearance.Options.UseTextOptions = true;
            this.lblLocation.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblLocation.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblLocation.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblLocation.Location = new System.Drawing.Point(188, 65);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(398, 53);
            this.lblLocation.TabIndex = 28;
            this.lblLocation.Text = "Serviced Suite No.21, Ground Floor at the Hotel Cambodiana,\r\n313 Sisowath Quay, P" +
    "hnom Penh, Cambodia.\r\nTel : (023)213-835 | Fax : (023)213-836";
            // 
            // sidePanel1
            // 
            this.sidePanel1.Appearance.BackColor = System.Drawing.Color.FloralWhite;
            this.sidePanel1.Appearance.Options.UseBackColor = true;
            this.sidePanel1.Controls.Add(this.panelButton);
            this.sidePanel1.Controls.Add(this.lblChangeRiel);
            this.sidePanel1.Controls.Add(this.lblUSDExchangeRate);
            this.sidePanel1.Controls.Add(this.lblCashInRiel);
            this.sidePanel1.Controls.Add(this.lblDiscountAmount);
            this.sidePanel1.Controls.Add(this.lblVATAmount);
            this.sidePanel1.Controls.Add(this.lblChangeUSD);
            this.sidePanel1.Controls.Add(this.lblCashInUSD);
            this.sidePanel1.Controls.Add(this.lblTotalAmount);
            this.sidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.sidePanel1.Location = new System.Drawing.Point(0, 484);
            this.sidePanel1.Name = "sidePanel1";
            this.sidePanel1.Size = new System.Drawing.Size(595, 198);
            this.sidePanel1.TabIndex = 26;
            this.sidePanel1.Text = "sidePanel1";
            // 
            // panelButton
            // 
            this.panelButton.Appearance.BackColor = System.Drawing.Color.FloralWhite;
            this.panelButton.Appearance.Options.UseBackColor = true;
            this.panelButton.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelButton.Controls.Add(this.btnListInvoice);
            this.panelButton.Controls.Add(this.btnReturn);
            this.panelButton.Controls.Add(this.btnClose);
            this.panelButton.Controls.Add(this.btnPayBill);
            this.panelButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelButton.Location = new System.Drawing.Point(0, 155);
            this.panelButton.Name = "panelButton";
            this.panelButton.Size = new System.Drawing.Size(595, 43);
            this.panelButton.TabIndex = 19;
            // 
            // btnListInvoice
            // 
            this.btnListInvoice.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            this.btnListInvoice.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnListInvoice.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.btnListInvoice.Appearance.Options.UseBackColor = true;
            this.btnListInvoice.Appearance.Options.UseFont = true;
            this.btnListInvoice.Appearance.Options.UseForeColor = true;
            this.btnListInvoice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnListInvoice.Location = new System.Drawing.Point(109, 4);
            this.btnListInvoice.Name = "btnListInvoice";
            this.btnListInvoice.Size = new System.Drawing.Size(100, 35);
            this.btnListInvoice.TabIndex = 3;
            this.btnListInvoice.Text = "&HISTORY";
            this.btnListInvoice.Click += new System.EventHandler(this.btnListInvoice_Click);
            this.btnListInvoice.MouseHover += new System.EventHandler(this.btnHistory_MouseHover);
            // 
            // btnReturn
            // 
            this.btnReturn.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnReturn.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnReturn.Appearance.Options.UseFont = true;
            this.btnReturn.Appearance.Options.UseForeColor = true;
            this.btnReturn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReturn.Location = new System.Drawing.Point(3, 4);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(100, 35);
            this.btnReturn.TabIndex = 2;
            this.btnReturn.Text = "&RETURN";
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnClose.Appearance.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.Appearance.Options.UseForeColor = true;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Location = new System.Drawing.Point(388, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 35);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&CLOSE";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPayBill
            // 
            this.btnPayBill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPayBill.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnPayBill.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnPayBill.Appearance.Options.UseFont = true;
            this.btnPayBill.Appearance.Options.UseForeColor = true;
            this.btnPayBill.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPayBill.Location = new System.Drawing.Point(495, 5);
            this.btnPayBill.Name = "btnPayBill";
            this.btnPayBill.Size = new System.Drawing.Size(100, 35);
            this.btnPayBill.TabIndex = 1;
            this.btnPayBill.Text = "&PAY BILL";
            this.btnPayBill.Click += new System.EventHandler(this.btnPayBill_Click);
            // 
            // lblChangeRiel
            // 
            this.lblChangeRiel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChangeRiel.Appearance.Font = new System.Drawing.Font("Khmer OS Content", 11F);
            this.lblChangeRiel.Appearance.Options.UseFont = true;
            this.lblChangeRiel.Appearance.Options.UseTextOptions = true;
            this.lblChangeRiel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblChangeRiel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblChangeRiel.Location = new System.Drawing.Point(260, 124);
            this.lblChangeRiel.Name = "lblChangeRiel";
            this.lblChangeRiel.Size = new System.Drawing.Size(326, 21);
            this.lblChangeRiel.TabIndex = 23;
            this.lblChangeRiel.Text = "Change (៛) : 0 ៛";
            // 
            // lblUSDExchangeRate
            // 
            this.lblUSDExchangeRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUSDExchangeRate.Appearance.Font = new System.Drawing.Font("Khmer OS Content", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUSDExchangeRate.Appearance.Options.UseFont = true;
            this.lblUSDExchangeRate.Appearance.Options.UseTextOptions = true;
            this.lblUSDExchangeRate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblUSDExchangeRate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblUSDExchangeRate.Location = new System.Drawing.Point(9, 73);
            this.lblUSDExchangeRate.Name = "lblUSDExchangeRate";
            this.lblUSDExchangeRate.Size = new System.Drawing.Size(350, 16);
            this.lblUSDExchangeRate.TabIndex = 24;
            this.lblUSDExchangeRate.Text = "Exchange Rate 1$  : 0 ៛";
            // 
            // lblCashInRiel
            // 
            this.lblCashInRiel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCashInRiel.Appearance.Font = new System.Drawing.Font("Khmer OS Content", 11F);
            this.lblCashInRiel.Appearance.Options.UseFont = true;
            this.lblCashInRiel.Appearance.Options.UseTextOptions = true;
            this.lblCashInRiel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblCashInRiel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCashInRiel.Location = new System.Drawing.Point(260, 73);
            this.lblCashInRiel.Name = "lblCashInRiel";
            this.lblCashInRiel.Size = new System.Drawing.Size(326, 21);
            this.lblCashInRiel.TabIndex = 22;
            this.lblCashInRiel.Text = "Cash In (៛) : 0 ៛";
            // 
            // lblDiscountAmount
            // 
            this.lblDiscountAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDiscountAmount.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscountAmount.Appearance.Options.UseFont = true;
            this.lblDiscountAmount.Appearance.Options.UseTextOptions = true;
            this.lblDiscountAmount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblDiscountAmount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDiscountAmount.Location = new System.Drawing.Point(9, 100);
            this.lblDiscountAmount.Name = "lblDiscountAmount";
            this.lblDiscountAmount.Size = new System.Drawing.Size(350, 16);
            this.lblDiscountAmount.TabIndex = 20;
            this.lblDiscountAmount.Text = "Discount Amount     : 0.0 $";
            // 
            // lblVATAmount
            // 
            this.lblVATAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVATAmount.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVATAmount.Appearance.Options.UseFont = true;
            this.lblVATAmount.Appearance.Options.UseTextOptions = true;
            this.lblVATAmount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblVATAmount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblVATAmount.Location = new System.Drawing.Point(9, 126);
            this.lblVATAmount.Name = "lblVATAmount";
            this.lblVATAmount.Size = new System.Drawing.Size(350, 16);
            this.lblVATAmount.TabIndex = 21;
            this.lblVATAmount.Text = "VAT Amount (10%) : 0.0 $";
            // 
            // lblChangeUSD
            // 
            this.lblChangeUSD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChangeUSD.Appearance.Font = new System.Drawing.Font("Khmer OS Content", 11F);
            this.lblChangeUSD.Appearance.Options.UseFont = true;
            this.lblChangeUSD.Appearance.Options.UseTextOptions = true;
            this.lblChangeUSD.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblChangeUSD.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblChangeUSD.Location = new System.Drawing.Point(260, 97);
            this.lblChangeUSD.Name = "lblChangeUSD";
            this.lblChangeUSD.Size = new System.Drawing.Size(326, 21);
            this.lblChangeUSD.TabIndex = 16;
            this.lblChangeUSD.Text = "Change ($) : 0 $";
            // 
            // lblCashInUSD
            // 
            this.lblCashInUSD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCashInUSD.Appearance.Font = new System.Drawing.Font("Khmer OS Content", 11F);
            this.lblCashInUSD.Appearance.Options.UseFont = true;
            this.lblCashInUSD.Appearance.Options.UseTextOptions = true;
            this.lblCashInUSD.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblCashInUSD.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCashInUSD.Location = new System.Drawing.Point(260, 49);
            this.lblCashInUSD.Name = "lblCashInUSD";
            this.lblCashInUSD.Size = new System.Drawing.Size(326, 21);
            this.lblCashInUSD.TabIndex = 17;
            this.lblCashInUSD.Text = "Cash In ($) : 0 $";
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalAmount.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalAmount.Appearance.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold);
            this.lblTotalAmount.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Appearance.Options.UseBackColor = true;
            this.lblTotalAmount.Appearance.Options.UseFont = true;
            this.lblTotalAmount.Appearance.Options.UseForeColor = true;
            this.lblTotalAmount.Appearance.Options.UseTextOptions = true;
            this.lblTotalAmount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblTotalAmount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTotalAmount.Location = new System.Drawing.Point(260, 0);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(326, 43);
            this.lblTotalAmount.TabIndex = 18;
            this.lblTotalAmount.Text = "Total : 0.0 $";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(307, 183);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(104, 26);
            this.labelControl3.TabIndex = 25;
            this.labelControl3.Text = "Invoice :";
            // 
            // lbShop
            // 
            this.lbShop.Appearance.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold);
            this.lbShop.Appearance.Options.UseFont = true;
            this.lbShop.Appearance.Options.UseTextOptions = true;
            this.lbShop.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lbShop.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbShop.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbShop.Location = new System.Drawing.Point(13, 60);
            this.lbShop.Name = "lbShop";
            this.lbShop.Size = new System.Drawing.Size(125, 40);
            this.lbShop.TabIndex = 23;
            this.lbShop.Text = "My Shop";
            // 
            // lblCashier
            // 
            this.lblCashier.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.lblCashier.Appearance.Options.UseFont = true;
            this.lblCashier.Appearance.Options.UseTextOptions = true;
            this.lblCashier.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblCashier.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCashier.Location = new System.Drawing.Point(13, 102);
            this.lblCashier.Name = "lblCashier";
            this.lblCashier.Size = new System.Drawing.Size(115, 16);
            this.lblCashier.TabIndex = 9;
            this.lblCashier.Text = "Cashier    : ";
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.LineVisible = true;
            this.labelControl7.Location = new System.Drawing.Point(10, 48);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(577, 16);
            this.labelControl7.TabIndex = 7;
            this.labelControl7.Text = "INVOICE";
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Appearance.Options.UseTextOptions = true;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Location = new System.Drawing.Point(9, 212);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(577, 21);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "SALE INVOICE";
            // 
            // sidePanel2
            // 
            this.sidePanel2.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.sidePanel2.Appearance.Options.UseBackColor = true;
            this.sidePanel2.Controls.Add(this.btnDeleteBrowse);
            this.sidePanel2.Controls.Add(this.labelControl6);
            this.sidePanel2.Controls.Add(this.luBrowse);
            this.sidePanel2.Controls.Add(this.btnSkip);
            this.sidePanel2.Controls.Add(this.btnClear);
            this.sidePanel2.Location = new System.Drawing.Point(0, 0);
            this.sidePanel2.Name = "sidePanel2";
            this.sidePanel2.Size = new System.Drawing.Size(595, 42);
            this.sidePanel2.TabIndex = 27;
            this.sidePanel2.Text = "sidePanel2";
            // 
            // btnDeleteBrowse
            // 
            this.btnDeleteBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteBrowse.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDeleteBrowse.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnDeleteBrowse.Appearance.ForeColor = System.Drawing.Color.Black;
            this.btnDeleteBrowse.Appearance.Options.UseBackColor = true;
            this.btnDeleteBrowse.Appearance.Options.UseFont = true;
            this.btnDeleteBrowse.Appearance.Options.UseForeColor = true;
            this.btnDeleteBrowse.Enabled = false;
            this.btnDeleteBrowse.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnDeleteBrowse.ImageOptions.SvgImage")));
            this.btnDeleteBrowse.Location = new System.Drawing.Point(388, 0);
            this.btnDeleteBrowse.Name = "btnDeleteBrowse";
            this.btnDeleteBrowse.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnDeleteBrowse.Size = new System.Drawing.Size(128, 43);
            this.btnDeleteBrowse.TabIndex = 23;
            this.btnDeleteBrowse.Text = "Delete";
            this.btnDeleteBrowse.Click += new System.EventHandler(this.btnDeleteBrowse_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(104, 8);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(93, 26);
            this.labelControl6.TabIndex = 21;
            this.labelControl6.Text = "Browse";
            // 
            // luBrowse
            // 
            this.luBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.luBrowse.Location = new System.Drawing.Point(198, 8);
            this.luBrowse.MenuManager = this.barManager1;
            this.luBrowse.Name = "luBrowse";
            this.luBrowse.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luBrowse.Properties.Appearance.Options.UseFont = true;
            this.luBrowse.Properties.Appearance.Options.UseTextOptions = true;
            this.luBrowse.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luBrowse.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luBrowse.Properties.AppearanceDropDown.Options.UseFont = true;
            this.luBrowse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luBrowse.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("InvoiceRefNbr", "No"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("InvoiceDate", "Date", 30, DevExpress.Utils.FormatType.DateTime, "yyyy-MM", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.luBrowse.Properties.NullText = "";
            this.luBrowse.Properties.NullValuePrompt = "--Select--";
            this.luBrowse.Properties.PopupSizeable = false;
            this.luBrowse.Properties.ShowFooter = false;
            this.luBrowse.Properties.EditValueChanged += new System.EventHandler(this.luInvoice_Properties_EditValueChanged);
            this.luBrowse.Size = new System.Drawing.Size(142, 28);
            this.luBrowse.TabIndex = 20;
            this.luBrowse.EditValueChanged += new System.EventHandler(this.luBrowse_EditValueChanged);
            // 
            // btnSkip
            // 
            this.btnSkip.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.btnSkip.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnSkip.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.btnSkip.Appearance.Options.UseBackColor = true;
            this.btnSkip.Appearance.Options.UseFont = true;
            this.btnSkip.Appearance.Options.UseForeColor = true;
            this.btnSkip.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSkip.Location = new System.Drawing.Point(0, -1);
            this.btnSkip.Name = "btnSkip";
            this.btnSkip.Size = new System.Drawing.Size(100, 43);
            this.btnSkip.TabIndex = 4;
            this.btnSkip.Text = "Skip";
            this.btnSkip.Click += new System.EventHandler(this.btnSkip_Click);
            // 
            // btnPrintNow
            // 
            this.btnPrintNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintNow.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnPrintNow.Appearance.Options.UseFont = true;
            this.btnPrintNow.Location = new System.Drawing.Point(875, 42);
            this.btnPrintNow.Name = "btnPrintNow";
            this.btnPrintNow.Size = new System.Drawing.Size(100, 28);
            this.btnPrintNow.TabIndex = 32;
            this.btnPrintNow.Text = "Print Now";
            this.btnPrintNow.Click += new System.EventHandler(this.btnPrintNow_Click);
            // 
            // scrSaleOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1470, 772);
            this.Controls.Add(this.panelProduct);
            this.Controls.Add(this.panelCustomer);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "scrSaleOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "POS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmSaleProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryTransaction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelHeader)).EndInit();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboprint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelFilter)).EndInit();
            this.panelFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelProduct)).EndInit();
            this.panelProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelSearch)).EndInit();
            this.panelSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picClear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelCustomer)).EndInit();
            this.panelCustomer.ResumeLayout(false);
            this.panelCustomer.PerformLayout();
            this.sidePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelButton)).EndInit();
            this.panelButton.ResumeLayout(false);
            this.sidePanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.luBrowse.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelHeader;
        private System.Windows.Forms.PictureBox picLogo;
        private DevExpress.XtraEditors.LabelControl lblCurrentDateTime;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelProduct;
        private DevExpress.XtraBars.Navigation.TileBar tileBarCategory;
        private DevExpress.XtraBars.Navigation.TileBarGroup tileGroupCategory;
        private DevExpress.XtraEditors.TileControl screenProduct;
        private DevExpress.XtraEditors.TileGroup tileGroupProductList;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PanelControl panelSearch;
        private DevExpress.XtraEditors.PictureEdit picClear;
        private DevExpress.XtraEditors.TextEdit txtSearch;
        private DevExpress.XtraEditors.PanelControl panelFilter;
        private DevExpress.XtraEditors.PictureEdit picFilter;
        private System.Windows.Forms.Timer tmCurrentDateTime;
        private DevExpress.XtraEditors.TileItem tileItem9;
        private DevExpress.XtraEditors.TileItem tileItem10;
        private DevExpress.XtraEditors.TileItem tileItem11;
        private DevExpress.XtraBars.Navigation.TileBarGroup tileBarGroup1;
        private DevExpress.XtraBars.Navigation.TileBarGroup tileBarGroup2;
        private DevExpress.XtraBars.Navigation.TileBarGroup tileBarGroup3;
        private DevExpress.XtraBars.PopupMenu popMenu;
        private DevExpress.XtraBars.BarButtonItem ppmUnPickupItem;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnAction;
        private DevExpress.XtraBars.BarLinkContainerItem ppmAction;
        private DevExpress.XtraBars.BarButtonItem ppmNew;
        private DevExpress.XtraBars.BarButtonItem ppmEdit;
        private DevExpress.XtraBars.BarButtonItem ppmDelete;
        private DevExpress.XtraBars.BarLinkContainerItem ppmViewLog;
        private DevExpress.XtraBars.BarButtonItem ppmLogOn;
        private DevExpress.XtraBars.BarButtonItem ppmLogOut;
        private DevExpress.XtraBars.BarLinkContainerItem ppmViewStatus;
        private DevExpress.XtraBars.BarButtonItem ppmActiveView;
        private DevExpress.XtraBars.BarButtonItem ppResetPassword;
        private DevExpress.XtraBars.BarButtonItem ppPermission;
        private DevExpress.XtraBars.BarButtonItem ppmAdvanceFilter;
        private DevExpress.XtraBars.BarButtonItem ppmExport;
        private DevExpress.XtraBars.BarButtonItem ppmViewAllLog;
        private DevExpress.XtraBars.BarButtonItem ppmViewAllStatus;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarButtonItem ppmInactiveView;
        private DevExpress.XtraBars.BarButtonItem ppmActive;
        private DevExpress.XtraBars.BarButtonItem ppmInactive;
        private DevExpress.XtraBars.BarButtonItem ppmDesignReport;
        private DevExpress.XtraBars.BarButtonItem btnClearDesignReport;
        private DevExpress.XtraBars.BarButtonItem ppmEditQuantity;
        private DevExpress.Utils.ToolTipController toolTipProduct;
        private DevExpress.XtraBars.Navigation.TileNavCategory tileNavCategory1;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryTransaction;
        private System.Windows.Forms.ToolTip toolTip1;
        private DevExpress.XtraEditors.PanelControl panelCustomer;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.LookUpEdit luCustomer;
        private DevExpress.XtraEditors.LookUpEdit luInvoice;
        internal DevExpress.XtraGrid.GridControl GC;
        internal DevExpress.XtraGrid.Views.Grid.GridView Gv;
        private DevExpress.XtraGrid.Columns.GridColumn ItemCode;
        private DevExpress.XtraGrid.Columns.GridColumn ItemDescriptionEn;
        private DevExpress.XtraGrid.Columns.GridColumn Price;
        private DevExpress.XtraGrid.Columns.GridColumn Quantity;
        private DevExpress.XtraGrid.Columns.GridColumn DisSalePrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColUnit;
        internal DevExpress.XtraGrid.Columns.GridColumn Amount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.TextEdit txtBarcode;
        private DevExpress.XtraEditors.LabelControl lblCustomer;
        private DevExpress.XtraEditors.LabelControl lblInvoiceNo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lblIssuedTime;
        private DevExpress.XtraEditors.LabelControl lblIssuedDate;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.SidePanel sidePanel1;
        private DevExpress.XtraEditors.PanelControl panelButton;
        private DevExpress.XtraEditors.SimpleButton btnListInvoice;
        private DevExpress.XtraEditors.SimpleButton btnReturn;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnPayBill;
        internal DevExpress.XtraEditors.LabelControl lblChangeRiel;
        private DevExpress.XtraEditors.LabelControl lblUSDExchangeRate;
        internal DevExpress.XtraEditors.LabelControl lblCashInRiel;
        private DevExpress.XtraEditors.LabelControl lblDiscountAmount;
        private DevExpress.XtraEditors.LabelControl lblVATAmount;
        internal DevExpress.XtraEditors.LabelControl lblChangeUSD;
        internal DevExpress.XtraEditors.LabelControl lblCashInUSD;
        private DevExpress.XtraEditors.LabelControl lblTotalAmount;
        private DevExpress.XtraEditors.SidePanel sidePanel2;
        private DevExpress.XtraEditors.SimpleButton btnSkip;
        private DevExpress.XtraEditors.LabelControl lbShop;
        private DevExpress.XtraEditors.LabelControl lblCashier;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LookUpEdit luBrowse;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl lbContact;
        private DevExpress.XtraEditors.LabelControl lblLocation;
        private DevExpress.XtraEditors.SimpleButton btnDeleteBrowse;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        internal DevExpress.XtraEditors.ComboBoxEdit cboprint;
        private DevExpress.XtraEditors.SimpleButton btnPrintNow;
    }
}