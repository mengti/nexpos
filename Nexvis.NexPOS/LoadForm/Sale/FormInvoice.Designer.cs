﻿namespace DgoStore.LoadForm.Sale
{
    partial class scrListInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navButton1 = new DevExpress.XtraBars.Navigation.NavButton();
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navExport = new DevExpress.XtraBars.Navigation.NavButton();
            this.navRefresh = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.Gc = new DevExpress.XtraGrid.GridControl();
            this.Gv = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.InvoiceRefNbr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Descr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InvoiceTypes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Qty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CustomerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SalePersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gv)).BeginInit();
            this.SuspendLayout();
            // 
            // navButton1
            // 
            this.navButton1.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButton1.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navButton1.AppearanceHovered.Options.UseForeColor = true;
            this.navButton1.Caption = "Delete";
            this.navButton1.Name = "navButton1";
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navExport);
            this.navHeader.Buttons.Add(this.navRefresh);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(1198, 40);
            this.navHeader.TabIndex = 11;
            this.navHeader.Text = "tileNavPane1";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Invoice History";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navExport
            // 
            this.navExport.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navExport.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navExport.AppearanceHovered.Options.UseForeColor = true;
            this.navExport.Caption = "Export";
            this.navExport.Name = "navExport";
            this.navExport.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navExport_ElementClick);
            // 
            // navRefresh
            // 
            this.navRefresh.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navRefresh.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navRefresh.AppearanceHovered.Options.UseForeColor = true;
            this.navRefresh.Caption = "Refresh";
            this.navRefresh.Name = "navRefresh";
            this.navRefresh.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navRefresh_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // Gc
            // 
            this.Gc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Gc.Location = new System.Drawing.Point(0, 40);
            this.Gc.MainView = this.Gv;
            this.Gc.Name = "Gc";
            this.Gc.Size = new System.Drawing.Size(1198, 628);
            this.Gc.TabIndex = 12;
            this.Gc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.Gv});
            this.Gc.Load += new System.EventHandler(this.FormInvoice_Load);
            this.Gc.Click += new System.EventHandler(this.Gc_Click);
            // 
            // Gv
            // 
            this.Gv.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Gv.Appearance.GroupPanel.Options.UseFont = true;
            this.Gv.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Gv.Appearance.GroupRow.Options.UseFont = true;
            this.Gv.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Gv.Appearance.HeaderPanel.Options.UseFont = true;
            this.Gv.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.Gv.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Gv.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Gv.Appearance.Row.Options.UseFont = true;
            this.Gv.Appearance.SelectedRow.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Gv.Appearance.SelectedRow.Options.UseFont = true;
            this.Gv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.InvoiceRefNbr,
            this.Descr,
            this.InvoiceTypes,
            this.InvoiceDate,
            this.Status,
            this.gridColumn1,
            this.Qty,
            this.gridColumn2,
            this.Amount,
            this.CustomerName,
            this.SalePersonID});
            this.Gv.GridControl = this.Gc;
            this.Gv.GroupCount = 1;
            this.Gv.Name = "Gv";
            this.Gv.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.Gv.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.Gv.OptionsBehavior.Editable = false;
            this.Gv.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.InvoiceRefNbr, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // InvoiceRefNbr
            // 
            this.InvoiceRefNbr.AppearanceCell.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.InvoiceRefNbr.AppearanceCell.Options.UseBackColor = true;
            this.InvoiceRefNbr.AppearanceHeader.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.InvoiceRefNbr.AppearanceHeader.Options.UseBackColor = true;
            this.InvoiceRefNbr.Caption = "InvoiceRefNbr";
            this.InvoiceRefNbr.FieldName = "InvoiceRefNbr";
            this.InvoiceRefNbr.Name = "InvoiceRefNbr";
            this.InvoiceRefNbr.Visible = true;
            this.InvoiceRefNbr.VisibleIndex = 0;
            // 
            // Descr
            // 
            this.Descr.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.Descr.AppearanceCell.Options.UseBackColor = true;
            this.Descr.AppearanceHeader.BackColor = System.Drawing.Color.Silver;
            this.Descr.AppearanceHeader.Options.UseBackColor = true;
            this.Descr.Caption = "Description";
            this.Descr.FieldName = "Descr";
            this.Descr.Name = "Descr";
            this.Descr.Visible = true;
            this.Descr.VisibleIndex = 0;
            // 
            // InvoiceTypes
            // 
            this.InvoiceTypes.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.InvoiceTypes.AppearanceCell.Options.UseBackColor = true;
            this.InvoiceTypes.AppearanceHeader.BackColor = System.Drawing.Color.Silver;
            this.InvoiceTypes.AppearanceHeader.Options.UseBackColor = true;
            this.InvoiceTypes.Caption = "InvoiceTypes";
            this.InvoiceTypes.FieldName = "InvoiceTypes";
            this.InvoiceTypes.Name = "InvoiceTypes";
            this.InvoiceTypes.Visible = true;
            this.InvoiceTypes.VisibleIndex = 1;
            // 
            // InvoiceDate
            // 
            this.InvoiceDate.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.InvoiceDate.AppearanceCell.Options.UseBackColor = true;
            this.InvoiceDate.AppearanceHeader.BackColor = System.Drawing.Color.Silver;
            this.InvoiceDate.AppearanceHeader.Options.UseBackColor = true;
            this.InvoiceDate.Caption = "InvoiceDate";
            this.InvoiceDate.DisplayFormat.FormatString = "u";
            this.InvoiceDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.InvoiceDate.FieldName = "InvoiceDate";
            this.InvoiceDate.Name = "InvoiceDate";
            this.InvoiceDate.Visible = true;
            this.InvoiceDate.VisibleIndex = 2;
            // 
            // Status
            // 
            this.Status.AppearanceCell.BackColor = System.Drawing.Color.LightGreen;
            this.Status.AppearanceCell.Options.UseBackColor = true;
            this.Status.AppearanceHeader.BackColor = System.Drawing.Color.LightGreen;
            this.Status.AppearanceHeader.Options.UseBackColor = true;
            this.Status.Caption = "Status";
            this.Status.FieldName = "StatusNew";
            this.Status.Name = "Status";
            this.Status.Tag = "R = Revers, C = Close";
            this.Status.Visible = true;
            this.Status.VisibleIndex = 9;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn1.AppearanceCell.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.Critical;
            this.gridColumn1.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn1.AppearanceCell.Options.UseForeColor = true;
            this.gridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.AppearanceHeader.BackColor = System.Drawing.Color.Silver;
            this.gridColumn1.AppearanceHeader.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.Critical;
            this.gridColumn1.AppearanceHeader.Options.UseBackColor = true;
            this.gridColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.gridColumn1.Caption = "Unit Price";
            this.gridColumn1.FieldName = "UnitPrice";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 3;
            // 
            // Qty
            // 
            this.Qty.AppearanceCell.BackColor = System.Drawing.Color.Gray;
            this.Qty.AppearanceCell.ForeColor = System.Drawing.Color.White;
            this.Qty.AppearanceCell.Options.UseBackColor = true;
            this.Qty.AppearanceCell.Options.UseForeColor = true;
            this.Qty.AppearanceHeader.BackColor = System.Drawing.Color.Gray;
            this.Qty.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.Qty.AppearanceHeader.Options.UseBackColor = true;
            this.Qty.AppearanceHeader.Options.UseForeColor = true;
            this.Qty.Caption = "Quantity";
            this.Qty.FieldName = "Qty";
            this.Qty.Name = "Qty";
            this.Qty.Visible = true;
            this.Qty.VisibleIndex = 5;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.gridColumn2.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn2.AppearanceHeader.BackColor = System.Drawing.Color.Silver;
            this.gridColumn2.AppearanceHeader.Options.UseBackColor = true;
            this.gridColumn2.Caption = "Discount Amount";
            this.gridColumn2.FieldName = "DisAmount";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 4;
            // 
            // Amount
            // 
            this.Amount.AppearanceCell.BackColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.Information;
            this.Amount.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.Amount.AppearanceCell.ForeColor = System.Drawing.Color.White;
            this.Amount.AppearanceCell.Options.UseBackColor = true;
            this.Amount.AppearanceCell.Options.UseFont = true;
            this.Amount.AppearanceCell.Options.UseForeColor = true;
            this.Amount.AppearanceCell.Options.UseTextOptions = true;
            this.Amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Amount.AppearanceHeader.BackColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.Information;
            this.Amount.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.Amount.AppearanceHeader.Options.UseBackColor = true;
            this.Amount.AppearanceHeader.Options.UseForeColor = true;
            this.Amount.Caption = "Amount";
            this.Amount.FieldName = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.Visible = true;
            this.Amount.VisibleIndex = 8;
            // 
            // CustomerName
            // 
            this.CustomerName.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.CustomerName.AppearanceCell.Options.UseBackColor = true;
            this.CustomerName.AppearanceHeader.BackColor = System.Drawing.Color.Silver;
            this.CustomerName.AppearanceHeader.Options.UseBackColor = true;
            this.CustomerName.Caption = "Customer ID";
            this.CustomerName.FieldName = "CustomerName";
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.Visible = true;
            this.CustomerName.VisibleIndex = 6;
            // 
            // SalePersonID
            // 
            this.SalePersonID.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.SalePersonID.AppearanceCell.Options.UseBackColor = true;
            this.SalePersonID.AppearanceHeader.BackColor = System.Drawing.Color.Silver;
            this.SalePersonID.AppearanceHeader.Options.UseBackColor = true;
            this.SalePersonID.Caption = "SalePerson ID";
            this.SalePersonID.FieldName = "SalePersonID";
            this.SalePersonID.Name = "SalePersonID";
            this.SalePersonID.Visible = true;
            this.SalePersonID.VisibleIndex = 7;
            // 
            // scrListInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1198, 668);
            this.Controls.Add(this.Gc);
            this.Controls.Add(this.navHeader);
            this.Name = "scrListInvoice";
            this.Text = "Form Invoice";
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraBars.Navigation.NavButton navButton1;
        private DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        private DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navExport;
        private DevExpress.XtraBars.Navigation.NavButton navRefresh;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraGrid.GridControl Gc;
        private DevExpress.XtraGrid.Views.Grid.GridView Gv;
        private DevExpress.XtraGrid.Columns.GridColumn InvoiceRefNbr;
        private DevExpress.XtraGrid.Columns.GridColumn InvoiceTypes;
        private DevExpress.XtraGrid.Columns.GridColumn InvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn Status;
        private DevExpress.XtraGrid.Columns.GridColumn SalePersonID;
        private DevExpress.XtraGrid.Columns.GridColumn Descr;
        private DevExpress.XtraGrid.Columns.GridColumn Qty;
        private DevExpress.XtraGrid.Columns.GridColumn Amount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn CustomerName;
    }
}