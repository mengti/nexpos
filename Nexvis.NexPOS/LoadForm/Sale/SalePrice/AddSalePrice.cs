﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;
using Nexvis.NexPOS.Data;

namespace Nexvis.NexPOS.Sale
{
    public partial class AddSalePrice : DevExpress.XtraEditors.XtraForm
    {
        #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        private scrSalePrice parentForm;

        #endregion

        #region --- Form Action ---
        public AddSalePrice(string transactionType, scrSalePrice objParent)
        {
            InitializeComponent();
            parentForm = objParent;

            if (transactionType == "NEW")
            {
                navEdit.Visible = false;
                isNew = true;
            }
            else if (transactionType == "EDIT")
            {
                navSave.Visible = false;

            }

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;

        }

        #endregion

        #region --- Header Action ---

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Clear value for new
            GlobalInitializer.GsClearValue(txtRecordID, luItem,
                luCurrency, txtSalePrice, dtEffectiveDate,luUOM,txtBreakQty,dtExpDate);

            navEdit.Visible = false;
            navSave.Visible = true;
            isNew = true;
            txtCustomerID.Enabled = false;
        }

        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(luItem,dtEffectiveDate,dtExpDate) == false)
            {
                //    // TODO: Save value to DB.
                parentForm.GsSaveData(this);


                // TODO: Validate which record exiting.
                if (isExiting && parentForm.isSuccessful)
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    luItem.Focus();

                    return;
                }

                if (parentForm.isSuccessful)
                    navNew_ElementClick(sender, e);
                this.Close();
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(luItem, dtEffectiveDate, dtExpDate) == false)
                parentForm.GsUpdateData(this);
           // this.Close();
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close(); // this.Parent.Controls.Remove(this);
            parentForm.navRefresh_ElementClick(sender, e);
        }

        #endregion

        private void txtCustomerName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtCustomerPhone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && navSave.Visible == true)
            {
                navSave_ElementClick(null, null);
            }
            else if (e.KeyCode == Keys.Enter && navSave.Visible == false)
            {
                navEdit_ElementClick(null, null);
            }
        }

        private void luItem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                LookUpEdit textEditor = (LookUpEdit)sender;
                if (textEditor.EditValue != null)
                {         
                    var list = parentForm._PUnitCon.Where(x => x.InventoryID == (int)textEditor.EditValue).ToList();
                    //var listUnit = parentForm._InUnit.Where(x => x.InventoryID == (int)textEditor.EditValue).ToList();
                    GlobalInitializer.GsFillLookUpEdit(list.ToList(), "Unit", "Unit", luUOM, (int)list.ToList().Count);
                    //if(listUnit != null)
                    //    txtRecordID.Text = listUnit.FirstOrDefault().RecordId.ToString();
                    //MessageBox.Show(txtRecordID.Text);
                    luUOM.Refresh();
                    txtAltermate.Text = "";
                }
            }
            catch (Exception ex)
            {
            }
            
        }

        private void luUOM_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ZooEntity DB = new ZooEntity();
                parentForm._InUnit = DB.INUnitConversions.ToList();
                LookUpEdit textEditor = (LookUpEdit)sender;
                if (textEditor.EditValue != null)
                {
                    //NeedFix
                    var getValue = parentForm._InUnit.Where(x => x.InventoryID == Convert.ToInt32(luItem.EditValue) && x.FromUnit == textEditor.EditValue.ToString()).ToList();
                    if (getValue.Count != 0)
                    {
                       // txtRecordID.Text = getValue.FirstOrDefault().RecordId.ToString();
                        txtAltermate.Text = getValue.FirstOrDefault().AltermateID.ToString();
                    }
                    else
                        txtAltermate.Text = "";
                    
                    luUOM.Refresh();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void luItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void luUOM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtSalePrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void luCurrency_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void dtEffectiveDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void dtExpDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && navSave.Visible == true)
            {
                navSave_ElementClick(null, null);
            }
            else if (e.KeyCode == Keys.Enter && navSave.Visible == false)
            {
                navEdit_ElementClick(null, null);
            }
        }

        private void dtEffectiveDate_EditValueChanged(object sender, EventArgs e)
        {
            //if (dtEffectiveDate.EditValue == null) return;
            //if (dtEffectiveDate.DateTime.Date < DateTime.Today) 
            //{
            //    dtEffectiveDate.EditValue = null;
            //    XtraMessageBox.Show("please check date again!.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //} 

        }

        private void dtExpDate_EditValueChanged(object sender, EventArgs e)
        {
            //if (dtExpDate.EditValue == null) return;
            //if (dtExpDate.DateTime.Date < DateTime.Today)
            //{
            //    dtExpDate.EditValue  = null;
            //    XtraMessageBox.Show("please check date again!.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
        }
    }
}