﻿namespace Nexvis.NexPOS.Sale
{
    partial class AddSalePrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navNew = new DevExpress.XtraBars.Navigation.NavButton();
            this.navSave = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtCustomerID = new DevExpress.XtraEditors.TextEdit();
            this.txtSalePrice = new DevExpress.XtraEditors.TextEdit();
            this.luItem = new DevExpress.XtraEditors.LookUpEdit();
            this.luCurrency = new DevExpress.XtraEditors.LookUpEdit();
            this.txtRecordID = new DevExpress.XtraEditors.TextEdit();
            this.dtEffectiveDate = new DevExpress.XtraEditors.DateEdit();
            this.luUOM = new DevExpress.XtraEditors.LookUpEdit();
            this.dtExpDate = new DevExpress.XtraEditors.DateEdit();
            this.txtBreakQty = new DevExpress.XtraEditors.SpinEdit();
            this.txtAltermate = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luCurrency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRecordID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEffectiveDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEffectiveDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luUOM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtExpDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtExpDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBreakQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAltermate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navNew);
            this.navHeader.Buttons.Add(this.navSave);
            this.navHeader.Buttons.Add(this.navEdit);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(575, 40);
            this.navHeader.TabIndex = 9;
            this.navHeader.Text = "tileNavPane1";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Add Sale Price";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navNew
            // 
            this.navNew.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navNew.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navNew.AppearanceHovered.Options.UseForeColor = true;
            this.navNew.Caption = "New";
            this.navNew.Name = "navNew";
            this.navNew.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navSave
            // 
            this.navSave.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navSave.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navSave.AppearanceHovered.Options.UseForeColor = true;
            this.navSave.Caption = "Save";
            this.navSave.Name = "navSave";
            this.navSave.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navSave_ElementClick);
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Update";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtCustomerID);
            this.layoutControl1.Controls.Add(this.txtSalePrice);
            this.layoutControl1.Controls.Add(this.luItem);
            this.layoutControl1.Controls.Add(this.luCurrency);
            this.layoutControl1.Controls.Add(this.txtRecordID);
            this.layoutControl1.Controls.Add(this.dtEffectiveDate);
            this.layoutControl1.Controls.Add(this.luUOM);
            this.layoutControl1.Controls.Add(this.dtExpDate);
            this.layoutControl1.Controls.Add(this.txtBreakQty);
            this.layoutControl1.Controls.Add(this.txtAltermate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem8});
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(711, 234, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(575, 514);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.Location = new System.Drawing.Point(147, 37);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(338, 20);
            this.txtCustomerID.StyleController = this.layoutControl1;
            this.txtCustomerID.TabIndex = 4;
            // 
            // txtSalePrice
            // 
            this.txtSalePrice.Location = new System.Drawing.Point(47, 217);
            this.txtSalePrice.Name = "txtSalePrice";
            this.txtSalePrice.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtSalePrice.Properties.Appearance.Options.UseFont = true;
            this.txtSalePrice.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSalePrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtSalePrice.Properties.Mask.EditMask = "n2";
            this.txtSalePrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSalePrice.Properties.MaxLength = 10;
            this.txtSalePrice.Size = new System.Drawing.Size(493, 28);
            this.txtSalePrice.StyleController = this.layoutControl1;
            this.txtSalePrice.TabIndex = 3;
            this.txtSalePrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSalePrice_KeyDown);
            // 
            // luItem
            // 
            this.luItem.Location = new System.Drawing.Point(47, 31);
            this.luItem.Name = "luItem";
            this.luItem.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luItem.Properties.Appearance.Options.UseFont = true;
            this.luItem.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luItem.Properties.AppearanceDropDown.Options.UseFont = true;
            this.luItem.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luItem.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.luItem.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luItem.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("InventoryCD", "Item Code"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Descr", "Description"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BaseUnit", "BaseUnit")});
            this.luItem.Properties.NullText = "";
            this.luItem.Properties.PopupSizeable = false;
            this.luItem.Properties.ShowFooter = false;
            this.luItem.Size = new System.Drawing.Size(493, 28);
            this.luItem.StyleController = this.layoutControl1;
            this.luItem.TabIndex = 0;
            this.luItem.TextChanged += new System.EventHandler(this.luItem_TextChanged);
            this.luItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luItem_KeyDown);
            // 
            // luCurrency
            // 
            this.luCurrency.EditValue = "USD";
            this.luCurrency.Enabled = false;
            this.luCurrency.Location = new System.Drawing.Point(47, 341);
            this.luCurrency.Name = "luCurrency";
            this.luCurrency.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luCurrency.Properties.Appearance.Options.UseFont = true;
            this.luCurrency.Properties.Appearance.Options.UseTextOptions = true;
            this.luCurrency.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luCurrency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luCurrency.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CuryID", "CuryID")});
            this.luCurrency.Properties.NullText = "USD";
            this.luCurrency.Properties.PopupSizeable = false;
            this.luCurrency.Properties.ShowFooter = false;
            this.luCurrency.Properties.ShowHeader = false;
            this.luCurrency.Size = new System.Drawing.Size(493, 28);
            this.luCurrency.StyleController = this.layoutControl1;
            this.luCurrency.TabIndex = 5;
            this.luCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luCurrency_KeyDown);
            // 
            // txtRecordID
            // 
            this.txtRecordID.Location = new System.Drawing.Point(112, 12);
            this.txtRecordID.Name = "txtRecordID";
            this.txtRecordID.Size = new System.Drawing.Size(357, 20);
            this.txtRecordID.StyleController = this.layoutControl1;
            this.txtRecordID.TabIndex = 11;
            // 
            // dtEffectiveDate
            // 
            this.dtEffectiveDate.EditValue = null;
            this.dtEffectiveDate.Location = new System.Drawing.Point(47, 403);
            this.dtEffectiveDate.Name = "dtEffectiveDate";
            this.dtEffectiveDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.dtEffectiveDate.Properties.Appearance.Options.UseFont = true;
            this.dtEffectiveDate.Properties.Appearance.Options.UseTextOptions = true;
            this.dtEffectiveDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dtEffectiveDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtEffectiveDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtEffectiveDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dtEffectiveDate.Properties.EditFormat.FormatString = "";
            this.dtEffectiveDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtEffectiveDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtEffectiveDate.Size = new System.Drawing.Size(493, 28);
            this.dtEffectiveDate.StyleController = this.layoutControl1;
            this.dtEffectiveDate.TabIndex = 6;
            this.dtEffectiveDate.EditValueChanged += new System.EventHandler(this.dtEffectiveDate_EditValueChanged);
            this.dtEffectiveDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtEffectiveDate_KeyDown);
            // 
            // luUOM
            // 
            this.luUOM.Location = new System.Drawing.Point(47, 93);
            this.luUOM.Name = "luUOM";
            this.luUOM.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luUOM.Properties.Appearance.Options.UseFont = true;
            this.luUOM.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luUOM.Properties.AppearanceDropDown.Options.UseFont = true;
            this.luUOM.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.luUOM.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.luUOM.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luUOM.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Unit", "Unit")});
            this.luUOM.Properties.NullText = "";
            this.luUOM.Properties.PopupSizeable = false;
            this.luUOM.Properties.ShowFooter = false;
            this.luUOM.Properties.ShowHeader = false;
            this.luUOM.Size = new System.Drawing.Size(493, 28);
            this.luUOM.StyleController = this.layoutControl1;
            this.luUOM.TabIndex = 1;
            this.luUOM.EditValueChanged += new System.EventHandler(this.luUOM_TextChanged);
            this.luUOM.TextChanged += new System.EventHandler(this.luUOM_TextChanged);
            this.luUOM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luUOM_KeyDown);
            // 
            // dtExpDate
            // 
            this.dtExpDate.EditValue = null;
            this.dtExpDate.Location = new System.Drawing.Point(47, 464);
            this.dtExpDate.Name = "dtExpDate";
            this.dtExpDate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.dtExpDate.Properties.Appearance.Options.UseFont = true;
            this.dtExpDate.Properties.Appearance.Options.UseTextOptions = true;
            this.dtExpDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dtExpDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtExpDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtExpDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dtExpDate.Properties.EditFormat.FormatString = "";
            this.dtExpDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtExpDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtExpDate.Size = new System.Drawing.Size(493, 28);
            this.dtExpDate.StyleController = this.layoutControl1;
            this.dtExpDate.TabIndex = 7;
            this.dtExpDate.EditValueChanged += new System.EventHandler(this.dtExpDate_EditValueChanged);
            this.dtExpDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtExpDate_KeyDown);
            // 
            // txtBreakQty
            // 
            this.txtBreakQty.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtBreakQty.Enabled = false;
            this.txtBreakQty.Location = new System.Drawing.Point(47, 279);
            this.txtBreakQty.Name = "txtBreakQty";
            this.txtBreakQty.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtBreakQty.Properties.Appearance.Options.UseFont = true;
            this.txtBreakQty.Properties.Appearance.Options.UseTextOptions = true;
            this.txtBreakQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtBreakQty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtBreakQty.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.txtBreakQty.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtBreakQty.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtBreakQty.Size = new System.Drawing.Size(493, 28);
            this.txtBreakQty.StyleController = this.layoutControl1;
            this.txtBreakQty.TabIndex = 4;
            // 
            // txtAltermate
            // 
            this.txtAltermate.Enabled = false;
            this.txtAltermate.Location = new System.Drawing.Point(47, 155);
            this.txtAltermate.Name = "txtAltermate";
            this.txtAltermate.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtAltermate.Properties.Appearance.Options.UseFont = true;
            this.txtAltermate.Properties.MaxLength = 13;
            this.txtAltermate.Size = new System.Drawing.Size(493, 28);
            this.txtAltermate.StyleController = this.layoutControl1;
            this.txtAltermate.TabIndex = 2;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtCustomerID;
            this.layoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(21, 25);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(456, 24);
            this.layoutControlItem1.Text = "Customer ID";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtRecordID;
            this.layoutControlItem8.Location = new System.Drawing.Point(30, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(431, 24);
            this.layoutControlItem8.Text = "RecordID";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(67, 16);
            // 
            // Root
            // 
            this.Root.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.Root.AppearanceItemCaption.Options.UseFont = true;
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem7,
            this.emptySpaceItem8,
            this.emptySpaceItem2,
            this.layoutControlItem9,
            this.emptySpaceItem9,
            this.layoutControlItem10,
            this.emptySpaceItem6,
            this.emptySpaceItem10});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(575, 514);
            this.Root.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(35, 494);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(532, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(23, 494);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.luItem;
            this.layoutControlItem2.Location = new System.Drawing.Point(35, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(497, 51);
            this.layoutControlItem2.Text = "Item";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(67, 16);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.luUOM;
            this.layoutControlItem3.Location = new System.Drawing.Point(35, 62);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(497, 51);
            this.layoutControlItem3.Text = "UOM";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(67, 16);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.txtSalePrice;
            this.layoutControlItem4.Location = new System.Drawing.Point(35, 186);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(497, 51);
            this.layoutControlItem4.Text = "Sale Price";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(67, 16);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.luCurrency;
            this.layoutControlItem5.Location = new System.Drawing.Point(35, 310);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(497, 51);
            this.layoutControlItem5.Text = "Currency";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(67, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.dtEffectiveDate;
            this.layoutControlItem6.Location = new System.Drawing.Point(35, 372);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(497, 51);
            this.layoutControlItem6.Text = "Valid From";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(67, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.dtExpDate;
            this.layoutControlItem7.Location = new System.Drawing.Point(35, 433);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(497, 51);
            this.layoutControlItem7.Text = "Valid To";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(67, 16);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(35, 51);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(497, 11);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(35, 175);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(497, 11);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(35, 299);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(497, 11);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(35, 361);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(497, 11);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(35, 423);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(497, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.txtBreakQty;
            this.layoutControlItem9.Location = new System.Drawing.Point(35, 248);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(497, 51);
            this.layoutControlItem9.Text = "Break QTY";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(67, 16);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(35, 237);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(497, 11);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.txtAltermate;
            this.layoutControlItem10.Location = new System.Drawing.Point(35, 124);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(497, 51);
            this.layoutControlItem10.Text = "Altermate";
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(67, 16);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(35, 113);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(497, 11);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(35, 484);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(497, 10);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // AddSalePrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 554);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Name = "AddSalePrice";
            this.Text = "Add Price";
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalePrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luCurrency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRecordID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEffectiveDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEffectiveDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luUOM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtExpDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtExpDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBreakQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAltermate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navNew;
        private DevExpress.XtraBars.Navigation.NavButton navSave;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        internal DevExpress.XtraEditors.TextEdit txtCustomerID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        internal DevExpress.XtraEditors.TextEdit txtSalePrice;
        internal DevExpress.XtraEditors.LookUpEdit luItem;
        internal DevExpress.XtraEditors.LookUpEdit luCurrency;
        internal DevExpress.XtraEditors.TextEdit txtRecordID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        internal DevExpress.XtraEditors.LookUpEdit luUOM;
        internal DevExpress.XtraEditors.DateEdit dtEffectiveDate;
        internal DevExpress.XtraEditors.DateEdit dtExpDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        internal DevExpress.XtraEditors.SpinEdit txtBreakQty;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        internal DevExpress.XtraEditors.TextEdit txtAltermate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
    }
}