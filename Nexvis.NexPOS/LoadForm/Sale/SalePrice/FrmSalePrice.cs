﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;
using ZooMangement;
using DevExpress.XtraGrid.Views.Grid;
using ZooManagement;
using Nexvis.NexPOS.Data;
using ZooManagement.Helper;
using Nexvis.NexPOS.Data;
using DgoStore;
using Nexvis.NexPOS.Helper;
using DevExpress.XtraBars.Navigation;

namespace Nexvis.NexPOS.Sale
{
    public partial class scrSalePrice : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrSalePrice()
        {
            InitializeComponent();

        }

        List<MDSalesPrice> _SalePrice = new List<MDSalesPrice>();
        List<CSUOM> cSUOMs = new List<CSUOM>();
        public  List<INItem> _Initem = new List<INItem>();
        List<CSCurrency> _CSCurrency = new List<CSCurrency>();
        public List<INUnitConversion> _InUnit = new List<INUnitConversion>();
        public List<ProcedUnitConversion_Result> _PUnitCon = new List<ProcedUnitConversion_Result>();
        List<ZooInfo> _Zoo = new List<ZooInfo>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
       
        public bool isSuccessful = true;

        private void FormCustomer_Load(object sender, EventArgs e)
        {
            _Initem = DB.INItems.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            _InUnit = DB.INUnitConversions.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            _CSCurrency = DB.CSCurrencies.ToList();
            cSUOMs = DB.CSUOMs.ToList();
            _PUnitCon = DB.ProcedUnitConversion().ToList();
            _SalePrice = DB.MDSalesPrices.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            GC.DataSource = _SalePrice.ToList();

            _RoleItems = DB.CSRoleItems.ToList();

            _Zoo = DB.ZooInfoes.ToList();
            CSShopModel.ZooID = _Zoo.FirstOrDefault().ZooInfoID;
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GV.RowCount > 0)
            {
                // TODO: Export data in GridControl as Excel file.
                GlobalInitializer.GsExportData(GC);
            }
            else
                XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormCustomer_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }
        
        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    MDSalesPrice result = new MDSalesPrice();
                    result = (MDSalesPrice)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {
                            MDSalesPrice customer = DB.MDSalesPrices.FirstOrDefault(st => st.RecordID.Equals(result.RecordID));

                            DB.MDSalesPrices.Remove(customer);
                            DB.SaveChanges();
                            FormCustomer_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                //NavButton navigator = (NavButton)sender;
                var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    AddSalePrice objForm = new AddSalePrice("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    GlobalInitializer.GsFillLookUpEdit(_Initem.ToList(), "InventoryID", "InventoryCD", objForm.luItem, (int)_Initem.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(_CSCurrency.ToList(), "CuryID", "CuryID", objForm.luCurrency, (int)_CSCurrency.ToList().Count);
                    // GlobalInitializer.GsFillLookUpEdit(_InUnit.ToList(), "UOMID", "UOMID", objForm.luUOM, (int)_InUnit.ToList().Count);

                    MDSalesPrice result = new MDSalesPrice();

                    result = (MDSalesPrice)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        //objForm.luUOM.Enabled = false;
                        objForm.txtRecordID.Text = result.RecordID.ToString();
                        objForm.luItem.Focus();
                        objForm.luItem.EditValue = result.InventoryID;
                        objForm.txtBreakQty.EditValue = result.BreakQty;
                        objForm.luCurrency.EditValue = result.CuryID;
                        objForm.luUOM.EditValue = result.UOM;
                        objForm.txtAltermate.Text = result.AlternateID;
                        objForm.txtSalePrice.EditValue = result.SalesPrice;
                        objForm.dtEffectiveDate.EditValue = result.EffectiveDate;
                        objForm.dtExpDate.Text = result.ExpirationDate.ToString();
                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    navRefresh_ElementClick(sender, e);
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    AddSalePrice objForm = new AddSalePrice("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    GlobalInitializer.GsFillLookUpEdit(_Initem.ToList(), "InventoryID", "InventoryCD", objForm.luItem, (int)_Initem.ToList().Count);
                    GlobalInitializer.GsFillLookUpEdit(_CSCurrency.ToList(), "CuryID", "CuryID", objForm.luCurrency, (int)_CSCurrency.ToList().Count);
                    // GlobalInitializer.GsFillLookUpEdit(cSUOMs.ToList(), "UOMID", "UOMID", objForm.luUOM, (int)cSUOMs.ToList().Count);
                    // objForm._cities = _cities;  
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Actions
        public void GsSaveData(AddSalePrice objChild)
        {
            try
            {
                objChild.isExiting = false;
                if (_SalePrice.Any(x => x.RecordID.ToString() == objChild.txtRecordID.Text.Trim().ToLower()))
                {
                    objChild.isExiting = true;
                    return;
                }

                if (objChild.dtExpDate.EditValue == null || objChild.dtExpDate == null) return;
                if (objChild.dtExpDate.DateTime.Date > DateTime.Today)
                {
                    MDSalesPrice salesPrice = new MDSalesPrice()
                    {
                        CompanyID = DACClasses.CompanyID,
                        InventoryID = (int)objChild.luItem.EditValue,
                        CustomerID = 1,
                        AlternateID = objChild.txtAltermate.Text,
                        SalesPrice = Convert.ToDecimal(objChild.txtSalePrice.EditValue),
                        CuryID = objChild.luCurrency.EditValue.ToString(),
                        UOM = objChild.luUOM.EditValue.ToString(),
                        PriceType = "BASE",
                        BreakQty = Convert.ToDecimal(objChild.txtBreakQty.EditValue),
                        SiteID = Convert.ToInt16(CSShopModel.ZooID),
                        EffectiveDate = objChild.dtEffectiveDate.DateTime.Date,
                        ExpirationDate = DateTimeConvert.ConvertStringToDate(objChild.dtExpDate.Text)

                    };

                    var checkList = _SalePrice.Where(x => x.InventoryID == salesPrice.InventoryID && x.UOM == salesPrice.UOM
                        && x.EffectiveDate <= DateTime.Today.Date && x.ExpirationDate > DateTime.Today.Date).ToList();

                    if (checkList.Count != 0)
                    {
                        if (salesPrice.EffectiveDate >= checkList.FirstOrDefault().EffectiveDate
                           && salesPrice.EffectiveDate <= checkList.FirstOrDefault().ExpirationDate
                           && salesPrice.EffectiveDate >= DateTime.Today.Date)
                        {

                            XtraMessageBox.Show("This item have ready.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }

                    DB.MDSalesPrices.Add(salesPrice);
                    var Row = DB.SaveChanges();

                    if (Row == 1)
                    {
                        isSuccessful = true;
                    }
                    GV.FocusedRowHandle = 0;
                    navRefresh_ElementClick(null, null);
                    XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    objChild.dtExpDate.EditValue = null;
                    XtraMessageBox.Show("please check date again!.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        
        }
        public void GsUpdateData(AddSalePrice objChild)
        {
            try
            {
                //if (_SalePrice.Any(x => x.RecordID.ToString() != objChild.txtRecordID.Text.Trim().ToLower()
                //&& x.InventoryID == (int)objChild.luItem.EditValue))
                //{
                //    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    objChild.luItem.Focus();
                //    return;
                //}


                if (objChild.dtExpDate.EditValue == null || objChild.dtExpDate == null) return;
                if (objChild.dtEffectiveDate.DateTime.Date <= DateTime.Today && objChild.dtExpDate.DateTime.Date > DateTime.Today)
                {
                    if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;

                    var _obj = DB.MDSalesPrices.FirstOrDefault(w => w.RecordID.ToString() == (objChild.txtRecordID.Text) && w.CompanyID.Equals(DACClasses.CompanyID));

                    _obj.InventoryID = Convert.ToInt32(objChild.luItem.EditValue);
                    _obj.AlternateID = objChild.txtAltermate.Text;
                    _obj.CuryID = objChild.luCurrency.EditValue.ToString();
                    _obj.UOM = objChild.luUOM.EditValue.ToString();
                    _obj.BreakQty = Convert.ToDecimal(objChild.txtBreakQty.EditValue);
                    _obj.SalesPrice = Convert.ToDecimal(objChild.txtSalePrice.EditValue);
                    _obj.EffectiveDate = (DateTime)objChild.dtEffectiveDate.DateTime.Date;
                    _obj.ExpirationDate = DateTimeConvert.ConvertStringToDate(objChild.dtExpDate.Text);

                    DB.MDSalesPrices.Attach(_obj);
                    DB.Entry(_obj).Property(w => w.InventoryID).IsModified = true;
                    DB.Entry(_obj).Property(w => w.AlternateID).IsModified = true;
                    DB.Entry(_obj).Property(w => w.UOM).IsModified = true;
                    DB.Entry(_obj).Property(w => w.CuryID).IsModified = true;
                    DB.Entry(_obj).Property(w => w.SalesPrice).IsModified = true;
                    DB.Entry(_obj).Property(w => w.EffectiveDate).IsModified = true;
                    DB.Entry(_obj).Property(w => w.ExpirationDate).IsModified = true;

                    var Row = DB.SaveChanges();
                    XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    navRefresh_ElementClick(null, null);
                    objChild.Close();


                }
                else
                {
                    objChild.dtExpDate.EditValue = null;
                    XtraMessageBox.Show("please check date again!.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void navExport_ElementClick_1(object sender, NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}