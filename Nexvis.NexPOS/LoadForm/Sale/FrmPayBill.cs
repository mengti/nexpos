﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;
using Nexvis.NexPOS.Data;
using ZooMangement;
using DevExpress.Utils;

namespace DgoStore.LoadForm.Sale
{
    public partial class FrmPayBill : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();

        public bool IsLogg { get; set; }
        public bool CheckDiscount { get; set; }

        private scrSaleOrder _objParent;
        private bool isCaseInUSDFocused;
        private bool isCaseInRielFocused;
        public decimal ExchangeRate;
        public decimal TotalPayment;
        public decimal DisTotalPayment;


        public bool isSuccessful = true;
        ClsGlobal _obj = new ClsGlobal(); 
        public  List<SaleProductDomain> SalePRoduct = new List<SaleProductDomain>(); 
         
        public List<CSPaymentMethod> cSPaymentMethods = new List<CSPaymentMethod>();

        public FrmPayBill(scrSaleOrder objParent)
        {
            InitializeComponent();
          //  checkEdit1_CheckedChanged(null,null);

            _objParent = objParent;
            cSPaymentMethods = DB.CSPaymentMethods.ToList();

            GlobalInitializer.GsFillLookUpEdit(cSPaymentMethods.ToList(), "PaymentMethodID", "PaymentMethodID", cboPaymentType, (int)cSPaymentMethods.ToList().Count);

        }

        private void FrmPayment_Load(object sender, EventArgs e)
        {
            this.SuspendLayout();
            txtDisGrandD.EditValue = txtGrandTotalUSD.EditValue;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            //Application.Exit();
            IsLogg = false;
            this.Close();
            
        }

        private void btnNumberOne_Click(object sender, EventArgs e)
        {
            //SetTextValue(sender, e);
            if(isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberOne.Text;
            }
            else if(isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberOne.Text;
            }
            btnDot_Click(sender, e);

        }
        private void btnNumberTwo_Click(object sender, EventArgs e)
        {
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberTwo.Text;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberTwo.Text;
            }
            btnDot_Click(sender, e);
        }

        private void btnNumberThree_Click(object sender, EventArgs e)
        {
            //SetTextValue(sender, e);
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberThree.Text;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberThree.Text;
            }
            btnDot_Click(sender, e);
        }

        private void btnNumberFour_Click(object sender, EventArgs e)
        {
            // txtCashInUSD.Text = txtCashInUSD.Text + btnNumberFour.Text;
            //SetTextValue(sender, e);
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberFour.Text;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberFour.Text;
            }
            btnDot_Click(sender, e);
        }

        private void btnNumberFive_Click(object sender, EventArgs e)
        {
            //SetTextValue(sender, e);
          
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberFive.Text;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberFive.Text;
            }
            btnDot_Click(sender, e);
        }

        private void btnNumberSix_Click(object sender, EventArgs e)
        {
            // SetTextValue(sender, e);
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberSix.Text;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberSix.Text;
            }
            btnDot_Click(sender, e);
        }

        private void btnNumberSeven_Click(object sender, EventArgs e)
        {
          
            //SetTextValue(sender, e);
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberSeven.Text;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberSeven.Text;
            }
            btnDot_Click(sender, e);
        }

        private void btnNumberEight_Click(object sender, EventArgs e)
        {
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberEight.Text;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberEight.Text;
            }
            btnDot_Click(sender, e);
        }

        private void btnNumberNine_Click(object sender, EventArgs e)
        {
         
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberNine.Text;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberNine.Text;
            }
            btnDot_Click(sender, e);
        }

        private void btnNumberZero_Click(object sender, EventArgs e)
        {
           
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.Text = txtCashInUSD.Text + btnNumberZero.Text;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.Text = txtCashInRiel.Text + btnNumberZero.Text;
            }
            btnDot_Click(sender, e);
        }

        private void btnDot_Click(object sender, EventArgs e)
        {
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.Focus();
                txtCashInUSD.SelectionStart = txtCashInUSD.Text.Length;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.Focus();
                txtCashInRiel.SelectionStart = txtCashInRiel.Text.Length;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (isCaseInUSDFocused)
            {
                txtCashInUSD.EditValue = 0;
                txtCashInUSD.Focus();
                txtCashInUSD.SelectionStart = txtCashInUSD.Text.Length;
            }
            else if (isCaseInRielFocused)
            {
                txtCashInRiel.EditValue = 0;
                txtCashInRiel.Focus();
                txtCashInRiel.SelectionStart = txtCashInRiel.Text.Length;
            }
        }

        private void txtCashInUSD_Enter(object sender, EventArgs e)
        {
            isCaseInUSDFocused = true;
            isCaseInRielFocused = false;
        }

        private void txtChangeUSD_Enter(object sender, EventArgs e)
        {
            isCaseInUSDFocused = false;
            isCaseInRielFocused = false;
        }

        private void txtChangeRiel_Enter(object sender, EventArgs e)
        {
            isCaseInUSDFocused = false;
            isCaseInRielFocused = false;
        }

        private void txtCashInRiel_Enter(object sender, EventArgs e)
        {
            isCaseInRielFocused = true;
            isCaseInUSDFocused = false;
        }

        private void txtCashInUSD_TextChanged(object sender, EventArgs e)
        {
            CashChange();
        }

        private void txtCashInRiel_TextChanged(object sender, EventArgs e)
        {
            CashChange();

        }
        
        private void btnEnter_Click(object sender, EventArgs e)
        {
            IsLogg = true;
            if (GlobalInitializer.GfCheckNullValue(txtCashInUSD, txtCashInRiel))
                return;
            else
            {

                if (txtDisAmount.EditValue != "0" || txtDiscPercent.EditValue != "0")
                {
                    CheckDiscount = true;

                }
                else CheckDiscount = false;

                if ((Convert.ToInt32(txtCashInUSD.EditValue)<0 || Convert.ToInt32(txtCashInRiel.EditValue) <0 ) || (Convert.ToInt32(txtCashInUSD.EditValue) <= 0 && Convert.ToInt32(txtCashInRiel.EditValue) <= 0))
                {
                    XtraMessageBox.Show("The CASH IN ($) and CASH IN (៛) must be bigger than zero !", "Invoice Payment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    txtCashInUSD.Focus();
                    return;
                }
                else
                {
                    _objParent.lblCashInUSD.Text = string.Format("{0} : {1:N0} {2}", "Cash In ($)", Convert.ToDecimal(txtCashInUSD.EditValue), "$");
                    _objParent.lblCashInRiel.Text = string.Format("{0} : {1:N0} {2}", "Cash In (៛)", Convert.ToDecimal(txtCashInRiel.EditValue), "៛");
                    _objParent.lblChangeUSD.Text = string.Format("{0} : {1:N0} {2}", "Change ($)", Convert.ToDecimal(txtChangeUSD.EditValue), "$");
                    _objParent.lblChangeRiel.Text = string.Format("{0} : {1:N0} {2}", "Change (៛)", Convert.ToDecimal(txtChangeRiel.EditValue), "៛");

                    _objParent._SaleProducts.ForEach(x => x.CashInUSD = Convert.ToDecimal(txtCashInUSD.EditValue));
                    _objParent._SaleProducts.ForEach(x => x.CashInRiel = Convert.ToDecimal(txtCashInRiel.EditValue));
                    _objParent._SaleProducts.ForEach(x => x.ChangeUSD = Convert.ToDecimal(txtChangeUSD.EditValue));
                    _objParent._SaleProducts.ForEach(x => x.ChangeRiel = Convert.ToDecimal(txtChangeRiel.EditValue));
      

                     this.Close();
                }
            }

        }

        #region --- Internal Sub Procedure ---
        private void SetTextValue(object sender, EventArgs e)
        {
            txtCashInUSD.Text = btnNumberOne.Text;
        }
        //TODO: Change Cash for the remaining only riel
        //private void CashChange()
        //{
        //    try
        //    {
        //        decimal CashInUSD = Convert.Todecimal(txtCashInUSD.Text == "" ? 0 : txtCashInUSD.EditValue);
        //        decimal CashInRiel = Convert.Todecimal(txtCashInRiel.Text == "" ? 0 : txtCashInRiel.EditValue);
        //        if (CashInUSD + (CashInRiel / ExchangeRate) >= TotalPayment)
        //        {
        //            decimal CashInUSDRemaining = 0;
        //            CashInUSDRemaining = CashInUSD + (CashInRiel / ExchangeRate) - TotalPayment;
        //            decimal ChangeUSD = Convert.ToInt32(CashInUSDRemaining.ToString().Split('.')[0]);
        //            txtChangeUSD.EditValue = ChangeUSD;
        //            txtChangeRiel.EditValue = (decimal)(CashInUSDRemaining - ChangeUSD) * ExchangeRate;
        //        }
        //        else
        //        {
        //            txtChangeUSD.EditValue = 0;
        //            txtChangeRiel.EditValue = CashInRiel - (decimal)(TotalPayment - CashInUSD) * ExchangeRate;
        //        }
        //        if ((decimal)txtChangeRiel.EditValue >= 0)
        //            txtChangeRiel.EditValue = _obj.RoundRielAmountDown(Math.Round((decimal)txtChangeRiel.EditValue, 0));
        //        else
        //            txtChangeRiel.EditValue = _obj.RoundRielAmountUp(Math.Round((decimal)txtChangeRiel.EditValue, 0) * -1) * -1;
        //    }
        //    catch (Exception ex)
        //    {
        //        XtraMessageBox.Show(ex.Message, "Invoice Payment Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        //TODO: Change Cash for the remaining Dollar and Riel
        private Int32 FractionalPart(decimal n)
        {
            string s = n.ToString("#.#########", System.Globalization.CultureInfo.InvariantCulture);
            return Int32.Parse(s.Substring(s.IndexOf(".") + 1));
        }
        private void CashChange()
        {
            try
            {
                //   txtCashInUSD.EditValue = (decimal)txtCashInUSD.EditValue - (decimal)txtChangeUSD.EditValue;
                decimal CashInUSD = Convert.ToDecimal(txtCashInUSD.Text == "" ? 0 : txtCashInUSD.EditValue);
                decimal CashInRiel = Convert.ToDecimal(txtCashInRiel.Text == "" ? 0 : txtCashInRiel.EditValue);
                decimal CashInUSDRemaining = 0; 
                decimal CashInReilRemaining = 0;
                decimal ChangeUSD = 0;
                decimal ChangeReil = 0;

               // decimal DisDolla = 0;

                if ((CashInUSD+CashInRiel)>0)
                {
                    if(txtDisAmount.EditValue != null || txtDiscPercent.EditValue != null)
                    {

                        DisTotalPayment = TotalPayment - Convert.ToDecimal(txtDisAmount.EditValue);

                        CashInUSDRemaining = CashInUSD + (CashInRiel / ExchangeRate) - DisTotalPayment;
                        CashInReilRemaining = CashInUSDRemaining * ExchangeRate;

                        //   ChangeUSD = CashInUSDRemaining;
                        ChangeUSD = Convert.ToDecimal(CashInUSDRemaining.ToString().Split('.')[0]);
                        ChangeReil = Convert.ToInt32(CashInReilRemaining.ToString().Split('.')[0]);

                        
    
                       // ChangeUSD = FractionalPart(ChangeUSD);

                        decimal numberM = Convert.ToDecimal(CashInUSDRemaining);
                        decimal fraction = 0;
                        if (numberM < 0)
                        {
                             fraction = - Math.Abs(numberM - Math.Truncate(numberM));
                        }
                        else
                        {
                             fraction = Math.Abs(numberM - Math.Truncate(numberM));
                        }
                        
                        //int mantissa = Convert.ToInt32((double)fraction * Math.Pow(10, fraction.ToString().Length - 2));

                        txtChangeRiel.EditValue = fraction * ExchangeRate;
                        txtChangeUSD.EditValue = ChangeUSD.ToString();
                        //txtChangeRiel.EditValue = (decimal)(CashInUSDRemaining - ChangeUSD) * ExchangeRate;



                        if ((decimal)txtChangeRiel.EditValue >= 0)
                            txtChangeRiel.EditValue = _obj.RoundRielAmountDown(Math.Round((decimal)txtChangeRiel.EditValue, 0));
                        else
                            txtChangeRiel.EditValue = _obj.RoundRielAmountUp(Math.Round((decimal)txtChangeRiel.EditValue, 0) * -1) * -1;
                     }
                    else
                    {
                        CashInUSDRemaining = CashInUSD + (CashInRiel / ExchangeRate) - TotalPayment;
                        CashInReilRemaining = CashInUSDRemaining * ExchangeRate;

                        //   ChangeUSD = CashInUSDRemaining;
                        ChangeUSD = Convert.ToDecimal(CashInUSDRemaining.ToString());
                        ChangeReil = Convert.ToInt32(CashInReilRemaining.ToString().Split('.')[0]);

                        txtChangeUSD.EditValue = ChangeUSD.ToString();
                        txtChangeRiel.EditValue = ChangeReil;
                        //txtChangeRiel.EditValue = (decimal)(CashInUSDRemaining - ChangeUSD) * ExchangeRate;

                        if ((decimal)txtChangeRiel.EditValue >= 0)
                            txtChangeRiel.EditValue = _obj.RoundRielAmountDown(Math.Round((decimal)txtChangeRiel.EditValue, 0));
                        else
                            txtChangeRiel.EditValue = _obj.RoundRielAmountUp(Math.Round((decimal)txtChangeRiel.EditValue, 0) * -1) * -1;
                    }      
                }
                else
                {
                    txtChangeUSD.EditValue = 0;
                    txtChangeRiel.EditValue = 0;
                }
             //   objPayBill.txtChangeUSD.EditValue = (_SaleProducts.SingleOrDefault().ExchangeRate * (Convert.ToDecimal(objPayBill.txtCashInRiel.EditValue)));
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Invoice Payment Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        #endregion

        private void txtGrandTotalRiel_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void txtDisGrandD_TextChanged(object sender, EventArgs e)
        {
            // decimal CashInUSD = Convert.ToDecimal(txtCashInUSD.Text == "" ? 0 : txtCashInUSD.EditValue);
            // decimal CashInRiel = Convert.ToDecimal(txtCashInRiel.Text == "" ? 0 : txtCashInRiel.EditValue);
           // decimal DisGrandDolla = 0;
            decimal DisPercent = Convert.ToDecimal(txtDiscPercent.Text == "" ? 0 : txtDiscPercent.EditValue);
            decimal DisAmount = Convert.ToDecimal(txtDisAmount.Text == "" ? 0 : txtDisAmount.EditValue);

            try
            {
                txtDisGrandD.EditValue = Convert.ToDecimal(txtGrandTotalUSD.EditValue) - Convert.ToDecimal(txtDisAmount.EditValue);
                //txtDisGrandD.EditValue = DisGrandDolla;
                txtDisGrandR.EditValue = Convert.ToDecimal(txtDisGrandD.EditValue) * ExchangeRate;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void txtDisAmount_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                CashChange();
                txtDisGrandD.EditValue = Convert.ToDecimal(txtGrandTotalUSD.EditValue) - Convert.ToDecimal(txtDisAmount.EditValue);
                txtDiscPercent.EditValue =  (Convert.ToDecimal(txtDisAmount.EditValue) / Convert.ToDecimal(txtGrandTotalUSD.EditValue)) * 100;
               
            }catch(Exception ex)
            {

            }
        }

        private void txtDiscPercent_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                //DiscountPrice = FullPrice - (FullPrice * DiscountPercent);
                CashChange();
                txtDisAmount.EditValue = (Convert.ToDecimal(txtGrandTotalUSD.EditValue) *  Convert.ToDecimal(txtDiscPercent.EditValue) / 100);
                txtDisGrandD.EditValue = Convert.ToDecimal(txtGrandTotalUSD.EditValue) - Convert.ToDecimal(txtDisAmount.EditValue) * Convert.ToDecimal(txtDiscPercent.EditValue);
                
            }
            catch (Exception ex)
            {

            }
        }

        private void txtCashInUSD_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            //if(checkEdit1.Checked == false)
            //{
            //    txtDisAmount.Enabled = false;
            //    txtDiscPercent.Enabled = false;
            //}
            //else
            //{
            //    txtDisAmount.Enabled = true;
            //    txtDiscPercent.Enabled = true;
            //}
           
        }

        private void txtCashInUSD_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnEnter.PerformClick();
            }
        }

        private void txtCashInRiel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnEnter.PerformClick();
            }
        }
    }
}