﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using DgoStore.AddForm.NewVersion;
using Nexvis.NexPOS.Helper;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Grid;
using ZooMangement;
using ZooManagement.Helper;
using ZooManagement;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm
{
    public partial class scrStoreInfor : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();


        public scrStoreInfor()
        {
            InitializeComponent();

        }

        List<ZooInfo> _ZooInfo = new List<ZooInfo>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
       // List<tb_Category> _Category = new List<tb_Category>();


        public bool isSuccessful = true;

        private void FormStoreInfo_Load(object sender, EventArgs e)
        {
            _ZooInfo = DB.ZooInfoes.ToList();
            Gc.DataSource = _ZooInfo.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
        }
        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormStoreInfo_Load(sender, e);

            //TODO : Clear in field for filter.
            Gv.ClearColumnsFilter();
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    ZooInfo result = new ZooInfo();
                    result = (ZooInfo)Gv.GetRow(Gv.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {

                            ZooInfo cSCompany = DB.ZooInfoes.FirstOrDefault(st => st.ZooInfoID.Equals(result.ZooInfoID));

                            DB.ZooInfoes.Remove(cSCompany);
                            DB.SaveChanges();
                            FormStoreInfo_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

               

            }

            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {

                // NavButton navigator = (NavButton)sender;
                var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    AddFormStoreInfo objForm = new AddFormStoreInfo("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //  GlobalInitializer.GsFillLookUpEdit(_Category.ToList(), "CateID", "CateName", objForm.luCategory, (int)_Category.ToList().Count);


                    ZooInfo result = new ZooInfo();

                    result = (ZooInfo)Gv.GetRow(Gv.FocusedRowHandle);

                    CSShopModel.ZooName = objForm.txtStoreName.Text;
                    CSShopModel.ZooAddress = objForm.txtAddress.Text;
                    CSShopModel.ZooPhone = objForm.txtContact.Text;
                   
                    if (result != null)
                    {
                        objForm.txtStoreID.Enabled = false;
                        // objForm.txtItemID.Enabled = false;
                        //objForm.txtSto.Focus();
                        objForm.txtStoreID.Text = result.ZooInfoID.ToString();
                        objForm.txtStoreName.Text = result.ZooInfoName;
                        objForm.dtRegisterDate.Text = result.RegisterDate.ToString();
                        objForm.tStartTime.EditValue = result.StartTime;
                        objForm.tCloseTime.EditValue = result.CloseTime;
                        objForm.txtSpokenLanguage.Text = result.SpokenLanguage.ToString();

                        objForm.txtOpenDay.Text = result.OpenDay.ToString();
                        objForm.txtRate.Text = result.Rate.ToString();

                        objForm.txtAtmosphere.Text = result.Atmosphere;
                        objForm.txtShortAddress.Text = result.ShortAddress;
                        objForm.txtContact.Text = result.Contact;
                        objForm.txtEmail.Text = result.Email;

                        objForm.txtWebsite.Text = result.Website;

                        objForm.txtAddress.Text = result.Address;
                        objForm.txtInformation.Text = result.Information;

                    //    objForm.pbTimeline.Image = result.UrlImageTimeLine == null ? objForm.pbTimeline.Image : DACClasses.ReadImage(result.UrlImageTimeLine);

                        objForm.pbLogo.Image = result.UrlImageLogo == null ? objForm.pbLogo.Image : DACClasses.ReadImage(result.UrlImageLogo);
                    
                        Shadow.ShowDialog();

                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    AddFormStoreInfo objForm = new AddFormStoreInfo("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //objForm.txtStoreID.Text = "-1";
                    objForm.txtStoreID.Enabled = false;
                    objForm.pbLogo.Image = null;
                  //  objForm.pbTimeline.Image = null;

                    //TODO: Binding value to comboBox or LookupEdit.
                    //GlobalInitializer.GsFillLookUpEdit(_Category.ToList(), "CateID", "CateName", objForm.luCategory, (int)_Category.ToList().Count);

                    Shadow.ShowDialog();

                    FormStoreInfo_Load(null, null);
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Actions
        public void GsSaveData(AddFormStoreInfo objChild)
        {

            objChild.isExiting = false;
            if (_ZooInfo.Any(x => x.ZooInfoID.ToString() == objChild.txtStoreID.Text.Trim().ToLower()))
            {
                objChild.isExiting = true;
                return;
            }
            StoreModel storeModel = new StoreModel();

            try
            {
               // var resul = new CFNumberRank("SH", DocConfType.Normal, true);
                ZooInfo item = new ZooInfo()
                {

                    //ItemID = //int.Parse(objChild.txtItemID.Text),Max(p => p.ItemID) + 1
                  //  ZooInfoID = resul.NextNumberRank.Trim(),
                    ZooInfoName = objChild.txtStoreName.Text,
                    RegisterDate = objChild.dtRegisterDate.DateTime,
                    StartTime = objChild.tStartTime.Time,
                    CloseTime = objChild.tStartTime.Time,
                    SpokenLanguage = objChild.txtSpokenLanguage.Text,
                    OpenDay = objChild.txtOpenDay.Text,
                    Rate = Convert.ToDecimal(objChild.txtRate.Text),
                   // StoreTypeID = objChild.txtStoreTypeID.Text,
                    Atmosphere = objChild.txtAtmosphere.Text,
                    ShortAddress = objChild.txtShortAddress.Text,

                    Contact = objChild.txtContact.Text,
                    Email = objChild.txtEmail.Text,
                    Website = objChild.txtWebsite.Text,
                    Address = objChild.txtAddress.Text,
                    Information = objChild.txtInformation.Text,
                    UrlImageLogo = DACClasses.ReturnImage(objChild.pbLogo)
                 //   UrlImageTimeLine = DACClasses.ReturnImage(objChild.pbTimeline)

                };

                DB.ZooInfoes.Add(item);
                var Row = DB.SaveChanges();
                if (Row == 1)
                {
                    isSuccessful = true;
                }
                Gv.FocusedRowHandle = 0;
                XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public void GsUpdateData(AddFormStoreInfo objChild)
        {
            try
            {
                if (_ZooInfo.Any(x => x.ZooInfoID.ToString() != objChild.txtStoreID.Text.Trim().ToLower()
                && x.ZooInfoName.Trim().ToLower() == objChild.txtStoreName.Text.Trim().ToLower()))
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtStoreName.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;



                var _obj = DB.ZooInfoes.SingleOrDefault(w => w.ZooInfoID.ToString() == objChild.txtStoreID.Text && w.CompanyID.Equals(DACClasses.CompanyID));
        
                _obj.ZooInfoID = Convert.ToInt32(objChild.txtStoreID.Text);
                _obj.ZooInfoName = objChild.txtStoreName.Text;
                _obj.RegisterDate = objChild.dtRegisterDate.DateTime.Date;
                _obj.StartTime = objChild.tStartTime.Time;
                _obj.CloseTime = objChild.tCloseTime.Time;
                _obj.SpokenLanguage = objChild.txtSpokenLanguage.Text;
     
                _obj.OpenDay = objChild.txtOpenDay.Text;
                _obj.Rate = Convert.ToDecimal(objChild.txtRate.Text);

                _obj.Atmosphere = objChild.txtAtmosphere.Text;
                _obj.ShortAddress = objChild.txtShortAddress.Text;
                _obj.Contact = objChild.txtContact.Text;
                _obj.Email = objChild.txtEmail.Text;

                _obj.Website = objChild.txtWebsite.Text;
                _obj.Address = objChild.txtAddress.Text;
                _obj.Information = objChild.txtInformation.Text;

                _obj.UrlImageLogo = DACClasses.ReturnImage(objChild.pbLogo);
              //  _obj.UrlImageTimeLine = DACClasses.ReturnImage(objChild.pbTimeline);
                CSShopModel.ShopLogo = _obj.UrlImageLogo;
                CSShopModel.ZooName = _obj.ZooInfoName;
                CSShopModel.ZooID = _obj.ZooInfoID;
                CSShopModel.ZooAddress = _obj.Address;
                CSShopModel.ZooPhone = _obj.Contact;

                DB.ZooInfoes.Attach(_obj);

                DB.Entry(_obj).Property(w => w.ZooInfoName).IsModified = true;
                DB.Entry(_obj).Property(w => w.RegisterDate).IsModified = true;
                DB.Entry(_obj).Property(w => w.StartTime).IsModified = true;
                DB.Entry(_obj).Property(w => w.CloseTime).IsModified = true;
                DB.Entry(_obj).Property(w => w.SpokenLanguage).IsModified = true;
              
                DB.Entry(_obj).Property(w => w.OpenDay).IsModified = true;
                DB.Entry(_obj).Property(w => w.Rate).IsModified = true;

                DB.Entry(_obj).Property(w => w.Email).IsModified = true;
                DB.Entry(_obj).Property(w => w.Website).IsModified = true;
                DB.Entry(_obj).Property(w => w.Address).IsModified = true;
                DB.Entry(_obj).Property(w => w.Information).IsModified = true;

                DB.Entry(_obj).Property(w => w.Atmosphere).IsModified = true;
                DB.Entry(_obj).Property(w => w.ShortAddress).IsModified = true;
                DB.Entry(_obj).Property(w => w.Contact).IsModified = true;
                DB.Entry(_obj).Property(w => w.UrlImageTimeLine).IsModified = true;
                DB.Entry(_obj).Property(w => w.UrlImageLogo).IsModified = true;

                var Row = DB.SaveChanges();

                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void navExport_ElementClick(object sender, NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (Gv.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(Gc);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}