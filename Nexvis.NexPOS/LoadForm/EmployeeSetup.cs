﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;
using DevExpress.DataProcessing;

namespace DgoStore.LoadForm.Employee
{
    public partial class EmployeeSetup : DevExpress.XtraEditors.XtraForm
    {
        #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        private scrEmployee parentForm;

        #endregion

        #region --- Form Action ---
        public EmployeeSetup(string transactionType, scrEmployee objParent)
        {
            InitializeComponent();
            parentForm = objParent;

            if (transactionType == "NEW")
            {
                navEdit.Visible = false;
                isNew = true;
            }
            else if (transactionType == "EDIT")
            {
                navSave.Visible = false;
            }

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;

        }

        #endregion

        #region --- Header Action ---

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Clear value for new
            GlobalInitializer.GsClearValue(txtEmployeeID, txtEmployeeName, cboGender, dtDOB,
                dtStartdate, dtResignDate, txtEmployeeAdress, txtEmployeePhone, txtSalary);
            navEdit.Visible = false;
            navSave.Visible = true;
            isNew = true;
            ckStatus = null;
            txtEmployeeID.Enabled = false;
            dtResignDate.Enabled = false;
            dtStartdate.Text = DateTime.Today.ToShortDateString();
            cboGender.Text = "Male";
            txtEmployeeName.Focus();
          //  ckStatus.Checked = null;
            // cmbGender.Properties.Items.Add("M");
        }

        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtEmployeeName, txtSalary) == false)
            {
                
                //    // TODO: Save value to DB.
                if (cboGender.Text == "Male")
                {
                    cboGender.EditValue = "M";
                }
                else
                {
                    cboGender.EditValue = "F";
                }
                parentForm.GsSaveData(this);
                // TODO: Validate which record exiting.
                if (isExiting && parentForm.isSuccessful)
                {
                    XtraMessageBox.Show("This record ready existing !", "POS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtEmployeeName.Focus();
                    return;
                }

                if (parentForm.isSuccessful)
                    navNew_ElementClick(sender, e);
                this.Close();
            }
        }

       
        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            dtResignDate.Enabled = true;
            if (GlobalInitializer.GfCheckNullValue(txtEmployeeName) == false)
            {
                parentForm.GsUpdateData(this);
            //    MessageBox.Show("Updated");
                this.Close();

            }
               
            
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close(); // this.Parent.Controls.Remove(this);
        }

        #endregion

        private void EmployeeSetup_Load(object sender, EventArgs e)
        {
            getGender();
          //  cboGender. = genders.ToList();
        }

        public class Gender
        {
            public string id { get; set; }
            public string sex { get; set; }
        }
        List<Gender> genders = new List<Gender>();
        public void getGender()
        {
            Gender gender = new Gender();
            gender.id = "M";
            gender.sex = "Male";

            genders.Add(gender);

           
            gender.id = "F";
            gender.sex = "Female";

            genders.Add(gender);
        }

        private void txtEmployeeName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtSalary_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && navSave.Visible == true)
            {
                navSave_ElementClick(null, null);
            }
            else if (e.KeyCode == Keys.Enter && navSave.Visible == false)
            {
                navEdit_ElementClick(null, null);
            }
        }
    }
}