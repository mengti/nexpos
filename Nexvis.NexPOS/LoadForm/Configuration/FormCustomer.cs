﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;
using ZooMangement;
using DevExpress.XtraGrid.Views.Grid;
using ZooManagement;
using Nexvis.NexPOS.Data;
using ZooManagement.Helper;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.Configuration
{
    public partial class scrCustomer : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrCustomer()
        {
            InitializeComponent();
        }

        List<ARCustomer> _Cus = new List<ARCustomer>();
        List<getGenderCus_Result> getGenderCus_Results = new List<getGenderCus_Result>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();

        public string defaultCus = "General Customer";

        public bool isSuccessful = true;

        private void FormCustomer_Load(object sender, EventArgs e)
        {
            //    _Cus = DB.ARCustomers.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            getGenderCus_Results = DB.getGenderCus().ToList();
            GC.DataSource = getGenderCus_Results.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
           
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormCustomer_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }
        
        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    getGenderCus_Result result = new getGenderCus_Result();
                    result = (getGenderCus_Result)GV.GetRow(GV.FocusedRowHandle);


                    if (result != null)
                    {
                        if (result.CustomerName == defaultCus)
                        {
                            XtraMessageBox.Show("Can't delete defaul customer.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {
                            ARCustomer customer = DB.ARCustomers.FirstOrDefault(st => st.CustomerID.Equals(result.CustomerID));

                            DB.ARCustomers.Remove(customer);
                            DB.SaveChanges();
                            FormCustomer_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                // NavButton navigator = (NavButton)sender;
               var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    CustomerSetup objForm = new CustomerSetup("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    getGenderCus_Result result = new getGenderCus_Result();

                    result = (getGenderCus_Result)GV.GetRow(GV.FocusedRowHandle);

                    if (result.CustomerName == defaultCus)
                    {
                        XtraMessageBox.Show("Can't edit default customer.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    if (result != null)
                    {
                        objForm.txtCustomerID.Text = result.CustomerID;
                        objForm.txtCustomerID.Enabled = false;
                        objForm.txtCustomerName.Focus();
                        objForm.txtCustomerName.Text = result.CustomerName;
                        objForm.cmbGender.Text = result.Gender;
                        objForm.txtCustomerAddress.Text = result.CustomerAddress;
                        objForm.txtCustomerPhone.Text = result.CustomerPhone;

                        objForm.cmbGender.Text = Gender.setFullGender(objForm.cmbGender.Text);

                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    CustomerSetup objForm = new CustomerSetup("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    objForm.txtCustomerID.Enabled = false;

                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
             
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Actions
        public void GsSaveData(CustomerSetup objChild)
        {
            try
            {
                objChild.isExiting = false;
                if (_Cus.Any(x => x.CustomerID.ToString() == objChild.txtCustomerID.Text.Trim().ToLower()))
                {
                    objChild.isExiting = true;
                    return;
                }

                var resul = new CFNumberRank("CUS", DocConfType.Normal, true);

                objChild.cmbGender.Text = Gender.setFullGender(objChild.cmbGender.Text);
                ARCustomer cus = new ARCustomer()
                {
                    CompanyID = DACClasses.CompanyID,
                    CustomerID = resul.NextNumberRank.Trim(),
                    CustomerName = objChild.txtCustomerName.Text,
                    Gender = objChild.cmbGender.Text,
                    CustomerAddress = objChild.txtCustomerAddress.Text,
                    CustomerPhone = objChild.txtCustomerPhone.Text

                };


                DB.ARCustomers.Add(cus);
                var Row = DB.SaveChanges();

                objChild.cmbGender.Text = Gender.setFullGender(objChild.cmbGender.Text);

                if (Row == 1)
                {
                    isSuccessful = true;
                }
                GV.FocusedRowHandle = 0;
                navRefresh_ElementClick(null, null);
              //  XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        
        }
        public void GsUpdateData(CustomerSetup objChild)
        {
            try
            {
                if (_Cus.Any(x => x.CustomerID.Trim().ToLower() != objChild.txtCustomerID.Text.Trim().ToLower()
                && x.CustomerName.Trim().ToLower() == objChild.txtCustomerName.Text.Trim().ToLower()))
                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtCustomerName.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                

                //       var _obj = DB.ARCustomers.FirstOrDefault(w => w.CustomerID.Trim().ToLower().ToString() == objChild.txtCustomerID.Text);
                var _obj = DB.ARCustomers.FirstOrDefault(w => w.CustomerID.ToString() == (objChild.txtCustomerID.Text) && w.CompanyID.Equals(DACClasses.CompanyID));

                objChild.cmbGender.Text = Gender.setFullGender(objChild.cmbGender.Text);
                if (_obj.CustomerName == defaultCus)
                {
                    XtraMessageBox.Show("Can't update default customer.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                _obj.CustomerName = objChild.txtCustomerName.Text;
                _obj.CustomerPhone = objChild.txtCustomerPhone.Text;
                _obj.CustomerAddress = objChild.txtCustomerAddress.Text;
                _obj.Gender = objChild.cmbGender.Text;

                

                DB.ARCustomers.Attach(_obj);
                DB.Entry(_obj).Property(w => w.CustomerName).IsModified = true;
                DB.Entry(_obj).Property(w => w.CustomerPhone).IsModified = true;
                DB.Entry(_obj).Property(w => w.CustomerAddress).IsModified = true;
                DB.Entry(_obj).Property(w => w.Gender).IsModified = true;

                objChild.cmbGender.Text = Gender.setFullGender(objChild.cmbGender.Text);

                var Row = DB.SaveChanges();
                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                navRefresh_ElementClick(null,null);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void navExport_ElementClick_1(object sender, NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}