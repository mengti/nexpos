﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using ZooMangement;
using Nexvis.NexPOS.Helper;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.Configuration.UOM
{
    public partial class scrUOM : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrUOM()
        {
            InitializeComponent();
        }

        List<CSUOM> _UOM = new List<CSUOM>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        public bool isSuccessful = true;

        private void FormUOM_Load(object sender, EventArgs e)
        {
            _UOM = DB.CSUOMs.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            GC.DataSource = _UOM.ToList();

            _RoleItems = DB.CSRoleItems.ToList();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormUOM_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    CSUOM result = new CSUOM();
                    result = (CSUOM)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "POS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {
                            CSUOM customer = DB.CSUOMs.FirstOrDefault(st => st.CompanyID.Equals(result.CompanyID) && st.UOMID.Equals(result.UOMID));

                            DB.CSUOMs.Remove(customer);
                            DB.SaveChanges();
                            FormUOM_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                //NavButton navigator = (NavButton)sender;
                var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    UOMSetup objForm = new UOMSetup("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    // GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    CSUOM result = new CSUOM();

                    result = (CSUOM)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        objForm.txtUOMID.Text = result.UOMID;
                        objForm.txtDescr.Text = result.Descr;
                        objForm.txtUOMID.Enabled = false;
                        objForm.txtDescr.Enabled = true;
                        objForm.txtDescr.Focus();
                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    UOMSetup objForm = new UOMSetup("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //TODO: Binding value to comboBox or LookupEdit.
                    //Global Initializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    // objForm._cities = _cities;  
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Actions
        public void GsSaveData(UOMSetup objChild)
        {
            objChild.isExiting = false;
            if (_UOM.Any(x => x.UOMID.Trim().ToLower() == objChild.txtUOMID.Text.Trim().ToLower()))
            {
                objChild.isExiting = true;
                return;
            }

           // if (XtraMessageBox.Show("Are you sure you want to save this record?", "POS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            CSUOM uom = new CSUOM()
            {
                CompanyID = DACClasses.CompanyID,
                UOMID = objChild.txtUOMID.Text,
                Descr = objChild.txtDescr.Text

            };

            DB.CSUOMs.Add(uom);
            var Row = DB.SaveChanges();
            if (Row == 1)
            {
                isSuccessful = true;
            }
            GV.FocusedRowHandle = 0;
            navRefresh_ElementClick(null, null);
            XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public void GsUpdateData(UOMSetup objChild)
        {
            try
            {
                //if (_CSTax.Any(x => x.TaxID.Trim().ToLower() != objChild.txtTaxID.Text.Trim().ToLower() && x.CompanyID.Equals(DACClasses.CompanyID)))
                //{
                //    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    objChild.txtTaxRate.Focus();
                //    return;
                //}

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                var _obj = DB.CSUOMs.SingleOrDefault(w => w.UOMID == objChild.txtUOMID.Text && w.CompanyID.Equals(DACClasses.CompanyID));
                _obj.UOMID = objChild.txtUOMID.Text;
                _obj.Descr = objChild.txtDescr.Text;

                DB.CSUOMs.Attach(_obj);
                DB.Entry(_obj).Property(w => w.Descr).IsModified = true;

                var Row = DB.SaveChanges();
                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                objChild.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void navExport_ElementClick_1(object sender, NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}