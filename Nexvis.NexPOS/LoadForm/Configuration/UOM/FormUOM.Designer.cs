﻿namespace DgoStore.LoadForm.Configuration.UOM
{
    partial class scrUOM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tileNavPane1 = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navButton1 = new DevExpress.XtraBars.Navigation.NavButton();
            this.navButton2 = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navExport = new DevExpress.XtraBars.Navigation.NavButton();
            this.navDelete = new DevExpress.XtraBars.Navigation.NavButton();
            this.navRefresh = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.GC = new DevExpress.XtraGrid.GridControl();
            this.GV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.tileNavPane1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tileNavPane1
            // 
            this.tileNavPane1.Buttons.Add(this.navButton1);
            this.tileNavPane1.Buttons.Add(this.navButton2);
            this.tileNavPane1.Buttons.Add(this.navEdit);
            this.tileNavPane1.Buttons.Add(this.navExport);
            this.tileNavPane1.Buttons.Add(this.navDelete);
            this.tileNavPane1.Buttons.Add(this.navRefresh);
            this.tileNavPane1.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.tileNavPane1.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.tileNavPane1.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.tileNavPane1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tileNavPane1.Location = new System.Drawing.Point(0, 0);
            this.tileNavPane1.Name = "tileNavPane1";
            this.tileNavPane1.Size = new System.Drawing.Size(943, 40);
            this.tileNavPane1.TabIndex = 8;
            this.tileNavPane1.Text = "tileNavPane1";
            // 
            // navButton1
            // 
            this.navButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navButton1.Appearance.Options.UseFont = true;
            this.navButton1.Caption = "Unit Of Mesure List";
            this.navButton1.Enabled = false;
            this.navButton1.Name = "navButton1";
            // 
            // navButton2
            // 
            this.navButton2.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButton2.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navButton2.AppearanceHovered.Options.UseForeColor = true;
            this.navButton2.Caption = "New";
            this.navButton2.Name = "navButton2";
            this.navButton2.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Edit";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navExport
            // 
            this.navExport.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navExport.Caption = "Export";
            this.navExport.Name = "navExport";
            this.navExport.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navExport_ElementClick_1);
            // 
            // navDelete
            // 
            this.navDelete.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navDelete.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navDelete.AppearanceHovered.Options.UseForeColor = true;
            this.navDelete.Caption = "Delete";
            this.navDelete.Name = "navDelete";
            this.navDelete.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navDelete_ElementClick);
            // 
            // navRefresh
            // 
            this.navRefresh.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navRefresh.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navRefresh.AppearanceHovered.Options.UseForeColor = true;
            this.navRefresh.Caption = "Refresh";
            this.navRefresh.Name = "navRefresh";
            this.navRefresh.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navRefresh_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(923, 448);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(50, 20);
            // 
            // GC
            // 
            this.GC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GC.Location = new System.Drawing.Point(0, 40);
            this.GC.MainView = this.GV;
            this.GC.Name = "GC";
            this.GC.Size = new System.Drawing.Size(943, 468);
            this.GC.TabIndex = 11;
            this.GC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GV,
            this.gridView1});
            // 
            // GV
            // 
            this.GV.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.GV.Appearance.GroupPanel.Options.UseFont = true;
            this.GV.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.GV.Appearance.HeaderPanel.Options.UseFont = true;
            this.GV.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GV.Appearance.Row.Options.UseFont = true;
            this.GV.ColumnPanelRowHeight = 30;
            this.GV.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.GV.CustomizationFormBounds = new System.Drawing.Rectangle(1156, 368, 210, 242);
            this.GV.GridControl = this.GC;
            this.GV.IndicatorWidth = 35;
            this.GV.Name = "GV";
            this.GV.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV.OptionsBehavior.Editable = false;
            this.GV.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GV.OptionsView.EnableAppearanceEvenRow = true;
            this.GV.OptionsView.ShowGroupPanel = false;
            this.GV.RowHeight = 30;
            this.GV.DoubleClick += new System.EventHandler(this.GV_DoubleClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "UOM ID";
            this.gridColumn1.FieldName = "UOMID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Descr";
            this.gridColumn2.FieldName = "Descr";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.GC;
            this.gridView1.Name = "gridView1";
            // 
            // scrUOM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 508);
            this.Controls.Add(this.GC);
            this.Controls.Add(this.tileNavPane1);
            this.Name = "scrUOM";
            this.Text = "UOM";
            this.Load += new System.EventHandler(this.FormUOM_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tileNavPane1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileNavPane tileNavPane1;
        private DevExpress.XtraBars.Navigation.NavButton navButton1;
        private DevExpress.XtraBars.Navigation.NavButton navButton2;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navDelete;
        private DevExpress.XtraBars.Navigation.NavButton navRefresh;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.GridControl GC;
        private DevExpress.XtraGrid.Views.Grid.GridView GV;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.Navigation.NavButton navExport;
    }
}