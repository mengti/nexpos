﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;

namespace DgoStore.LoadForm.Configuration
{
    public partial class CustomerSetup : DevExpress.XtraEditors.XtraForm
    {
        #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        private scrCustomer parentForm;

        #endregion

        #region --- Form Action ---
        public CustomerSetup(string transactionType, scrCustomer objParent)
        {
            InitializeComponent();
            parentForm = objParent;


            if (transactionType == "NEW")
            {
                navEdit.Visible = false;
                isNew = true;
            }
            else if (transactionType == "EDIT")
            {
                navSave.Visible = false;
              
            }

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;

        }

        #endregion

        #region --- Header Action ---

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Clear value for new
            GlobalInitializer.GsClearValue(txtCustomerID, txtCustomerName,
                txtCustomerPhone, txtCustomerAddress);

            navEdit.Visible = false;
            navSave.Visible = true;
            isNew = true;
            //if (cmbGender.Text == "M")
            //{
            //    cmbGender.EditValue = "Male";
            //}
            //else
            //{
            //    cmbGender.EditValue = "Female";
            //}

            //  txtImageUrl.Image = null;
            // txtImageUrl.Invalidate();
            txtCustomerID.Enabled = false;
           // txtInventoryID.Enabled = false;
           // txtInventoryCD.Focus();

        }

        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue( txtCustomerName) == false)
            {
                //    // TODO: Save value to DB.
                parentForm.GsSaveData(this);


                // TODO: Validate which record exiting.
                if (isExiting && parentForm.isSuccessful)
                {
                    XtraMessageBox.Show("This record ready existing !", "POS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCustomerName.Focus();

                    return;
                }

                if (parentForm.isSuccessful)
                {
                    this.Close();
                    navNew_ElementClick(sender, e);
                }
                    
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtCustomerID, txtCustomerName) == false)
                parentForm.GsUpdateData(this);
            this.Close();
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close(); // this.Parent.Controls.Remove(this);
            parentForm.navRefresh_ElementClick(sender, e);
        }

        #endregion

        private void txtCustomerName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtCustomerPhone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && navSave.Visible == true)
            {
                navSave_ElementClick(null,null);
            }else if(e.KeyCode == Keys.Enter && navSave.Visible == false)
            {
                navEdit_ElementClick(null, null);
            }
        }
    }
}