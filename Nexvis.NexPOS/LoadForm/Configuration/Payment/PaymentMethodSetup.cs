﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Helper;

namespace DgoStore.LoadForm.Configuration.Payment
{
    public partial class PaymentMethodSetup : DevExpress.XtraEditors.XtraForm
    {
        #region --- Variable Declaration ---
        public bool isExiting = false;
        private bool isNew = false;
        private scrPayment parentForm;

        #endregion
        #region --- Form Action ---
        public PaymentMethodSetup(string transactionType, scrPayment objParent)
        {
            InitializeComponent();
            parentForm = objParent;

            if (transactionType == "NEW")
            {
                navEdit.Visible = false;
                isNew = true;
            }
            else if (transactionType == "EDIT")
            {
                navSave.Visible = false;
            }

            // TODO: Set hand symbol when mouse over in header button.
            navHeader.MouseMove += GlobalEvent.objNavPanel_MouseMove;

        }

        #endregion

        #region --- Form Header ---
        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Clear value for new
            GlobalInitializer.GsClearValue(txtPaymentMethodID, txtDescr);
            navEdit.Visible = false;
            navSave.Visible = true;
            isNew = true;
            //txtCompanyID.Enabled = true;
            txtPaymentMethodID.Focus();
        }

        private void navSave_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtPaymentMethodID) == false)
            {
                //    // TODO: Save value to DB.
                parentForm.GsSaveData(this);

                // TODO: Validate which record exiting.
                if (isExiting && parentForm.isSuccessful)
                {
                    XtraMessageBox.Show("This record ready existing !", "POS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPaymentMethodID.Focus();
                    return;
                }

                if (parentForm.isSuccessful)
                    navNew_ElementClick(sender, e);
                this.Close();
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (GlobalInitializer.GfCheckNullValue(txtPaymentMethodID) == false)
                parentForm.GsUpdateData(this);
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            if (XtraMessageBox.Show("Are you sure you want to close this form?", "Gunze Sport Membership System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                this.Close(); // this.Parent.Controls.Remove(this);
            parentForm.navRefresh_ElementClick(sender, e);
        }
        #endregion

        private void txtPaymentMethodID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && navSave.Visible == true)
            {
                navSave_ElementClick(null, null);
            }
            else if (e.KeyCode == Keys.Enter && navSave.Visible == false)
            {
                navEdit_ElementClick(null, null);
            }
        }
    }
}