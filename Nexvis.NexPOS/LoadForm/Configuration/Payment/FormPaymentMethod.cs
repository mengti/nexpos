﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using Nexvis.NexPOS.Helper;
using ZooMangement;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.Configuration.Payment
{
    public partial class scrPayment : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrPayment()
        {
            InitializeComponent();
        }
        List<CSPaymentMethod> paymentmethode = new List<CSPaymentMethod>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();
        //List<CSCompany> _Company = new List<CSCompany>();
        public bool isSuccessful = true;
        private void FormPaymentMethod_Load(object sender, EventArgs e)
        {
            paymentmethode = DB.CSPaymentMethods.ToList();
            GC.DataSource = paymentmethode.ToList();
            _RoleItems = DB.CSRoleItems.ToList();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

           
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    PaymentMethodSetup objForm = new PaymentMethodSetup("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //TODO: Binding value to comboBox or LookupEdit.
                    //GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    // objForm._cities = _cities;
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

               
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }
        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormPaymentMethod_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    CSPaymentMethod result = new CSPaymentMethod();
                    result = (CSPaymentMethod)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {
                            CSPaymentMethod payment = DB.CSPaymentMethods.FirstOrDefault(st => st.PaymentMethodID.Equals(result.PaymentMethodID));

                            DB.CSPaymentMethods.Remove(payment);
                            DB.SaveChanges();
                            FormPaymentMethod_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

             
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void GsSaveData(PaymentMethodSetup objChild)
        {
            objChild.isExiting = false;
            if (paymentmethode.Any(x => x.PaymentMethodID == objChild.txtPaymentMethodID.Text))
            {
                objChild.isExiting = true;
                return;
            }

          //  if (XtraMessageBox.Show("Are you sure you want to save this record?", "POS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
             //   return;

            CSPaymentMethod pay = new CSPaymentMethod()
            {
                CompanyID = DACClasses.CompanyID,
                PaymentMethodID = objChild.txtPaymentMethodID.Text,
                Descr = objChild.txtDescr.Text
            };

            DB.CSPaymentMethods.Add(pay);
            var Row = DB.SaveChanges();
            if (Row == 1)
            {
                isSuccessful = true;
            }
            GV.FocusedRowHandle = 0;
            navRefresh_ElementClick(null,null);
            XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public void GsUpdateData(PaymentMethodSetup objChild)
        {
            try
            {
                // && x.CompanyName.Trim().ToLower() == objChild.txtCompanyName.Text.Trim().ToLower())
                if (paymentmethode.Any(x => x.PaymentMethodID.Trim().ToLower() != objChild.txtPaymentMethodID.Text.Trim().ToLower()
                    && x.Descr.Trim().ToLower() == objChild.txtDescr.Text.Trim().ToLower()))

                {
                    XtraMessageBox.Show("This record ready existing !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    objChild.txtDescr.Focus();
                    return;
                }

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;


                var _comp = DB.CSPaymentMethods.SingleOrDefault(w => w.PaymentMethodID == objChild.txtPaymentMethodID.Text);


                _comp.CompanyID = DACClasses.CompanyID;
                _comp.PaymentMethodID = objChild.txtPaymentMethodID.Text;
                _comp.Descr = objChild.txtDescr.Text;


                DB.CSPaymentMethods.Attach(_comp);
                DB.Entry(_comp).Property(w => w.Descr).IsModified = true;
                var Row = DB.SaveChanges();

                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                objChild.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                // NavButton navigator = (NavButton)sender;
               var nameAction = "Edit";
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == nameAction))
                {
                    PaymentMethodSetup objForm = new PaymentMethodSetup("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    CSPaymentMethod result = new CSPaymentMethod();

                    result = (CSPaymentMethod)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {

                        objForm.txtPaymentMethodID.Enabled = false;
                        objForm.txtDescr.Focus();
                        objForm.txtPaymentMethodID.Text = result.PaymentMethodID;
                        objForm.txtDescr.Text = result.Descr;

                        Shadow.ShowDialog();
                    }

                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + nameAction + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
  
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
            {
                GridView view = (GridView)sender;
                Point point = view.GridControl.PointToClient(Control.MousePosition);
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
                if (info.InRow || info.InRowCell)
                    navEdit_ElementClick(null, null);
            }
            else
            {
                XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}