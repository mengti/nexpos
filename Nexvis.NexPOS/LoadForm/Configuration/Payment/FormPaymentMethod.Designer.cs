﻿namespace DgoStore.LoadForm.Configuration.Payment
{
    partial class scrPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tileNavPane1 = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navButton1 = new DevExpress.XtraBars.Navigation.NavButton();
            this.navButton2 = new DevExpress.XtraBars.Navigation.NavButton();
            this.navButton3 = new DevExpress.XtraBars.Navigation.NavButton();
            this.navButton4 = new DevExpress.XtraBars.Navigation.NavButton();
            this.navButton5 = new DevExpress.XtraBars.Navigation.NavButton();
            this.navButton6 = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.GC = new DevExpress.XtraGrid.GridControl();
            this.GV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.PaymentMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Descr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.tileNavPane1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // tileNavPane1
            // 
            this.tileNavPane1.Buttons.Add(this.navButton1);
            this.tileNavPane1.Buttons.Add(this.navButton2);
            this.tileNavPane1.Buttons.Add(this.navButton3);
            this.tileNavPane1.Buttons.Add(this.navButton4);
            this.tileNavPane1.Buttons.Add(this.navButton5);
            this.tileNavPane1.Buttons.Add(this.navButton6);
            this.tileNavPane1.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.tileNavPane1.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.tileNavPane1.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.tileNavPane1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tileNavPane1.Location = new System.Drawing.Point(0, 0);
            this.tileNavPane1.Name = "tileNavPane1";
            this.tileNavPane1.Size = new System.Drawing.Size(893, 40);
            this.tileNavPane1.TabIndex = 10;
            this.tileNavPane1.Text = "tileNavPane1";
            // 
            // navButton1
            // 
            this.navButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navButton1.Appearance.Options.UseFont = true;
            this.navButton1.Caption = "Payment Method Setup";
            this.navButton1.Enabled = false;
            this.navButton1.Name = "navButton1";
            // 
            // navButton2
            // 
            this.navButton2.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButton2.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navButton2.AppearanceHovered.Options.UseForeColor = true;
            this.navButton2.Caption = "New";
            this.navButton2.Name = "navButton2";
            this.navButton2.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navButton3
            // 
            this.navButton3.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButton3.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navButton3.AppearanceHovered.Options.UseForeColor = true;
            this.navButton3.Caption = "Edit";
            this.navButton3.Name = "navButton3";
            this.navButton3.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navButton4
            // 
            this.navButton4.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButton4.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navButton4.AppearanceHovered.Options.UseForeColor = true;
            this.navButton4.Caption = "Delete";
            this.navButton4.Name = "navButton4";
            this.navButton4.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navDelete_ElementClick);
            // 
            // navButton5
            // 
            this.navButton5.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButton5.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navButton5.AppearanceHovered.Options.UseForeColor = true;
            this.navButton5.Caption = "Export";
            this.navButton5.Name = "navButton5";
            this.navButton5.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navExport_ElementClick);
            // 
            // navButton6
            // 
            this.navButton6.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navButton6.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navButton6.AppearanceHovered.Options.UseForeColor = true;
            this.navButton6.Caption = "Refresh";
            this.navButton6.Name = "navButton6";
            this.navButton6.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navRefresh_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.GC);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(893, 454);
            this.layoutControl1.TabIndex = 11;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // GC
            // 
            this.GC.Location = new System.Drawing.Point(12, 12);
            this.GC.MainView = this.GV;
            this.GC.Name = "GC";
            this.GC.Size = new System.Drawing.Size(869, 430);
            this.GC.TabIndex = 5;
            this.GC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GV});
            // 
            // GV
            // 
            this.GV.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.GV.Appearance.GroupPanel.Options.UseFont = true;
            this.GV.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.GV.Appearance.HeaderPanel.Options.UseFont = true;
            this.GV.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GV.Appearance.Row.Options.UseFont = true;
            this.GV.ColumnPanelRowHeight = 30;
            this.GV.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.PaymentMethodID,
            this.Descr});
            this.GV.CustomizationFormBounds = new System.Drawing.Rectangle(1156, 368, 210, 242);
            this.GV.GridControl = this.GC;
            this.GV.IndicatorWidth = 35;
            this.GV.Name = "GV";
            this.GV.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV.OptionsBehavior.Editable = false;
            this.GV.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GV.OptionsView.EnableAppearanceEvenRow = true;
            this.GV.OptionsView.ShowGroupPanel = false;
            this.GV.RowHeight = 30;
            this.GV.DoubleClick += new System.EventHandler(this.GV_DoubleClick);
            // 
            // PaymentMethodID
            // 
            this.PaymentMethodID.Caption = "PaymentMethod ID";
            this.PaymentMethodID.FieldName = "PaymentMethodID";
            this.PaymentMethodID.Name = "PaymentMethodID";
            this.PaymentMethodID.Visible = true;
            this.PaymentMethodID.VisibleIndex = 0;
            this.PaymentMethodID.Width = 139;
            // 
            // Descr
            // 
            this.Descr.Caption = "Description";
            this.Descr.FieldName = "Descr";
            this.Descr.Name = "Descr";
            this.Descr.Visible = true;
            this.Descr.VisibleIndex = 1;
            this.Descr.Width = 110;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(893, 454);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.GC;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(873, 434);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // scrPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 494);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.tileNavPane1);
            this.Name = "scrPayment";
            this.Text = "Payment Method";
            this.Load += new System.EventHandler(this.FormPaymentMethod_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tileNavPane1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileNavPane tileNavPane1;
        private DevExpress.XtraBars.Navigation.NavButton navButton1;
        private DevExpress.XtraBars.Navigation.NavButton navButton2;
        private DevExpress.XtraBars.Navigation.NavButton navButton3;
        private DevExpress.XtraBars.Navigation.NavButton navButton4;
        private DevExpress.XtraBars.Navigation.NavButton navButton5;
        private DevExpress.XtraBars.Navigation.NavButton navButton6;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl GC;
        private DevExpress.XtraGrid.Views.Grid.GridView GV;
        private DevExpress.XtraGrid.Columns.GridColumn PaymentMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn Descr;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}