﻿namespace DgoStore.LoadForm.Configuration.Currency
{
    partial class scrCuryRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navNew = new DevExpress.XtraBars.Navigation.NavButton();
            this.navExport = new DevExpress.XtraBars.Navigation.NavButton();
            this.navRefresh = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.GC = new DevExpress.XtraGrid.GridControl();
            this.GV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FromCuryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ToBaseCuryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Indicator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CuryRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RcptRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EffectiveDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navNew);
            this.navHeader.Buttons.Add(this.navExport);
            this.navHeader.Buttons.Add(this.navRefresh);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(913, 40);
            this.navHeader.TabIndex = 8;
            this.navHeader.Text = "tileNavPane1";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Add Exchange Rate";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navNew
            // 
            this.navNew.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navNew.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navNew.AppearanceHovered.Options.UseForeColor = true;
            this.navNew.Caption = "New";
            this.navNew.Name = "navNew";
            this.navNew.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navExport
            // 
            this.navExport.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navExport.Caption = "Export";
            this.navExport.Name = "navExport";
            this.navExport.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navExport_ElementClick_1);
            // 
            // navRefresh
            // 
            this.navRefresh.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navRefresh.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navRefresh.AppearanceHovered.Options.UseForeColor = true;
            this.navRefresh.Caption = "Refresh";
            this.navRefresh.Name = "navRefresh";
            this.navRefresh.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navRefresh_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // GC
            // 
            this.GC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GC.Location = new System.Drawing.Point(0, 40);
            this.GC.MainView = this.GV;
            this.GC.Name = "GC";
            this.GC.Size = new System.Drawing.Size(913, 439);
            this.GC.TabIndex = 5;
            this.GC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GV});
            // 
            // GV
            // 
            this.GV.Appearance.GroupPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.GV.Appearance.GroupPanel.Options.UseFont = true;
            this.GV.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.GV.Appearance.HeaderPanel.Options.UseFont = true;
            this.GV.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GV.Appearance.Row.Options.UseFont = true;
            this.GV.ColumnPanelRowHeight = 30;
            this.GV.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.FromCuryID,
            this.ToBaseCuryID,
            this.Indicator,
            this.CuryRate,
            this.RcptRate,
            this.EffectiveDate,
            this.gridColumn1});
            this.GV.CustomizationFormBounds = new System.Drawing.Rectangle(1156, 368, 210, 242);
            this.GV.GridControl = this.GC;
            this.GV.IndicatorWidth = 35;
            this.GV.Name = "GV";
            this.GV.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.GV.OptionsBehavior.Editable = false;
            this.GV.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GV.OptionsView.EnableAppearanceEvenRow = true;
            this.GV.OptionsView.ShowGroupPanel = false;
            this.GV.RowHeight = 30;
            // 
            // FromCuryID
            // 
            this.FromCuryID.Caption = "From CuryID";
            this.FromCuryID.FieldName = "FromCuryID";
            this.FromCuryID.Name = "FromCuryID";
            this.FromCuryID.Visible = true;
            this.FromCuryID.VisibleIndex = 0;
            this.FromCuryID.Width = 139;
            // 
            // ToBaseCuryID
            // 
            this.ToBaseCuryID.Caption = "To CuryID";
            this.ToBaseCuryID.FieldName = "ToBaseCuryID";
            this.ToBaseCuryID.Name = "ToBaseCuryID";
            this.ToBaseCuryID.Visible = true;
            this.ToBaseCuryID.VisibleIndex = 1;
            this.ToBaseCuryID.Width = 110;
            // 
            // Indicator
            // 
            this.Indicator.Caption = "Indicator";
            this.Indicator.DisplayFormat.FormatString = "n";
            this.Indicator.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Indicator.FieldName = "Indicator";
            this.Indicator.Name = "Indicator";
            // 
            // CuryRate
            // 
            this.CuryRate.Caption = "Cury Rate";
            this.CuryRate.DisplayFormat.FormatString = "n2";
            this.CuryRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CuryRate.FieldName = "CuryRate";
            this.CuryRate.Name = "CuryRate";
            this.CuryRate.Visible = true;
            this.CuryRate.VisibleIndex = 2;
            this.CuryRate.Width = 269;
            // 
            // RcptRate
            // 
            this.RcptRate.Caption = "Receipt Rate";
            this.RcptRate.DisplayFormat.FormatString = "n2";
            this.RcptRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.RcptRate.FieldName = "RcptRate";
            this.RcptRate.Name = "RcptRate";
            // 
            // EffectiveDate
            // 
            this.EffectiveDate.Caption = "Effective Date";
            this.EffectiveDate.DisplayFormat.FormatString = "d";
            this.EffectiveDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.EffectiveDate.FieldName = "EffectiveDate";
            this.EffectiveDate.Name = "EffectiveDate";
            this.EffectiveDate.Visible = true;
            this.EffectiveDate.VisibleIndex = 3;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Expired Date";
            this.gridColumn1.DisplayFormat.FormatString = "d";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn1.FieldName = "ExpiredDate";
            this.gridColumn1.Name = "gridColumn1";
            // 
            // scrCuryRate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 479);
            this.Controls.Add(this.GC);
            this.Controls.Add(this.navHeader);
            this.Name = "scrCuryRate";
            this.Text = "Exchange Rate";
            this.Load += new System.EventHandler(this.FormCurrencyRate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        private DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navNew;
        private DevExpress.XtraBars.Navigation.NavButton navRefresh;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraGrid.GridControl GC;
        private DevExpress.XtraGrid.Views.Grid.GridView GV;
        private DevExpress.XtraGrid.Columns.GridColumn FromCuryID;
        private DevExpress.XtraGrid.Columns.GridColumn ToBaseCuryID;
        private DevExpress.XtraGrid.Columns.GridColumn Indicator;
        private DevExpress.XtraGrid.Columns.GridColumn CuryRate;
        private DevExpress.XtraGrid.Columns.GridColumn RcptRate;
        private DevExpress.XtraGrid.Columns.GridColumn EffectiveDate;
        private DevExpress.XtraBars.Navigation.NavButton navExport;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}