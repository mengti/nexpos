﻿namespace DgoStore.LoadForm.Configuration.Currency
{
    partial class CurrencySetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtCurrencyID = new DevExpress.XtraEditors.TextEdit();
            this.txtDescr = new DevExpress.XtraEditors.MemoEdit();
            this.txtSymbol = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.D1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.D2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navNew = new DevExpress.XtraBars.Navigation.NavButton();
            this.navSave = new DevExpress.XtraBars.Navigation.NavButton();
            this.navEdit = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrencyID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSymbol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtCurrencyID);
            this.layoutControl1.Controls.Add(this.txtDescr);
            this.layoutControl1.Controls.Add(this.txtSymbol);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(758, 371);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtCurrencyID
            // 
            this.txtCurrencyID.EnterMoveNextControl = true;
            this.txtCurrencyID.Location = new System.Drawing.Point(12, 31);
            this.txtCurrencyID.Name = "txtCurrencyID";
            this.txtCurrencyID.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyID.Properties.Appearance.Options.UseFont = true;
            this.txtCurrencyID.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.txtCurrencyID.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtCurrencyID.Properties.MaxLength = 100;
            this.txtCurrencyID.Size = new System.Drawing.Size(269, 22);
            this.txtCurrencyID.StyleController = this.layoutControl1;
            this.txtCurrencyID.TabIndex = 0;
            // 
            // txtDescr
            // 
            this.txtDescr.Location = new System.Drawing.Point(12, 76);
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Properties.Appearance.Options.UseFont = true;
            this.txtDescr.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.txtDescr.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtDescr.Properties.MaxLength = 100;
            this.txtDescr.Size = new System.Drawing.Size(550, 283);
            this.txtDescr.StyleController = this.layoutControl1;
            this.txtDescr.TabIndex = 2;
            // 
            // txtSymbol
            // 
            this.txtSymbol.EditValue = "";
            this.txtSymbol.Location = new System.Drawing.Point(285, 31);
            this.txtSymbol.Name = "txtSymbol";
            this.txtSymbol.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSymbol.Properties.Appearance.Options.UseFont = true;
            this.txtSymbol.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent;
            this.txtSymbol.Properties.AppearanceFocused.Options.UseBorderColor = true;
            this.txtSymbol.Properties.MaxLength = 100;
            this.txtSymbol.Size = new System.Drawing.Size(277, 22);
            this.txtSymbol.StyleController = this.layoutControl1;
            this.txtSymbol.TabIndex = 1;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.D1,
            this.D2,
            this.layoutControlItem7,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(758, 371);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // D1
            // 
            this.D1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.D1.AppearanceItemCaption.Options.UseFont = true;
            this.D1.Control = this.txtCurrencyID;
            this.D1.CustomizationFormText = "User ID";
            this.D1.Location = new System.Drawing.Point(0, 0);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(273, 45);
            this.D1.Text = "Currency ID";
            this.D1.TextLocation = DevExpress.Utils.Locations.Top;
            this.D1.TextSize = new System.Drawing.Size(67, 16);
            // 
            // D2
            // 
            this.D2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.D2.AppearanceItemCaption.Options.UseFont = true;
            this.D2.Control = this.txtSymbol;
            this.D2.CustomizationFormText = "User ID";
            this.D2.Location = new System.Drawing.Point(273, 0);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(281, 45);
            this.D2.Text = "Symbol";
            this.D2.TextLocation = DevExpress.Utils.Locations.Top;
            this.D2.TextSize = new System.Drawing.Size(67, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.txtDescr;
            this.layoutControlItem7.CustomizationFormText = "Student Name";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 45);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(554, 306);
            this.layoutControlItem7.Text = "Description";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(67, 16);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(554, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(184, 351);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navNew);
            this.navHeader.Buttons.Add(this.navSave);
            this.navHeader.Buttons.Add(this.navEdit);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(758, 40);
            this.navHeader.TabIndex = 5;
            this.navHeader.Text = "tileNavPane1";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Currency Information";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navNew
            // 
            this.navNew.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navNew.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navNew.AppearanceHovered.Options.UseForeColor = true;
            this.navNew.Caption = "New";
            this.navNew.Name = "navNew";
            this.navNew.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navNew_ElementClick);
            // 
            // navSave
            // 
            this.navSave.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navSave.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navSave.AppearanceHovered.Options.UseForeColor = true;
            this.navSave.Caption = "Save";
            this.navSave.Name = "navSave";
            this.navSave.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navSave_ElementClick);
            // 
            // navEdit
            // 
            this.navEdit.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navEdit.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navEdit.AppearanceHovered.Options.UseForeColor = true;
            this.navEdit.Caption = "Update";
            this.navEdit.Name = "navEdit";
            this.navEdit.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navEdit_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // CurrencySetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 411);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Name = "CurrencySetup";
            this.Text = "Currency Setup";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrencyID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSymbol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem D1;
        private DevExpress.XtraLayout.LayoutControlItem D2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navNew;
        private DevExpress.XtraBars.Navigation.NavButton navSave;
        private DevExpress.XtraBars.Navigation.NavButton navEdit;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        internal DevExpress.XtraEditors.TextEdit txtCurrencyID;
        internal DevExpress.XtraEditors.MemoEdit txtDescr;
        internal DevExpress.XtraEditors.TextEdit txtSymbol;
    }
}