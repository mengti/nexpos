﻿namespace DgoStore.LoadForm.Configuration.Currency
{ 
    partial class CurrencyRateSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navHeader = new DevExpress.XtraBars.Navigation.TileNavPane();
            this.navHome = new DevExpress.XtraBars.Navigation.NavButton();
            this.navSave = new DevExpress.XtraBars.Navigation.NavButton();
            this.navClose = new DevExpress.XtraBars.Navigation.NavButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtRcptRate = new DevExpress.XtraEditors.TextEdit();
            this.luFromCuryID = new DevExpress.XtraEditors.LookUpEdit();
            this.luToBaseCuryID = new DevExpress.XtraEditors.LookUpEdit();
            this.dtEffectiveDate = new DevExpress.XtraEditors.DateEdit();
            this.txtCurrInfo = new DevExpress.XtraEditors.TextEdit();
            this.txtIndicator = new DevExpress.XtraEditors.SpinEdit();
            this.dtExpriedDate = new DevExpress.XtraEditors.DateEdit();
            this.txtCuryRate = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRcptRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luFromCuryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luToBaseCuryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEffectiveDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEffectiveDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrInfo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndicator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtExpriedDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtExpriedDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCuryRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // navHeader
            // 
            this.navHeader.Buttons.Add(this.navHome);
            this.navHeader.Buttons.Add(this.navSave);
            this.navHeader.Buttons.Add(this.navClose);
            // 
            // tileNavCategory1
            // 
            this.navHeader.DefaultCategory.Name = "tileNavCategory1";
            // 
            // 
            // 
            this.navHeader.DefaultCategory.Tile.DropDownOptions.BeakColor = System.Drawing.Color.Empty;
            this.navHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.navHeader.Location = new System.Drawing.Point(0, 0);
            this.navHeader.Name = "navHeader";
            this.navHeader.Size = new System.Drawing.Size(488, 40);
            this.navHeader.TabIndex = 3;
            this.navHeader.Text = "q";
            // 
            // navHome
            // 
            this.navHome.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.navHome.Appearance.Options.UseFont = true;
            this.navHome.Caption = "Exchange Rate";
            this.navHome.Enabled = false;
            this.navHome.Name = "navHome";
            // 
            // navSave
            // 
            this.navSave.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navSave.AppearanceHovered.ForeColor = System.Drawing.Color.Gold;
            this.navSave.AppearanceHovered.Options.UseForeColor = true;
            this.navSave.Caption = "Save";
            this.navSave.Name = "navSave";
            this.navSave.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navSave_ElementClick);
            // 
            // navClose
            // 
            this.navClose.Alignment = DevExpress.XtraBars.Navigation.NavButtonAlignment.Right;
            this.navClose.AppearanceHovered.ForeColor = System.Drawing.Color.Red;
            this.navClose.AppearanceHovered.Options.UseForeColor = true;
            this.navClose.Caption = "Close";
            this.navClose.Name = "navClose";
            this.navClose.ElementClick += new DevExpress.XtraBars.Navigation.NavElementClickEventHandler(this.navClose_ElementClick);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtRcptRate);
            this.layoutControl1.Controls.Add(this.luFromCuryID);
            this.layoutControl1.Controls.Add(this.luToBaseCuryID);
            this.layoutControl1.Controls.Add(this.dtEffectiveDate);
            this.layoutControl1.Controls.Add(this.txtCurrInfo);
            this.layoutControl1.Controls.Add(this.txtIndicator);
            this.layoutControl1.Controls.Add(this.dtExpriedDate);
            this.layoutControl1.Controls.Add(this.txtCuryRate);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.layoutControlItem2});
            this.layoutControl1.Location = new System.Drawing.Point(0, 40);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(847, 304, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(488, 128);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtRcptRate
            // 
            this.txtRcptRate.EditValue = "0";
            this.txtRcptRate.Location = new System.Drawing.Point(51, 232);
            this.txtRcptRate.Name = "txtRcptRate";
            this.txtRcptRate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtRcptRate.Properties.Appearance.Options.UseFont = true;
            this.txtRcptRate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtRcptRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtRcptRate.Properties.DisplayFormat.FormatString = "n2";
            this.txtRcptRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtRcptRate.Properties.Mask.EditMask = "n";
            this.txtRcptRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtRcptRate.Size = new System.Drawing.Size(514, 22);
            this.txtRcptRate.StyleController = this.layoutControl1;
            this.txtRcptRate.TabIndex = 4;
            this.txtRcptRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luFromCuryID_KeyDown);
            // 
            // luFromCuryID
            // 
            this.luFromCuryID.Location = new System.Drawing.Point(51, 67);
            this.luFromCuryID.Name = "luFromCuryID";
            this.luFromCuryID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luFromCuryID.Properties.Appearance.Options.UseFont = true;
            this.luFromCuryID.Properties.Appearance.Options.UseTextOptions = true;
            this.luFromCuryID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luFromCuryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luFromCuryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CuryID", "CuryID")});
            this.luFromCuryID.Properties.NullText = "---Select---";
            this.luFromCuryID.Properties.PopupSizeable = false;
            this.luFromCuryID.Properties.ShowFooter = false;
            this.luFromCuryID.Properties.ShowHeader = false;
            this.luFromCuryID.Size = new System.Drawing.Size(230, 22);
            this.luFromCuryID.StyleController = this.layoutControl1;
            this.luFromCuryID.TabIndex = 0;
            this.luFromCuryID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luFromCuryID_KeyDown);
            // 
            // luToBaseCuryID
            // 
            this.luToBaseCuryID.Location = new System.Drawing.Point(51, 67);
            this.luToBaseCuryID.Name = "luToBaseCuryID";
            this.luToBaseCuryID.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.luToBaseCuryID.Properties.Appearance.Options.UseFont = true;
            this.luToBaseCuryID.Properties.Appearance.Options.UseTextOptions = true;
            this.luToBaseCuryID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luToBaseCuryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luToBaseCuryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CuryID", "CuryID")});
            this.luToBaseCuryID.Properties.NullText = "---Select---";
            this.luToBaseCuryID.Properties.PopupSizeable = false;
            this.luToBaseCuryID.Properties.ShowFooter = false;
            this.luToBaseCuryID.Properties.ShowHeader = false;
            this.luToBaseCuryID.Size = new System.Drawing.Size(514, 22);
            this.luToBaseCuryID.StyleController = this.layoutControl1;
            this.luToBaseCuryID.TabIndex = 1;
            this.luToBaseCuryID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luFromCuryID_KeyDown);
            // 
            // dtEffectiveDate
            // 
            this.dtEffectiveDate.EditValue = null;
            this.dtEffectiveDate.Location = new System.Drawing.Point(51, 192);
            this.dtEffectiveDate.Name = "dtEffectiveDate";
            this.dtEffectiveDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.dtEffectiveDate.Properties.Appearance.Options.UseFont = true;
            this.dtEffectiveDate.Properties.Appearance.Options.UseTextOptions = true;
            this.dtEffectiveDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.dtEffectiveDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtEffectiveDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtEffectiveDate.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dtEffectiveDate.Properties.MaxValue = new System.DateTime(((long)(0)));
            this.dtEffectiveDate.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dtEffectiveDate.Size = new System.Drawing.Size(514, 22);
            this.dtEffectiveDate.StyleController = this.layoutControl1;
            this.dtEffectiveDate.TabIndex = 5;
            this.dtEffectiveDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtEffectiveDate_KeyDown);
            // 
            // txtCurrInfo
            // 
            this.txtCurrInfo.Location = new System.Drawing.Point(130, 12);
            this.txtCurrInfo.Name = "txtCurrInfo";
            this.txtCurrInfo.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtCurrInfo.Properties.Appearance.Options.UseFont = true;
            this.txtCurrInfo.Size = new System.Drawing.Size(303, 22);
            this.txtCurrInfo.StyleController = this.layoutControl1;
            this.txtCurrInfo.TabIndex = 6;
            // 
            // txtIndicator
            // 
            this.txtIndicator.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtIndicator.Location = new System.Drawing.Point(51, 122);
            this.txtIndicator.Name = "txtIndicator";
            this.txtIndicator.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtIndicator.Properties.Appearance.Options.UseFont = true;
            this.txtIndicator.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtIndicator.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.txtIndicator.Properties.Mask.EditMask = "d";
            this.txtIndicator.Size = new System.Drawing.Size(514, 22);
            this.txtIndicator.StyleController = this.layoutControl1;
            this.txtIndicator.TabIndex = 2;
            this.txtIndicator.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luFromCuryID_KeyDown);
            // 
            // dtExpriedDate
            // 
            this.dtExpriedDate.EditValue = null;
            this.dtExpriedDate.Location = new System.Drawing.Point(51, 355);
            this.dtExpriedDate.Name = "dtExpriedDate";
            this.dtExpriedDate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.dtExpriedDate.Properties.Appearance.Options.UseFont = true;
            this.dtExpriedDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtExpriedDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtExpriedDate.Size = new System.Drawing.Size(514, 22);
            this.dtExpriedDate.StyleController = this.layoutControl1;
            this.dtExpriedDate.TabIndex = 7;
            // 
            // txtCuryRate
            // 
            this.txtCuryRate.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCuryRate.Location = new System.Drawing.Point(42, 60);
            this.txtCuryRate.Name = "txtCuryRate";
            this.txtCuryRate.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtCuryRate.Properties.Appearance.Options.UseFont = true;
            this.txtCuryRate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCuryRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCuryRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtCuryRate.Properties.DisplayFormat.FormatString = "n0";
            this.txtCuryRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCuryRate.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.txtCuryRate.Properties.Mask.EditMask = "n0";
            this.txtCuryRate.Properties.MaxLength = 10;
            this.txtCuryRate.Size = new System.Drawing.Size(391, 22);
            this.txtCuryRate.StyleController = this.layoutControl1;
            this.txtCuryRate.TabIndex = 3;
            this.txtCuryRate.EditValueChanged += new System.EventHandler(this.txtCuryRate_EditValueChanged);
            this.txtCuryRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.luFromCuryID_KeyDown);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.Control = this.dtExpriedDate;
            this.layoutControlItem8.Location = new System.Drawing.Point(39, 324);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(518, 45);
            this.layoutControlItem8.Text = "Expried Date";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.txtRcptRate;
            this.layoutControlItem6.Location = new System.Drawing.Point(39, 201);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(518, 45);
            this.layoutControlItem6.Text = "Receipt Rate";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(124, 16);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.txtIndicator;
            this.layoutControlItem4.Location = new System.Drawing.Point(39, 91);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(518, 45);
            this.layoutControlItem4.Text = "Indicator";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(124, 16);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.luFromCuryID;
            this.layoutControlItem1.Location = new System.Drawing.Point(39, 36);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(234, 45);
            this.layoutControlItem1.Text = "From Currency ID";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(124, 16);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.luToBaseCuryID;
            this.layoutControlItem3.Location = new System.Drawing.Point(39, 36);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(518, 45);
            this.layoutControlItem3.Text = "To Base Currency ID";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(124, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.dtEffectiveDate;
            this.layoutControlItem7.Location = new System.Drawing.Point(39, 161);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(518, 45);
            this.layoutControlItem7.Text = "Effective Date";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(124, 16);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.txtCurrInfo;
            this.layoutControlItem2.Location = new System.Drawing.Point(30, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(395, 26);
            this.layoutControlItem2.Text = "CurrInfo";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(124, 16);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.layoutControlItem5,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.emptySpaceItem8});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(488, 128);
            this.Root.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(30, 74);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(395, 34);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtCuryRate;
            this.layoutControlItem5.Location = new System.Drawing.Point(30, 29);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(395, 45);
            this.layoutControlItem5.Text = "Currency Rate";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(85, 16);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(30, 108);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(425, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(43, 108);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(30, 0);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(395, 29);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // CurrencyRateSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 168);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.navHeader);
            this.Name = "CurrencyRateSetup";
            this.Text = "Exchange RateSetup";
            ((System.ComponentModel.ISupportInitialize)(this.navHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRcptRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luFromCuryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luToBaseCuryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEffectiveDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEffectiveDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrInfo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndicator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtExpriedDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtExpriedDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCuryRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal DevExpress.XtraBars.Navigation.NavButton navHome;
        private DevExpress.XtraBars.Navigation.NavButton navSave;
        private DevExpress.XtraBars.Navigation.NavButton navClose;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        public DevExpress.XtraBars.Navigation.TileNavPane navHeader;
        public DevExpress.XtraEditors.TextEdit txtRcptRate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        public DevExpress.XtraEditors.LookUpEdit luFromCuryID;
        public DevExpress.XtraEditors.LookUpEdit luToBaseCuryID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        internal DevExpress.XtraEditors.DateEdit dtEffectiveDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        internal DevExpress.XtraEditors.TextEdit txtCurrInfo;
        internal DevExpress.XtraEditors.SpinEdit txtIndicator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        internal DevExpress.XtraEditors.DateEdit dtExpriedDate;
        internal DevExpress.XtraEditors.SpinEdit txtCuryRate;
    }
}