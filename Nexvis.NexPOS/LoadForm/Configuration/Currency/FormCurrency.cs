﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ZooMangement;
using Nexvis.NexPOS.Helper;
using Nexvis.NexPOS.Data;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.Configuration.Currency
{
    public partial class scrCury : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrCury()
        {
            InitializeComponent();
        }

        List<CSCurrency> _CSCurrency = new List<CSCurrency>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();


        public bool isSuccessful = true;

        private void FormCurrency_Load(object sender, EventArgs e)
        {
            _CSCurrency = DB.CSCurrencies.Where(x => x.CompanyID.Equals(DACClasses.CompanyID)).ToList();
            GC.DataSource = _CSCurrency.ToList();

            _RoleItems = DB.CSRoleItems.ToList();
        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {

        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormCurrency_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    CSCurrency result = new CSCurrency();
                    result = (CSCurrency)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
                        {
                            CSCurrency cSCurrency = DB.CSCurrencies.FirstOrDefault(st => st.CompanyID.Equals(result.CompanyID) && st.CuryID.Equals(result.CuryID));

                            DB.CSCurrencies.Remove(cSCurrency);
                            DB.SaveChanges();
                            FormCurrency_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navEdit_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == "Edit"))
                {
                    CurrencySetup objForm = new CurrencySetup("EDIT", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    // GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                    CSCurrency result = new CSCurrency();

                    result = (CSCurrency)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        objForm.txtCurrencyID.Text = result.CuryID;
                        objForm.txtCurrencyID.Enabled = false;
                        objForm.txtDescr.Focus();
                        objForm.txtDescr.Text = result.Descr;
                        objForm.txtSymbol.Text = result.Symbol;

                        Shadow.ShowDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access Edit !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


               

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                // TODO: Declare from FrmCompanyProfileModification for asign values.
                CurrencySetup objForm = new CurrencySetup("NEW", this);
                FormShadow Shadow = new FormShadow(objForm);

                //TODO: Binding value to comboBox or LookupEdit.
                //Global Initializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.cboCurrency, (int)currencies.ToList().Count);

                // objForm._cities = _cities;  
                Shadow.ShowDialog();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Actions
        public void GsSaveData(CurrencySetup objChild)
        {
            objChild.isExiting = false;
            if (_CSCurrency.Any(x => x.CuryID.Trim().ToLower() == objChild.txtCurrencyID.Text.Trim().ToLower()))
            {
                objChild.isExiting = true;
                return;
            }

            if (XtraMessageBox.Show("Are you sure you want to save this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            CSCurrency cSCurrency = new CSCurrency()
            {
                CompanyID = DACClasses.CompanyID,
                CuryID = objChild.txtCurrencyID.Text,
                Symbol = objChild.txtSymbol.Text,
                Descr = objChild.txtDescr.Text
            };

            DB.CSCurrencies.Add(cSCurrency);
            var Row = DB.SaveChanges();
            if (Row == 1)
            {
                isSuccessful = true;
            }
            GV.FocusedRowHandle = 0;
            XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
            navRefresh_ElementClick(null, null);
        }
        public void GsUpdateData(CurrencySetup objChild)
        {
            try
            {
                //if (_CSTax.Any(x => x.TaxID.Trim().ToLower() != objChild.txtTaxID.Text.Trim().ToLower() && x.CompanyID.Equals(DACClasses.CompanyID)))
                //{
                //    XtraMessageBox.Show("This record ready existing !", "POS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    objChild.txtTaxRate.Focus();
                //    return;
                //}

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

                var _obj = DB.CSCurrencies.SingleOrDefault(w => w.CuryID == objChild.txtCurrencyID.Text && w.CompanyID.Equals(DACClasses.CompanyID));
                _obj.Descr = objChild.txtDescr.Text;
                _obj.Symbol = objChild.txtSymbol.Text;

                DB.CSCurrencies.Attach(_obj);
                DB.Entry(_obj).Property(w => w.Descr).IsModified = true;
                DB.Entry(_obj).Property(w => w.Symbol).IsModified = true;

                var Row = DB.SaveChanges();
                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                objChild.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void GV_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            Point point = view.GridControl.PointToClient(Control.MousePosition);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = view.CalcHitInfo(point);
            if (info.InRow || info.InRowCell)
                navEdit_ElementClick(null, null);
        }

        private void navExport_ElementClick_1(object sender, NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}