﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Nexvis.NexPOS.Data;
using ZooMangement;
using Nexvis.NexPOS.Helper;
using DevExpress.XtraGrid.Views.Grid;
using Nexvis.NexPOS;
using DevExpress.XtraBars.Navigation;

namespace DgoStore.LoadForm.Configuration.Currency
{
    public partial class scrCuryRate : DevExpress.XtraEditors.XtraForm
    {
        ZooEntity DB = new ZooEntity();
        public scrCuryRate()
        {
            InitializeComponent();

        }

        List<CSCurrency> currencies = new List<CSCurrency>();
        List<CSCuryRate> _CSCurrencyRate = new List<CSCuryRate>();
        List<CSRoleItem> _RoleItems = new List<CSRoleItem>();


        public bool isSuccessful = true;
        private void FormCurrencyRate_Load(object sender, EventArgs e)
        {
            try
            {
                _CSCurrencyRate = DB.CSCuryRates.Where(x => x.CompanyID == DACClasses.CompanyID).ToList();
                GC.DataSource = _CSCurrencyRate.ToList();
                currencies = DB.CSCurrencies.ToList();
                _RoleItems = DB.CSRoleItems.ToList();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void navExport_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {

            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
           
        }

        private void navNew_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    // TODO: Declare from FrmCompanyProfileModification for asign values.
                    CurrencyRateSetup objForm = new CurrencyRateSetup("NEW", this);
                    FormShadow Shadow = new FormShadow(objForm);

                    //TODO: Binding value to comboBox or LookupEdit.s
                    GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.luFromCuryID, (int)currencies.ToList().Count);

                    // GlobalInitializer.GsFillLookUpEdit(_CSCurrencyRate.ToList(), "ToBaseCuryID", "ToBaseCuryID", objForm.luToBaseCuryID, (int)_CSCurrencyRate.ToList().Count);

                    GlobalInitializer.GsFillLookUpEdit(currencies.ToList(), "CuryID", "CuryID", objForm.luToBaseCuryID, (int)currencies.ToList().Count);


                    //objForm.dtEffectiveDate.DateTime = DateTime.Today.Date;
                    Shadow.ShowDialog();
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

              
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navClose_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        public void navRefresh_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            //TODO: Reload records.
            FormCurrencyRate_Load(sender, e);

            //TODO : Clear in field for filter.
            GV.ClearColumnsFilter();
        }

        private void navDelete_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            try
            {
                NavButton navigator = (NavButton)sender;
                if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
                {
                    CSCuryRate result = new CSCuryRate();
                    result = (CSCuryRate)GV.GetRow(GV.FocusedRowHandle);

                    if (result != null)
                    {
                        if (XtraMessageBox.Show("Are you sure you want to delete this record?", "POS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            return;
                        {
                            CSCuryRate site = DB.CSCuryRates.FirstOrDefault(st => st.CuryInfoID.Equals(result.CuryInfoID));

                            DB.CSCuryRates.Remove(site);
                            DB.SaveChanges();
                            FormCurrencyRate_Load(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("You cannot access " + navigator.Caption + " !", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }


              
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
      
        public void GsSaveData(CurrencyRateSetup objChild)
        {
            try
            {
                objChild.isExiting = false;
                if (_CSCurrencyRate.Any(x => x.CuryInfoID.ToString() == objChild.txtCurrInfo.Text.Trim().ToLower()))
                {
                    objChild.isExiting = true;
                    return;
                }

                CSCuryRate site = new CSCuryRate()
                {
                    CompanyID = DACClasses.CompanyID,
                    FromCuryID = "KHR",
                    ToBaseCuryID = "USD",
                    CuryRate = Convert.ToDecimal(objChild.txtCuryRate.EditValue),
                    EffectiveDate = DateTime.Today.Date,
                    IsActive = true
                };

                DB.CSCuryRates.Add(site);

                _CSCurrencyRate.ForEach(x => x.IsActive = false);

                foreach (var item in _CSCurrencyRate)
                {
                    DB.Entry(item).State = (System.Data.Entity.EntityState)EntityState.Modified;
                }

                var Row = DB.SaveChanges();
                if (Row != 0)
                {
                    isSuccessful = true;
                }


                GV.FocusedRowHandle = 0;
                XtraMessageBox.Show("This field has been saved.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                navRefresh_ElementClick(null, null);
            }
            catch(Exception ex)
            {
                
                XtraMessageBox.Show("Chose Currency Rate again!!!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
         
        }
        public void GsUpdateData(CurrencyRateSetup objChild)
        {
            try
            {

                if (XtraMessageBox.Show("Are you sure you want to modify this record?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;


                var _comp = DB.CSCuryRates.SingleOrDefault(w => w.CuryInfoID.ToString() == objChild.txtCurrInfo.Text);

                _comp.CuryRate = Convert.ToDecimal(objChild.txtCuryRate.EditValue);
              //  _comp.RcptRate = Convert.ToDecimal(objChild.txtRcptRate.EditValue);
                _comp.EffectiveDate = (DateTime?)objChild.dtEffectiveDate.DateTime.Date;






                DB.CSCuryRates.Attach(_comp);
                //DB.Entry(_comp).Property(w => w.Indicator).IsModified = true;
                DB.Entry(_comp).Property(w => w.CuryRate).IsModified = true;

                var Row = DB.SaveChanges();
                XtraMessageBox.Show("This field has been updated.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                objChild.Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void navExport_ElementClick_1(object sender, NavElementEventArgs e)
        {
            NavButton navigator = (NavButton)sender;
            if (_RoleItems.Any(x => x.RoleId == CSUserModel.Role && x.ScreenId == this.Name && x.ActionName == navigator.Caption))
            {
                if (GV.RowCount > 0)
                {
                    // TODO: Export data in GridControl as Excel file.
                    GlobalInitializer.GsExportData(GC);
                }
                else
                    XtraMessageBox.Show("Have no once record for export!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                XtraMessageBox.Show("You cannot access " + navigator.Caption + "!", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}