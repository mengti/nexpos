﻿
using DevExpress.Utils.MVVM.Services;
using Nexvis.NexPOS.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZooManagement;
using ZooManagement.Connection;
using Nexvis.NexPOS.Data;
using DevExpress.XtraBars;
using ZooManagement.Helper;
using ZooMangement;

namespace Nexvis.NexPOS.LoadForm
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
           // frmLogin_Load(null, null);
        }
        //DefaultSoapClient soapClient;

        public List<CSUser> UserList = new List<CSUser>();
        ZooEntity DB = new ZooEntity();
        private void frmLogin_Load(object sender, EventArgs e)
        {
            frmConnection cn = new frmConnection();
            try
            {
                var listuser = DB.CSUsers.ToList();

                foreach (var row in listuser)
                {
                    CSUser user = new CSUser()
                    {
                        CompanyID = row.CompanyID,
                        UserID = row.UserID,
                        UserName = row.UserName,
                        Status = row.Status,
                        Password = row.Password
                    };
                    UserList.Add(user);
                }
               // this.Refresh();

            }
           catch (Exception ex)
           {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                cn.ShowDialog();

                Application.Exit();
            }
        }

        void InitialBarButton(frmMain main)
        {
            main.scrDashboard.Visibility = BarItemVisibility.Never;
            main.scrSaleOrder.Visibility = BarItemVisibility.Never;
            main.scrCategory.Visibility = BarItemVisibility.Never;
            main.scrItem.Visibility = BarItemVisibility.Never;
            main.scrDailyInvoice.Visibility = BarItemVisibility.Never;
            main.scrDetailsInvoice.Visibility = BarItemVisibility.Never;
            main.scrCustomer.Visibility = BarItemVisibility.Never;
            main.scrCury.Visibility = BarItemVisibility.Never;
            main.scrCuryRate.Visibility = BarItemVisibility.Never;
            main.scrCompany.Visibility = BarItemVisibility.Never;
            main.scrEmployee.Visibility = BarItemVisibility.Never;
            main.scrPayment.Visibility = BarItemVisibility.Never;
            main.scrRole.Visibility = BarItemVisibility.Never;
            main.scrSalePrice.Visibility = BarItemVisibility.Never;
            main.scrStoreInfor.Visibility = BarItemVisibility.Never;
            main.scrUOM.Visibility = BarItemVisibility.Never;
            main.scrUser.Visibility = BarItemVisibility.Never;
            main.scrPermission.Visibility = BarItemVisibility.Never;
            main.scrCashierBalance.Visibility = BarItemVisibility.Never;
            main.scrBarcode.Visibility = BarItemVisibility.Never;
        }
        public static string isAuthenticated(string userName, string Pwd)
        {
            ZooEntity DB = new ZooEntity();
            List<CSUser> usrList = DB.CSUsers.Where(w => w.UserName == userName).ToList();
            if (usrList.Count > 0)
            {

                var listUser = DB.CSUsers.ToList().Count;

                CSUser obj = usrList.First();
                //if (obj.ExpireDate < DateTime.Now)
                //{
                //    return "USR_EXP";
                //}
                //if (obj.IsActive == false)
                //{
                //    return "USR_INC";
                //}

                bool b = PasswordHash.ValidatePassword(Pwd, obj.Password);
                if (b == true)
                {
                    return SYConstant.OK;
                }
            }
            return "AUTH_FAIL";
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            List<CSUser> user = DB.CSUsers.Where(w => w.UserName == txtusername.Text).ToList();
            frmMain main = new frmMain();

            CSUser usr = null;      
            InitialBarButton(main);
            string msg = "";

            msg = isAuthenticated(txtusername.Text, txtpass.Text);
            if (msg == SYConstant.OK)
            {
                if (user != null)
                {
                    if (user.Count > 0)
                    {
                        foreach (CSUser read in user)
                        {
                            usr = read;
                        }
                        List<CSUserRole> ListUserRole = DB.CSUserRoles.Where(w => w.UserID == usr.UserID).ToList();
                        List<CSRoleItem> ListRoleItem = new List<CSRoleItem>();
                        if (ListUserRole.Count > 0)
                        {
                            foreach (var read in ListUserRole)
                            {

                                List<CSRoleItem> ListRoleItem_t = DB.CSRoleItems.Where(w => w.RoleId == read.RoleId).ToList();
                                foreach (var ri_t in ListRoleItem_t)
                                {
                                    if (ListRoleItem.Where(w => w.ScreenId == ri_t.ScreenId && w.ActionName == ri_t.ActionName).ToList().Count == 0)
                                    {
                                        ListRoleItem.Add(ri_t);
                                    }
                                }

                                //Show Screen
                                foreach (var item in ListRoleItem.ToList())
                                {
                                    if (item.ScreenId == main.scrDashboard.Name)
                                    {
                                        main.scrDashboard.Visibility = BarItemVisibility.Always;
                                        main.dash = true;
                                    }

                                    else if (item.ScreenId == main.scrStoreInfor.Name) main.scrStoreInfor.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrDailyInvoice.Name) main.scrDailyInvoice.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrDetailsInvoice.Name) main.scrDetailsInvoice.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrCategory.Name) main.scrCategory.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrItem.Name) main.scrItem.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrSalePrice.Name) main.scrSalePrice.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrSaleOrder.Name) main.scrSaleOrder.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrRole.Name) main.scrRole.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrPermission.Name) main.scrPermission.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrPayment.Name) main.scrPayment.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrCompany.Name) main.scrCompany.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrCury.Name) main.scrCury.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrCuryRate.Name) main.scrCuryRate.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrCustomer.Name) main.scrCustomer.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrUOM.Name) main.scrUOM.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrUser.Name) main.scrUser.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrEmployee.Name) main.scrEmployee.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrCashierBalance.Name) main.scrCashierBalance.Visibility = BarItemVisibility.Always;
                                    else if (item.ScreenId == main.scrBarcode.Name) main.scrBarcode.Visibility = BarItemVisibility.Always;

                                }
                                
                                CSUserModel.User = usr.UserID;
                                CSUserModel.LoginName = usr.UserName;
                                CSUserModel.Role = read.RoleId;
                                CSShopModel.ShopLogo = DB.ZooInfoes.First().UrlImageLogo ;
                                CSShopModel.ZooName = DB.ZooInfoes.First().ZooInfoName;
                                CSShopModel.ZooID = DB.ZooInfoes.First().ZooInfoID;
                                CSShopModel.ZooAddress = DB.ZooInfoes.First().Address;
                                CSShopModel.ZooPhone = DB.ZooInfoes.First().Contact;
                                this.Hide();
                                main.Show();
                                
                            }
                        }
                    }
                }
            }
            else
            {
                lblshow.Text = "ឈ្មោះ នឹង លេខសំងាត់ខុស !/n សូមវាយម្ដងទៀត !";
            }
            #region From server
            //login
            //soapClient = new DefaultSoapClient();
            //soapClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(DgoService.connectServer);
            //soapClient.Login
            //(
            //  "admin",
            //  "@dmin123",
            //  "Live",
            //  null,
            //  null
            //);

            #region Category
            //get category into comboBox
            //List<CategoryModel> cate = new List<CategoryModel>();
            //DgoEndPoint.Category sto = new DgoEndPoint.Category
            //{
            // StoreClassID = new StringSearch { Value =DgoService.storeClassID }
            //};

            //Entity[] stos = soapClient.GetList(sto);
            //SqlCommand cmd = new SqlCommand(@"Delete From tb_Category ", Connections.conn);
            //cmd.ExecuteNonQuery();
            //foreach (var cItem in stos)
            //{
            //    DgoEndPoint.Category ct = (DgoEndPoint.Category)cItem;
            //    cmd = new SqlCommand();
            //    cmd.Connection = Connections.conn;
            //    string str = @"INSERT INTO tb_Category(CateID,CateName) VALUES('" + ct.CategoryID.Value + "','" + ct.CateName.Value + "')";
            //    cmd.CommandText = str;
            //    cmd.ExecuteNonQuery();
            //}

            #endregion

            #region ItemDetail
            //Get itemDetail into dataGridview

            //DgoEndPoint.Store customer = new DgoEndPoint.Store
            //{
            //    StoreClassID = new StringSearch { Value = DgoService.storeClassID },
            //    StoreID = new StringSearch { Value = DgoService.storeID },
            //    ItemDetail = new ItemDetail[]
            //     {
            //    new ItemDetail {ReturnBehavior = ReturnBehavior.All}
            //     },
            //};
            //DgoEndPoint.Store store = (DgoEndPoint.Store)soapClient.Get(customer);

            //SqlCommand cmd = new SqlCommand(@"Delete From tb_Item ", Connections.conn);
            //cmd.ExecuteNonQuery();
            //foreach (ItemDetail ids in store.ItemDetail)
            //{
            //    cmd = new SqlCommand();
            //    cmd.Connection = Connections.conn;
            //    string str = "INSERT INTO tb_Item (CateID,ItemID,ItemCode,ItemName,Description,MealTime,Price,DisPrice,DisPercent,UrlImage) " +
            //        "VALUES('" + ids.CategoryID.Value + "','" + ids.ItemID.Value + "','" + ids.ItemCode.Value + "','" + ids.ItemName.Value + "','" + ids.Description.Value + "','" + ids.MealTime.Value + "','" + ids.Price.Value + "','" + ids.DiscountPrice.Value + "','" + ids.DiscountPercentage.Value + "','" + ids.UrlImage.Value + "')";
            //    cmd.CommandText = str;
            //    cmd.ExecuteNonQuery();
            //}
            #endregion

            #region store infor
            ////Get store info
            //List<StoreModel> storeModels = new List<StoreModel>();
            //DgoEndPoint.Store store = new DgoEndPoint.Store
            //{
            //    StoreClassID = new StringSearch { Value = DgoService.storeClassID },
            //    StoreID = new StringSearch { Value = DgoService.storeID },

            //};
            //Entity[] stores = soapClient.GetList(store);

            //SqlCommand cmd = new SqlCommand("Delete From tb_StoreInfor ", Connections.conn);
            //cmd.ExecuteNonQuery();

            //foreach (var cItem in stores)
            //{

            //    DgoEndPoint.Store ct = (DgoEndPoint.Store)cItem;
            //    cmd = new SqlCommand();
            //    cmd.Connection = Connections.conn;
            //    string str = "INSERT INTO tb_StoreInfor(StoreClassID,StoreID,StoreName,RegisterDate,StartTime,CloseTime,SpokenLanguage,OrderType,OpenDay,StoreTypeID,Atmosphere,Address,Contact,Information,Email,Website) " +
            //        "VALUES('" + ct.StoreClassID.Value + "','" + ct.StoreID.Value + "','" + ct.StoreName.Value + "','" + ct.RegisterDate.Value + "','" + ct.StartTime.Value + "','" + ct.CloseTime.Value + "','" + ct.SpokenLanguages.Value + "','" + ct.OrderType.Value + "','" + ct.OpenDays.Value + "','" + ct.StoreType.Value + "','" + ct.Atmospheres.Value + "','" + ct.Address.Value + "','" + ct.ContactNumber.Value + "','" + ct.Information.Value + "','" + ct.Email.Value + "','" + ct.Website.Value + "')";
            //    cmd.CommandText = str;
            //    cmd.ExecuteNonQuery();
            //}

            #endregion

            #endregion
    }

        private void chshowpass_CheckedChanged(object sender, EventArgs e)
        {
            txtpass.Properties.UseSystemPasswordChar = !chshowpass.Checked;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtusername_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtpass.Focus();
                txtpass.SelectAll();
            }
        }

        private void txtpass_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin_Click(sender, e);
            }
        }
        public  bool ToRestart = false;


        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
            frmConnection cn = new frmConnection();
            FormShadow sh = new FormShadow(cn);
            ToRestart = true;
            sh.ShowDialog();
           
        }

        private void frmLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
