﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DgoStore
{
    public partial class PROGRESSBAR_FRM : Form//DevComponents.DotNetBar.Office2007Form
    {
        public PROGRESSBAR_FRM()
        {
            InitializeComponent();
        }

        private void PROGRESSBAR_FRM_Load(object sender, EventArgs e)
        {
            lblTimeRemaining.Text = "Time remaining: Calculating...";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
