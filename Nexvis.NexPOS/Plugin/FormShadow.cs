﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nexvis.NexPOS
{ 
    public partial class FormShadow : Form
    {
        private Form _FormDisplay; 
        private XtraUserControl _ControlDisplay;
        private Process _Process;

        public FormShadow(Form FormDisplay, [Optional] bool isTransparency)
        {
            InitializeComponent();
            this.Opacity = 0.5;
            this.BackColor = SystemColors.WindowFrame;
            this.ShowInTaskbar = false;

            this.FormBorderStyle = FormBorderStyle.None;
            this.StartPosition = FormStartPosition.Manual;
            this.WindowState = FormWindowState.Maximized;

            FormDisplay.ControlBox = false;
            FormDisplay.Text = string.Empty;
            FormDisplay.FormBorderStyle = FormBorderStyle.FixedDialog;
            FormDisplay.StartPosition = FormStartPosition.CenterScreen;
            FormDisplay.WindowState = FormWindowState.Normal;

            _FormDisplay = FormDisplay;
            if (isTransparency == true)
            {
                _FormDisplay.BackColor = Color.Blue;
                _FormDisplay.TransparencyKey = Color.Blue;
            }
            _FormDisplay.FormClosed += new FormClosedEventHandler(_FormClosed);
            this.Shown+= new EventHandler(_FormShown);
        }

        public FormShadow(XtraUserControl FormDisplay)
        {
            InitializeComponent();
            this.Opacity = 0.5;
            this.BackColor = SystemColors.WindowFrame;
            this.ShowInTaskbar = false;

            this.FormBorderStyle = FormBorderStyle.None;
            this.StartPosition = FormStartPosition.Manual;
            this.WindowState = FormWindowState.Maximized;

            _ControlDisplay = FormDisplay;
            this.ControlRemoved += new ControlEventHandler(_ControlRemoved);
            this.Shown += new EventHandler(_ControlShown);
        }



        public FormShadow(Process process)
        {
            InitializeComponent();
            this.Opacity = 0.5;
            this.BackColor = SystemColors.WindowFrame;
            this.ShowInTaskbar = false;

            this.FormBorderStyle = FormBorderStyle.None;
            this.StartPosition = FormStartPosition.Manual;
            this.WindowState = FormWindowState.Maximized;

            _Process = process;
            this.ControlRemoved += new ControlEventHandler(_ControlRemoved);
            this.Shown += new EventHandler(_ControlShown);
        }

        private void _ControlShown(object sender, EventArgs e)
        {
            try
            {
                this.Controls.Add(_ControlDisplay);
                _ControlDisplay.Location = new Point(this.ClientSize.Width / 2 - _ControlDisplay.Size.Width / 2, this.ClientSize.Height / 2 - _ControlDisplay.Size.Height / 2);
                _ControlDisplay.Anchor = AnchorStyles.None;
            }
            catch (Exception)
            {
                this.Close();
            }
        }

        private void _ControlRemoved(object sender, ControlEventArgs e)
        {
            if (this.Controls.Count <= 0) this.Close();
        }

        private void _FormShown(object sender, EventArgs e)
        {
            try
            {
               _FormDisplay.ShowInTaskbar = false;
                _FormDisplay.ShowDialog();
            }
                catch (Exception)
            {
                this.Close();
            }            
        }

        private void _FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = _FormDisplay.DialogResult;
            this.Close();
        }

        private void FrmShadow_Load(object sender, EventArgs e)
        {
            //_FormDisplay.ShowDialog();
            return;
        }
    }
}
