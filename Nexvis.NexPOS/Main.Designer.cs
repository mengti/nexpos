﻿namespace Nexvis.NexPOS
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            DevExpress.Utils.Animation.Transition transition1 = new DevExpress.Utils.Animation.Transition();
            DevExpress.Utils.Animation.SlideFadeTransition slideFadeTransition1 = new DevExpress.Utils.Animation.SlideFadeTransition();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.scrCategory = new DevExpress.XtraBars.BarButtonItem();
            this.scrItem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.scrStoreInfor = new DevExpress.XtraBars.BarButtonItem();
            this.scrSaleOrder = new DevExpress.XtraBars.BarButtonItem();
            this.scrEmployee = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonUOM = new DevExpress.XtraBars.BarButtonItem();
            this.scrDashboard = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.scrUOM = new DevExpress.XtraBars.BarButtonItem();
            this.scrUser = new DevExpress.XtraBars.BarButtonItem();
            this.scrRole = new DevExpress.XtraBars.BarButtonItem();
            this.scrCompany = new DevExpress.XtraBars.BarButtonItem();
            this.scrPayment = new DevExpress.XtraBars.BarButtonItem();
            this.scrCury = new DevExpress.XtraBars.BarButtonItem();
            this.scrCuryRate = new DevExpress.XtraBars.BarButtonItem();
            this.scrCustomer = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.galleryDropDown1 = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.scrDailyInvoice = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.scrDetailsInvoice = new DevExpress.XtraBars.BarButtonItem();
            this.scrSalePrice = new DevExpress.XtraBars.BarButtonItem();
            this.scrPermission = new DevExpress.XtraBars.BarButtonItem();
            this.scrCashierBalance = new DevExpress.XtraBars.BarButtonItem();
            this.scrBarcode = new DevExpress.XtraBars.BarButtonItem();
            this.rbPageSale = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageProduct = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPagePrice = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageSaleManag = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rbPageGeneral = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageApp = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgPageReports = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rbPageConfiguration = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageCompanySetup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGensetup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageUserPermission = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.transitionManager = new DevExpress.Utils.Animation.TransitionManager(this.components);
            this.btnLogout = new DevExpress.XtraEditors.SimpleButton();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.ribbonControl1.SearchEditItem,
            this.scrCategory,
            this.scrItem,
            this.barButtonItem1,
            this.scrStoreInfor,
            this.scrSaleOrder,
            this.scrEmployee,
            this.barButtonUOM,
            this.scrDashboard,
            this.barButtonItem3,
            this.scrUOM,
            this.scrUser,
            this.scrRole,
            this.scrCompany,
            this.scrPayment,
            this.scrCury,
            this.scrCuryRate,
            this.scrCustomer,
            this.barButtonItem7,
            this.barButtonItem8,
            this.scrDailyInvoice,
            this.barButtonItem2,
            this.barButtonItem9,
            this.scrDetailsInvoice,
            this.scrSalePrice,
            this.scrPermission,
            this.scrCashierBalance,
            this.scrBarcode});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 33;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rbPageSale,
            this.rbPageGeneral,
            this.rbPageConfiguration});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2019;
            this.ribbonControl1.Size = new System.Drawing.Size(1384, 156);
            // 
            // scrCategory
            // 
            this.scrCategory.Caption = "​​​​   Category                            ";
            this.scrCategory.Id = 1;
            this.scrCategory.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrCategory.ImageOptions.SvgImage")));
            this.scrCategory.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCategory.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrCategory.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCategory.ItemAppearance.Normal.Options.UseFont = true;
            this.scrCategory.Name = "scrCategory";
            this.scrCategory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnCategory_ItemClick);
            // 
            // scrItem
            // 
            this.scrItem.Caption = "      Item                      ";
            this.scrItem.Id = 2;
            this.scrItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrItem.ImageOptions.Image")));
            this.scrItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("scrItem.ImageOptions.LargeImage")));
            this.scrItem.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrItem.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrItem.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrItem.ItemAppearance.Normal.Options.UseFont = true;
            this.scrItem.Name = "scrItem";
            this.scrItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnItem_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "      Close                        ";
            this.barButtonItem1.Id = 3;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // scrStoreInfor
            // 
            this.scrStoreInfor.Caption = "Store Infomation                            ";
            this.scrStoreInfor.Id = 4;
            this.scrStoreInfor.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrStoreInfor.ImageOptions.Image")));
            this.scrStoreInfor.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("scrStoreInfor.ImageOptions.LargeImage")));
            this.scrStoreInfor.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrStoreInfor.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrStoreInfor.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrStoreInfor.ItemAppearance.Normal.Options.UseFont = true;
            this.scrStoreInfor.Name = "scrStoreInfor";
            this.scrStoreInfor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnStoreInfor_ItemClick);
            // 
            // scrSaleOrder
            // 
            this.scrSaleOrder.Caption = "      Sale Order                      ";
            this.scrSaleOrder.Id = 5;
            this.scrSaleOrder.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrSaleOrder.ImageOptions.SvgImage")));
            this.scrSaleOrder.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrSaleOrder.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrSaleOrder.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrSaleOrder.ItemAppearance.Normal.Options.UseFont = true;
            this.scrSaleOrder.Name = "scrSaleOrder";
            this.scrSaleOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnSale_ItemClick);
            // 
            // scrEmployee
            // 
            this.scrEmployee.Caption = "    Employees                  ";
            this.scrEmployee.Id = 6;
            this.scrEmployee.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrEmployee.ImageOptions.Image")));
            this.scrEmployee.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("scrEmployee.ImageOptions.LargeImage")));
            this.scrEmployee.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrEmployee.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrEmployee.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrEmployee.ItemAppearance.Normal.Options.UseFont = true;
            this.scrEmployee.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrEmployee.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.scrEmployee.Name = "scrEmployee";
            this.scrEmployee.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnStaff_ItemClick);
            // 
            // barButtonUOM
            // 
            this.barButtonUOM.Caption = "Unit Of Mesure";
            this.barButtonUOM.Id = 9;
            this.barButtonUOM.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonUOM.ImageOptions.Image")));
            this.barButtonUOM.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonUOM.ImageOptions.LargeImage")));
            this.barButtonUOM.Name = "barButtonUOM";
            // 
            // scrDashboard
            // 
            this.scrDashboard.Caption = "​​​​   Dashboard                            ";
            this.scrDashboard.Id = 10;
            this.scrDashboard.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrDashboard.ImageOptions.SvgImage")));
            this.scrDashboard.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrDashboard.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrDashboard.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrDashboard.ItemAppearance.Normal.Options.UseFont = true;
            this.scrDashboard.Name = "scrDashboard";
            this.scrDashboard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonDashboard_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Position";
            this.barButtonItem3.Id = 12;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // scrUOM
            // 
            this.scrUOM.Caption = "​​​​   Unit Of Measure                            ";
            this.scrUOM.Id = 13;
            this.scrUOM.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrUOM.ImageOptions.Image")));
            this.scrUOM.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrUOM.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrUOM.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrUOM.ItemAppearance.Normal.Options.UseFont = true;
            this.scrUOM.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrUOM.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.scrUOM.Name = "scrUOM";
            this.scrUOM.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.scrUOM.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonUnitOfMe_ItemClick);
            // 
            // scrUser
            // 
            this.scrUser.Caption = "​​​​   User                            ";
            this.scrUser.Id = 15;
            this.scrUser.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrUser.ImageOptions.Image")));
            this.scrUser.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("scrUser.ImageOptions.LargeImage")));
            this.scrUser.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrUser.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrUser.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrUser.ItemAppearance.Normal.Options.UseFont = true;
            this.scrUser.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrUser.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.scrUser.Name = "scrUser";
            this.scrUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonIUser_ItemClick);
            // 
            // scrRole
            // 
            this.scrRole.Caption = "​​​​   Role                            ";
            this.scrRole.Id = 16;
            this.scrRole.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrRole.ImageOptions.Image")));
            this.scrRole.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("scrRole.ImageOptions.LargeImage")));
            this.scrRole.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrRole.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrRole.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrRole.ItemAppearance.Normal.Options.UseFont = true;
            this.scrRole.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrRole.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.scrRole.Name = "scrRole";
            this.scrRole.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonRole_ItemClick);
            // 
            // scrCompany
            // 
            this.scrCompany.Caption = "​​​​   Company Profile                            ";
            this.scrCompany.Id = 17;
            this.scrCompany.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrCompany.ImageOptions.SvgImage")));
            this.scrCompany.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCompany.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrCompany.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCompany.ItemAppearance.Normal.Options.UseFont = true;
            this.scrCompany.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCompany.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.scrCompany.Name = "scrCompany";
            this.scrCompany.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonCompany_ItemClick);
            // 
            // scrPayment
            // 
            this.scrPayment.Caption = "​​​​   Payment                            ";
            this.scrPayment.Id = 18;
            this.scrPayment.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrPayment.ImageOptions.SvgImage")));
            this.scrPayment.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrPayment.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrPayment.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrPayment.ItemAppearance.Normal.Options.UseFont = true;
            this.scrPayment.Name = "scrPayment";
            this.scrPayment.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.scrPayment.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonPayment_ItemClick);
            // 
            // scrCury
            // 
            this.scrCury.Caption = "​​​​   Currency                           ";
            this.scrCury.Id = 19;
            this.scrCury.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrCury.ImageOptions.SvgImage")));
            this.scrCury.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCury.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrCury.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCury.ItemAppearance.Normal.Options.UseFont = true;
            this.scrCury.Name = "scrCury";
            this.scrCury.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonCurrency_ItemClick);
            // 
            // scrCuryRate
            // 
            this.scrCuryRate.Caption = "​​​​   Exchange Rate                            ";
            this.scrCuryRate.Id = 20;
            this.scrCuryRate.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrCuryRate.ImageOptions.SvgImage")));
            this.scrCuryRate.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCuryRate.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrCuryRate.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCuryRate.ItemAppearance.Normal.Options.UseFont = true;
            this.scrCuryRate.Name = "scrCuryRate";
            this.scrCuryRate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonCurrencyRate_ItemClick);
            // 
            // scrCustomer
            // 
            this.scrCustomer.Caption = "     Customer                    ";
            this.scrCustomer.Id = 21;
            this.scrCustomer.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrCustomer.ImageOptions.SvgImage")));
            this.scrCustomer.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCustomer.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrCustomer.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCustomer.ItemAppearance.Normal.Options.UseFont = true;
            this.scrCustomer.Name = "scrCustomer";
            this.scrCustomer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonCus_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.ActAsDropDown = true;
            this.barButtonItem7.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem7.Caption = "Logout";
            this.barButtonItem7.DropDownControl = this.galleryDropDown1;
            this.barButtonItem7.Id = 22;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // galleryDropDown1
            // 
            this.galleryDropDown1.Name = "galleryDropDown1";
            this.galleryDropDown1.Ribbon = this.ribbonControl1;
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "barButtonItem8";
            this.barButtonItem8.Id = 23;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // scrDailyInvoice
            // 
            this.scrDailyInvoice.Caption = "   Daily Invoice                      ";
            this.scrDailyInvoice.Id = 24;
            this.scrDailyInvoice.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrDailyInvoice.ImageOptions.Image")));
            this.scrDailyInvoice.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("scrDailyInvoice.ImageOptions.LargeImage")));
            this.scrDailyInvoice.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrDailyInvoice.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrDailyInvoice.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrDailyInvoice.ItemAppearance.Normal.Options.UseFont = true;
            this.scrDailyInvoice.Name = "scrDailyInvoice";
            this.scrDailyInvoice.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnSaleReport_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "​​​​   LotSerClass                            ";
            this.barButtonItem2.Id = 25;
            this.barButtonItem2.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem2.ImageOptions.SvgImage")));
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Logoout";
            this.barButtonItem9.Description = "Logout";
            this.barButtonItem9.Id = 26;
            this.barButtonItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.Image")));
            this.barButtonItem9.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.LargeImage")));
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // scrDetailsInvoice
            // 
            this.scrDetailsInvoice.Caption = "    Details Invoice                      ";
            this.scrDetailsInvoice.Id = 28;
            this.scrDetailsInvoice.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrDetailsInvoice.ImageOptions.Image")));
            this.scrDetailsInvoice.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("scrDetailsInvoice.ImageOptions.LargeImage")));
            this.scrDetailsInvoice.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrDetailsInvoice.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrDetailsInvoice.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrDetailsInvoice.ItemAppearance.Normal.Options.UseFont = true;
            this.scrDetailsInvoice.Name = "scrDetailsInvoice";
            this.scrDetailsInvoice.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnDetails_ItemClick);
            // 
            // scrSalePrice
            // 
            this.scrSalePrice.Caption = "      Price List                      ";
            this.scrSalePrice.Id = 29;
            this.scrSalePrice.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrSalePrice.ImageOptions.SvgImage")));
            this.scrSalePrice.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrSalePrice.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrSalePrice.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrSalePrice.ItemAppearance.Normal.Options.UseFont = true;
            this.scrSalePrice.Name = "scrSalePrice";
            this.scrSalePrice.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnSalePrice_ItemClick);
            // 
            // scrPermission
            // 
            this.scrPermission.Caption = "​​​​   User Management                            ";
            this.scrPermission.Id = 30;
            this.scrPermission.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrPermission.ImageOptions.Image")));
            this.scrPermission.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("scrPermission.ImageOptions.LargeImage")));
            this.scrPermission.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrPermission.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrPermission.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrPermission.ItemAppearance.Normal.Options.UseFont = true;
            this.scrPermission.Name = "scrPermission";
            this.scrPermission.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barbtnPerrmiss_ItemClick);
            // 
            // scrCashierBalance
            // 
            this.scrCashierBalance.Caption = "     Cashier Amount List                    ";
            this.scrCashierBalance.Id = 31;
            this.scrCashierBalance.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("scrCashierBalance.ImageOptions.Image")));
            this.scrCashierBalance.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("scrCashierBalance.ImageOptions.LargeImage")));
            this.scrCashierBalance.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCashierBalance.ItemAppearance.Hovered.Options.UseFont = true;
            this.scrCashierBalance.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.scrCashierBalance.ItemAppearance.Normal.Options.UseFont = true;
            this.scrCashierBalance.Name = "scrCashierBalance";
            this.scrCashierBalance.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.scrCashierBalance_ItemClick);
            // 
            // scrBarcode
            // 
            this.scrBarcode.Caption = "    Barcode                      ";
            this.scrBarcode.Id = 32;
            this.scrBarcode.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("scrBarcode.ImageOptions.SvgImage")));
            this.scrBarcode.Name = "scrBarcode";
            this.scrBarcode.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.scrBarcode_ItemClick);
            // 
            // rbPageSale
            // 
            this.rbPageSale.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.rbPageSale.Appearance.Options.UseFont = true;
            this.rbPageSale.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageProduct,
            this.ribbonPagePrice,
            this.ribbonPageSaleManag});
            this.rbPageSale.Name = "rbPageSale";
            this.rbPageSale.Text = "POS";
            // 
            // ribbonPageProduct
            // 
            this.ribbonPageProduct.ItemLinks.Add(this.scrCategory);
            this.ribbonPageProduct.ItemLinks.Add(this.scrItem);
            this.ribbonPageProduct.Name = "ribbonPageProduct";
            this.ribbonPageProduct.Text = "Product";
            // 
            // ribbonPagePrice
            // 
            this.ribbonPagePrice.ItemLinks.Add(this.scrSalePrice);
            this.ribbonPagePrice.Name = "ribbonPagePrice";
            this.ribbonPagePrice.Text = "Price";
            // 
            // ribbonPageSaleManag
            // 
            this.ribbonPageSaleManag.ItemLinks.Add(this.scrSaleOrder);
            this.ribbonPageSaleManag.Name = "ribbonPageSaleManag";
            this.ribbonPageSaleManag.Text = "Sale Management";
            // 
            // rbPageGeneral
            // 
            this.rbPageGeneral.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.rbPageGeneral.Appearance.Options.UseFont = true;
            this.rbPageGeneral.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageApp,
            this.rpgPageReports});
            this.rbPageGeneral.Name = "rbPageGeneral";
            this.rbPageGeneral.Text = "Management";
            // 
            // ribbonPageApp
            // 
            this.ribbonPageApp.ItemLinks.Add(this.scrDashboard);
            this.ribbonPageApp.ItemLinks.Add(this.scrStoreInfor);
            this.ribbonPageApp.Name = "ribbonPageApp";
            this.ribbonPageApp.Text = "App";
            // 
            // rpgPageReports
            // 
            this.rpgPageReports.ItemLinks.Add(this.scrDailyInvoice);
            this.rpgPageReports.ItemLinks.Add(this.scrDetailsInvoice);
            this.rpgPageReports.ItemLinks.Add(this.scrBarcode);
            this.rpgPageReports.Name = "rpgPageReports";
            this.rpgPageReports.Text = "Reports";
            // 
            // rbPageConfiguration
            // 
            this.rbPageConfiguration.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.rbPageConfiguration.Appearance.Options.UseFont = true;
            this.rbPageConfiguration.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageCompanySetup,
            this.ribbonPageGensetup,
            this.ribbonPageUserPermission});
            this.rbPageConfiguration.Name = "rbPageConfiguration";
            this.rbPageConfiguration.Text = "System";
            // 
            // ribbonPageCompanySetup
            // 
            this.ribbonPageCompanySetup.ItemLinks.Add(this.scrCompany);
            this.ribbonPageCompanySetup.ItemLinks.Add(this.scrEmployee);
            this.ribbonPageCompanySetup.ItemLinks.Add(this.scrRole);
            this.ribbonPageCompanySetup.Name = "ribbonPageCompanySetup";
            this.ribbonPageCompanySetup.Text = "Company Setup";
            // 
            // ribbonPageGensetup
            // 
            this.ribbonPageGensetup.ItemLinks.Add(this.scrUser);
            this.ribbonPageGensetup.ItemLinks.Add(this.scrUOM);
            this.ribbonPageGensetup.ItemLinks.Add(this.scrCuryRate);
            this.ribbonPageGensetup.ItemLinks.Add(this.scrCustomer);
            this.ribbonPageGensetup.ItemLinks.Add(this.scrCashierBalance);
            this.ribbonPageGensetup.Name = "ribbonPageGensetup";
            this.ribbonPageGensetup.Text = "Setup";
            // 
            // ribbonPageUserPermission
            // 
            this.ribbonPageUserPermission.ItemLinks.Add(this.scrPermission);
            this.ribbonPageUserPermission.Name = "ribbonPageUserPermission";
            this.ribbonPageUserPermission.Text = "User Permission";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControlTop.Size = new System.Drawing.Size(1384, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 476);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControlBottom.Size = new System.Drawing.Size(1384, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1384, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(2);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // documentManager1
            // 
            this.documentManager1.MdiParent = this;
            this.documentManager1.MenuManager = this.ribbonControl1;
            this.documentManager1.View = this.tabbedView1;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView1});
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "​​​​   Category                            ";
            this.barButtonItem4.Id = 1;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.LargeImage")));
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "​​​​   Category                            ";
            this.barButtonItem5.Id = 1;
            this.barButtonItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.Image")));
            this.barButtonItem5.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.LargeImage")));
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "    Staff                  ";
            this.barButtonItem6.Id = 6;
            this.barButtonItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.ImageOptions.Image")));
            this.barButtonItem6.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.ImageOptions.LargeImage")));
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // transitionManager
            // 
            this.transitionManager.FrameInterval = 1000;
            this.transitionManager.ShowWaitingIndicator = false;
            transition1.Control = null;
            transition1.ShowWaitingIndicator = DevExpress.Utils.DefaultBoolean.False;
            slideFadeTransition1.Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromRight;
            slideFadeTransition1.Parameters.FrameInterval = 5000;
            transition1.TransitionType = slideFadeTransition1;
            this.transitionManager.Transitions.Add(transition1);
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.btnLogout.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnLogout.Appearance.Options.UseBackColor = true;
            this.btnLogout.Appearance.Options.UseFont = true;
            this.btnLogout.AppearanceHovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnLogout.AppearanceHovered.Options.UseBackColor = true;
            this.btnLogout.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnLogout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.ImageOptions.Image")));
            this.btnLogout.Location = new System.Drawing.Point(1347, 22);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(38, 33);
            this.btnLogout.TabIndex = 12;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "​​​​   Category                            ";
            this.barButtonItem10.Id = 1;
            this.barButtonItem10.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem10.ImageOptions.SvgImage")));
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "​​​​   Category                            ";
            this.barButtonItem11.Id = 1;
            this.barButtonItem11.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItem11.ImageOptions.SvgImage")));
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "      Item                      ";
            this.barButtonItem12.Id = 2;
            this.barButtonItem12.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem12.ImageOptions.Image")));
            this.barButtonItem12.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem12.ImageOptions.LargeImage")));
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // frmMain
            // 
            this.Appearance.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 476);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.IconOptions.Image = ((System.Drawing.Image)(resources.GetObject("frmMain.IconOptions.Image")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Text = "Home ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonUOM;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGensetup;
        private DevExpress.Utils.Animation.TransitionManager transitionManager;
        internal DevExpress.XtraBars.Ribbon.RibbonPage rbPageGeneral;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageApp;
        internal DevExpress.XtraBars.Ribbon.RibbonPage rbPageSale;
        internal DevExpress.XtraBars.Ribbon.RibbonPage rbPageConfiguration;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageProduct;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageSaleManag;
        internal DevExpress.XtraBars.BarButtonItem scrSaleOrder;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown galleryDropDown1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraEditors.SimpleButton btnLogout;
        internal DevExpress.XtraBars.BarButtonItem scrDashboard;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgPageReports;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPagePrice;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageCompanySetup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageUserPermission;
        internal DevExpress.XtraBars.BarButtonItem scrCategory;
        internal DevExpress.XtraBars.BarButtonItem scrItem;
        internal DevExpress.XtraBars.BarButtonItem scrStoreInfor;
        internal DevExpress.XtraBars.BarButtonItem scrDailyInvoice;
        internal DevExpress.XtraBars.BarButtonItem scrDetailsInvoice;
        internal DevExpress.XtraBars.BarButtonItem scrSalePrice;
        internal DevExpress.XtraBars.BarButtonItem scrEmployee;
        internal DevExpress.XtraBars.BarButtonItem scrUOM;
        internal DevExpress.XtraBars.BarButtonItem scrUser;
        internal DevExpress.XtraBars.BarButtonItem scrRole;
        internal DevExpress.XtraBars.BarButtonItem scrCompany;
        internal DevExpress.XtraBars.BarButtonItem scrPayment;
        internal DevExpress.XtraBars.BarButtonItem scrCury;
        internal DevExpress.XtraBars.BarButtonItem scrCuryRate;
        internal DevExpress.XtraBars.BarButtonItem scrCustomer;
        internal DevExpress.XtraBars.BarButtonItem scrPermission;
        internal DevExpress.XtraBars.BarButtonItem scrCashierBalance;
        internal DevExpress.XtraBars.BarButtonItem scrBarcode;
    }
}

