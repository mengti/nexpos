﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DgoStore
{
    public class StoreModel
    {
        public string StoreClassID{get; set;}
        public string StoreCD{get; set;}
        public string StoreID{get; set;}
        public bool? Active{get; set;}
        public string CustomerID{get; set;}
        public DateTime RegisterDate{get; set;}
        public string StoreName{get; set;}
        public string Atmospheres{get; set;}
        public String Status{ get; set;}
        public string SpokenLanguages{get; set;}
        public string Address{get; set;}
        public DateTime? StartTime{get; set;}
        public DateTime? CloseTime{get; set;}
        public int? Reservations{get; set;}
        public string Rate { get; set;}
        public int? CurrentPercentage{get; set;}
        public string ShortAddress{get; set;}
        public string ContactNumber{get; set;}
        public string Website{get; set;}
        public string Email{get; set;}
        public string Latitude{get; set;}
        public string Longitude{get; set;}
        public string StoreTypeID{ get; set;}
        public string OpenDays{get; set;}
        public string OrderType{get; set;}
        public string Info{ get; set;}
        public byte[] UrlImageLogo { get; set; }
        public byte[] UrlImageTimeline { get; set; }
    }

    public class StoreModelDB
    {
        public List<StoreModel> GetStoreInformation()
        {
            try
            {
                List<StoreModel> storeList = new List<StoreModel>();

                string sql = @"SELECT * FROM tb_StoreInfor";
                SqlCommand cmd = new SqlCommand(sql, Connections.conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StoreModel storeModel = new StoreModel()
                    {
                        StoreClassID = dr["StoreClassID"].ToString(),
                        StoreID = dr["StoreID"].ToString(),
                        StoreName = dr["StoreName"].ToString(),
                       // CustomerID = dr["CustomerID"].ToString(),
                        // Active=dr["Active"].ToString(),
                        RegisterDate = Convert.ToDateTime(dr["RegisterDate"]),
                        StartTime = Convert.ToDateTime(dr["StartTime"]),
                        CloseTime = Convert.ToDateTime(dr["CloseTime"]),
                        // Status=dr["Status"].ToString(),
                        SpokenLanguages = dr["SpokenLanguage"].ToString(),
                        OrderType = dr["OrderType"].ToString(),
                        OpenDays = dr["OpenDay"].ToString(),
                        StoreTypeID = dr["StoreTypeID"].ToString(),
                        Address =dr["Address"].ToString(),
                        ContactNumber=dr["Contact"].ToString(),
                        Email=dr["Email"].ToString(),
                        Website=dr["Website"].ToString(),
                        Atmospheres= dr["Atmosphere"].ToString(),
                        Rate=dr["Rate"].ToString(),
                        Info=dr["Information"].ToString(),
                        UrlImageLogo= dr["UrlImageLogo"] is DBNull? null:(byte[])dr["UrlImageLogo"],
                        UrlImageTimeline = dr["UrlImageTimeLine"] is DBNull ? null : (byte[])dr["UrlImageTimeLine"],
                    };

                    storeList.Add(storeModel);
                }
                dr.Close();
                return storeList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
