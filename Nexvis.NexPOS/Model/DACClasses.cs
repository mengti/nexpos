﻿using DevExpress.XtraEditors;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZooMangement
{
    public class DACClasses
    {
        public static string UserID;
        //public static string EmployeeID;
        public static int CompanyID=2;
       // public static string Barcode ;
       // public static int 
        //public static string CuryID; 
        internal static String EncryptString(String sIn, String sKey)
        {
            var DES = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
            var hashMD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            //*******scramble the key*********
            sKey = ScrambleKey(sKey);
            //*******Compute the MD5 hash********
            DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(sKey));
            //*******Set the cipher mode*********
            DES.Mode = System.Security.Cryptography.CipherMode.ECB;
            //*******Create the encryptor********
            System.Security.Cryptography.ICryptoTransform DESEncrypt = DES.CreateEncryptor();
            //********Get a byte array of the string**********
            Byte[] Buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(sIn);
            //*********Transform and return the string***********
            return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }

        internal static String DecryptString(String sOut, String sKey)
        {
            var DES = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
            var hashMD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            //********scramble the key*********
            sKey = ScrambleKey(sKey);
            //********Compute the MD5 hash*********
            DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(sKey));
            //********Set the cipher mode***********
            DES.Mode = System.Security.Cryptography.CipherMode.ECB;
            //*********Create the decryptor************
            System.Security.Cryptography.ICryptoTransform DESDecrypt = DES.CreateDecryptor();
            byte[] Buffer = Convert.FromBase64String(sOut);
            //*********Transform and return the string***********
            return System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }

        private static String ScrambleKey(String v_strKey)
        {
            var sbKey = new System.Text.StringBuilder();
            int intPtr;
            for (intPtr = 1; intPtr <= v_strKey.Length; intPtr++)
            {
                int intIn = v_strKey.Length - intPtr + 1;
                sbKey.Append(Strings.Mid(v_strKey, intIn, 1));
            }
            String strKey = sbKey.ToString();
            return sbKey.ToString();
        }
        internal static Boolean CheckEmpty(params Control[] Controls)
        {
            String str = "enter";
            foreach (Control C in Controls)
            {
                if (C.Text == "")
                {
                    if (C.GetType() == typeof(System.Windows.Forms.ComboBox))
                        str = "select";
                    else
                        str = "enter";
                    C.BackColor = Color.Tomato;
                    if (C.Tag.ToString() != "")
                        MessageBox.Show("Please " + str + " a " + C.Tag + " for this record!", "Check Empty Control", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    else
                        MessageBox.Show("Please " + str + " data for this box!", "Check Empty Control", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    C.BackColor = Color.White;
                    C.Focus();
                    return true;
                }
            }
            return false;
        }
        public static Image GfConvertByteArrayToImage(byte[] objByteArray)
        {
            if (null == objByteArray || objByteArray.Length == 0)
                return null;
            MemoryStream ms = new MemoryStream(objByteArray);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        public static byte[] ReturnImage(PictureEdit pictureBox)
        {
            if (pictureBox.Image == null)
            {
                return null;
            }
            var memStream = new MemoryStream();
            pictureBox.Image.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            var byBLOBData = new byte[memStream.Length - 1];
            memStream.Position = 0;

            memStream.Read(byBLOBData, 0, byBLOBData.Length);
            return byBLOBData;
        }
        public static Image ReadImage(byte[] imageByte)
        {
            if (null == imageByte || imageByte.Length == 0)
                return null;
            return Image.FromStream(new MemoryStream((byte[])imageByte));
        }

        public static void objGridView_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            // TODO: Add row number of each row in GridView.
            e.Info.DisplayText = "1";
            e.Info.ImageIndex = -1;
            int rowIndex = e.RowHandle;
            if (e.Info.IsRowIndicator && rowIndex >= 0)
            {
                rowIndex++;
                e.Info.DisplayText = rowIndex.ToString();
            }
            else
            {
                // TODO: Add text to filter row of GridView.
                e.Info.DisplayText = "";
            }

            // TODO: Add column header text to column indicator.
            if (e.Info.Kind == DevExpress.Utils.Drawing.IndicatorKind.Header)
            {
                e.Info.DisplayText = "Nº";
            }
        }
        public static void GsFillLookUpEdit<T>(List<T> objDataView, string objValueMember, string objDisplayMember, LookUpEdit objLookUpEdit)
        {
            objLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 10F);
            objLookUpEdit.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Calibri", 10F);
            objLookUpEdit.Properties.DataSource = objDataView;
            objLookUpEdit.Properties.ValueMember = objValueMember; 
            objLookUpEdit.Properties.DisplayMember = objDisplayMember;
            objLookUpEdit.Properties.DropDownRows = objDataView.ToList().Count() + 1;
        }

        public static void FillLookUpEdit<T>(List<T> objDataView, string objValueMember, string objDisplayMember, LookUpEdit objLookUpEdit)
        {
            objLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS Battambang", 10F);
            objLookUpEdit.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Khmer OS Battambang", 10F);
            objLookUpEdit.Properties.DataSource = objDataView;
            objLookUpEdit.Properties.ValueMember = objValueMember;
            objLookUpEdit.Properties.DisplayMember = objDisplayMember;
            objLookUpEdit.Properties.DropDownRows = objDataView.ToList().Count() + 1;
        }
    }
}
