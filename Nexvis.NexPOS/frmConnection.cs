﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using ZooManagement.Connection;
using ZooManagement.Helper;
using System.Data.EntityClient;
using System.Configuration;
using Nexvis.NexPOS.LoadForm;
using DevExpress.XtraEditors;
using System.Diagnostics;

namespace ZooManagement
{
    public partial class frmConnection : Form
    {
        public frmConnection()
        {
            InitializeComponent();
            txtServer.Focus();
        }

        //SqlConnection cn;
        public bool logout { get; set; }
        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            string ConnectionString = string.Format("data source={0};initial catalog={1};user id={2};password={3};", txtServer.Text, txtDatabase.Text, txtUserName.Text, txtPassword.Text);
            try
            {
                SqlHelper helper = new SqlHelper(ConnectionString);
                if (helper.IsConnection)
                {
                    XtraMessageBox.Show("Test Connection Succeed.", "NEXPOS System", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btnConnect_Click(object sender, EventArgs e)
        {
            string ConnectionString = string.Format("data source={0};initial catalog={1};user id={2};password={3};", txtServer.Text, txtDatabase.Text, txtUserName.Text, txtPassword.Text);
            try
            {
                SqlHelper helper = new SqlHelper(ConnectionString);
                if (helper.IsConnection)
                {
                    AppSetting setting = new AppSetting();
                    string strCon =  ConnectionString + "MultipleActiveResultSets=True;App=EntityFramework&quot;";
                    setting.UpdateConnectSting("ZooEntity", ConnectionString);
                    logout = true;
                    XtraMessageBox.Show("Your connection string has been successfully saved.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Application.Restart();
                    Application.Exit();
                    Process.Start(Application.StartupPath + "\\Nexvis.NexPOS.exe");
                    //Application.Restart();
                    // this.Close();

                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            logout = false;
            this.Close();       
        }

        private void txtServer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtDatabase.Focus();
                //SendKeys.Send("{TAB}");     
            }
        }
        private void txtDatabase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtUserName.Focus();
            }
        }

        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPassword.Focus();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnConnect.PerformClick();
            }
        }

        private void btnConnect_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Alt + C",btnConnect);
        }

        private void btnCancel_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Alt + E", btnConnect);
        }

        private void btnTestConnection_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Alt + T", btnConnect);
        }

        private void btnSetupdb_Click(object sender, EventArgs e)
        {
            //FrmSetup setup = new FrmSetup();
            //setup.ShowDialog();
        }
    }
}
