//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nexvis.NexPOS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class OptionDetail
    {
        public int CompanyID { get; set; }
        public string StoreID { get; set; }
        public int ItemID { get; set; }
        public int OptionID { get; set; }
        public int OptionItemID { get; set; }
        public string OptionItemName { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<bool> FirebaseUp { get; set; }
        public Nullable<decimal> PriceR { get; set; }
    }
}
