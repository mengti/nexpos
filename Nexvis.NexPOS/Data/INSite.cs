//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nexvis.NexPOS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class INSite
    {
        public int CompanyID { get; set; }
        public int SiteID { get; set; }
        public string SiteCD { get; set; }
        public string Descr { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
