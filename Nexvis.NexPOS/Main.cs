﻿using DevExpress.DashboardCommon;
using DevExpress.XtraBars.Docking2010.Views;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using DgoStore.LoadForm;
using DgoStore.LoadForm.Configuration;
using DgoStore.LoadForm.Configuration.Currency;
using DgoStore.LoadForm.Configuration.Payment;
using DgoStore.LoadForm.Configuration.UOM;
using DgoStore.LoadForm.Employee;
using DgoStore.LoadForm.Organizations;
using DgoStore.LoadForm.Sale;
using DgoStore.LoadForm.User;
using GymCoffee.Report;
using Nexvis.NexPOS.LoadForm;
using Nexvis.NexPOS.LoadForm.Counter;
using Nexvis.NexPOS.LoadForm.User;
using Nexvis.NexPOS.Report;
using Nexvis.NexPOS.Sale;
//using Nexvis.NexPOS.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ZooManagement;
using ZooManagement.AddForm.NewVersion;
using ZooManagement.Report;

namespace Nexvis.NexPOS
{
    public partial class frmMain : DevExpress.XtraEditors.XtraForm
    {
        #region --- Form Action ---
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //User = new UserDomain()
            //{
            //    UserLog = "0",
            //    UserID = _UserLogins.Select(x => x.UserID).FirstOrDefault()
            //};
            //UpdateUserLog(sender, e);
            Application.Exit();
        }

        #endregion

        #region --- Header Action ---

        private void navMinimum_ElementClick(object sender, DevExpress.XtraBars.Navigation.NavElementEventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        #endregion
        public bool dash { get; set; }
        public bool IsLog = false;

        public frmMain()
        {
            InitializeComponent();
        }

        private void barbtnCategory_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(scrCategory));
            if (form == null)
            {
                scrCategory cate = new scrCategory();
                cate.MdiParent = this;
                cate.Show();
            }
            else
                form.Activate();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Application.Exit();
        }

        private void barbtnItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(scrItem));
            if (form == null)
            {
                scrItem itm = new scrItem();
                itm.MdiParent = this;
                itm.Show();
            }
            else
                form.Activate();
        }
        private void barbtnSale_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

              scrSaleOrder st = Application
              .OpenForms
              .OfType<scrSaleOrder>()
              .LastOrDefault();
               if (st != null)
               {
                     st.BringToFront();
               }
               else
               {
                    st = new scrSaleOrder();
                   st.Show();
               }
        }

        private void barbtnStaff_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(scrEmployee));
            if (form == null)
            {
                scrEmployee employee = new scrEmployee();
                employee.MdiParent = this;
                employee.Show();
            }
            else
                form.Activate();
        }

        private void barbtnStoreInfor_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(scrStoreInfor));
            if (form == null)
            {
                scrStoreInfor storeInf = new scrStoreInfor();
                storeInf.MdiParent = this;
                storeInf.Show();
            }
            else
                form.Activate();

        }
        private void frmMain_Load(object sender, EventArgs e)
        {
            if (dash == true)
            {
                barButtonDashboard_ItemClick(null, null);
            }
            
        }

        private void barButtonIUser_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(scrUser));
            if (form == null)
            {
                scrUser user = new scrUser();
                user.MdiParent = this;
                user.Show();
            }
            else
                form.Activate();
        }

        private void barButtonRole_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           

            Form form = IsActive(typeof(scrRole));
            if (form == null)
            {
                scrRole role = new scrRole();
                role.MdiParent = this;
                role.Show();
            }
            else
                form.Activate();
        }

        private void barButtonUnitOfMe_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(scrUOM));
            if (form == null)
            {
                scrUOM uom = new scrUOM();
                uom.MdiParent = this;
                uom.Show();
            }
            else
                form.Activate();
        }

        private void barButtonPayment_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Form form = IsActive(typeof(scrPayment));
            //if (form == null)
            //{
            //    scrPayment uom = new scrPayment();
            //    uom.MdiParent = this;
            //    uom.Show();
            //}
            //else
            //    form.Activate();
        }

        private void barButtonCompany_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           

            Form form = IsActive(typeof(scrCompany));
            if (form == null)
            {
                scrCompany com = new scrCompany();
                com.MdiParent = this;
                com.Show();
            }
            else
                form.Activate();
        }


        private void barButtonCurrencyRate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           

            Form form = IsActive(typeof(scrCuryRate));
            if (form == null)
            {
                scrCuryRate uom = new scrCuryRate();
                uom.MdiParent = this;
                uom.Show();
            }
            else
                form.Activate();
        }

        private void barButtonCurrency_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            //Form form = IsActive(typeof(scrCury));
            //if (form == null)
            //{
            //    scrCury uom = new scrCury();
            //    uom.MdiParent = this;
            //    uom.Show();
            //}
            //else
            //    form.Activate();
        }

        private void barButtonCus_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
            Form form = IsActive(typeof(scrCustomer));
            if (form == null)
            {
                scrCustomer cus = new scrCustomer();
                cus.MdiParent = this;
                cus.Show();
            }
            else
                form.Activate();
        }

        private void barButtonDashboard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(LoadDashboard));
            if (form == null)
            {
                LoadDashboard dashboard = new LoadDashboard();
                dashboard.MdiParent = this;
                dashboard.Show();
            }
            else
                form.Activate();
        }

        private void barbtnSaleReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DailyReport report = new DailyReport();
           // rptA5.DataSource = _SaleProducts;
           // rptA5.Parameters["Cashier"].Value = CSUserModel.LoginName;
           // rptA5.RequestParameters = false;
            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreview();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmLotSerCLass dd = new FrmLotSerCLass();
            dd.MdiParent = this;
            dd.Show();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            IsLog = true;
            var result = XtraMessageBox.Show("Are you sure you want to Logout?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Restart();
            }
        }

        private void barbtnDetails_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Invoice report = new Invoice();

            ReportPrintTool printTool = new ReportPrintTool(report);
            printTool.ShowPreview();
        }

        private void barbtnSalePrice_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            Form form = IsActive(typeof(scrSalePrice));
            if (form == null)
            {
                scrSalePrice itm = new scrSalePrice();
                itm.MdiParent = this;
                itm.Show();
            }
            else
                form.Activate();
        }
        private Form IsActive(Type ftype)
        {
            foreach (Form f in this.MdiChildren)
            {
                if (f.GetType() == ftype) return f;
            }
            return null;
        }

        private void barbtnPerrmiss_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(scrPermission));
            if (form == null)
            {
                scrPermission itm = new scrPermission();
                itm.MdiParent = this;
                itm.Show();
            }
            else
                form.Activate();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsLog == false)
            {
                var result = XtraMessageBox.Show("Are you sure you want to exit?", "NEXPOS System", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes) Environment.Exit(0);
                else e.Cancel = true;

                //DialogResult dg = MessageBox.Show("Do you want to exit?", "Closing", MessageBoxButtons.YesNo);

                //if (dg == DialogResult.Yes)
                //{
                //    //Thread myThread = new Thread(...);
                //    //myThread.IsBackground = true; // <-- Set your thread to background
                //    //myThread.Start(...);
                //  //  Application.Exit(0);
                //    Environment.Exit(0);
                //}
                //else if (dg == DialogResult.No)
                //{
                //    e.Cancel = true;
                //}
            }
        }

        private void scrCashierBalance_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(scrCashierBalance));
            if (form == null)
            {
                scrCashierBalance itm = new scrCashierBalance();
                itm.MdiParent = this;
                itm.Show();
            }
            else
                form.Activate();
        }

        private void scrBarcode_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form form = IsActive(typeof(scrBarcode));
            if (form == null)
            {
                scrBarcode itm = new scrBarcode();
                itm.MdiParent = this;
                itm.Show();
            }
            else
                form.Activate();
        }
    }
}
