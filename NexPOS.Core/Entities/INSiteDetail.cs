//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NexPOS.Core.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class INSiteDetail
    {
        public int CompanyID { get; set; }
        public int SiteID { get; set; }
        public int LocationID { get; set; }
        public string LocationCD { get; set; }
        public string LocationName { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<bool> DefaultForSale { get; set; }
        public Nullable<bool> DefaultForPurchase { get; set; }
    }
}
