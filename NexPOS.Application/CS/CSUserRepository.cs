﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.CS
{
    public class CSUserRepository : Repository<CSUser>, ICSUserRepository
    { 
        public CSUserRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<CSUser> GetUserId(string count)
        {
            return NexPOSDbContext.CSUser.OrderByDescending(c => c.UserName).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
