﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.CS
{
    public class CSUserDto
    {
        public int CompanyID { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string RoleID { get; set; }
        public string RoleName { get; set; }
        public Nullable<bool> Status { get; set; }
        public string Password { get; set; }
        public string UserEmployee { get; set; }
        public string UR { get; set; }
    }
}
