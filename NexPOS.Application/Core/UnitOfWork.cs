﻿using NexPOS.Application.CS;
using NexPOS.Application.ModelDto.Currency;
using NexPOS.Application.ModelDto.Customer;
using NexPOS.Application.ModelDto.Invoice;
using NexPOS.Application.ModelDto.InvoicesDetail;
using NexPOS.Application.ModelDto.Item;
using NexPOS.Application.ModelDto.ItemBarcode;
using NexPOS.Application.ModelDto.ItemsBalance;
using NexPOS.Application.ModelDto.Payment;
using NexPOS.Application.ModelDto.PaymentDetail;
using NexPOS.Application.ModelDto.Shop;
using NexPOS.EF.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.Core
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        private NexPOSDbContext _context;
        public ICSUserRepository CSUsers { get; private set; }
        public IARCustomerRepository ARCustomer { get; }
        public IARInvoiceRepository ARInvoice { get; }
        public IARInvoiceDetailRepository ARInvoiceDetail { get; }
        public IARPaymentRepository ARPayment { get; }
        public IARPaymentDetailRepository ARPaymentDetail { get; }
        public ICSCurrencyRepository CSCurrency { get; }
        public IItemRepository INItem { get; }
        public IItemBarcodeRepository INItemBarcode { get; }
        public IItemCategoryRepository INItemCategory { get; }
        public IItemsBalanceRepository INItemBalance { get; }
        public IShopInfoRepository ShopInfo { get; }
        public UnitOfWork(NexPOSDbContext context)
        {
            _context = context;
            CSUsers = new CSUserRepository(_context);
            ARCustomer = new ARCustomerRepository(_context);
            ARInvoice = new ARInvoiceRepository(_context);
            ARInvoiceDetail = new ARInvoicesDetailRepository(_context);
            ARPayment = new ARPaymentRepository(_context);
            ARPaymentDetail = new ARPaymentDetailRepository(_context);
            CSCurrency = new CSCurrencyRepository(_context);
            INItem = new ItemRepository(_context);
            INItemBarcode = new ItemBarcodeRepository(_context);
            INItemCategory = new ItemCategoryRepository(_context);
            INItemBalance = new ItemBalanceRepository(_context);
            ShopInfo = new ShopInfoRepository(_context);
        }
          

        public virtual async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
        public int Completed()
        {
            return _context.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
