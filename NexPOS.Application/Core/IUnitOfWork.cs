﻿using NexPOS.Application.CS;
using NexPOS.Application.ModelDto.Currency;
using NexPOS.Application.ModelDto.Customer;
using NexPOS.Application.ModelDto.Invoice;
using NexPOS.Application.ModelDto.InvoicesDetail;
using NexPOS.Application.ModelDto.Item;
using NexPOS.Application.ModelDto.ItemBarcode;
using NexPOS.Application.ModelDto.ItemsBalance;
using NexPOS.Application.ModelDto.Payment;
using NexPOS.Application.ModelDto.PaymentDetail;
using NexPOS.Application.ModelDto.Shop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.Core
{ 
    public interface IUnitOfWork
    {
        int Completed();
        Task CompleteAsync(); 
        ICSUserRepository CSUsers { get; }
        IARCustomerRepository ARCustomer { get; }
        IARInvoiceRepository ARInvoice { get; }
        IARInvoiceDetailRepository ARInvoiceDetail { get; }
        IARPaymentRepository ARPayment { get; }
        IARPaymentDetailRepository ARPaymentDetail { get; }
        ICSCurrencyRepository CSCurrency { get; }
        IItemRepository INItem { get; }
        IItemBarcodeRepository INItemBarcode { get; }
        IItemCategoryRepository INItemCategory { get; }
        IItemsBalanceRepository INItemBalance { get; }
        IShopInfoRepository ShopInfo { get; }
    }
}
