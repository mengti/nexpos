﻿using NexPOS.Application.ModelDto.InvoicesDetail;
using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Invoice
{
    public class ARInvoicesDetailRepository : Repository<ARInvoicesDetail>, IARInvoiceDetailRepository
    { 
        public ARInvoicesDetailRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<ARInvoicesDetail> GetInvoiceDetail(string Id)
        {
            return NexPOSDbContext.ARInvoicesDetail.OrderByDescending(c => c.InvoiceRefNbr).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
