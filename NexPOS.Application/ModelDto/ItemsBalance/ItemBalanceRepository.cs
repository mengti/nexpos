﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.ItemsBalance
{
    public class ItemBalanceRepository : Repository<INItemsBalance>, IItemsBalanceRepository
    { 
        public ItemBalanceRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<INItemsBalance> GetItemsBalance(string Id)
        {
            return NexPOSDbContext.INItemsBalance.OrderByDescending(c => c.InventoryID).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
