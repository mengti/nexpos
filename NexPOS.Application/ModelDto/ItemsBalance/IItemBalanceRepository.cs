﻿using NexPOS.Core.Entities;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.ItemsBalance
{
    public interface IItemsBalanceRepository : IRepository<INItemsBalance>
    {
        IEnumerable<INItemsBalance> GetItemsBalance(string Id); 
        //IEnumerable<CSUser> GetCoursesWithAuthors(int pageIndex, int pageSize);
    }
}
