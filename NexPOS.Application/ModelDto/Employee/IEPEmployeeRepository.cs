﻿using NexPOS.Core.Entities;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Employee
{
    public interface IEPEmployeeRepository : IRepository<EPEmployee>
    {
        IEnumerable<EPEmployee> GetEmployee(string Id); 
    }
}
