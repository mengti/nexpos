﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Employee
{
    public class EPEmployeeRepository : Repository<EPEmployee>, IEPEmployeeRepository
    { 
        public EPEmployeeRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<EPEmployee> GetEmployee(string Id)
        {
            return NexPOSDbContext.EPEmployee.OrderByDescending(c => c.EmployeeID).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
