﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Customer
{
    public class ItemCategoryRepository : Repository<INItemCategory>, IItemCategoryRepository
    { 
        public ItemCategoryRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<INItemCategory> GetItemCategory(string Id)
        {
            return NexPOSDbContext.INItemCategory.OrderByDescending(c => c.CategoryName).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
