﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Payment
{
    public class ARPaymentRepository : Repository<ARPayment>, IARPaymentRepository
    { 
        public ARPaymentRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<ARPayment> GetPayment(string Id)
        {
            return NexPOSDbContext.ARPayment.OrderByDescending(c => c.PaymentRefNbr).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
