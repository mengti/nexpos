﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Customer
{
    public class ARCustomerRepository : Repository<ARCustomer>, IARCustomerRepository
    { 
        public ARCustomerRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<ARCustomer> GetCustomer(string Id)
        {
            return NexPOSDbContext.ARCustomer.OrderByDescending(c => c.CustomerName).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
