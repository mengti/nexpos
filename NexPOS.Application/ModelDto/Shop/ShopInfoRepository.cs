﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Shop
{
    public class ShopInfoRepository : Repository<ShopInfo>, IShopInfoRepository
    { 
        public ShopInfoRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<ShopInfo> GetShop(string Id)
        {
            return NexPOSDbContext.ShopInfo.OrderByDescending(c => c.ShopID).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
