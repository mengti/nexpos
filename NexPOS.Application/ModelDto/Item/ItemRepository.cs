﻿using NexPOS.Core.Entities;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Item
{
    public interface IItemRepository : IRepository<INItem>
    {
        IEnumerable<INItem> GetItem(string Id); 
        //IEnumerable<CSUser> GetCoursesWithAuthors(int pageIndex, int pageSize);
    }
}
