﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Item
{
    public class ItemRepository : Repository<INItem>, IItemRepository
    { 
        public ItemRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<INItem> GetItem(string Id)
        {
            return NexPOSDbContext.INItem.OrderByDescending(c => c.InventoryID).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
