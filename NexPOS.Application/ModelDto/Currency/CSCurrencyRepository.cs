﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Currency
{
    public class CSCurrencyRepository : Repository<CSCurrency>, ICSCurrencyRepository
    { 
        public CSCurrencyRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<CSCurrency> GetCustomer(string Id)
        {
            return NexPOSDbContext.CSCurrency.OrderByDescending(c => c.CuryID).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
