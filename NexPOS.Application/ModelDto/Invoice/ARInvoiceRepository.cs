﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Invoice
{
    public class ARInvoiceRepository : Repository<ARInvoice>, IARInvoiceRepository
    { 
        public ARInvoiceRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<ARInvoice> GetInvoice(string Id)
        {
            return NexPOSDbContext.ARInvoice.OrderByDescending(c => c.InvoiceRefNbr).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
