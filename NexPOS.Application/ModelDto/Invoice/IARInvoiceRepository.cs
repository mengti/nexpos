﻿using NexPOS.Core.Entities;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.Invoice
{
    public interface IARInvoiceRepository : IRepository<ARInvoice>
    {
        IEnumerable<ARInvoice> GetInvoice(string Id);
        // IEnumerable<CSUser> GetCoursesWithAuthors(int pageIndex, int pageSize);
    }
}
