﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.ItemBarcode
{
    public class ItemBarcodeRepository : Repository<INItemBarcode>, IItemBarcodeRepository
    { 
        public ItemBarcodeRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<INItemBarcode> GetItemBarcode(string Id)
        {
            return NexPOSDbContext.INItemBarcode.OrderByDescending(c => c.InventoryID).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
