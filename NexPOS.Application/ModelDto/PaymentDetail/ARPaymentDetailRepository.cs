﻿using NexPOS.Core.Entities;
using NexPOS.EF.Context;
using NexPOS.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.ModelDto.PaymentDetail
{
    public class ARPaymentDetailRepository : Repository<ARPaymentDetail>, IARPaymentDetailRepository
    { 
        public ARPaymentDetailRepository(NexPOSDbContext context)
            : base(context) 
        {
        }

        public IEnumerable<ARPaymentDetail> GetPaymentDetail(string Id)
        {
            return NexPOSDbContext.ARPaymentDetail.OrderByDescending(c => c.PaymentRefNbr).ToList();
        }

        public NexPOSDbContext NexPOSDbContext
        {
            get { return _context as NexPOSDbContext; }
        }
    }
}
