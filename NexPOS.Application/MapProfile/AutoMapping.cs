﻿using AutoMapper;
using NexPOS.Application.CS;
using NexPOS.Application.Currency.Dto;
using NexPOS.Application.Customer.Dto;
using NexPOS.Application.Invoice.Dto;
using NexPOS.Application.InvoicesDetail.Dto;
using NexPOS.Application.Item.Dto;
using NexPOS.Application.ItemBarcode.Dto;
using NexPOS.Application.ItemCategory.Dto;
using NexPOS.Application.ModelDto.ItemsBalance.Dto;
using NexPOS.Application.Payment.Dto;
using NexPOS.Application.PaymentDetail.Dto;
using NexPOS.Application.Shop.Dto;
using NexPOS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.Application.MapProfile
{
    public class AutoMapping : Profile
    { 
        public AutoMapping()
        {
           CreateMap<CSUser, CSUserDto>();
           CreateMap<INItem, INItemDto>();
           CreateMap<INItemBarcode, INItemBarcodeDto>();
           CreateMap<INItemCategory, INItemCategoryDto>();
           CreateMap<CSCurrency, CSCurrencyDto>();
           CreateMap<ARCustomer, ARCustomerDto>();
           CreateMap<ARInvoice, ARInvoiceDto>();
           CreateMap<ARInvoicesDetail, ARInvoicesDetailDto>();
           CreateMap<ARPayment, ARPaymentDto>();
           CreateMap<ARPaymentDetail, ARPaymentDetailDto>();
           CreateMap<ShopInfo, ShopInfoDto>();
           CreateMap<INItemsBalance, INItemsBalanceDto>();

        }
    }
}
