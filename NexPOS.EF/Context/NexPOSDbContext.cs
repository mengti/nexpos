﻿using NexPOS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.EF.Context
{
    public class NexPOSDbContext : DbContext
    {
        public NexPOSDbContext()
            : base("name=NexPOSDbContext")
        {
        }

        public virtual DbSet<CSUser> CSUser { get; set; }
        public virtual DbSet<ARCustomer> ARCustomer { get; set; }
        public virtual DbSet<ARInvoice> ARInvoice { get; set; }

        public virtual DbSet<ARInvoicesDetail> ARInvoicesDetail { get; set; }
        public virtual DbSet<ARPayment> ARPayment { get; set; }
        public virtual DbSet<ARPaymentDetail> ARPaymentDetail { get; set; }
        public virtual DbSet<CSCurrency> CSCurrency { get; set; }
        public virtual DbSet<EPEmployee> EPEmployee { get; set; }
        public virtual DbSet<ShopInfo> ShopInfo { get; set; }

        public virtual DbSet<INItem> INItem { get; set; }
        public virtual DbSet<INItemBarcode> INItemBarcode { get; set; }
        public virtual DbSet<INItemCategory> INItemCategory { get; set; }
        public virtual DbSet<INItemsBalance> INItemsBalance { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            // modelBuilder.Entity<User>().Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity); 

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<CSUser>().HasKey(b => new { b.CompanyID, b.UserID, b.RoleID });
            modelBuilder.Entity<ARCustomer>().HasKey(b => new { b.CompanyID, b.CustomerID });
            modelBuilder.Entity<ARInvoice>().HasKey(b => new { b.CompanyID, b.InvoiceRefNbr, b.InvoiceTypes });
            modelBuilder.Entity<ARInvoicesDetail>().HasKey(b => new { b.CompanyID, b.InvoiceRefNbr, b.InvoiceTypes,b.InventoryID });
            modelBuilder.Entity<ARPayment>().HasKey(b => new { b.CompanyID, b.PaymentRefNbr, b.PaymentTypes});
            modelBuilder.Entity<ARPaymentDetail>().HasKey(b => new { b.CompanyID, b.PaymentRefNbr, b.PaymentTypes,b.InvoiceTypes,b.InvoiceRefNbr });
           
            modelBuilder.Entity<CSCurrency>().HasKey(b => new { b.CompanyID, b.CuryID });
            modelBuilder.Entity<ShopInfo>().HasKey(b => new { b.CompanyID, b.ShopID });
            modelBuilder.Entity<INItem>().HasKey(b => new { b.CompanyID, b.InventoryID });
            modelBuilder.Entity<INItemBarcode>().HasKey(b => new { b.CompanyID, b.InventoryID, b.Barcode });
            modelBuilder.Entity<INItemCategory>().HasKey(b => new { b.CompanyID, b.CategoryID });
            modelBuilder.Entity<INItemsBalance>().HasKey(b => new { b.CompanyID, b.InventoryID,b.SiteID,b.LocationID });

        }
    }
}
