﻿using NexPOS.EF.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.EF.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly NexPOSDbContext _context;
        protected readonly DbSet<TEntity> dbSet; 
        public Repository(NexPOSDbContext context)
        { 
            this._context = context;
            this.dbSet = context.Set<TEntity>();
        } 
        public virtual async Task<IEnumerable<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }
        public virtual async Task<IEnumerable<TEntity>> GetWhere(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = dbSet;
            if (filter != null)
            {
                return await Task.Run(() => query.Where(filter));
            }
            else
            {
                return query.ToList();
            }
        }
        public virtual async Task<TEntity> FirstOrDefaltAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = dbSet;
            if (filter != null)
            {
                return await query.Where(filter).FirstOrDefaultAsync();
            }
            else
            {
                return await query.FirstOrDefaultAsync();
            }
        }    
        public virtual async Task<TEntity> GetByIDAsync(object id)
        {
            try
            {
                return await dbSet.FindAsync(id);
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public virtual async Task InsertAsync(TEntity entity)
        {
            
            await Task.Run(() => dbSet.Add(entity));
            //await  dbSet.Add(entity);
            // dbSet.Add(entity);  
            //await _context.SaveChangesAsync().ConfigureAwait(false);
        }
        public void InsertRange(IEnumerable<TEntity> entity)
        {
            //await  dbSet.Add(entity);
            //_context.SaveChangesAsync().ConfigureAwait(false);
             
            dbSet.AddRange(entity);
           
        }
        public virtual async Task InsertRangeAsync(IEnumerable<TEntity> entity)
        { 
            //dbSet.AddRange(entity);
            //await _context.SaveChangesAsync().ConfigureAwait(false);
            await  Task.Run(() => dbSet.AddRange(entity));
        } 
        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }
        public virtual void Delete(TEntity entityToDelete)
        {
            if (_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
            
        }

        public virtual void DeleteRange(IEnumerable<TEntity> entityToDelete)
        {
            //if (_context.Entry(entityToDelete).State == EntityState.Detached)
            //{
            //    dbSet.Attach(entityToDelete);
            //}
            dbSet.RemoveRange(entityToDelete);

        }
        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
        }
        public virtual async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
