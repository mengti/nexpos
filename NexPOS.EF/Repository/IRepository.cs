﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NexPOS.EF.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Delete(object id);
        void Delete(TEntity entityToDelete);
        void DeleteRange(IEnumerable<TEntity> entityToDelete); 
        Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");
        Task<TEntity> GetByIDAsync(object id);
        Task<IEnumerable<TEntity>> GetWhere(Expression<Func<TEntity, bool>> filter);
        Task<TEntity> FirstOrDefaltAsync(Expression<Func<TEntity, bool>> filter);
        Task InsertAsync(TEntity entity);
        void InsertRange(IEnumerable<TEntity> entity);
        Task InsertRangeAsync(IEnumerable<TEntity> entity); 
        void Update(TEntity entityToUpdate); 
        Task SaveAsync();
    }
}
